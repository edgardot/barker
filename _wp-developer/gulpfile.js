var gulp = require('gulp');
var babel = require('gulp-babel');
var webserver = require('gulp-webserver');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var pug = require('gulp-pug');
var babelify = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var rename = require('gulp-rename');
var es = require('event-stream');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var notify = require('gulp-notify');
var imagemin = require('gulp-imagemin');

var main = 'main.js';

var config = {
	styles: {
		main: 'src/sass/main.scss',
		watch: 'src/sass/**/*.scss',
		output: 'test/css'
	},
	scripts: {
		// main: 'main.js',
		folder: 'src/js/',
		files: ['main.js'],
		watch: 'src/js/**/*.js',
		output: 'test/js'
	},
	html: {
		main: 'src/pug/views/*.pug',
		watch: 'src/pug/**/*.pug',
		output: 'test'
	},
	images: {
		main: 'src/images/**/*',
		output: 'test/images'
	},
  fonts: {
    main: 'src/fonts/**/*',
    output: 'test/fonts'
  }
}

gulp.task('server', () => {
	gulp.src('./test')
		.pipe(webserver({
			host: '0.0.0.0',
			port: 8080,
			livereload: true
		}))
});

gulp.task('build:css', () => {
	gulp.src(config.styles.main)
		.pipe(plumber())
		.pipe(sassGlob())
		.pipe(sourcemaps.init())
		.pipe(sass({
			errorLogToConsole: true,
			includePaths: ['node_modules/'],
			outputStyle: 'compressed'
		}))
		.on('error', console.error.bind( console ))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(rename('main.min.css'))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(config.styles.output))
})

gulp.task('build:js', () => {
	config.scripts.files.map( (entry) => {
		return browserify({
			entries: [config.scripts.folder + entry]
		})
		.transform( babelify, { presets: ['env'] } )
		.bundle()
		// .on('error', console.error.bind(console))
		// .on('error', function (error) {
		// 	console.error(error.message)
		// 	this.emit('end')
		// })
		.on('error', notify.onError({
			message: "Error: <%= error.message %>",
			title: "Failed running browserify"
		}))
		.pipe( source( entry ) )
		.pipe( rename({ extname: '.min.js' }) )
		.pipe( buffer() )
		.pipe( sourcemaps.init({ loadMaps: true }) )
		.pipe( uglify() )
		.pipe( sourcemaps.write( './' ) )
		.pipe( gulp.dest(config.scripts.output) )
	})
});

gulp.task('build:html', () => {
	gulp.src(config.html.main)
		.pipe(plumber())
		.pipe(pug({
			pretty: true
		}))
		.on('error', (err) => {
      console.error('Error', err.message);
    })
		.pipe(gulp.dest(config.html.output))
});

gulp.task('images', () => {
	return gulp.src(config.images.main)
		.pipe(imagemin([
			imagemin.gifsicle({ interlaced: true }),
			imagemin.jpegtran({ progressive: true }),
			imagemin.optipng({ optimizationLevel: 5 })
			// imagemin.svgo({ plugins: [{ removeViewBox: true }] })
		]))
		.pipe(gulp.dest(config.images.output));
})

gulp.task('fonts', () => {
  gulp.src(config.fonts.main)
    .pipe(gulp.dest(config.fonts.output))
})

gulp.task('watch', () => {
	gulp.watch(config.html.watch, ['build:html'])
	gulp.watch(config.styles.watch, ['build:css'])
	gulp.watch(config.scripts.watch, ['build:js'])
	// gulp.watch(config.images.main, ['images'])
});

gulp.task('build', ['build:html', 'build:css', 'build:js'])

gulp.task('default',['server', 'watch', 'build', /*'fonts'*/])