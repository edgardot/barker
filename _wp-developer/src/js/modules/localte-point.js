import baseAssets from './data-locale';

class LocatePoint {
  constructor() {
    this.userEvents();
    
    this.renderPoints(baseAssets);
    this.getLocationUser();
  }

  userEvents() {
    $('.js-btn-search').on('click', this.getValInput.bind(this));
  }

  renderPoints(data) {
    let html = [];

    data.map((p, i) => {
      let tpl = this.tplPoint();
      tpl = tpl.replace(':district:', p.district);
      tpl = tpl.replace(':name:', p.local);
      tpl = tpl.replace(':address:', p.address);
      tpl = tpl.replace(':phone:', p.phone);
      html.push(tpl);
    });
    $('.js-point-container').append(html.join(''));

    setTimeout(() => {
      this.googleMapInitialize(data)
    }, 250);
  }

  googleMapInitialize(data) {
    if (document.getElementById('map'))
      google.maps.event.addDomListener(window, 'load', this.googleMapConfig(data));
  }
  
  googleMapConfig(data) {
    var punto = { lat: -33.92, lng: 151.25 };
		var image = {
			url: './images/icons/pin.png',
			scaledSize: new google.maps.Size(59, 67), // scaled size
    };
    
    var map = new google.maps.Map(document.getElementById('map'), {
	  	mapTypeId: google.maps.MapTypeId.ROADMAP
	  });

		var bounds = new google.maps.LatLngBounds();
		var infowindow = new google.maps.InfoWindow();

	  var marker, i;

	  for (i = 0; i < data.length; i++) {
	  	marker = new google.maps.Marker({
	      position: new google.maps.LatLng(Number(data[i].coords[0]), Number(data[i].coords[1])),
	      map: map,
	      icon: image,
	      title: data[i].local
		  });

		  //extend the bounds to include each marker's position
  		bounds.extend(marker.position);
		  
		  google.maps.event.addListener(marker, 'click', (function(marker, i) {
	      return function() {
	        infowindow.setContent(data[i].local);
	        infowindow.open(map, marker);
	      }
	    })(marker, i));
	  }

	  map.fitBounds(bounds);
  }

  getLocationUser() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        function success(pos) {
          console.log(pos.coords.latitude, pos.coords.longitude);
        },
        function error(errMsg) {
          console.error('A ocurrido un error al recuperar la ubicación', errMsg);
        }
      );
    } else {
      console.log('geolocation is not enabled on this browser')
    }
  }

  matchLocations(currPos, locations) {
    
  }

  getValInput(e) {
    let $searchField = $('.js-search-field');
    let val = $searchField.val();

    if (val != '') {
      this.removeCardPoints();
      this.searchWord(val)
    } else 
      $searchField.focus();
  }

  removeCardPoints(val) {
    let $pointContainer = $('.js-point-container');
    $pointContainer.children().remove();
  }

  searchWord(val) {
    let localeResult = [];
    var re = RegExp(val);

    locale.map((el) => {
      if (re.test(el.district) && val != '') {
        localeResult.push(el);
      }
    });

    if (localeResult.length) {
      this.renderPoints(localeResult);
    } else {
      $('.js-point-container').append('<p>No se encontraron resultados</p>');
    }
  }

  tplPoint() {
    return `<div class="point-sale col-lg-4">
              <div class="point-sale__box">
                <h2 class="point-sale__title js-district-name">:district:</h2>
                <ul class="point-sale__data">
                  <li>
                    <span><svg><use xlink:href="./images/sprite.svg#local"></use></svg></span>
                    :name:
                  </li>
                  <li>
                    <span><svg><use xlink:href="./images/sprite.svg#location"></use></svg></span>
                    :address:
                  </li>
                  <li>
                    <span><svg><use xlink:href="./images/sprite.svg#phone"></use></svg></span>
                    :phone:
                  </li>
                </ul>
              </div>
            </div>`;
  }
}

export default LocatePoint;