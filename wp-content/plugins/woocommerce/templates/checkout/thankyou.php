<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="woocommerce-order">

	<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); ?></p>

			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

				<li class="woocommerce-order-overview__order order">
					<p class="thanks-box_head"><?php _e( 'Order number:', 'woocommerce' ); ?></p>
					<p class="thanks-box_info"><?php echo $order->get_order_number(); ?></p>
				</li>

				<li class="woocommerce-order-overview__date date">
					<p class="thanks-box_head"><?php _e( 'Date:', 'woocommerce' ); ?></p>
					<p class="thanks-box_info"> <?php echo wc_format_datetime( $order->get_date_created() ); ?></p>
				</li>

				<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<li class="woocommerce-order-overview__email email">
						<p class="thanks-box_head"><?php _e( 'Email:', 'woocommerce' ); ?></p>
						<p class="thanks-box_info"><?php echo $order->get_billing_email(); ?></p>
					</li>
				<?php endif; ?>

				<li class="woocommerce-order-overview__total total">
					<p class="thanks-box_head"><?php _e( 'Total:', 'woocommerce' ); ?></p>
					<p class="thanks-box_info"><?php echo $order->get_formatted_order_total(); ?></p>
				</li>

				<?php if ( $order->get_payment_method_title() ) : ?>
					<li class="woocommerce-order-overview__payment-method method">
						<p class="thanks-box_head"><?php _e( 'Payment method:', 'woocommerce' ); ?></p>
						<p class="thanks-box_info"><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></p>
					</li>
				<?php endif; ?>

			</ul>
			
			<div class="thanks_button_container">
				<a href="<?php echo get_home_url()?>/mis-pedidos/members-area"><button class="btn btn--primary btn--block">Ir a mis pedidos</button></a>
				<a href="<?php echo get_home_url()?>/suscripcion"><button class="btn btn--primary btn--block">Agregar otra mascota</button></a>
				<a href="<?php echo get_home_url()?>"><button class="btn btn--secondary btn--block is-active">Ir a la pagina principal</button></a>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

	<?php endif; ?>

</div>
