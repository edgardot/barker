<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://culqi.com
 * @since             1.0.0
 * @package           Culqi_Woocommerce
 *
 * @wordpress-plugin
 * Plugin Name:       Culqi WooCommerce
 * Plugin URI:        https://www.culqi.com/docs/
 * Description:       Plugin Culqi WooCommerce. Acepta tarjetas de crédito y débito en tu tienda online.
 * Version:           2.1.1
 * Author:            Brayan Cruces, Willy Aguirre
 * Author URI:        http://culqi.com
 * License:           GPL-2.0+
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
 * Text Domain:       culqi-woocommerce
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}
function wc_culqi_styles()
{
    // Register the style like this for a plugin:
    wp_register_style( 'custom-style', plugins_url( '/assets/css/waitMe.css', __FILE__ ), array(), '1.0.0', 'all' );
    // For either a plugin or a theme, you can then enqueue the style:
    wp_enqueue_style( 'custom-style' );
}
function wc_culqi_scripts()
{
    // Register the script like this for a plugin:
    wp_register_script( 'custom-script', plugins_url( '/assets/js/waitMe.js', __FILE__ ), array('jquery') );
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'custom-script' );
}
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    add_action('plugins_loaded', 'init_wc_culqi_payment_gateway', 0);
    
    add_action('woocommerce_checkout_process', 'some_custom_checkout_field_process');

    add_action( 'wc_memberships_user_membership_cancelled', 'letsgo_suscription_cancelled', 10, 1);

    /*  Cancelled Suscription */
    function letsgo_suscription_cancelled($wc_members) {
        include_once("includes/libraries/culqi-php/lib/culqi.php");

        update_option('ftest11',print_r($wc_members->get_order_id(),true));

        if( is_numeric($wc_members->get_order_id()) ) {

            $order_id = $wc_members->get_order_id();
            $order = wc_get_order($order_id);

            update_option('ftest12',print_r($order,true));

            $suscription_id = $order->get_meta( 'wc_culqi_subscription', true );

            update_option('ftest13',print_r($suscription_id,true));

            if( !empty($suscription_id) ) {

                update_option('ftest14','entro14');

                try {
                    update_option('ftest15','entro15');

                    $culqi = new Culqi\Culqi(array('api_key' => 'sk_live_0wUt29TwPgHlRAkE'));
                    //sk_live_0wUt29TwPgHlRAkE
                    //pk_test_vFwzV9Pi0xuEcPY9
                    update_option('ftest16',print_r($culqi,true));

                    $suscription = $culqi->Subscriptions->delete($suscription_id);
                    update_option('ftest17',print_r($suscription,true));
                } catch(Exception $e) {
                    // ERROR: El cargo tuvo algún error o fue rechazado
                    //echo 'Se dio una excepcion';
                    update_option('ftest18',print_r($e->getMessage(),true));
                }               
            }
        }

    }
    /**
     * Validacion de campos antes de pasar a Culqi
     */
    function some_custom_checkout_field_process() {
        error_log("[CULQI]...Validando...");
        //if(preg_match('/^[^0-9±!@£$%^&*_+§¡€#¢§¶•ªº«\\<>?:;|=.,]{2,50}$/', $_POST['billing_first_name'])) {
            //error_log("Nombre correcto");
        //} else {
        //    wc_add_notice('Por favor, ingresa un <strong>nombre </strong>válido', 'error' );
        //}
        //if(preg_match('/^[^0-9±!@£$%^&*_+§¡€#¢§¶•ªº«\\<>?:;|=.,]{2,50}$/', $_POST['billing_last_name'])) {
            //error_log("Apellido correcto");
        //} else {
        //    wc_add_notice('Por favor, ingresa un <strong>apellido </strong>válido.', 'error' );
        //}
        if(strlen ($_POST['billing_email'])>4 && strlen ($_POST['billing_email'])<50) {
            //error_log("Email correcto");
        } else {
            wc_add_notice('Por favor, ingresa un <strong>e-mail </strong>válido. Usa menos de 50 caracteres y más de 4.', 'error' );
        }
        if(strlen ($_POST['billing_phone'])>5 && strlen ($_POST['billing_phone'])<15 &&
                  preg_match('/^[1-9][0-9]*$/', $_POST['billing_phone']) ) {
            //error_log("Teléfono correcto");
        } else {
            wc_add_notice('Por favor, ingresa un <strong>número telefónico </strong>válido. Solo numeros', 'error' );
        }
        if(strlen ($_POST['billing_country'])>1 && strlen ($_POST['billing_country'])<3) {
            //error_log("País correcto");
        } else {
            wc_add_notice('Por favor, ingresa un <strong>país </strong>válido.', 'error' );
        }
        if(strlen ($_POST['billing_city'])>2 && strlen ($_POST['billing_city'])<30) {
            //error_log("Ciudad correcto");
        } else {
            wc_add_notice('Por favor, ingresa una <strong>ciudad </strong>válida.', 'error' );
        }
        if(strlen ($_POST['billing_address_1'])>5 && strlen ($_POST['billing_address_1'])<100) {
            //error_log("Dirección correcto");
        } else {
            wc_add_notice('Por favor, ingresa una <strong>dirección </strong>válida.', 'error' );
        }
    }
    function init_wc_culqi_payment_gateway() {
        if (!class_exists('WC_Payment_Gateway')) {
            return;
        }
        DEFINE('PLUGIN_DIR', plugins_url(basename(plugin_dir_path(__FILE__)), basename(__FILE__)) . '/');
        //DEFINE('CULQI_CODIGO', '',TRUE);
        //DEFINE('CULQI_KEY', '',TRUE);
        class WC_culqi extends WC_Payment_Gateway
        {
            public function __construct() {
                global $woocommerce;
                $this->includes();
                $this->id = 'culqi';
                $this->icon = home_url() . '/wp-content/plugins/' . dirname(plugin_basename(__FILE__)) . '/assets/images/cards.png';
                $this->method_title = __('Culqi', 'WC_culqi');
                $this->method_description = __('Acepta tarjetas de crédito, débito o prepagadas.', 'WC_culqi');
                $this->order_button_text = __('Pagar', 'WC_culqi');
                $this->has_fields = false;
                $this->supports = array(
                    'products'
                );
                $this->init_form_fields();
                $this->init_settings();
                $this->title = 'Tarjeta de crédito o débito';
                $this->description = 'Paga con tarjeta de crédito, débito o prepagada de todas las marcas.';
                // Obtener credenciales y entorno
                $this->culqi_codigoComercio = $this->get_option('culqi_codigoComercio');

                $this->culqi_key = $this->get_option('culqi_key');
                $this->culqi_nombre_comercio = get_bloginfo('name');
                
                if( !defined('CULQI_CODIGO') ) {
                    //DEFINE('CULQI_CODIGO', $this->get_option('culqi_codigoComercio'));
                    //Llave Producción Pub
                    DEFINE('CULQI_CODIGO', 'pk_live_y5iBWk4GypWM8nrH');
                }

                if( !defined('CULQI_KEY') ) {
                    //DEFINE('CULQI_KEY', $this->get_option('culqi_key'));
                    //Llave producción priv
                    DEFINE('CULQI_KEY', 'sk_live_0wUt29TwPgHlRAkE');
                }

                add_action('woocommerce_api_' . strtolower(get_class($this)), array($this, 'crear_cargo'));// Crear Cargo
                add_action('woocommerce_receipt_culqi', array(&$this, 'receipt_page'));
                add_action('woocommerce_update_options_payment_gateways_ '.$this->id, array($this, 'process_admin_options'));
                if (!$this->is_valid_for_use()) $this->enabled = false;
            }
            public function pathModule()
            {
                $dir = home_url() . '/wp-content/plugins/' . dirname(plugin_basename(__FILE__)) . '/';
                return $dir;
            }
            /**
             * Enviar correos
             */
            public function mailNotifyPayment($id_order, $email, $status, $message) {
                $wc_sp = new Culqi();
                $mailer = WC()->mailer();
                switch ($status) {
                    case 'pending':
                        $msg_status = 'Se genero una orden de compra';
                        break;
                    case 'success':
                        $msg_status = "El pago de su pedido  $id_order fue aceptado";
                        break;
                    case 'cancelled':
                        $msg_status = "Su pedido $id_order no fue aceptado";
                        break;
                    default:
                        $msg_status = 'Información de Orden';
                        break;
                }
                $subject = $this->culqi_nombre_comercio . ' -  ' . $msg_status;
                add_filter('wp_mail_content_type', create_function('', 'return "text/html";'));
                wp_mail($email, $subject, $body, 'header');
            }
            public function acuse($js) {
                global $woocommerce;
                if (function_exists('wc_enqueue_js')) {
                    wc_enqueue_js($js);
                } else {
                    $woocommerce->add_inline_js($js);
                }
            }
            /**
             * Incluye dependencias
             *
             */
            private function includes() {
                // Cargamos Requests y Culqi PHP
                include_once("includes/libraries/culqi-php/lib/culqi.php");
            }
            /**
             * Crear Cargo (recibe token y procesa venta)
             * Via WC_API
             */
            function crear_cargo() {
                if (isset($_POST['token_id']) && isset($_POST['order_id'])) {
                    global $wpdb, $woocommerce;
                    $order = new WC_Order($_POST['order_id']);
                    $numeroPedido = str_pad($order->get_id(), 2, "0", STR_PAD_LEFT);
                    $total = str_replace('.', '', number_format($order->get_total(), 2, '.', ''));
                    $total = str_replace(',', '',$total);
                    $culqi = new Culqi\Culqi(array('api_key' => $this->culqi_key));
                    // Generamos un Código de pedido único (ejemplo)
                    $pedidoId = $this->generateRandomString(4)."-".$numeroPedido;
                    error_log("Número de pedido: ". $pedidoId);
                    error_log("Token: ". $_POST['token_id'] );
                    /**
                     * Validando y formateando datos (one more time)                             *
                     *
                     */
                     $dataUser = $order->get_user();
                     $fono = $dataUser->billing_phone;
                     $descripcion = '';
                     $i = 1;
                     $separador = ' - ';
                     foreach ($order->get_items() as $product ){
                             if($i == count($order->get_items())){
                                     $separador = '';
                             }
                             $descripcion .= $product['name'].$separador;
                             $i++;
                     }
                     if(strlen ($descripcion)>5 && strlen ($descripcion)<60) {
                             error_log("Descripción correcto");
                     } else {
                             $descripcion = "Compra";
                     }
                     $datos_ciudad = "";
                     $datos_correo = "";
                     $datos_apellido = "";
                     $datos_nombre = "";
                     $datos_telefono = "";
                     $datos_direccion = "";
                    if ($order->get_billing_city() == null) {
                        $datos_ciudad = "Ciudad";
                    } else {
                        $datos_ciudad = $order->get_billing_city();
                    }
                    if ($order->get_billing_first_name() == null){
                        $datos_nombre = "Nombre";
                    }else {
                        $datos_nombre = $order->get_billing_first_name();
                    }
                    if ($order->get_billing_last_name() == null){
                        $datos_apellido = "Apellido";
                    }else {
                        $datos_apellido = $order->get_billing_last_name();
                    }
                    if ($order->get_billing_email() == null){
                        $datos_correo = "integrate@culqi.com";
                    } else {
                        $datos_correo = $order->get_billing_email();
                    }
                    if ($order->get_billing_phone() == null){
                        $datos_telefono = "12313123";
                    } else {
                        $datos_telefono = $order->get_billing_phone();
                    }
                    if ($order->get_billing_address_1() == null) {
                        $datos_direccion = "Avenida 123";
                    } else {
                        $datos_direccion = $order->get_billing_address_1();
                    }   
                    // Creando Cargo
                    try {
                                            $charge = $culqi->Charges->create(array(
                            "amount" => $total,
                            "antifraud_details" => array(
                                "address" => $datos_direccion,
                                "address_city" => $datos_ciudad,
                                "country_code" => $order->get_billing_country(),
                                "first_name" => $datos_nombre,
                                "last_name" => $datos_apellido,
                                "phone_number" => $datos_telefono,
                            ),
                            "capture" => true,
                            "currency_code" => $order->get_order_currency(),
                            "description" => $descripcion,
                            "email" => $datos_correo,
                            "installments" => (int)$_POST['installments'],
                            "metadata" => array(
                                "order_id" => (string)$pedidoId
                            ),
                            "source_id" => $_POST['token_id']
                        ));
                        if($charge->object == "charge") {
                            $order->payment_complete();
                        }
                        echo wp_send_json($charge);
                    } catch(Exception $e) {
                        // ERROR: El cargo tuvo algún error o fue rechazado
                        //echo 'Se dio una excepcion';
                        echo wp_send_json($e->getMessage());
                    }
               } else {
                    global $woocommerce;
                    $woocommerce->cart->empty_cart();
               }
               exit;
            }
            function is_valid_for_use() {
                if (!in_array(get_woocommerce_currency(), array('PEN', 'USD'))) return false;
                return true;
            }
            public function admin_options() {
                ?>
                    <h3><?php _e('Culqi', 'wc_culqi_payment_gateway'); ?></h3>
                    <table class="form-table">
                        <?php
                        if ($this->is_valid_for_use()) :
                            $this->generate_settings_html();
                        else :
                        ?>
                        <div class="inline error">
                            <p>
                                <strong>
                                    <?php _e('Gateway Disabled', 'wc_culqi_payment_gateway'); ?>
                                </strong>:
                                <?php _e('Error en la configuración.', 'wc_culqi_payment_gateway'); ?>
                            </p>
                        </div>
                        <?php
                        endif;
                        ?>
                    </table>
                <?php
            }
            function init_form_fields() {
                global $woocommerce;
                $this->form_fields = array(
                    'enabled' => array
                    (
                        'title' => __('Habilitar/Deshabilitar', 'wc_culqi_payment_gateway'),
                        'type' => 'checkbox',
                        'label' => __('Habilitar Culqi', 'wc_culqi_payment_gateway'),
                        'default' => 'yes'
                    ),
                    'culqi_codigoComercio' => array
                    (
                        'title' => __('Llave Pública', 'wc_culqi_payment_gateway'),
                        'type' => 'text',
                        'required' => true,
                        'description' => __('Ingresar Llave Pública', 'wc_culqi_payment_gateway'),
                        'default' => ''
                    ),
                    'culqi_key' => array
                    (
                        'title' => __('Llave Secreta', 'wc_culqi_payment_gateway'),
                        'type' => 'text',
                        'required' => true,
                        'description' => __('Ingresar Llave Secreta', 'wc_culqi_payment_gateway'),
                        'default' => ''
                    )
                );
            }
            function process_payment($order_id) {
                $order = new WC_Order($order_id);
                $order->reduce_order_stock();
                return array
                (
                    'result' => 'success',
                    'redirect' => add_query_arg('order-pay', $order->get_id(), add_query_arg('key', $order->get_order_key(), get_permalink(woocommerce_get_page_id('pay'))))
                );
            }
            function receipt_page($order_id) {
                echo "jot";
                $order = new WC_Order($order_id);
                $numeroPedido = str_pad($order->get_id(), 2, "0", STR_PAD_LEFT);
                /**
                 * Datos de la compra
                 *
                 */
                $total = str_replace('.', '', number_format($order->get_total(), 2, '.', ''));
                $total = str_replace(',', '',$total);
                $dataUser = $order->get_user();
                $fono = $dataUser->billing_phone;
                $descripcion = '';
                $i = 1;
                $separador = ' - ';
                foreach ($order->get_items() as $product ) {
                    if($i == count($order->get_items())){
                        $separador = '';
                    }
                    $descripcion .= $product['name'].$separador;
                    $i++;
                }
                if(strlen ($descripcion)>5 && strlen ($descripcion)<60) {
                    error_log("Descripción correcto");
                } else {
                    $descripcion = "Compra";
                }
                $datos_ciudad = "";
                $datos_correo = "";
                $datos_apellido = "";
                $datos_nombre = "";
                $datos_telefono = "";
                $datos_direccion = "";
                if ($order->get_billing_city() == null) {
                    $datos_ciudad = "Ciudad";
                } else {
                    $datos_ciudad = $order->get_billing_city();
                }
                if ($order->get_billing_first_name() == null){
                    $datos_nombre = "Nombre";
                }else {
                    $datos_nombre = $order->get_billing_first_name();
                }
                if ($order->get_billing_last_name() == null){
                    $datos_apellido = "Apellido";
                }else {
                    $datos_apellido = $order->get_billing_last_name();
                }
                if ($order->get_billing_email() == null){
                    $datos_correo = "integrate@culqi.com";
                } else {
                    $datos_correo = $order->get_billing_email();
                }
                if ($order->get_billing_phone() == null){
                    $datos_telefono = "12313123";
                } else {
                    $datos_telefono = $order->get_billing_phone();
                }
                if ($order->get_billing_address_1() == null) {
                    $datos_direccion = "Avenida 123";
                } else {
                    $datos_direccion = $order->get_billing_address_1();
                }
                /*  End Crear Cargo  */
                ?>
                <div id="info_payment">
                    <span>Realiza la compra presionando <strong>Pagar</strong><br>Si deseas cambiar de medio de pago presiona <strong>Cancelar</strong></span><br><br>
                    <button id="pagar-now">Pagar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="btn-back">Cancelar</button>
                    <div id="culqi_notify" style="padding:10px 0px;"></div>
                              </div>

                <script src="https://checkout.culqi.com/plugins/v2/"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
                <script defer src="<?php echo plugins_url("/assets/js/waitMe.js", __FILE__ ) ?>"></script>
                <link rel='stylesheet' href='<?php echo plugins_url("/assets/css/waitMe.css", __FILE__ ) ?>' type='text/css' media='all' />

                <script>

                    Culqi.publicKey = '<?php echo $this->culqi_codigoComercio ?>';

                    Culqi.settings({
                        title: '<?php echo $this->culqi_nombre_comercio; ?>',
                        currency: '<?php echo get_woocommerce_currency(); ?>',
                        description: '<?php echo $descripcion; ?>',
                        amount: <?php echo $total; ?>
                    });

                    function run_waitMe(){
                        $('#info_payment').waitMe({
                            effect: 'orbit',
                            text: 'Procesando pago...',
                            bg: 'rgba(255,255,255,0.7)',
                            color:'#28d2c8'
                        });
                    }

                    // Recibimos Token del Culqi.js
                    function culqi() {
                        if(Culqi.error) {
                            // Mostramos JSON de objeto error en consola
                            console.log(Culqi.error);
                            $('#info_payment > #culqi_notify').html('<p style="color:#e54848; font-weight:bold">'+ Culqi.error.user_message + '</p>');
                        } else {
                            console.log(Culqi.token.id);
                            $(document).ajaxStart(function(){
                                $('#culqi_notify').empty();
                                run_waitMe();
                            });
                            $(document).ajaxComplete(function(){
                                $('#info_payment').waitMe('hide');
                            });
                            $.ajax({
                                url: "?wc-api=WC_culqi",
                                type: "POST",
                                data: {token_id: Culqi.token.id, order_id: "<?php echo $numeroPedido ?>", installments: Culqi.token.metadata.installments },
                                dataType: 'json',
                                success: function(data) {
                                    if(data === "Error de autenticación") {
                                        var err_msg = data + ": verificar si su Llave Secreta es la correcta";
                                        $('#info_payment > #culqi_notify').html('<p style="color:#e54848; font-weight:bold">'+ err_msg + '</p>');
                                    } else {
                                        var result = "";
                                    if(data.constructor == String){
                                        result = JSON.parse(data);
                                    }
                                    if(data.constructor == Object){
                                        result = JSON.parse(JSON.stringify(data));
                                    }
                                    if (result.object === "error") {
                                        // Mostrar error
                                        var message = result.user_message;
                                        $('#info_payment > #culqi_notify').html('<p style="color:#e54848; font-weight:bold">'+ message + '</p>');
                                    } else {
                                        if (result.object === "charge") {
                                            $('#notify').empty();
                                            $("#info_payment").remove();
                                            $('div.woocommerce').append("<h1 style='text-align: center;'>Pago Exitoso</h1>" +
                                            "<p style='color:#46e6aa; font-weight:bold'>Pago realizado exitosamente</p>" +
                                            "<br><button id='home'>Seguir comprando</button>");
                                            // Procesar Venta en WooCommerce
                                            $.ajax({
                                                url: "?wc-api=WC_culqi",
                                                type: "POST",
                                                data: {emptyCart: 1},
                                                success: function (data) {
                                                    // console.log(data);
                                                }
                                            });
                                            } else {
                                                $('#info_payment > #culqi_notify').html('<p style="color:#e54848; font-weight:bold">ERROR EN LA RESPUESTA JSON</p>');
                                            }
                                        }
                                    }
                                },
                                error: function() {
                                    $('#culqi_notify').empty();
                                    $('#culqi_notify').html('Hubo algún problema en el procesamiento de la compra. Intenta nuevamente por favor.');
                                }
                            });
                        }
                    };
                    // End culqi()
                    $(document).ready(function() {
                        $('div.woocommerce').prepend("<h1 style='text-align: center;' id='title-result'></h1>");
                        $("#info_payment").on('click','#refresh', function(){
                            var url = '<?php echo wc_get_checkout_url(); ?>';
                            window.location.replace(url);
                        });
                        $("div.woocommerce").on('click','#home', function(){
                            var url = '<?php echo home_url(); ?>';
                            window.location.replace(url);
                        });
                        $('#pagar-now').on('click', function (e) {
                            Culqi.open();
                            $('#culqi_notify').empty();
                            e.preventDefault();
                        });
                        $('#btn-back').on('click', function(e){
                            var url = '<?php echo wc_get_checkout_url(); ?>';
                            window.location.replace(url);
                        });
                    });
                </script>

                <?php
            }
            function generateRandomString($length = 10) {
                $characters = '0123456789';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return $randomString;
            }
            public function restore_order_stock($order_id) {
                $order = new WC_Order($order_id);
                if (!get_option('woocommerce_manage_stock') == 'yes' && !sizeof($order->get_items()) > 0) {
                    return;
                }
                foreach ($order->get_items() as $item) {
                    if ($item['product_id'] > 0) {
                        $_product = $order->get_product_from_item($item);
                        if ($_product && $_product->exists() && $_product->managing_stock()) {
                            $old_stock = $_product->stock;
                            $qty = apply_filters('woocommerce_order_item_quantity', $item['qty'], $this, $item);
                            $new_quantity = $_product->increase_stock($qty);
                            do_action('woocommerce_auto_stock_restored', $_product, $item);
                            $order->add_order_note(sprintf(__('Item #%s stock incremented from %s to %s.', 'woocommerce'), $item['product_id'], $old_stock, $new_quantity));
                            $order->send_stock_notifications($_product, $new_quantity, $item['qty']);
                        }
                    }
                }
            }
        }

        class WC_culqi_Subscription extends WC_Payment_Gateway
        {
            public function __construct() {
                global $woocommerce;
                //$culqiWcClass= new WC_culqi();
                $this->includes();
                $this->id = 'culqi_subscription';
                $this->icon = home_url() . '/wp-content/plugins/' . dirname(plugin_basename(__FILE__)) . '/assets/images/cards.png';
                $this->method_title = __('Culqi Subscription', 'WC_culqi_Subscription');
                $this->method_description = __('Acepta tarjetas de crédito, débito o prepagadas.JVT', 'WC_culqi_Subscription');
                $this->order_button_text = __('Pagar', 'WC_culqi_Subscription');
                $this->has_fields = false;
                $this->supports = array(
                    'products'
                );
                $this->init_form_fields();
                $this->init_settings();
                $this->title = 'Tarjeta de crédito o débito';
                $this->description = 'Paga con tarjeta de crédito, débito o prepagada de todas las marcas.JVT';
                // Obtener credenciales y entorno
                $this->culqi_codigoComercio = CULQI_CODIGO;
                $this->culqi_key = CULQI_KEY;
                $this->culqi_nombre_comercio = get_bloginfo('name');
                add_action('woocommerce_api_' . strtolower(get_class($this)), array($this, 'crear_cargo_subscription'));// Crear Cargo
                add_action('woocommerce_receipt_culqi_subscription', array(&$this, 'receipt_page_subscription'));
                add_action('woocommerce_update_options_payment_gateways_ '.$this->id, array($this, 'process_admin_options'));
                if (!$this->is_valid_for_use()) $this->enabled = false;
            }

            /**
             * Incluye dependencias
             *
             */
            private function includes() {
                // Cargamos Requests y Culqi PHP
                include_once("includes/libraries/culqi-php/lib/culqi.php");
            }

            function get_plan_data($order, $plans) {
                
                $plan_id = 0;
                if( $order ) {

                    foreach( $order->get_items() as $item_id => $item_product ){
                        //$product_id = $item_product->get_product_id(); //Get the product ID
                        $quantity = $item_product->get_quantity(); //Get the product QTY
                        $product_name = $item_product->get_name(); //Get the product NAME

                         // Get an instance of the WC_Product object (can be a product variation  too)
                        $product = $item_product->get_product();

                         // Get the product description (works for product variation)
                        //$description = $product->get_description();

                        // Only for product variation
                        if($product->is_type('variation')){
                             // Get the variation attributes
                            $variation_attributes = $product->get_variation_attributes();

                            // Loop through each selected attributes
                            /*foreach($variation_attributes as $attribute_taxonomy => $term_slug){
                                $taxonomy = str_replace('attribute_', '', $attribute_taxonomy );
                                // The name of the attribute
                                $attribute_name = get_taxonomy( $taxonomy )->labels->singular_name;
                                // The term name (or value) for this attribute
                                $attribute_value = get_term_by( 'slug', term_slug, $taxonomy )->name;
                            }*/

                            if( isset($variation_attributes['attribute_suscripcion']) ) {
                                $suscription = $variation_attributes['attribute_suscripcion'];

                                switch($suscription) {
                                    case 'Semana': $limit = 1; break;
                                    case '1 Mes': $limit = 1; break;
                                    case '3 Meses': $limit = 3; break;
                                    case '6 Meses': $limit = 6; break;
                                    case 'Anual': $limit = 12; break;
                                    default : $limit = 12;
                                }
                            } else
                                $limit = 12;

                            $amount = ceil($order->get_total()/$limit)*100;
                            $product_slug = sanitize_title($product_name);
                            $plan_key = uniqid();

                            $plan_data = array(
                                            "alias" => $product_slug.'-'.$plan_key,
                                            "amount" => $amount,
                                            "currency_code" => get_woocommerce_currency(),
                                            "interval" => "meses",
                                            "interval_count" => 1,
                                            "limit" => $limit,
                                            "name" => $product_name.' '.$plan_key
                                        );

                            if( isset($plans) && is_array($plans) && count($plans) > 0 ) {
                                foreach($plans as $data) {

                                    if( $data['amount'] == $amount &&
                                        $data['limit'] == $limit
                                    ) {
                                        //$plan_id = $data['plan_id'];
                                        //$plan_data = array_merge($plan_data, array('plan_id' => $plan_id));
                                        $plan_data = $data;
                                        break;
                                    }
                                }
                            }

                            
                        }
                    }
                }

                return $plan_data;
            }
            /**
             * Crear Cargo (recibe token y procesa venta)
             * Via WC_API
             */
            function crear_cargo_subscription() {
                /*token_id: Culqi.token.id, order_id: "<?php echo $numeroPedido ?>", installments: Culqi.token.metadata.installments*/

                if (isset($_POST['token_id']) && isset($_POST['order_id'])) {
                    global $wpdb, $woocommerce;
                    $order = new WC_Order($_POST['order_id']);
                    $numeroPedido = str_pad($order->get_id(), 2, "0", STR_PAD_LEFT);
                    $total = str_replace('.', '', number_format($order->get_total(), 2, '.', ''));
                    $total = str_replace(',', '',$total);

                    $culqi = new Culqi\Culqi(array('api_key' => $this->culqi_key));

                    // Generamos un Código de pedido único (ejemplo)
                    $pedidoId = $this->generateRandomString(4)."-".$numeroPedido;
                    error_log("Número de pedido: ". $pedidoId);
                    error_log("Token: ". $_POST['token_id'] );
                    /**
                     * Validando y formateando datos (one more time)                             *
                     *
                     */
                     $dataUser = $order->get_user();
                     $user_id = $order->get_user_id();
                     $fono = $dataUser->billing_phone;
                     $descripcion = '';
                     $i = 1;
                     $separador = ' - ';
                     foreach ($order->get_items() as $product ){
                             if($i == count($order->get_items())){
                                     $separador = '';
                             }
                             $descripcion .= $product['name'].$separador;
                             $i++;
                     }
                     if(strlen ($descripcion)>5 && strlen ($descripcion)<60) {
                             error_log("Descripción correcto");
                     } else {
                             $descripcion = "Compra";
                     }
                     $datos_ciudad = "";
                     $datos_correo = "";
                     $datos_apellido = "";
                     $datos_nombre = "";
                     $datos_telefono = "";
                     $datos_direccion = "";
                     if ($order->get_billing_city() == null) {
                        $datos_ciudad = "Ciudad";
                    } else {
                        $datos_ciudad = $order->get_billing_city();
                    }
                    if ($order->get_billing_first_name() == null){
                        $datos_nombre = "Nombre";
                    }else {
                        $datos_nombre = $order->get_billing_first_name();
                    }
                    if ($order->get_billing_last_name() == null){
                        $datos_apellido = "Apellido";
                    }else {
                        $datos_apellido = $order->get_billing_last_name();
                    }
                    if ($order->get_billing_email() == null){
                        $datos_correo = "integrate@culqi.com";
                    } else {
                        $datos_correo = $order->get_billing_email();
                    }
                    if ($order->get_billing_phone() == null){
                        $datos_telefono = "12313123";
                    } else {
                        $datos_telefono = $order->get_billing_phone();
                    }
                    if ($order->get_billing_address_1() == null) {
                        $datos_direccion = "Avenida 123";
                    } else {
                        $datos_direccion = $order->get_billing_address_1();
                    }
                    // Creando Cargo
                    try {

                        update_option('ftest1','entro1');

                        $plans = get_option('wc_culqi_plans', array());

                        update_option('ftest2',print_r($plans,true));

                        $plan_data = $this->get_plan_data($order, $plans);

                        update_option('ftest3',print_r($plan_data,true));

                        if( isset($plan_data['plan_id']) )
                            $plan_id = $plan_data['plan_id'];
                        else {
                            $obj_plan = $culqi->Plans->create($plan_data);
                            $plan_id = $obj_plan->id;

                            update_option('ftest4',print_r($plan_id,true));

                            $plans[] = array_merge($plan_data, array('plan_id' => $plan_id));

                            update_option('wc_culqi_plans', $plans);
                        }

                        update_option('ftest5',print_r($plan_id,true));

                        $customer_id = get_user_meta($user_id,'wc_culqi_customer',true);

                        if( !isset($customer_id) || empty($customer_id) ) {
                            //Crear Cliente
                            $customer = $culqi->Customers->create(
                              array(
                                "address" => $datos_direccion,
                                "address_city" => $datos_ciudad,
                                "country_code" => $order->get_billing_country(),
                                "email" => $datos_correo,
                                "first_name" => $datos_nombre,
                                "last_name" => $datos_apellido,
                                "metadata" => array("order_id" => (string)$pedidoId),
                                "phone_number" => $datos_telefono
                              )
                            );

                            $customer_id = $customer->id;
                            update_user_meta($user_id,'wc_culqi_customer',$customer_id);
                        }

                        update_option('ftest6',print_r($customer,true));

                        //Crear Tarjeta
                        $card = $culqi->Cards->create(
                          array(
                            "customer_id" => $customer_id,
                            "token_id" => $_POST['token_id']
                          )
                        );

                        $card_id = $card->id;

                        update_option('ftest7',print_r($card,true));

                        //Create Subscription
                        $suscription = $culqi->Subscriptions->create(
                          array(
                              "card_id" => $card_id,
                              "plan_id" => $plan_id
                          )
                        );

                        update_option('ftest8',print_r($suscription,true));

                        if($suscription->object == "subscription") {

                            $order->update_meta_data( 'wc_culqi_plan', $plan_id );
                            $order->update_meta_data( 'wc_culqi_customer', $customer_id );
                            $order->update_meta_data( 'wc_culqi_card', $card_id );
                            $order->update_meta_data( 'wc_culqi_subscription', $suscription->id );

                            $order->payment_complete();
                        }
                        
                        wp_send_json($suscription);

                    } catch(Exception $e) {
                        // ERROR: El cargo tuvo algún error o fue rechazado
                        //echo 'Se dio una excepcion';
                        update_option('ftest9',print_r($e->getMessage(),true));
                        echo wp_send_json($e->getMessage());
                    }
               } else {
                    global $woocommerce;
                    $woocommerce->cart->empty_cart();
               }
               exit;
            }
            function is_valid_for_use() {
                if (!in_array(get_woocommerce_currency(), array('PEN', 'USD'))) return false;
                return true;
            }
            public function admin_options() {
                ?>
                    <h3><?php _e('Culqi Subscription', 'wc_culqi_payment_gateway'); ?></h3>
                    <table class="form-table">
                        <?php
                        if ($this->is_valid_for_use()) :
                            $this->generate_settings_html();
                        else :
                        ?>
                        <div class="inline error">
                            <p>
                                <strong>
                                    <?php _e('Gateway Disabled', 'wc_culqi_payment_gateway'); ?>
                                </strong>:
                                <?php _e('Error en la configuración.', 'wc_culqi_payment_gateway'); ?>
                            </p>
                        </div>
                        <?php
                        endif;
                        ?>
                    </table>
                <?php
            }
            function init_form_fields() {
                global $woocommerce;
                $this->form_fields = array(
                    'enabled' => array
                    (
                        'title' => __('Habilitar/Deshabilitar', 'wc_culqi_payment_gateway'),
                        'type' => 'checkbox',
                        'label' => __('Habilitar Culqi', 'wc_culqi_payment_gateway'),
                        'default' => 'yes'
                    )
                );
            }
            function process_payment($order_id) {
                $order = new WC_Order($order_id);
                $order->reduce_order_stock();
                return array
                (
                    'result' => 'success',
                    'redirect' => add_query_arg('order-pay', $order->get_id(), add_query_arg('key', $order->get_order_key(), get_permalink(woocommerce_get_page_id('pay'))))
                );
            }
            function receipt_page_subscription($order_id) {
                $order = new WC_Order($order_id);
                $numeroPedido = str_pad($order->get_id(), 2, "0", STR_PAD_LEFT);
                /**
                 * Datos de la compra
                 *
                 */
                $total = str_replace('.', '', number_format($order->get_total(), 2, '.', ''));
                $total = str_replace(',', '',$total);
                $dataUser = $order->get_user();
                $fono = $dataUser->billing_phone;
                $descripcion = '';
                $i = 1;
                $separador = ' - ';
                foreach ($order->get_items() as $product ) {
                    if($i == count($order->get_items())){
                        $separador = '';
                    }
                    $descripcion .= $product['name'].$separador;
                    $i++;
                }
                if(strlen ($descripcion)>5 && strlen ($descripcion)<60) {
                    error_log("Descripción correcto");
                } else {
                    $descripcion = "Compra";
                }
                $datos_ciudad = "";
                $datos_correo = "";
                $datos_apellido = "";
                $datos_nombre = "";
                $datos_telefono = "";
                $datos_direccion = "";
                if ($order->get_billing_city() == null) {
                    $datos_ciudad = "Ciudad";
                } else {
                    $datos_ciudad = $order->get_billing_city();
                }
                if ($order->get_billing_first_name() == null){
                    $datos_nombre = "Nombre";
                }else {
                    $datos_nombre = $order->get_billing_first_name();
                }
                if ($order->get_billing_last_name() == null){
                    $datos_apellido = "Apellido";
                }else {
                    $datos_apellido = $order->get_billing_last_name();
                }
                if ($order->get_billing_email() == null){
                    $datos_correo = "integrate@culqi.com";
                } else {
                    $datos_correo = $order->get_billing_email();
                }
                if ($order->get_billing_phone() == null){
                    $datos_telefono = "12313123";
                } else {
                    $datos_telefono = $order->get_billing_phone();
                }
                if ($order->get_billing_address_1() == null) {
                    $datos_direccion = "Avenida 123";
                } else {
                    $datos_direccion = $order->get_billing_address_1();
                }
                /*  End Crear Cargo  */

                ?>
                <div class="title u-text-center title-paypage">
                    <h2>Completa el pago con tu tarjeta de crédito</h2>
                </div>

                <div id="info_payment">
                    <span>Realiza la compra presionando <strong>Pagar</strong><br>Si deseas cambiar de medio de pago presiona <strong>Cancelar</strong></span><br><br>
                    <!---->
                    <div id="culqi-card" class="field-group">

                        <div class="card-wrapper"></div>

                        <form action="" method="POST" id="culqi-card-form">
                          <div class="field-group">
                            <div class="field-wrapper field-wrapper--simple barker-paypageCard_email">
                              <input type="text" size="50" name="email" data-culqi="card[email]" id="card[email]" value="<?php echo $datos_correo; ?>" readonly>
                            </div>
                          </div>
                          <div class="field-group" style="width:100%;">
                            <div class="field-wrapper field-wrapper--simple barker-paypageCard_number">
                              <input type="text" size="20" name="number" data-culqi="card[number]" id="card[number]" placeholder="Ingrese los 14 dígitos de la tarjeta">
                            </div>
                          </div>
                          <div class="field-group">
                            <div class="field-wrapper field-wrapper--simple barker-paypageCard_name">
                              <input type="text" size="50" name="name" data-culqi="card[name]" id="card[name]" placeholder="Nombre del titular de la tarjeta">
                            </div>
                          </div>
                          <div class="field-group barker-paypageCard_year">
                            <div class="field-wrapper field-wrapper--simple">
                              <input type="text" size="2" name="exp_month" data-culqi="card[exp_month]" id="card[exp_month]" placeholder="Mes">
                              <span>/</span>
                              <input type="text" size="4" name="exp_year" data-culqi="card[exp_year]" id="card[exp_year]" placeholder="Año">
                            </div>
                          </div>
                          <div class="field-group barker-paypageCard_code">
                            <div class="field-wrapper field-wrapper--simple">
                              <input type="text" size="4" name="cvv" data-culqi="card[cvv]" id="card[cvv]" placeholder="CVC">
                            </div>
                          </div>
                          <button id="pagar-now" class="btn btn--primary btn--block">Pagar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="btn-back" class="btn btn--secondary btn--block is-active">Cancelar</button>
                        </form>
                    <script>
                        //Culqi.createToken();
                    </script>
                    </div>
                    <div id="culqi_notify" style="padding:10px 0px;"></div>
                              </div>

                <script src="https://checkout.culqi.com/v2/"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
                <script src="<?php echo plugins_url("/public/js/card.js", __FILE__ ) ?>"></script>
                <script src="<?php echo plugins_url("/public/js/culqi-woocommerce-public.js", __FILE__ ) ?>"></script>
                <script defer src="<?php echo plugins_url("/assets/js/waitMe.js", __FILE__ ) ?>"></script>
                <link rel='stylesheet' href='<?php echo plugins_url("/assets/css/waitMe.css", __FILE__ ) ?>' type='text/css' media='all' />

                <script>

                    Culqi.publicKey = '<?php echo $this->culqi_codigoComercio ?>';
                    Culqi.init();

                    function run_waitMe(){
                        $('#info_payment').waitMe({
                            effect: 'ios',
                            text: 'Procesando pago...',
                            bg: 'rgba(255,255,255,0.7)',
                            color:'#28d2c8'
                        });
                    }

                    // Recibimos Token del Culqi.js
                    function culqi() {
                        if(Culqi.error) {
                            // Mostramos JSON de objeto error en consola
                            console.log(Culqi.error);
                            $('#info_payment > #culqi_notify').html('<p style="color:#e54848; font-weight:bold">'+ Culqi.error.user_message + '</p>');
                        } else {
                            console.log(Culqi.token.id);
                            $(document).ajaxStart(function(){
                                $('#culqi_notify').empty();
                                run_waitMe();
                            });
                            $(document).ajaxComplete(function(){
                                $('#info_payment').waitMe('hide');
                            });
                            $.ajax({
                                url: "index.php?wc-api=WC_culqi_Subscription",
                                type: "POST",
                                data: {token_id: Culqi.token.id, order_id: "<?php echo $numeroPedido ?>", installments: Culqi.token.metadata.installments },
                                dataType: 'json',
                                success: function(data) {
                                    if(data === "Error de autenticación") {
                                        var err_msg = data + ": verificar si su Llave Secreta es la correcta";
                                        $('#info_payment > #culqi_notify').html('<p style="color:#e54848; font-weight:bold">'+ err_msg + '</p>');
                                    } else {
                                        var result = "";
                                        if(data.constructor == String){
                                            result = JSON.parse(data);
                                        }
                                        if(data.constructor == Object){
                                            result = JSON.parse(JSON.stringify(data));
                                        }

                                        if (result.object === "error") {
                                            // Mostrar error
                                            var message = result.merchant_message;
                                            $('#info_payment > #culqi_notify').html('<p style="color:#e54848; font-weight:bold">'+ message + '</p>');
                                        } else {
                                            if (result.object === "subscription") {
                                                $('#notify').empty();
                                                $("#info_payment").remove();
                                                $('div.woocommerce').append("<h1 style='text-align: center;'>Pago Exitoso</h1>" +
                                                "<p style='color:#46e6aa; font-weight:bold'>Pago realizado exitosamente</p>" +
                                                "<br><button id='home'>Seguir comprando</button>");
                                                // Procesar Venta en WooCommerce
                                                /*$.ajax({
                                                    url: "index.php?wc-api=WC_culqi_Subscription",
                                                    type: "POST",
                                                    data: {emptyCart: 1},
                                                    success: function (data) {
                                                        // console.log(data);
                                                    }
                                                });*/

                                                location.href = "<?php echo $order->get_checkout_order_received_url(); ?>";

                                            } else {
                                                $('#info_payment > #culqi_notify').html('<p style="color:#e54848; font-weight:bold">ERROR EN LA RESPUESTA JSON</p>');
                                            }
                                        }
                                    }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.log(jqXHR);
                                    console.log(textStatus);
                                    console.log(errorThrown);
                                    
                                    $('#culqi_notify').empty();
                                    $('#culqi_notify').html('Hubo algún problema en el procesamiento de la compra. Intenta nuevamente por favor.');
                                }
                            });
                        }
                    };
                    // End culqi()
                    $(document).ready(function() {
                        $('div.woocommerce').prepend("<h1 style='text-align: center;' id='title-result'></h1>");
                        $("#info_payment").on('click','#refresh', function(){
                            var url = '<?php echo wc_get_checkout_url(); ?>';
                            window.location.replace(url);
                        });
                        $("div.woocommerce").on('click','#home', function(){
                            var url = '<?php echo home_url(); ?>';
                            window.location.replace(url);
                        });
                        $('#pagar-now').on('click', function (e) {
                            Culqi.createToken();
                            $('#culqi_notify').empty();
                            e.preventDefault();
                        });
                        $('#btn-back').on('click', function(e){
                            var url = '<?php echo wc_get_checkout_url(); ?>';
                            window.location.replace(url);
                        });
                    });
                </script>

                <?php
            }
            function generateRandomString($length = 10) {
                $characters = '0123456789';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return $randomString;
            }
            public function restore_order_stock($order_id) {
                $order = new WC_Order($order_id);
                if (!get_option('woocommerce_manage_stock') == 'yes' && !sizeof($order->get_items()) > 0) {
                    return;
                }
                foreach ($order->get_items() as $item) {
                    if ($item['product_id'] > 0) {
                        $_product = $order->get_product_from_item($item);
                        if ($_product && $_product->exists() && $_product->managing_stock()) {
                            $old_stock = $_product->stock;
                            $qty = apply_filters('woocommerce_order_item_quantity', $item['qty'], $this, $item);
                            $new_quantity = $_product->increase_stock($qty);
                            do_action('woocommerce_auto_stock_restored', $_product, $item);
                            $order->add_order_note(sprintf(__('Item #%s stock incremented from %s to %s.', 'woocommerce'), $item['product_id'], $old_stock, $new_quantity));
                            $order->send_stock_notifications($_product, $new_quantity, $item['qty']);
                        }
                    }
                }
            }
        }
        function woocommerce_culqi_add_gateway($methods) {
            $methods[] = 'WC_culqi';
            $methods[] = 'WC_culqi_Subscription';
            return $methods;
        }
        add_filter('woocommerce_payment_gateways', 'woocommerce_culqi_add_gateway');
    }
}
?>