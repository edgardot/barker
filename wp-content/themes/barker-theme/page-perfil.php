<?php
get_header();
if ( !is_user_logged_in() ) {
    auth_redirect();
}
?>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#edit"></use>
<div class="page-wrap">
      <main class="main">
        <div class="page-name">
          <h2>Datos de perfil</h2>
        </div>
        <section class="section faq">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-10">
                <div class="section-header">
                  <div class="title u-text-center">
                    <h2>Datos de perfil</h2>
                  </div>
                  <p class="u-text-center">Type something Type something texto falso, es la muestra del texto falso, la del texto falso es un lorem ipsum la del texto falso es un lorem ipsum</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-lg-4 profile-data">
                <div class="profile-data__box">
                  <div class="profile-data__text">
                    <h6>Nombre completo:</h6><span>Pedro Rodriguez</span>
                  </div>
                  <div class="profile-data__action">
                    <button class="js-btn-edit-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#edit"></use>
                      </svg>
                    </button>
                    <button class="js-btn-remove-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#trash"></use>
                      </svg>
                    </button>
                  </div>
                </div>
                <div class="js-modal-input-edit">
                  <div class="profile-data__edit modal-confirm">
                    <div class="row justify-content-center">
                      <div class="col-md-9">
                        <div class="modal-confirm__body">
                          <h4>Escribe tu nombre completo</h4>
                          <div class="form">
                            <div class="field-wrapper">
                              <input class="u-text-center" type="text" name="f-fullname" id="f-fullname" placeholder="Ejemplo: Pelusa">
                            </div>
                          </div>
                        </div>
                        <div class="modal-confirm__footer">
                          <button class="btn btn--secondary" type="button">Cancelar</button>
                          <button class="btn btn--primary" type="button">Guardar</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 profile-data">
                <div class="profile-data__box">
                  <div class="profile-data__text">
                    <h6>DNI:</h6><span>12345678</span>
                  </div>
                  <div class="profile-data__action">
                    <button class="js-btn-edit-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#edit"></use>
                      </svg>
                    </button>
                    <button class="js-btn-remove-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#trash"></use>
                      </svg>
                    </button>
                  </div>
                </div>
                <div class="js-modal-input-edit">
                  <div class="profile-data__edit modal-confirm">
                    <div class="row justify-content-center">
                      <div class="col-md-9">
                        <div class="modal-confirm__body">
                          <h4>Escribe tu nombre completo</h4>
                          <div class="form">
                            <div class="field-wrapper">
                              <input class="u-text-center" type="text" name="f-fullname" id="f-fullname" placeholder="Ejemplo: Pelusa">
                            </div>
                          </div>
                        </div>
                        <div class="modal-confirm__footer">
                          <button class="btn btn--secondary" type="button">Cancelar</button>
                          <button class="btn btn--primary" type="button">Guardar</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 profile-data">
                <div class="profile-data__box">
                  <div class="profile-data__text">
                    <h6>Teléfono:</h6><span>951 310 788</span>
                  </div>
                  <div class="profile-data__action">
                    <button class="js-btn-edit-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#edit"></use>
                      </svg>
                    </button>
                    <button class="js-btn-remove-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#trash"></use>
                      </svg>
                    </button>
                  </div>
                </div>
                <div class="js-modal-input-edit">
                  <div class="profile-data__edit modal-confirm">
                    <div class="row justify-content-center">
                      <div class="col-md-9">
                        <div class="modal-confirm__body">
                          <h4>Escribe tu nombre completo</h4>
                          <div class="form">
                            <div class="field-wrapper">
                              <input class="u-text-center" type="text" name="f-fullname" id="f-fullname" placeholder="Ejemplo: Pelusa">
                            </div>
                          </div>
                        </div>
                        <div class="modal-confirm__footer">
                          <button class="btn btn--secondary" type="button">Cancelar</button>
                          <button class="btn btn--primary" type="button">Guardar</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 profile-data">
                <div class="profile-data__box">
                  <div class="profile-data__text">
                    <h6>Correo:</h6><span>nombre@gmail.com</span>
                  </div>
                  <div class="profile-data__action">
                    <button class="js-btn-edit-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#edit"></use>
                      </svg>
                    </button>
                    <button class="js-btn-remove-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#trash"></use>
                      </svg>
                    </button>
                  </div>
                </div>
                <div class="js-modal-input-edit">
                  <div class="profile-data__edit modal-confirm">
                    <div class="row justify-content-center">
                      <div class="col-md-9">
                        <div class="modal-confirm__body">
                          <h4>Escribe tu nombre completo</h4>
                          <div class="form">
                            <div class="field-wrapper">
                              <input class="u-text-center" type="text" name="f-fullname" id="f-fullname" placeholder="Ejemplo: Pelusa">
                            </div>
                          </div>
                        </div>
                        <div class="modal-confirm__footer">
                          <button class="btn btn--secondary" type="button">Cancelar</button>
                          <button class="btn btn--primary" type="button">Guardar</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 profile-data">
                <div class="profile-data__box">
                  <div class="profile-data__text">
                    <h6>Número de tarjeta:</h6><span>1234 5678 9012 3456</span>
                  </div>
                  <div class="profile-data__action">
                    <button class="js-btn-edit-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#edit"></use>
                      </svg>
                    </button>
                    <button class="js-btn-remove-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#trash"></use>
                      </svg>
                    </button>
                  </div>
                </div>
                <div class="js-modal-input-edit">
                  <div class="profile-data__edit modal-confirm">
                    <div class="row justify-content-center">
                      <div class="col-md-9">
                        <div class="modal-confirm__body">
                          <h4>Escribe tu nombre completo</h4>
                          <div class="form">
                            <div class="field-wrapper">
                              <input class="u-text-center" type="text" name="f-fullname" id="f-fullname" placeholder="Ejemplo: Pelusa">
                            </div>
                          </div>
                        </div>
                        <div class="modal-confirm__footer">
                          <button class="btn btn--secondary" type="button">Cancelar</button>
                          <button class="btn btn--primary" type="button">Guardar</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 profile-data">
                <div class="profile-data__box">
                  <div class="profile-data__text">
                    <h6>Afiliado:</h6><span>14/07/97</span>
                  </div>
                  <div class="profile-data__action">
                    <button class="js-btn-edit-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#edit"></use>
                      </svg>
                    </button>
                    <button class="js-btn-remove-data-profile">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#trash"></use>
                      </svg>
                    </button>
                  </div>
                </div>
                <div class="js-modal-input-edit">
                  <div class="profile-data__edit modal-confirm">
                    <div class="row justify-content-center">
                      <div class="col-md-9">
                        <div class="modal-confirm__body">
                          <h4>Escribe tu nombre completo</h4>
                          <div class="form">
                            <div class="field-wrapper">
                              <input class="u-text-center" type="text" name="f-fullname" id="f-fullname" placeholder="Ejemplo: Pelusa">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-confirm__footer">
                      <button class="btn btn--secondary" type="button">Cancelar</button>
                      <button class="btn btn--primary" type="button">Guardar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>                        
<?php
get_footer();