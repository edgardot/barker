<?php
get_header();
?>
<nav class="nav-secondary">
      <div class="nav-secondary__content">
        <div class="container u-flex"><a href="<?php echo home_url().'/nosotros' ?>">Nosotros</a><a href="<?php echo home_url().'/ingredientes' ?>">Ingredientes</a><a class="is-active" href="<?php echo home_url().'/como-funciona' ?>">Cómo funciona</a><a href="<?php echo home_url().'/faq' ?>">Preguntas frecuentes</a></div>
      </div>
    </nav>
    <div class="page-wrap">
      <main class="main">
        <div class="page-name">
          <h2>Como funciona</h2>
        </div>
        <section class="section function">
          <div class="container">
            <div class="section-header">
              <div class="title u-text-center">
                <h2>¿Cómo funciona?</h2>
              </div>
            </div>
            <div class="function__main tabs-nav">
              <?php
                $resPosts=query_posts(array('post_type' => 'pasos','posts_per_page' => 0) );
                  if(count($resPosts)>0){
                      $i=1;
                      foreach ($resPosts as $resPost) { 
                          $imagenespasos.=' <div class="card-simple">
                      <figure class="card-simple__image"><img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'" alt=""></figure>
                       <h3 class="card-simple__title">'.$resPost->post_title.'</h3>
                    <p>'.$resPost->post_excerpt.'</p>
                    </div>';
                    $contenidopasos.='<li class="is-active" data-tab="'.$i.'">'.$resPost->post_title.'</li>';
                          $i++;
                      }
                  }
                  wp_reset_query();
              ?>
              <ul class="function__list js-function-items u-visible-tablet">
                <?php echo $contenidopasos; ?>
              </ul>
              <div class="function__carousel">
                <div class="owl-carousel js-function-carousel">
                  <?php echo $imagenespasos; ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 u-text-center">
                <p class="u-mtb--lv5"><span class="text-black">¿Cuántas hamburguesas barker debes darle a tu perro?</span><a class="link js-hamburguer-popup" href="">¡Acá la respuesta!</a></p>
                <p class="u-text-center"><a class="btn btn--primary" href="<?php echo home_url().'/suscripcion' ?>">Diseña tu pedido</a></p>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
<?php
get_footer();
