<?php
get_header();
?>
<div class="page-wrap">
      <main class="main">
        <div class="page-name">
          <h2><?php the_title(); ?></h2>
        </div>
        <section class="section conditions">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-10">
                <div class="conditions-header">
                  <h1>Condiciones</h1>
                  <h5>Última actualización el 17 de junio de 2018.</h5>
                </div>
                <div class="conditions-body">
                  <div class="subtitle-common">
                    <h6>1. TÉRMINOS Y CONDICIONES BARKER</h6>
                  </div>
                  <p>Todo uso y acceso a la página web <a href="https://www.barker.pe/">www.barker.pe</a> se rige por los términos y condiciones que se describirán a continuación y de igual por la legislación que sea aplicable en la republica del Perú. Estos términos y condiciones se basan en el intercambio entre el cliente que realice transacciones en la página web, en adelante llamado “usuario”, y Albar Inversiones SAC de nombre comercial Barker, en adelante llamado “Barker”.</p>
                  <div class="subtitle-common">
                    <h6>2. Aceptación</h6>
                  </div>
                  <p>Se presentan las condiciones generales que rigen el uso de la página web tanto para revisar información como realizar una transacción de suscripción. Al visitar y especialmente, registrarte como usuario, aceptas las siguientes condiciones generales y manifiestas:</p>
                  <ul>
                    <li>Que ha leído, entiende y comprende lo aquí expuesto,</li>
                    <li>Que es una persona con capacidad suficiente para contratar,</li>
                    <li>Que asume todas las obligaciones aquí dispuestas,</li>
                    <li>Que tiene la mayoría de edad legal (18 años) para acceder a este sitio web así como para adquirir los productos que se ofertan en el mismo</li>
                  </ul>
                  <p>De igual manera, al realizar una suscripción a BARKER, el usuario acepta adicionalmente las condiciones de suscripción que serán detalladas a continuación.</p>
                  <p>CUALQUIER PERSONA QUE NO ACEPTE ESTOS TÉRMINOS Y CONDICIONES GENERALES Y LAS POLÍTICAS DE PRIVACIDAD, LOS CUALES TIENEN UN CARÁCTER OBLIGATORIO Y VINCULANTE, DEBERÁ ABSTENERSE DE UTILIZAR EL SITIO Y/O LOS Condiciones de suscripción.</p>
                  <div class="subtitle-common">
                    <h6>3. REGISTRO DEL USUARIO</h6>
                  </div>
                  <p>Será un requisito necesario para la adquisición de productos y servicios ofrecidos en esta página web, la aceptación de las presentes condiciones y el registro del usuario, debiendo este contar con un nombre de usuario y clave de acceso. Se dará como hecho el conocimiento y aceptación de estos términos y condiciones por el solo hecho del registro del usuario en la página web de Barker.</p>
                  <p>El registro en <a href="https://www.barker.pe/">www.barker.pe</a> es gratuito. El ingreso del usuario con la clave le permitirá al usuario realizar suscripciones de manera personalizada, segura y confidencial.</p>
                  <div class="subtitle-common">
                    <h6>4. CLAVE SECRETA DEL USUARIO</h6>
                  </div>
                  <p>El usuario asume completa responsabilidad en mantener la confidencialidad de la clave secreta que le asigna al registrarse por <a href="https://www.barker.pe/">www.barker.pe</a>. Esta clave le permitirá poder realizar suscripciones en la página web y es de uso personal. La entrega a terceros de la clave secreta no involucra responsabilidad de BARKER o de sus relacionadas en caso de mala utilización.</p>
                  <div class="subtitle-common">
                    <h6>5. DERECHO DEL USUARIO DE ESTA PÁGINA WEB</h6>
                  </div>
                  <p>El usuario gozará de todos los derechos reconocidos en la legislación sobre protección al consumidor vigente en el territorio de Perú, además de los que se les otorgan en estos términos y condiciones. El usuario dispondrá en todo momento de los derechos de información y rectificación de los datos personales. </p>
                  <div class="subtitle-common">
                    <h6>6. PROCEDIMIENTO PARA HACER USO DE ESTA PÁGINA WEB</h6>
                  </div>
                  <p>BARKER informará de manera inequívoca y fácilmente accesible, los pasos que debe seguir el usuario para realizar su compra e informará vía email cuando fue recibida la solicitud del pedido. Esta solicitud pasará por un proceso de validación de datos del cliente, recolección de productos del pedido, en base al stock disponible, y, luego, se cerrará el pedido emitiéndose un comunicado final con fecha de entrega vía correo electrónico. BARKER indicará, además, su dirección de correo electrónico y los medios técnicos a disposición del usuario para detectar y corregir errores en el envío o en sus datos.<br>MEDIOS DE PAGO QUE SE PODRÁN UTILIZAR EN ESTA PÁGINA WEB<br>INFO DE CULQI</p>
                  <div class="subtitle-common">
                    <h6>7. DESPACHO DE LOS PRODUCTOS</h6>
                  </div>
                  <p>Los productos adquiridos a través de www.barker.pe sujetarán a las condiciones de despacho y entrega. La información del lugar de entrega es de exclusiva responsabilidad del usuario.<br>Los pedidos serán atendidos mediante rangos horarios definidos y de acuerdo con la disponibilidad del servicio courrier. La información respecto al despacho será confirmada vía correo electrónico al usuario.</p>
                  <div class="subtitle-common">
                    <h6>8. DERECHOS DE ANULACIÓN</h6>
                  </div>
                  <p>En caso de anular su pedido, ponerse en contacto con BARKER enviando un correo a: contacto@barker.pe o a través del botón pedidos de la página web www.barker.pe</p>
                  <div class="subtitle-common">
                    <h6>9.  POLÍTICA DE CAMBIOS Y DEVOLUCIONES</h6>
                  </div>
                  <No>se aceptan cambios ni devoluciones del producto. </No>
                  <div class="subtitle-common">
                    <h6>10.  USO DE LOS DATOS PERSONALES REGISTRADOS EN EL SITIO</h6>
                  </div>
                  <p>Los datos referidos en estos términos y condiciones tendrán como finalidad validar los pedidos y mejorar la labor de información y comercialización de los productos vendidos por www.barker.pe</p>
                  <div class="subtitle-common">
                    <h6>11.  OTROS SITIOS WEB</h6>
                  </div>
                  <p>BARKER no garantiza, avala ni respalda, en ningún caso, el acceso a información o contenido de cualquier otro sitio web o portal.</p>
                  <div class="subtitle-common">
                    <h6>12.  REPRESENTANTES</h6>
                  </div>
                  <p>Toda presentación o reclamo relacionado con actos o contratos ejecutados o celebrados a través de este sitio, deberá ser notificado por correo a por correo a <a href="mailto:contacto@barker.pe">contacto@barker.pe</a></p>
                  <div class="subtitle-common">
                    <h6>13.  CAPACIDAD LEGAL PARA CONTRATAR</h6>
                  </div>
                  <p>Los servicios de BABRKER están disponibles sólo para aquellos individuos que tengan capacidad legal para contratar, según lo dispuesto por la legislación vigente. Los servicios ofrecidos con exclusivo para mayores de 18 años. Si una persona no tiene capacidad legal para contratar o tiene menos de 18 años, debe abstenerse de utilizar los servicios ofrecidos en este sitio. Www.barker.pe , podrá suspender la participación de usuarios en cualquier momento.</p>
                  <div class="subtitle-common">
                    <h6>14.  DELIMITACIÓN DE RESPONSABILIDAD</h6>
                  </div>
                  <p>BARKER no se responsabiliza frente a los usuarios o terceros por los daños y perjuicios que sean consecuencia directa o indirecta de la interrupción, suspensión o finalización de los servicios ofrecidos por el sitio.</p>
                  <div class="subtitle-common">
                    <h6>15.  PREGUNTAS ADICIONALES</h6>
                  </div>
                  <Les>invitamos a visitar nuestra página de preguntas en Preguntas frecuentes</Les>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
    <?php
get_footer();
