
<!-- FOOTER -->
    <footer class="site-footer u-hidden-tablet">
      <div class="container">
        <div class="site-footer__header"><span class="u-flex">
            <svg>
              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#barker"></use>
            </svg></span></div>
        <div class="row">
          <div class="col-lg-4">
            <h6 class="site-footer__title">MAPA DE SITIO</h6>
            <ul class="site-footer__links">
              <li><a href="<?php echo home_url().'/nosotros' ?>">Nosotros</a></li>
              <li><a href="<?php echo home_url().'/como-funciona' ?>">Como funciona</a></li>
              <li><a href="<?php echo home_url().'/sabores' ?>">Productos</a></li>
              <li><a href="<?php echo home_url().'/pedidos' ?>">Suscripción</a></li>
              <li><a href="<?php echo home_url().'/faq' ?>">Preguntas frecuentes</a></li>
            </ul>
          </div>
          <div class="col-lg-4">
            <h6 class="site-footer__title">CONTACTO</h6>
            <ul class="site-footer__links">
              <li><a href="mailto:<?php echo get_theme_mod('bk_mail',''); ?>" target="_blank"><span class="icon">
                    <svg>
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#email"></use>
                    </svg></span><?php echo get_theme_mod('bk_mail',''); ?></a></li>
              <li><a href="<?php echo get_theme_mod('bk_instagram',''); ?>" target="_blank"><span class="icon">
                    <svg>
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#instagram"></use>
                    </svg></span>instagram</a></li>
              <li><a href="<?php echo get_theme_mod('bk_facebook',''); ?>"  target="_blank"><span class="icon">
                    <svg>
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#facebook"></use>
                    </svg></span>facebook</a></li>
              <li><a href="whatsapp://send?abid=+51<?php echo get_theme_mod('ars_whatsapp',''); ?>" target="_blank"><span class="icon">
                    <svg>
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#phone"></use>
                    </svg></span><?php echo get_theme_mod('bk_whatsapp',''); ?></a></li>
            </ul>
          </div>
          <div class="col-lg-4">
            <h6 class="site-footer__title">CONSEJOS, RECETAS Y TIPS PARA TU MASCOTA</h6>
            <form id="subscriptionBrk" action="#" method="post">
              <div class="field-wrapper subscribe">
                <input type="text" name="email-subscribe" id="email-subscribe" placeholder="Ingresa tu correo">
                <input type="hidden" name="action" value="brk_suscription_ajax">
                <input type="hidden" name="submit-suscription-form" value="<?php echo md5("Subs".date("Ymd"));?>">
                <button class="button icon" type="submit">ENVIAR</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </footer>
    <!-- /ENDS FOOTER-->
    <!-- /ENDS FOOTER-->
    <div class="hamburguer popup-inner" id="barker-hamburguer">
      <div class="hamburguer__content">
        <div class="subtitle-xs">
          <h3>¿Cuántas hamburguesas barker debes darle a tu perro?</h3>
        </div>
        <p>*Cada hamburguesa contiene 50 gramos de barker</p>
        <div class="hamburguer__table">
          <table>
            <thead>
              <tr>
                <th>PESO</th>
                <th>1.5m a 11m</th>
                <th>12m a 8 años</th>
                <th>8 años a más</th>
                <th>Gestante/lactante</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1 - 5 kg</td>
                <td>1</td>
                <td>2</td>
                <td>2</td>
                <td>3</td>
              </tr>
              <tr>
                <td>5 - 10 kg</td>
                <td>3</td>
                <td>4</td>
                <td>3</td>
                <td>6</td>
              </tr>
              <tr>
                <td>10 - 22 kg</td>
                <td>5</td>
                <td>8</td>
                <td>6</td>
                <td>12</td>
              </tr>
              <tr>
                <td>22 - 45 kg</td>
                <td>11</td>
                <td>16</td>
                <td>12</td>
                <td>24</td>
              </tr>
            </tbody>
          </table>
        </div>
        <p class="hamburguer__feedback">*Actividad Física intermedia. De ser el caso de perros de alta actividad, considerar duplicar o triplicar la ración de acuerdo a la exigencia física.</p>
      </div>
    </div>
    <?php wp_footer(); ?>
    
    <script type="text/javascript" >

      jQuery("#subscriptionBrk").submit(function(e){
        e.preventDefault(); 
        var form = $(this);
        $.ajax({
               type: "POST",
               url: brk_ajax_vars.ajaxurl,
               data: form.serialize(), // serializes the form's elements.
               success: function(data)
               {
                   if(data.msg=="ok"){
                    alert("Gracias por suscribirse");
                   }else{
                    alert("Gracias, usted ya está suscrito con nosotros");
                   }
                   jQuery("#email-subscribe").val("");
               }
             });
      });

      jQuery("#sendAddDistrict").click(function(e){
        console.log('Se ha hecho click en enviar formulario de distrito');
        // e.preventDefault(); 
        // $.ajax({
        //        type: "POST",
        //        url: brk_ajax_vars.ajaxurl,
        //        data: {action:"brk_add_district_ajax", email:$("#f-email-interested").val(),district:$("#js-add-district-select").val()}, // serializes the form's elements.
        //        success: function(data)
        //        {
        //            if(data.msg=="ok"){
        //             alert("Gracias por ayudarnos");
        //            }else{
        //             alert("Lo sentimos, ocurrio un problema");
        //            }
        //            jQuery("#email-subscribe").val("");
        //        }
        //      });
      });
      
    </script>
  </body>
</html>