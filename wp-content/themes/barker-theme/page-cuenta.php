<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="shortcut icon" type="image/png" href="">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title><?php wp_title(); ?></title>
<?php 
wp_head(); 


?>
  </head>
<body <?php body_class(); ?>> <!-- https://developer.wordpress.org/reference/functions/body_class/ -->
 <main class="main account is-register">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-lg-5 account__main">
            <div class="account__main-wrap">
              <div class="account__logo"><a href="<?php echo home_url() ?>">
                  <svg>
                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#barker"></use>
                  </svg></a></div>
              <div class="account__message subtitle-xs">
                <h5>Bienvenido</h5>
              </div>
              <div class="account__options"><a class="is-active js-btn-form-type" href="" data-form="form-register">Registro</a><a class="js-btn-form-type" id="IniSecBar" href="" data-form="form-login">Iniciar sesión</a></div>
              <div class="account__form-type js-account-form-type">
                <div class="account__form" id="form-register">
                  <p>Para poder registrarte rellena el formulario con tus datos. </p>
                  <form class="form" action="?login_page=ok" method="post">
                    <div class="field-wrapper">
                      <input type="text" name="rg-name" id="rg-name" placeholder="Nombres" autocomplete="off">
                    </div>
                    <div class="field-wrapper">
                      <input type="text" name="rg-apellidos" id="rg-apellidos" placeholder="Apellidos" autocomplete="off">
                    </div>
                    <div class="field-wrapper">
                      <input type="text" name="rg-phone" id="rg-phone" placeholder="Celular" autocomplete="off">
                    </div>
                    <div class="field-wrapper">
                      <input type="text" name="rg-email" id="rg-email" placeholder="Correo electrónico" autocomplete="off">
                    </div>
                    <div class="field-wrapper">
                      <input type="password" name="rg-password" id="rg-password" placeholder="Contraseña" autocomplete="off">
                    </div>
                    <div class="field-action">
                      <button class="btn btn--primary btn--block" type="submit">Registrarme</button>
                    </div>
                    <div id="mess-reg">
                      <?php
                      if ( $_POST ) {
          
                              $error = 0;
                                      
                     
                              $username = $email = esc_sql($_REQUEST['rg-email']);
                              if ( !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", $email) ) {  
                                
                                echo 'Por favor ingresar un correo válido';
                                $error = 1;
                              }
                              
                              if ( $error == 0 ) {
                                
                                $random_password = wp_generate_password( 12, false );  
                                $user_id = wp_create_user( $username, $_POST['rg-password'] , $email );  
                                wp_update_user( array( 'ID' => $user_id, 'first_name' => $_POST['rg-name'] ) );
                                wp_update_user( array( 'ID' => $user_id, 'last_name' => $_POST['rg-apellidos'] ) );
                                update_user_meta( $user_id, 'billing_phone', esc_sql( $_POST['rg-phone'] ) );
                                if ( is_wp_error($user_id) ) {
                                
                                  echo 'Ya existe una cuenta con el correo electrónico registrado';  
                                } else {
                                  
                                  //$from     = "contacto@barker.pe";  
                                  //$headers   = 'From: '.$from . "\r\n";  
                                  $headers[] = 'From: Barker <contacto@barker.pe>';
                                  $headers[] = 'Content-Type: text/html; charset=UTF-8';
                                  $subject   = "Te has registrado exitosamente en Barker";  
                                  $message='
                                      <!DOCTYPE html>
                                      <html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
                                      <head>
                                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                                        <meta name="viewport" content="initial-scale=1.0">
                                        <meta name="format-detection" content="telephone=no">
                                        <title>Barker: Registro exitoso.</title>
                                        <style type="text/css">
                                          body{
                                            margin:0;
                                            padding:0;
                                            font-family: Arial, Helvetica, sans-serif;
                                            color: #333333;
                                            font-size: 14px;
                                          }
                                          /* yahoo, hotmail */
                                          .ReadMsgBody{ width:100%; }
                                          .ExternalClass{ width:100%; }
                                          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{ line-height: 100%; }
                                          .yshortcuts a{ border-bottom: none !important; }
                                          .vb-outer{ min-width: 0 !important; }
                                          .RMsgBdy, .ExternalClass{
                                            width: 100%;
                                            background-color: #3f3f3f;
                                            background-color: #3f3f3f;
                                          }
                                          /* outlook/office365 add buttons outside not-linked images and safari have 2px margin */
                                          [o365] button{ margin: 0 !important; }
                                          /* outlook */
                                          table{ mso-table-rspace: 0pt; mso-table-lspace: 0pt; }
                                          #outlook a{ padding: 0; }
                                          a { border: 0 !important; text-decoration: none !important; }
                                          img{ outline: none; text-decoration: none; border: none; -ms-interpolation-mode: bicubic; }
                                          a img{ border: none; }
                                          /* Gmail */
                                          .ii a[href] {
                                            color: #333333 !important;
                                          }

                                          @media only screen and (max-width: 480px) {
                                            .mobile-hide, td[class="mobile-hide"]{ display: none !important; }
                                            .mobile-textcenter, td[class="mobile-textcenter"] { text-align: center !important; }
                                            .mobile-height-auto {
                                              height: auto !important;
                                                      min-height: 0 !important;
                                            }
                                            .lh-1 {
                                              line-height: 1.3 !important;
                                            }
                                            .mobile-sinborde {
                                              border: 0 !important;
                                              border: none !important;
                                            }
                                            .mobile-img-full {
                                              width: 100% !important;
                                            }
                                            .mobile-margen {
                                              width: 8% !important;
                                            }
                                            td[class="mobile-margen"] {
                                              width: 8% !important;
                                            }
                                            .mobile-full, table[class="mobile-full"] {
                                              width: 100% !important;
                                              max-width: none !important;
                                            }
                                            .mobile-borde-right {
                                              width: 100% !important;
                                              max-width: none !important;
                                              border-right: 1px solid #e4e4e4 !important;
                                            }
                                            .mobile-borde-top {
                                              width: 100% !important;
                                              max-width: none !important;
                                              border-top: 0 !important;
                                            }
                                            .mobile-borde-bottom {
                                              width: 100% !important;
                                              max-width: none !important;
                                              border-bottom: 0 !important;
                                            }
                                            .mobile-borde-bottom-1 {
                                              width: 100% !important;
                                              max-width: none !important;
                                              border-bottom: 1px solid #e4e4e4 !important;
                                            }
                                            .textmobile {
                                              font-size: 24px !important;
                                              line-height: 35px !important;
                                            }
                                          }
                                        </style>
                                      </head>
                                      <body alink="#00a94f" vlink="#00a94f" style="color: #333333;">
                                      <!--[if (gte mso 9)|(IE)]>
                                        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="color: #333333;">
                                          <tr>
                                            <td>
                                              <![endif]-->
                                              <table border="0" cellspacing="0" cellpadding="0" style="margin:auto; width:100%; max-width:600px; font-family:Arial;">
                                                <tr>
                                                  <td height="auto">
                                                    <img border="0" src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/barker_mail/image-r1-c1.jpg" style="width:100%; max-width:600px; display: block;">
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="auto">
                                                    <img border="0" src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/barker_mail/image-r2-c1.jpg" style="width:100%; max-width:600px; display: block;">
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="auto">
                                                    <img border="0" src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/barker_mail/image-r3-c1.jpg" style="width:100%; max-width:600px; display: block;">
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="20"></td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                        <td style="width: 90px;" class="mobile-margen">&nbsp;</td>
                                                        <td>
                                                          <!--[if gte mso 9]>
                                                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                              <td align="left" valign="top" width="250">
                                                          <![endif]-->
                                                          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#f0c344;">
                                                            <tbody>
                                                              <tr>
                                                                <td height="35" align="center" style="font-size: 15px; color: #ffffff; text-transform: uppercase; font-weight: bold; letter-spacing: 1px;">Gracias por su suscripción</td>
                                                              </tr>

                                                            </tbody>
                                                          </table>
                                                        </td>
                                                        <td style="width: 90px;" class="mobile-margen">&nbsp;</td>
                                                      </tr>
                                                    </table>
                                                    <div style="display:block;margin-left:35%">
                                          <h2><em><strong>Nombres:</strong></em>'.$_POST['rg-name'].'</h2>
                                          <h2><em><strong>Apellidos:</strong></em>'.$_POST['rg-apellidos'].'</h2>
                                          <h2><em><strong>correo:</strong></em>'.$username.'</h2>
                                          <h2><em><strong>Teléfono:</strong></em>'.$_POST['rg-phone'].'</h2>
                                          <h2><em><strong>Contrase&ntilde;a:</strong></em>'.$_POST['rg-password'].'</h2>
                                          </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="20"></td>
                                                </tr>
                                                <tr>
                                                  <td height="auto">
                                                    <img border="0" src="https://demo.barker.pe/wp-content/themes/barker-theme/assets/images/barker_mail/image-r5-c1.jpg" style="width:100%; max-width:600px; display: block;">
                                                  </td>
                                                </tr>
                                              </table>
                                              <!--[if (gte mso 9)|(IE)]>
                                            </td>
                                          </tr>
                                        </table>
                                      <![endif]-->
                                      </body>
                                      </html>
                                          
                                          ';
                                  // Email password and other details to the user
                                  wp_mail( $email, $subject, $message, $headers );  
                                 
                                  echo '<script>jQuery(document).ready(function ($) {jQuery("#IniSecBar").trigger("click"); jQuery("#regOK").html("<strong>Gracias por registrarte:</strong> ");});</script>';
                                  $error = 2; // We will check for this variable before showing the sign up form. 
                                }
                              }
                     
                            }
                      ?>
                    </div>
                  </form>
                </div>
                <div class="account__form" id="form-login" style="display:none;">
                  <p><span id="regOK"></span> Ingresa poniendo tu correo y contraseña.</p>
                  <form name="loginform" id="loginform" class="form" action="<?php echo home_url();?>/wp-login.php" method="post">
                    <div class="field-wrapper">
                      <input type="text" name="log" id="user_login" placeholder="Correo" autocomplete="off">
                    </div>
                    <div class="field-wrapper">
                      <input type="password" name="pwd" id="user_pass" placeholder="Contraseña" autocomplete="off">
                    </div>
                    <div class="field-action">
                      
                      <button id="wp-submit" name="wp-submit" class="btn btn--primary btn--block" type="sumbit">Iniciar sesión</button>
                      <p class="account__call-reset u-text-center"><a class="js-btn-form-type" href="" data-form="form-reset">Olvide mi contraseña</a></p>
                    </div>
                    <div id="message"></div>
                  </form>
                </div>
                <div class="account__form" id="form-reset" style="display:none;">
                  <p>Ingresa tu correo para poder ayudarte.</p>
                  <form class="form" action="" method="post">
                    <div class="field-wrapper">
                      <input type="text" name="rs-user-name" id="rs-user-name" placeholder="Correo" autocomplete="off">
                    </div>
                    <div class="field-action">
                      <button class="btn btn--primary btn--block" type="submit">Enviar</button>
                      <p class="account__call-reset u-text-center"><a class="js-btn-form-type" href="" data-form="form-login">Me acordé mi contraseña</a></p>
                    </div>
                  </form>
                </div>
              </div>
              <div class="account__register-options">
                <h5>También puedes registrarte con:</h5>
                <div class="account__register-option google"><a href="<?php echo home_url().'/wp-login.php?action=wordpress_social_authenticate&mode=login&provider=Google&redirect='.wsl_get_current_url(); ?>">Ingresa con Google</a></div>
                <div class="account__register-option facebook"><a href="<?php echo home_url().'/wp-login.php?action=wordpress_social_authenticate&mode=login&provider=Facebook&redirect='.wsl_get_current_url(); ?>">Ingresa con Facebook</a></div>
              </div>
            </div>
          </div>
          <div class="col-lg-7 account__bg u-padding--lv0 u-hidden-tablet-wide">
            <div class="account__bg-image"></div>
          </div>
        </div>
      </div>
    </main>
    
<?php wp_footer(); ?>
<?php 
    if(isset($_GET["action"]) && $_GET["action"]=="login"){
      echo '<script>jQuery(document).ready(function ($) {jQuery("#IniSecBar").trigger("click");});</script>';      
    }
    ?>
  </body>
</html>
