<?php
get_header();
?>
<section class="intro">
      <div class="owl-carousel intro__carousel js-banner-home-carousel">
         <?php
                $resPosts=get_posts(array('post_type' => 'slider-home','posts_per_page' => 3) );
                foreach ($resPosts as $resPost) { 
                  echo '
        <div class="item" style="background-image: url(\''.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'full')[0].'\');"></div>';
            }
              ?>
      </div>
      <div class="intro__wrap">
        <div class="container u-full-height">
          <div class="row justify-content-center u-full-height">
            <div class="intro__content col-lg-10">
              <div class="intro__text u-text-center">
                <div class="subtitle">
                  <h1><?php echo get_theme_mod('bk_titulo'); ?></h1>
                  <p><?php echo get_theme_mod('bk_subtitulo'); ?></p>
                </div>
              </div>
              <div class="intro__action">
                <div class="intro__action-links u-flex justify-content-between">
                  <div class="intro__action-item"><a class="btn btn--primary btn--block" href="<?php echo get_theme_mod('bk_botonsemanaI',''); ?>"><?php echo get_theme_mod('bk_botonsemana'); ?></a></div>
                  <div class="intro__action-item"><a class="btn btn--primary btn--block" href="<?php echo get_theme_mod('bk_botondisenaI',''); ?>"><?php echo get_theme_mod('bk_botondisena'); ?></a></div>
                </div>
                <div class="intro__action-nav u-text-center">
                  <div class="intro__action-button"><span class="text">Conoce el producto</span>
                    <button class="icon js-btn-godown" type="button">
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#arrow-down"></use>
                      </svg>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <main class="main page-main">
      <!--  QUÉ ES BARKER-->
      <section class="section what-is">
        <div class="container">
          <div class="row align-items-center u-text-justify">
            <div class="title col u-visible-tablet-wide">
              <h2>¿Qué es Barker?</h2>
            </div>
            <div class="col-lg-7 order-1 what-is__text">
              <div class="title u-hidden-tablet-wide">
                <h2><?php echo get_theme_mod('bk_barker'); ?></h2>
              </div>
              <p><?php echo get_theme_mod('bk_contenido'); ?></p>
            </div>
            <div class="what-is__img col-lg-5 order-lg-1" id="scene">
              <div class="what-is__food food">
                <div class="food__item food__item--insumos"></div>
                <div class="food__item food__item--preparada js-slide-toggle"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- INGREDIENTES-->
      <section class="section section--big ingredients u-hidden-tablet">
        <div class="container">
          <div class="title u-text-center">
            <h2>Ingredientes</h2>
          </div>
          <article class="ingredients__featured">
            <div class="row align-items-center u-text-justify">
              <div class="col-lg-6 order-lg-2"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/nosotros/comida/comida-insumos.png" alt=""></div>
              <div class="col-lg-6 order-lg-1">
                <div class="subtitle">
                  <h4>Insumos frescos de<br>Grado Humano:</h4>
                </div>
                <p>Los insumos utilizados para la formulación de Barker® son de grado de consumo humano, seleccionados con altos estándares de calidad y cada lote es aprobado con rigurosos análisis.</p>
              </div>
            </div>
          </article>
          <article class="ingredients__featured">
            <div class="row align-items-center u-text-justify">
              <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ingredientes/balance-nutricional.png" alt=""></div>
              <div class="col-lg-6">
                <div class="subtitle">
                  <h4>Balance Nutricional</h4>
                </div>
                <p>Barker® está diseñado para cubrir responsablemente, todos los requerimientos nutricionales y el importante balance entre ellos:</p>
                <p>Desde el importante nivel de Energía, los específicos niveles de aminoácidos y calidad de proteína, pasando por los niveles de fibras solubles, importantes para las movilizaciones peristálticas y el buen funcionamiento gastrointestinal así como las importantes vitaminas y minerales.</p>
              </div>
            </div>
          </article>
        </div>
      </section>
      <section class="section benefits u-hidden-tablet">
        <div class="container">
          <div class="row justify-content-center u-text-center">
            <div class="col-lg-8">
              <div class="subtitle u-text-center">
                <h2>Beneficios de los ingredientes</h2>
              </div>
              <div class="benefits__main">
                <?php
                $resPosts=query_posts(array('post_type' => 'Beneficios','posts_per_page' => 0) );
                  if(count($resPosts)>0){
                      $i=1;
                      foreach ($resPosts as $resPost) { 
                          $imagenesBeneficios.=' <li class="benefits__item '.($i==1?"is-active":"").'" data-tab="'.$i.'">
                       <figure>
                        <img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'" alt="">
                       <img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'" alt=""></figure>
                    </li>';
                    $contenidoBeneficios.='<div class="item">
                     <div class="subtitle-sm">
                        <h4>'.$resPost->post_title.'</h4>
                      </div>
                      <p>'.$resPost->post_excerpt.'</p>
                      </div>';
                          $i++;
                      }
                  }
                  wp_reset_query();
              ?>
                <ul class="benefits__list js-benefit-items">
                  <?php echo $imagenesBeneficios; ?>
                </ul>
                <div class="benefits__text">
                  <div class="owl-carousel js-benefit-carousel">
                    <?php echo $contenidoBeneficios; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <p class="u-text-center"><a class="btn btn--primary" href="<?php echo home_url().'/suscripcion' ?>">Diseña tu pedido</a></p>
        </div>
      </section>
      <!-- COMO FUNCIONA-->
      <section class="section section--highlight u-hidden-tablet">
        <div class="container">
          <div class="section-header">
            <div class="title u-text-center">
              <h2>¿Cómo funciona?</h2>
            </div>
          </div>
          <div class="row row u-mb--lv5">
             <?php
          query_posts('order=DESC&post_type=pasos&posts_per_page=3');
          $i=0;
          if (have_posts()) : while (have_posts()) : the_post();
            echo '<div class="col-lg-4 card-simple">
              <figure class="card-simple__image"><img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID()),'full')[0].'" alt=""></figure>
              <h3 class="card-simple__title">'.the_title('','',false).'</h3>
              <p>'.get_the_excerpt().'</p>
            </div>';
       endwhile; endif;
          wp_reset_query();
        ?>
          </div>
          <div class="row">
            <div class="col-lg-12 u-text-center">
              <p class="u-mtb--lv5"><span class="text-black">¿Cuántas hamburguesas barker debes darle a tu perro?</span><a class="link js-hamburguer-popup" href="">¡Acá la respuesta!</a></p>
            </div>
          </div>
        </div>
      </section>
      <!-- NOSOTROS-->
      <section class="section aboutus">
        <div class="container">
          <div class="row justify-content-center u-text-center">
            <div class="col-lg-10">
              <div class="title u-text-center">
                <h3>Nosotros</h3>
              </div>
              <div class="team tab">
                 <?php
                $resPosts=query_posts(array('post_type' => 'nosotros-barker','posts_per_page' => 0) );
                  if(count($resPosts)>0){
                      $i=1;
                      foreach ($resPosts as $resPost) { 
                          $imagenesNosotros.='<li class="members__item" data-tab="'.$i.'">
                      <figure class="members__item-img">
                        <img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'">
                        <img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'" alt=""></figure>
                      <div class="members__item-text">
                        <h6>'.$resPost->post_title.'</h6>
                      </div>
                    </li>';
                    $contenidoNosotros.='<div class="tab__content-item" id="'.$i.'"><p>'.$resPost->post_excerpt.'</p></div>';
                          $i++;
                      }
                  }
                  wp_reset_query();
              ?>
                <div class="team-members js-tabs hover">
                  <ul class="members tab__items">
                   <?php echo $imagenesNosotros; ?>
                  </ul>
                </div>
                <div class="tab__content">
                 <?php echo $contenidoNosotros; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- TESTIMONIOS-->
      <section class="section testimony">
        <div class="container">
          <div class="section-header">
            <div class="title u-text-center">
              <h2>Testimonios y experiencias Barker</h2>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col-lg-8">
              <div class="testimony__carousel">
                <div class="owl-carousel js-testimony-carousel">
                   <?php
                $resPosts=query_posts(array('post_type' => 'Testimonios','posts_per_page' => 5) );
                  if(count($resPosts)>0){
                      $i=1;
                      foreach ($resPosts as $resPost) { 
                          echo '<div class="testimony__item">
                          <div class="testimony__text"><p>'.$resPost->post_excerpt.'</p></div>
                        <figure class="u-flex"><img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'" alt="">
                        <figcaption>
                        <h4>'.$resPost->post_title.'</h4>
                      </figcaption>
                    </figure></div>';
                          $i++;
                      }
                  }
                  wp_reset_query();
              ?>
                </div>
              </div>
            </div>
          </div>
          <p class="u-text-center"><a class="btn btn--primary" href="<?php echo home_url().'/nosotros' ?>">Conoce más de Barker</a></p>
          <p class="u-text-center u-visible-tablet"><a class="btn btn--primary" href="<?php echo home_url().'/pedidos' ?>">Diseña tu pedido</a></p>
        </div>
      </section>
    </main>
<?php
get_footer();
