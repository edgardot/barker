<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_puntos-de-ventas',
		'title' => 'puntos de ventas',
		'fields' => array (
			array (
				'key' => 'field_5b445fe49201c',
				'label' => 'Distrito',
				'name' => 'distrito',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5b44600fd760e',
				'label' => 'Nombre del Veterinario',
				'name' => 'nombre_del_veterinario',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5b44603833995',
				'label' => 'Direccion',
				'name' => 'direccion',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5b44604fcb995',
				'label' => 'telefono',
				'name' => 'telefono',
				'type' => 'number',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_5b44606f41eff',
				'label' => 'ubicacion',
				'name' => 'ubicacion',
				'type' => 'google_map',
				'center_lat' => '',
				'center_lng' => '',
				'zoom' => '',
				'height' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'punto-de-ventas',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_ubicacion',
		'title' => 'Ubicación',
		'fields' => array (
			array (
				'key' => 'field_5b445d968a7cb',
				'label' => 'Ubicacion',
				'name' => 'ubicacion',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'testimonios',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

	register_field_group(array (
        'id' => 'acf_suscripcion',
        'title' => 'Suscripción',
        'fields' => array (
            array (
                'key' => 'field_5b69fa5f153b8',
                'label' => 'Productos',
                'name' => 'barker_suscripcion_productos',
                'type' => 'relationship',
                'post_type' => array (
                    0 => 'product',
                ),
                'taxonomy' => array (
                    0 => 'all',
                ),
                'filters' => array (
                    0 => 'search',
                    1 => 'post_type',
                ),
                'result_elements' => array (
                    0 => 'featured_image',
                    1 => 'post_title',
                    2 => 'post_type',
                ),
                'max' => '',
                'return_format' => 'object',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page',
                    'operator' => '==',
                    'value' => '125',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 2,
    ));
}
