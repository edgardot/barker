export default class Profile {
  constructor() {
    this.showModalEdit();
  }

  showModalEdit() {
    let $btnEditeDataProfile = $('.js-btn-edit-data-profile');

    $btnEditeDataProfile.on('click', (e) => {
      console.log($(e.currentTarget));
      let modalTpl = $(e.currentTarget).closest('.profile-data__box').next().html();

      $.magnificPopup.open({
        items: {
          src: $(modalTpl),
          type: 'inline'
        }
      });
    });
  }
}