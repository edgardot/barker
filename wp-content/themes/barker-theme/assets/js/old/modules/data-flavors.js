export default [
  {
    name: 'pollo',
    image: 'pollo',
    text: '<p>Piezas de pollo molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>'
  },
  {
    name: 'pavo',
    image: 'pavo',
    text: '<p>Piezas de pavo molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>'
  },
  {
    name: 'carne de res',
    image: 'res',
    text: '<p>Piezas de carne de res molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>'
  }
]