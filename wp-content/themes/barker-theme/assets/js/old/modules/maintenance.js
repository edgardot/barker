class Maintenance {
  constructor() {
    this.userEvents();
    this.changeStatus();
  }

  userEvents() {
    $('input[name^="f-suscription-status"]').on('change', this.changeStatus);
    $('.js-btn-remove-order').on('click', this.removeOrder.bind(this));
    $('.js-btn-enable-order').on('click', this.enableOrder.bind(this));
    $('.js-order-short > div').on('click', this.showDetail);
    $('.js-show-order-all').on('click', this.showAll);
    $('.js-btn-info-order').on('click', this.showInfo);
  }

  showDetail(e) {
    let $parent = $(e.currentTarget).parent();
    $parent.children().eq(1).slideDown();
    $parent.siblings().fadeOut('fast');

    $('.orders-current__head').addClass('is-show-action');
  }

  showAll(e) {
    let $orders = $('.js-order-short');
    $.each($orders, (i, el) => {
      $(el).children().eq(1).slideUp();
      $(el).fadeIn('fast');
    });

    $('.orders-current__head').removeClass('is-show-action');
  }

  showInfo(e) {
    let $this = $(e.currentTarget);
    let $parent = $this.closest('.order-short__box');
    let $info = $parent.siblings('.js-info-order').html();

    $.magnificPopup.open({
      items: {
        src: $info,
        type: 'inline'
      }
    })
  }

  changeStatus(e) {
    let $selector = e ? $(e.currentTarget) : $('input[name^="f-suscription-status"]');
    
    $.each($selector, (i, el) => {
      let $resultElem = $(el).parent().next();
      if ($(el).is(':checked'))
        $resultElem.text('Renovación automática.');
      else
        $resultElem.text('Renovación automática cancelada.');
    });
  }

  removeOrder(e) {
    e.stopPropagation();
    let orderId = $(e.currentTarget).closest('.js-order-short').data('order-id');
    this.confirmDialog(orderId, 'eliminar');
  }

  enableOrder(e) {
    e.stopPropagation();
    let orderId = $(e.currentTarget).closest('.js-order-short').data('order-id');
    this.confirmDialog(orderId, 'habilitar');
  }

  tplDialogConfirm(title) {
    return `<div class="js-confirm modal-confirm">
      <div class="modal-confirm__body">
        <h4>${title}</h4>
      </div>
      <div class="modal-confirm__footer">
        <button class="btn btn--secondary" type="button" data-response="not">No</button>
        <button class="btn btn--primary" type="button" data-response="yes">Sí</button>
      </div>
    </div>`;
  }

  confirmDialog(orderId, method) {
    let tpl = this.tplDialogConfirm(`¿Deseas ${method} el pedido?`);
    $.magnificPopup.open({
      closeOnBgClick: false,
      items: {
        src: tpl,
        type: 'inline'
      },
      callbacks: {
        open: () => {
          $('.js-confirm').on('click', 'button.btn', (e) => {
            let response = $(e.currentTarget).data('response');
            if (response == 'yes') {
              if (method == 'eliminar')
                this.sendRemove(orderId);
              else
                this.sendEnable(orderId);
            } else {
              $.magnificPopup.close();
            }
          })
        }
      }
    });
  }

  sendRemove(orderId) {
    console.log('eliminar');
    $.ajax({
      url: '',
      type: 'post',
      data: orderId,
    })
  }

  sendEnable(orderId) {
    console.log('hailitar')
  }
}

export default Maintenance;