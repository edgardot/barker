jQuery(document).ready(function($) {
	
	//Check URL for Register

	if (window.location.search == '?login_page=ok') {
		
		let bg_image_register_ok = '../images/cuenta/bg-registro-ok.jpg';
		let barker_bg_register = jQuery('.account__bg-image');
		barker_bg_register.addClass('barker_accoung_register_ok');

		jQuery('.js-btn-form-type').toggleClass('is-active');
		jQuery('#IniSecBar').click();

		
	}


	//DAY OF DELIVERY
	function getFormattedDate(today) {
		var week = new Array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
		var day = week[today.getDay()];
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!
		var yyyy = today.getFullYear();
		var hour = today.getHours();
		var minu = today.getMinutes();
		if(dd < 10) {
			dd = '0' + dd
		}
		if(mm < 10) {
			mm = '0' + mm
		}
		if(minu < 10) {
			minu = '0' + minu
		}
		return day + ' - ' + dd + '/' + mm + '/' + yyyy;
	}
	let barker_delivery = new Date();
	barker_delivery.setUTCDate(barker_delivery.getUTCDate() + (7 - barker_delivery.getUTCDay()) % 7 + 2);
	jQuery('#date_barker_delivery').append(getFormattedDate(barker_delivery));
	//Disabled qty for woo cart
	jQuery('.input-text.qty.text').attr({
		disabled: "disabled"
	});
	//Select2 for modal district
	jQuery("#barker-modal-link").click(function() {
		jQuery(".js-add-district-select").select2({
			placeholder: "Busca tu distrito",
			width: "100%"
		});
	});
	jQuery(".js-add-district-select").select2({
		placeholder: "Escoje tu horario",
		width: "100%"
	});
	jQuery('.hide-changue-schd').attr({
		disabled: 'disabled'
	});
	jQuery(".barker-open-schd").click(function() {
		let get_order_ref_openModal = jQuery(this).parents().eq(1).data('reference');
		let get_order_ref_field = jQuery('#order_changue_time');
		get_order_ref_field.val(get_order_ref_openModal);
		// alert(get_order_ref_openModal);
	});
	// Init woocommerce Cart
	// ======================
	//Init Price Box
	//--------------
	jQuery('#barkerPriceBox-pollo').removeClass('barker-suscription-item_priceBox__disable');
	jQuery('#barkerPriceBox-pollo').addClass('barker-suscription-item_priceBox__active');
	//BOX SELECTION
	//--------------
	jQuery('[name="f-flavors"]').click(function() {
		let barker_radio_value = this.value;
		let barker_description_box = '#barkerDescription-';
		let barker_price_box = '#barkerPriceBox-';
		let barker_description_dynamicID = barker_description_box + barker_radio_value;
		let barker_priceBox_dynamicID = barker_price_box + barker_radio_value;
		console.log(barker_radio_value);
		// alert(barker_priceBox_dynamicID);
		jQuery('.delivery-flavors__feedback').addClass('barker-suscription-item_description__disable');
		jQuery('.owl-stage-outer').addClass('barker-suscription-item_description__disable');
		jQuery('.filter-by-time').addClass('barker-suscription-item_priceBox__disable');
		//Description Box
		jQuery(barker_description_dynamicID).removeClass('barker-suscription-item_description__disable');
		jQuery(barker_description_dynamicID).addClass('barker-suscription-item_description__active');
		//Price Box
		jQuery(barker_priceBox_dynamicID).removeClass('barker-suscription-item_priceBox__disable');
		jQuery(barker_priceBox_dynamicID).addClass('barker-suscription-item_priceBox__active');
	});
	//BARKER CHECKOUT CUSTOM FIELDS
	//-----------------------------
	let billing_state_field = jQuery('#billing_state_field');
	let billing_city_field = jQuery('#billing_city_field');
	let barker_horario_reparto = jQuery('#barker_horario_reparto');
	let f_shipping_address_field = jQuery('#f_shipping_address_field');
	let barker_checkout_kind_house = jQuery('#barker_checkout_kind_house');
	jQuery(barker_horario_reparto).insertAfter(billing_state_field);
	jQuery(barker_checkout_kind_house).insertAfter(billing_city_field);
	jQuery(f_shipping_address_field).insertAfter(billing_city_field);
	//BARKER ORDER REVIEW
	//-------------------
	let payment = jQuery('#payment');
	let checkout_coupon = jQuery('.checkout_coupon');
	//RADIO ACTIVE
	//--------------
	let barker_checkout_radio_house_container = jQuery('.barker-checkout-radio-house-container > input[type="radio"]');
	jQuery(barker_checkout_radio_house_container).click(function() {
		let barker_radio_parent = jQuery(this).parent();
		let barker_radio_container = jQuery('.barker-checkout-radio-house-container');
		jQuery(barker_radio_container).removeClass('barker-checkout-radio-house-container-active')
		jQuery(barker_radio_parent).addClass('barker-checkout-radio-house-container-active');
		console.log(this.value);
	});
	//Value Checkout
	//--------------
	jQuery(function() {
		let f_first_name = jQuery('#f-first-name');
		let f_last_name = jQuery('#f-last-name');
		let f_telephone = jQuery('#f-telephone');
		let f_address_1 = jQuery('#f-address-1');
		let f_address_2 = jQuery('#f-address-2');
		let f_address_3 = jQuery('#f-address-3');
		let f_shipping_address_mask = jQuery('#f-shipping-address-mask');
		let woo_coupon = jQuery('#wooCouponCode');
		jQuery(woo_coupon).addClass('hola');
		let barker_billing_first_name = jQuery('#billing_first_name');
		let barker_billing_last_name = jQuery('#billing_last_name');
		let barker_billing_phone = jQuery('#billing_phone');
		let barker_billing_address_1 = jQuery('#billing_address_1');
		let barker_billing_address_2 = jQuery('#billing_address_2');
		let barker_billing_city = jQuery('#billing_city');
		let barker_shipping_address_mask = jQuery('#f_shipping_address');
		let barker_coupon = jQuery('#barker_coupon');
		let barker_coupon_btn = jQuery('#barker_coupon_btn');
		let barker_checkout_submit = jQuery('#barker_checkout_submit');
		jQuery(barker_coupon).change(function() {
			/* Act on the event */
			jQuery('#order_review').find('#wooCouponCode > .form-row > .input-text').val(barker_coupon.val());
			jQuery('.order-detail').find(barker_coupon_btn).removeClass('is-disabled');
			jQuery('.order-detail').find(barker_coupon_btn).addClass('is-active');
		});
		jQuery(barker_coupon_btn).click(function() {
			let woo_button_coupon = jQuery('#wooCouponCode > .form-row > .button');
			jQuery(woo_button_coupon).trigger('click');
		});
		jQuery(barker_checkout_submit).click(function() {
			let woo_button_submit = jQuery('#place_order');
			let woo_button_submit_2 = jQuery('#place_order_2');
			jQuery(woo_button_submit).trigger('click');
			jQuery(woo_button_submit_2).trigger('click');
		});
		// GET DEFAULT VALUE BILLING FORM
		// ===============================
		let value_billing_first_name = jQuery(barker_billing_first_name).val();
		let value_billing_last_name = jQuery(barker_billing_last_name).val();
		let value_billing_telephone = jQuery(barker_billing_phone).val();
		let value_address_1 = jQuery(barker_billing_address_1).val();
		let value_address_2 = jQuery(barker_billing_address_2).val();
		let value_address_3 = jQuery(barker_billing_city).val();
		jQuery(f_first_name).val(value_billing_first_name);
		jQuery(f_last_name).val(value_billing_last_name);
		jQuery(f_telephone).val(value_billing_telephone);
		jQuery(f_address_1).val(value_address_1);
		jQuery(f_address_2).val(value_address_2);
		jQuery(f_address_3).val(value_address_3);

		function onChange1() {
			barker_billing_first_name.val(f_first_name.val());
		};

		function onChange2() {
			barker_billing_last_name.val(f_last_name.val());
		};

		function onChange3() {
			barker_billing_phone.val(f_telephone.val());
		};

		function onChange4() {
			barker_billing_address_1.val(f_address_1.val());
		};

		function onChange5() {
			barker_billing_address_2.val(f_address_2.val());
		};

		function onChange6() {
			barker_billing_city.val(f_address_3.val());
		};

		function onChange7() {
			barker_shipping_address_mask.val(f_shipping_address_mask.val());
		};
		jQuery(f_first_name).change(onChange1).keyup(onChange1);
		jQuery(f_last_name).change(onChange2).keyup(onChange2);
		jQuery(f_telephone).change(onChange3).keyup(onChange3);
		jQuery(f_address_1).change(onChange4).keyup(onChange4);
		jQuery(f_address_2).change(onChange5).keyup(onChange5);
		jQuery(f_address_3).change(onChange6).keyup(onChange6);
		jQuery(f_shipping_address_mask).change(onChange7).keyup(onChange7);
		let woo_producto_simple = jQuery('.cart_item > .product-name').html();
		let woo_precio_simple = jQuery('tbody .cart_item > .product-total > .woocommerce-Price-amount').text();
		let woo_precio_total_simple = jQuery('.order-total > td > strong > .woocommerce-Price-amount').text();
		let woo_discount_simple = jQuery('.cart-discount > th').text;
		let woo_terms = jQuery('.woocommerce-form__label-for-checkbox');
		let barker_producto = jQuery('.order-item__text-desc');
		let barker_producto_name = jQuery('.order-item__text-desc > .product-name');
		let barker_precio = jQuery('.order-item__text-amount');
		let barker_precioTotal = jQuery('.barker-precioTotal');
		let barker_terms = jQuery('#barker_terms');
		let barker_checkbox_terms = jQuery('.checkbox-terms > .icon');
		let barker_title_discount = jQuery('.barker-title-discount');
		let barker_price_discount = jQuery('.barker-price-discount');
		// APLY CHANGUE AFTER LOAD AJAX ORDER REVIEW WOOCOMMERCE CHECKOUT
		// --------------------------------------------------------------
		jQuery('body').on('updated_checkout', function() {
			// jQuery('.order-detail').find(barker_coupon_btn).removeClass('is-disabled');
			jQuery('#order_review').find('.cart_item > .product-name').css('border', 'red');
			let woo_producto = jQuery('.cart_item > .product-name');
			let woo_precio = jQuery('tbody .cart_item > .product-total > .woocommerce-Price-amount');
			let woo_precio_total = jQuery('.order-total > td > strong > .woocommerce-Price-amount');
			let woo_discount = jQuery('.cart-discount > th');
			let woo_discount_price = jQuery('.cart-discount > td > .woocommerce-Price-amount');
			jQuery(woo_producto).clone().prependTo(barker_producto);
			let barker_producto_name = jQuery('.order-item__text-desc > .product-name');
			jQuery(woo_precio).clone().prependTo(barker_precio);
			jQuery(woo_precio_total).clone().prependTo(barker_precioTotal);
			jQuery(woo_discount).clone().prependTo(barker_title_discount);
			jQuery(woo_discount_price).clone().prependTo(barker_price_discount);
			// let woo_remove = jQuery('.product-remove');
			// jQuery(barker_producto_name).append(woo_remove);
			jQuery(barker_checkbox_terms).click(function() {
				jQuery('#order_review').find('.woocommerce-form__label-for-checkbox').trigger('click');
			});
			// jQuery(barker_producto_name).append('<span class="link-action js-remove-order">Eliminar</span>');
		});
	});
	let agregar_mascota = jQuery('.js-add-dog');
	let barker_editar = jQuery('.js-edit-order');
	let barker_horario_morning = jQuery('#f-moorning-1');
	let barker_horario_afternoon = jQuery('#f-afternoon-1');
	let barker_casa_house = jQuery('#f-house');
	let barker_casa_fifth = jQuery('#f-fifth');
	let barker_casa_building = jQuery('#f-building');
	let barker_casa_condominium = jQuery('#f-condominium');
	jQuery(agregar_mascota).click(function() {
		window.location.href = document.location.origin + '/suscripcion';
	});
	jQuery(barker_editar).click(function() {
		window.location.href = document.location.origin + '/suscripcion';
	});
	jQuery(barker_horario_morning).click(function() {
		let woo_horario_reparto = jQuery('#barker_horario_reparto > .barker-checkout-radio-container:nth-child(1) > input');
		jQuery(woo_horario_reparto).trigger('click');
	})
	jQuery(barker_horario_afternoon).click(function() {
		let woo_horario_reparto = jQuery('#barker_horario_reparto > .barker-checkout-radio-container:nth-child(2) > input');
		jQuery(woo_horario_reparto).trigger('click');
	})
	jQuery(barker_casa_house).click(function() {
		let woo_casa_tipo = jQuery('#barker_checkout_kind_house > .barker-checkout-radio-house-container:nth-child(1) > input');
		jQuery(woo_casa_tipo).trigger('click');
	})
	jQuery(barker_casa_fifth).click(function() {
		let woo_casa_tipo = jQuery('#barker_checkout_kind_house > .barker-checkout-radio-house-container:nth-child(2) > input');
		jQuery(woo_casa_tipo).trigger('click');
	})
	jQuery(barker_casa_building).click(function() {
		let woo_casa_tipo = jQuery('#barker_checkout_kind_house > .barker-checkout-radio-house-container:nth-child(3) > input');
		jQuery(woo_casa_tipo).trigger('click');
	})
	jQuery(barker_casa_condominium).click(function() {
			let woo_casa_tipo = jQuery('#barker_checkout_kind_house > .barker-checkout-radio-house-container:nth-child(4) > input');
			jQuery(woo_casa_tipo).trigger('click');
		})
		//PROFLE FEMALE STATUS
	let box_female_status = jQuery('.field-wrapper.filter-female');
	let radio_female = jQuery('#f-female');
	if(radio_female.is(':checked')) {
		box_female_status.css('display', 'block');
	}
	// BARKER MEMBERSHIPS PEDIDOS
	//BOX TEMPLATES
	//Active Box Template
	// CONDITIONALS FOR MEMBERSHIPS
	let get_url_project = '';
	let trash_icon = '<svg><use xlink:href="'+baseAssets+'images/sprite.svg#trash"></use></svg>';
	let renew_icon = '<svg><use xlink:href="'+baseAssets+'images/sprite.svg#enable"></use></svg>';
	let arrow_icon = '<svg><use xlink:href="'+baseAssets+'images/sprite.svg#arrow"></use></svg>';
	let membership_box_thead_start = '<p class="barker_thead_status">Creado el</p>';
	let membership_box_thead_finish = '<p class="barker_thead_status">Válido</p>';
	let membership_box_thead_status = '<p class="barker_thead_status">Estado</p>';
	let membership_start_date = jQuery('.membership-start-date');
	let membership_end_date = jQuery('.membership-end-date');
	let membership_status = jQuery('.membership-status');
	membership_start_date.prepend(membership_box_thead_start);
	membership_end_date.prepend(membership_box_thead_finish);
	membership_status.prepend(membership_box_thead_status);
	let membership_active = jQuery('.membership > .membership-actions > .cancel');
	let membership_renew = jQuery('.membership > .membership-actions > .renew');
	let membership_view = jQuery('.membership > .membership-actions > .view');
	let membership_expired = jQuery('.button.renew');
	jQuery(membership_view).removeAttr('href');
	membership_active.append(trash_icon);
	membership_renew.append(renew_icon);
	membership_view.append(arrow_icon);
	let barker_row_memberships_active = jQuery('#barker_row_memberships_active');
	let barker_row_memberships_expired = jQuery('#barker_row_memberships_expired');
	// IDENTIFICACION DE BLOQUES 
	//ORDER URL
	let barker_wcm_active = jQuery('.wcm-active');
	let barker_order_view_url_base = 'http://localhost/beta.tupino.com/barker.pe/pedidos-cuenta/view-order/'
	let barker_order_view_url_id = barker_wcm_active.data("reference");
	barker_order_view_url_full = barker_order_view_url_base + barker_order_view_url_id;
	// jQuery(membership_view).attr('href', barker_order_view_url_full);
	// jQuery('.membership-plan > a').attr('href', barker_order_view_url_full);
	jQuery('.membership-plan > a').removeAttr('href');
	//Order View
	jQuery('.woocommerce-view-order .woocommerce-table__product-name > a').removeAttr('href');
	//Order sidebar
	let card_expired = jQuery('.wcm-expired');
	let card_active = jQuery('.wcm-active');
	jQuery('.bkp_suscritos').click(function() {
		card_expired.removeClass('subscription_on');
		card_expired.addClass('subscription_off');
		card_active.removeClass('subscription_off');
		card_active.addClass('subscription_on')
	})
	jQuery('.bkp_finalizados').click(function() {
			card_active.removeClass('subscription_on');
			card_active.addClass('subscription_off');
			card_expired.removeClass('subscription_off');
			card_expired.addClass('subscription_on')
		})
		//subbox order
	let box_membership = jQuery('.membership')
	let box_membership_plan = jQuery('.membership > .membership-plan')
	let get_arrow = jQuery('.woocommerce-members_area .shop_table tbody .membership .membership-actions a.view svg');
	jQuery(get_arrow).click(function(event) {
		// get_arrow.css('transform', 'rotate(90deg)');
		let box_membership_data = jQuery(this).closest('.membership').data('reference');
		// alert(box_membership_data)		;
		let barker_order_sub_box = jQuery(this).closest('.membership');
		let barker_order_sub_box_find = barker_order_sub_box.find('.barker_order_sub_box');
		let barker_order_sub_box_data = barker_order_sub_box_find.data('reference')
			/* Act on the event */
		if(box_membership_data == barker_order_sub_box_data) {
			jQuery(box_membership).not(barker_order_sub_box).toggleClass('membership-hide');
			jQuery(barker_order_sub_box).parent('tbody').toggleClass('tbody-start');
			jQuery(barker_order_sub_box_find).toggleClass('sub-box-view');
			jQuery('.barker_pedidos_sidebar_button').toggleClass('blocked_nav');
			jQuery(get_arrow).toggleClass('move_arrow_up');
		}
	});
	// SaveOrder Slider
	let barkerq_init_sex = jQuery('input[name="f-dog-sex"]');
	let barkerq_init_female_status = jQuery('input[name="f_status_female"]');
	//Age
	let barkerq_age_item = jQuery('input[name="f-dog-age"]');
	let barkerq_age_item_val = jQuery('input[name="f-dog-age"]').val();
	let barkerq_activity_item = jQuery('input[name="f-level-activity"]');
	let barkerq_age_cachorro = 'cachorro';
	let barkerq_age_adulto = 'adulto';
	let barkerq_age_mayor = 'adulto mayor';
	let barkerq_age_pregnant = 'esta embarazada';
	let barkerq_age_puppies = 'tiene cachorros';
	let barkerq_age_puppy = jQuery('#f-puppy');
	let barkerq_age_adult = jQuery('#f-adult');
	let barkerq_age_older = jQuery('#f-older-adult');
	// Base Size
	let barkerq_cat_id = jQuery('#dog-kg');
	let barkerq_cat_one = 'Categoría 1 (1 - 5kg)';
	let barkerq_cat_two = 'Categoría 2 (5 - 10 kg)';
	let barkerq_cat_three = 'Categoría 3 (10 - 22kg)';
	let barkerq_cat_four = 'Categoría 4 (22 - 45kg)';
	let bq_factor_one = 1;
	let bq_factor_two = 1.15;
	let bq_factor_three = 1.30;
	// Barker Activity
	let barkerq_activity = jQuery('input[name="f-level-activity"]');
	let barkerq_activity_bajo = 'bajo';
	let barkerq_activity_medio = 'medio';
	let barkerq_activity_alto = 'alto';
	// START
	// if (barkerq_age_item.prop("checked", true)) {
	// 	// alert(barkerq_age_item.val());
	// 	console.log(barkerq_age_item.val());
	// }
	//Event
	let barkerq_rule = jQuery('.noUi-handle-lower');
	// Filter Pregnant
	jQuery(barkerq_init_female_status).click(function() {
		let check_female_status = jQuery(this).val();
		console.log(check_female_status);
		if(check_female_status == barkerq_age_pregnant || check_female_status == barkerq_age_puppies) {
			console.log('Se obtuvo el valor');
			//Open Filter 2
			let filter_age = jQuery('.filter-age');
			let filter_size = jQuery('.filter-size');
			let filter_activity = jQuery('.filter-activity');
			filter_size.removeClass('barker-section-off');
			filter_size.addClass('barker-section-on');
			filter_age.removeClass('barker-section-on');
			filter_age.addClass('barker-section-off');
		}
	});
	// Filter 1 -> Cachorro




	jQuery(barkerq_age_item).click(function() {
		/* Act on the event */
		// alert(jQuery(this).val());
		if(jQuery(this).val() == barkerq_age_cachorro) {
			console.log('Hey el Age es Cachorro');
			//Open Filter 2
			let filter_age = jQuery('.filter-age');
			let filter_size = jQuery('.filter-size');
			let filter_activity = jQuery('.filter-activity');
			filter_size.removeClass('barker-section-off');
			filter_size.addClass('barker-section-on');
			filter_age.removeClass('barker-section-on');
			filter_age.addClass('barker-section-off');
		}
		if(jQuery(this).val() == barkerq_age_adulto) {
			console.log('Hey el Age es Adulto');
			let dogkg_cat_one = 50;
			let dogkg_cat_two = 150;
			let dogkg_cat_three = 250;
			let dogkg_cat_four = 550;
			//Open Filter 2
			let filter_age = jQuery('.filter-age');
			let filter_size = jQuery('.filter-size');
			let filter_activity = jQuery('.filter-activity');
			filter_size.removeClass('barker-section-off');
			filter_size.addClass('barker-section-on');
			filter_age.removeClass('barker-section-on');
			filter_age.addClass('barker-section-off');
		}
		if(jQuery(this).val() == barkerq_age_mayor) {
			console.log('Hey el Age es Viejito');
			let dogkg_cat_one = 50;
			let dogkg_cat_two = 150;
			let dogkg_cat_three = 250;
			let dogkg_cat_four = 550;
			// //Open Filter 2
			let filter_age = jQuery('.filter-age');
			let filter_size = jQuery('.filter-size');
			let filter_activity = jQuery('.filter-activity');
			filter_size.removeClass('barker-section-off');
			filter_size.addClass('barker-section-on');
			filter_age.removeClass('barker-section-on');
			filter_age.addClass('barker-section-off');
		}
	});
	// let barker_action_button_prev = jQuery('.actions > ul > li:nth-child(1) > a');
	// 	let barker_action_button_next = jQuery('.actions > ul > li:nth-child(2)');
	// 	let barker_action_button_finish = jQuery('.actions > ul > li:nth-child(3)');
	// 	let barker_action_button_finish_a = jQuery('.actions > ul > li:nth-child(3) > a');
	jQuery('.actions > ul > li:nth-child(2)').click(function(event) {
		/* Act on the event */
		jQuery(this).addClass('barker-section-off');
	});
	// let dog_kg_k = jQuery('#dog-kg');
	// jQuery(dog_kg_k).bind('input', function(){
	//   console.log('this actually works');
	// });
	// Filter 2
	jQuery(barkerq_rule).bind('mouseup touchend', function() {
		//Get Selected Age 
		// let barker_get_selected_age = jQuery(barkerq_age_item).attr
		
		//Active activity box mobile
		jQuery('.filter-activity').removeClass('barker-section-off');
		// jQuery('.barker-tap-step').addClass('barker-tap-offset');
		// jQuery('.barker-tap-item').removeClass('barker-tap-opacity');


		//START PREÑANADA
		if(jQuery('#f-pregnat').is(':checked') || jQuery('#f-has-pups').is(':checked')) {
			console.log('Preñada o crías');
			let barkerq_value_category = barkerq_cat_id.val();
			if(barkerq_value_category == barkerq_cat_one) {
				let barkerq_value_category = barkerq_cat_id.val();
				barkerq_age_item.data('barkerq', '150');
				console.log(barkerq_age_item.data('barkerq'));
				barker_result_gr_by_day = barkerq_age_item.data('barkerq');
				jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
			}
			if(barkerq_value_category == barkerq_cat_two) {
				let barkerq_value_category = barkerq_cat_id.val();
				barkerq_age_item.data('barkerq', '300');
				console.log(barkerq_age_item.data('barkerq'));
				barker_result_gr_by_day = barkerq_age_item.data('barkerq');
				jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
			}
			if(barkerq_value_category == barkerq_cat_three) {
				let barkerq_value_category = barkerq_cat_id.val();
				barkerq_age_item.data('barkerq', '600');
				console.log(barkerq_age_item.data('barkerq'));
				barker_result_gr_by_day = barkerq_age_item.data('barkerq');
				jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
			}
			if(barkerq_value_category == barkerq_cat_four) {
				let barkerq_value_category = barkerq_cat_id.val();
				barkerq_age_item.data('barkerq', '1200');
				console.log(barkerq_age_item.data('barkerq'));
				barker_result_gr_by_day = barkerq_age_item.data('barkerq');
				jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
			}
		}
		//ENDS PREÑANADA
		//Check Age -> Cachorro
		// if( jQuery('#f-puppy').prop('checked', true) ) { 
		if(jQuery('#f-puppy').is(':checked')) {
			console.log('otro cachorro');
			let barkerq_value_category = barkerq_cat_id.val();
			if(barkerq_value_category == barkerq_cat_one) {
				let barkerq_value_category = barkerq_cat_id.val();
				barkerq_age_item.data('barkerq', '50');
				console.log(barkerq_age_item.data('barkerq'));
			}
			if(barkerq_value_category == barkerq_cat_two) {
				let barkerq_value_category = barkerq_cat_id.val();
				barkerq_age_item.data('barkerq', '150');
				console.log(barkerq_age_item.data('barkerq'));
			}
			if(barkerq_value_category == barkerq_cat_three) {
				let barkerq_value_category = barkerq_cat_id.val();
				barkerq_age_item.data('barkerq', '250');
				console.log(barkerq_age_item.data('barkerq'));
			}
			if(barkerq_value_category == barkerq_cat_four) {
				let barkerq_value_category = barkerq_cat_id.val();
				barkerq_age_item.data('barkerq', '550');
				console.log(barkerq_age_item.data('barkerq'));
			}
		}
		//Check Age -> Adulto
		if(jQuery('#f-adult').is(':checked')) {
			console.log('otro adulto');
			let barkerq_value_category = barkerq_cat_id.val();
			if(barkerq_value_category == barkerq_cat_one) {
				barkerq_age_item.data('barkerq', '100');
				console.log(barkerq_age_item.data('barkerq'));
			}
			if(barkerq_value_category == barkerq_cat_two) {
				barkerq_age_item.data('barkerq', '200');
				console.log(barkerq_age_item.data('barkerq'));
			}
			if(barkerq_value_category == barkerq_cat_three) {
				barkerq_age_item.data('barkerq', '400');
				console.log(barkerq_age_item.data('barkerq'));
			}
			if(barkerq_value_category == barkerq_cat_four) {
				barkerq_age_item.data('barkerq', '800');
				console.log(barkerq_age_item.data('barkerq'));
			}
		}
		//Check Age -> Mayor
		if(jQuery('#f-older-adult').is(':checked')) {
			console.log('otro mayor');
			let barkerq_value_category = barkerq_cat_id.val();
			if(barkerq_value_category == barkerq_cat_one) {
				barkerq_age_item.data('barkerq', '100');
				console.log(barkerq_age_item.data('barkerq'));
			}
			if(barkerq_value_category == barkerq_cat_two) {
				barkerq_age_item.data('barkerq', '150');
				console.log(barkerq_age_item.data('barkerq'));
			}
			if(barkerq_value_category == barkerq_cat_three) {
				barkerq_age_item.data('barkerq', '300');
				console.log(barkerq_age_item.data('barkerq'));
			}
			if(barkerq_value_category == barkerq_cat_four) {
				barkerq_age_item.data('barkerq', '600');
				console.log(barkerq_age_item.data('barkerq'));
			}
		}
	});
	//	mobile event
	// =============
	// Filter 3
	// =============
	jQuery(barkerq_rule).one('touchend, ', function() {
		let filter_activity = jQuery('.filter-activity');
		filter_activity.removeClass('barker-section-off');
		filter_activity.addClass('barker-section-on');
	});
	//Able Filter 3
	jQuery(barkerq_rule).one('click touchend, ', function() {
		let filter_activity = jQuery('.filter-activity');
		filter_activity.removeClass('barker-section-off');
		filter_activity.addClass('barker-section-on');
	});
	// Select Activity
	jQuery(barkerq_activity_item).click(function() {
		/* Act on the event */
		// alert(jQuery(this).val());
		if(jQuery(this).val() == barkerq_activity_bajo) {
			console.log('Actividad Baja');
			let filter_size = jQuery('.filter-size');
			filter_size.removeClass('barker-section-on');
			filter_size.addClass('barker-section-off');
			//START PREÑANADA
			if(jQuery('#f-pregnat').is(':checked') || jQuery('#f-has-pups').is(':checked')) {
				console.log('Preñada o crías');
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '150');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '300');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '600');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '1200');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//ENDS PREÑANADA			
			//Start New Values 
			if(jQuery('#f-puppy').is(':checked')) {
				console.log('otro cachorro Bajo');
				// Consultar
				// End consultar
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '50');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
					jQuery('#barkerResult_kg').addClass('Hola');
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '150');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '250');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '550');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//
			if(jQuery('#f-adult').is(':checked')) {
				console.log('otro adulto bajo');
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '100');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '200');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '400');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '800');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//
			if(jQuery('#f-older-adult').is(':checked')) {
				console.log('otro mayor');
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '100');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '150');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '300');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '600');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//End New Values			
		}
		if(jQuery(this).val() == barkerq_activity_medio) {
			console.log('Actividad Media');
			let filter_size = jQuery('.filter-size');
			filter_size.removeClass('barker-section-on');
			filter_size.addClass('barker-section-off');
			//START PREÑANADA
			if(jQuery('#f-pregnat').is(':checked') || jQuery('#f-has-pups').is(':checked')) {
				console.log('Preñada o crías');
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '200');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '350');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '700');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '1350');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//ENDS PREÑANADA
			//Start New Values 
			if(jQuery('#f-puppy').is(':checked')) {
				console.log('otro cachorro Medio');

				// Consultar
				// End consultar
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '50');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '200');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '300');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '650');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//
			if(jQuery('#f-adult').is(':checked')) {
				console.log('otro adulto medio');
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '100');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '250');
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					console.log(barker_result_gr_by_day);
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '450');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '900');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//
			if(jQuery('#f-older-adult').is(':checked')) {
				console.log('otro mayor');
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '100');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '200');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '400');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '700');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//End New Values
		}
		if(jQuery(this).val() == barkerq_activity_alto) {
			console.log('Actividad Alta');
			let filter_size = jQuery('.filter-size');
			filter_size.removeClass('barker-section-on');
			filter_size.addClass('barker-section-off');
			//START PREÑANADA
			if(jQuery('#f-pregnat').is(':checked') || jQuery('#f-has-pups').is(':checked')) {
				console.log('Preñada o crías');
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '200');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '400');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '800');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '1550');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//ENDS PREÑANADA
			//Start New Values 
			if(jQuery('#f-puppy').is(':checked')) {
				console.log('otro cachorro Alto');
				
				// Consultar
				
				// End consultar
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '100');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '200');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '350');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '700');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//
			if(jQuery('#f-adult').is(':checked')) {
				console.log('otro adulto bajo');
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '150');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '250');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '500');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '1050');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//
			if(jQuery('#f-older-adult').is(':checked')) {
				console.log('otro mayor');
				let barkerq_value_category = barkerq_cat_id.val();
				if(barkerq_value_category == barkerq_cat_one) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '150');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_two) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '200');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_three) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '400');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
				if(barkerq_value_category == barkerq_cat_four) {
					let barkerq_value_category = barkerq_cat_id.val();
					barkerq_age_item.data('barkerq', '800');
					console.log(barkerq_age_item.data('barkerq'));
					barker_result_gr_by_day = barkerq_age_item.data('barkerq');
					jQuery('#barkerResult_kg').val(barker_result_gr_by_day);
				}
			}
			//End New Values	
		}
		//open next
		jQuery('.js-kg-stimated').html(barkerq_age_item.data('barkerq'));
		jQuery('.actions > ul > li:nth-child(2)').removeClass('barker-section-off');
		
	});
	jQuery('.actions > ul > li:nth-child(3)').addClass('barker-section-off')
		// Filter 4
	let barker_get_flavor_item = jQuery('input[name="f-flavors"]');
	let barker_get_time_subscription = jQuery('input[name="f-suscription-time"]');
	// Set Flavor
	let barker_flavor_pollo = 'pollo';
	let barker_flavor_pavo = 'pavo';
	let barker_flavor_carne = 'carne-de-res';
	let barker_flavor_mixto = 'mixto';
	let flavor_value = jQuery(this).val();
	console.log(flavor_value);

	jQuery(barker_get_flavor_item).click(function() {
		/* Act on the event */
		//REMOVER SECIONT OFF FOR SUSCRIPTIONS
		jQuery('.barkerSubs-box').removeClass('barker-section-off');
	});


	/* Act on the event */
	jQuery(barker_get_time_subscription).click(function() {
		jQuery(barker_get_flavor_item).parent('.checkbox').addClass('barker-section-off');

		let price_value = jQuery(this).data('bsprice');
		let time_value = jQuery(this).val();
		//Save Data 
		let barkerResult_kg = parseInt(jQuery('#barkerResult_kg').val());
		//convert to quantity
		// Set time keys
		let barker_time_subs_week = '1 semana';
		let barker_time_subs_month = '1 mes';
		let barker_time_subs_three_months = '3 meses';
		let barker_time_subs_six_months = '6 meses';
		let barker_time_subs_year = 'anual';
		let set_week = 7;
		let set_month = 30;
		let set_month_three = 90;
		let set_month_six = 180;
		let set_year = 365;
		if(time_value == barker_time_subs_week) {
			let get_gr = barkerResult_kg * set_week;
			let get_kg = Math.round(get_gr / 1000);
			barker_get_time_subscription.data('quantity', get_kg);
			console.log('La cantidad es ' + get_kg);
		}
		if(time_value == barker_time_subs_month) {
			let get_gr = barkerResult_kg * set_month;
			let get_kg = Math.round(get_gr / 1000);
			barker_get_time_subscription.data('quantity', get_kg);
			console.log('La cantidad es ' + get_kg);
		}
		if(time_value == barker_time_subs_three_months) {
			let get_gr = barkerResult_kg * set_month_three;
			let get_kg = Math.round(get_gr / 1000);
			barker_get_time_subscription.data('quantity', get_kg);
			console.log('La cantidad es ' + get_kg);
		}
		if(time_value == barker_time_subs_six_months) {
			let get_gr = barkerResult_kg * set_month_six;
			let get_kg = Math.round(get_gr / 1000);
			barker_get_time_subscription.data('quantity', get_kg);
			console.log('La cantidad es ' + get_kg);
		}
		if(time_value == barker_time_subs_year) {
			let get_gr = barkerResult_kg * set_year;
			let get_kg = Math.round(get_gr / 1000);
			barker_get_time_subscription.data('quantity', get_kg);
			console.log('La cantidad es ' + get_kg);
		}
		// SET NEW VALUES FOR BUTONS PREV AND NEXT
		let barker_product_id = jQuery('[name="f-flavors"]:checked').data('id');
		let barker_suscriptionTime_attr_id = jQuery(this).data('variation');
		let barker_suscriptionTime_qty = jQuery(this).data('quantity');
		// alert('el id es' + barker_product_id + 'y el variation id es ' + barker_suscriptionTime_attr_id);
		// get last part url
		// build new url
		barker_url_suscription = window.location.pathname;
		// let barker_baseUrl = document.location.origin;
		let barker_baseUrl = window.location.protocol + '//' + barker_url_suscription.replace('/suscripcion/', '') + '/';
		let barker_addCart = '?add-to-cart=';
		let barker_variationID = '&variation_id=';
		let barker_quantity = '&quantity=';
		barker_fullCheckoutURL = barker_addCart + barker_product_id + barker_variationID + barker_suscriptionTime_attr_id + barker_quantity + barker_suscriptionTime_qty;
		// alert(barker_fullCheckoutURL);
		let barker_action_button_prev = jQuery('.actions > ul > li:nth-child(1) > a');
		let barker_action_button_next = jQuery('.actions > ul > li:nth-child(2)');
		let barker_action_button_finish = jQuery('.actions > ul > li:nth-child(3)');
		let barker_action_button_finish_a = jQuery('.actions > ul > li:nth-child(3) > a');
		let barker_submit = jQuery('a[href*="#next"]');
		// jQuery(barker_submit).attr('href', barker_fullCheckoutURL);
		let barker_reference_button = jQuery('#delivery-wizard > .actions > ul > li:nth-child(1)');
		let barker_remove_button = jQuery('#delivery-wizard > .actions > ul > li:nth-child(2)');
		let barker_button_checkout = '<li aria-hidden="false" aria-disabled="false"><a id="barker_goToCheckout_url" href="#next" role="menuitem">Continuar</a></li>';
		console.log(barker_fullCheckoutURL);
		jQuery('.actions > ul > li:nth-child(3)').removeClass('barker-section-off')
			//SET NEW VALUE FOR BUTTONS
		barker_submit.unbind('click');
		barker_action_button_prev.text('Reiniciar');
		barker_action_button_prev.click(function(event) {
			/* Act on the event */
			window.location.href = window.location.pathname;
		});
		barker_action_button_next.css({
			'display': 'none'
		});
		barker_action_button_finish.css({
			'display': 'block'
		});
		let barker_box_checkoutURL = jQuery('#barker_box_checkoutURL');
		let barker_box_checkoutURL_val = barker_box_checkoutURL.val();
		console.log(barker_box_checkoutURL_val);
		let barker_box_checkoutURL_new_val = barker_box_checkoutURL_val + '/' + barker_fullCheckoutURL
		console.log(barker_box_checkoutURL_new_val);
		barker_box_checkoutURL.val(barker_box_checkoutURL_new_val);
		//Go to checkout
		barker_action_button_finish_a.click(function(event) {
			/* Act on the event */
			window.location.href = barker_box_checkoutURL_new_val;
		});
	});
});