! function e(t, a, n) {
    function s(r, o) {
        if (!a[r]) {
            if (!t[r]) {
                var d = "function" == typeof require && require;
                if (!o && d) return d(r, !0);
                if (i) return i(r, !0);
                var l = new Error("Cannot find module '" + r + "'");
                throw l.code = "MODULE_NOT_FOUND", l
            }
            var u = a[r] = {
                exports: {}
            };
            t[r][0].call(u.exports, function(e) {
                var a = t[r][1][e];
                return s(a || e)
            }, u, u.exports, e, t, a, n)
        }
        return a[r].exports
    }
    for (var i = "function" == typeof require && require, r = 0; r < n.length; r++) s(n[r]);
    return s
}({
    1: [function(e, t, a) {
        (function(a) {
            var n = e;
            (function(e, t, a, s, i) {
                ! function(a) {
                    "use strict";
                    if ("function" == typeof s && s.amd) s(["jquery", "moment"], a);
                    else if ("object" == typeof t) e.exports = a(n("jquery"), n("moment"));
                    else {
                        if ("undefined" == typeof jQuery) throw "bootstrap-datetimepicker requires jQuery to be loaded first";
                        if ("undefined" == typeof moment) throw "bootstrap-datetimepicker requires Moment.js to be loaded first";
                        a(jQuery, moment)
                    }
                }(function(e, t) {
                    "use strict";
                    if (!t) throw new Error("bootstrap-datetimepicker requires Moment.js to be loaded first");
                    var a = function(a, n) {
                        var s, i, r, o, d, l, u, _ = {},
                            m = !0,
                            c = !1,
                            h = !1,
                            p = 0,
                            f = [{
                                clsName: "days",
                                navFnc: "M",
                                navStep: 1
                            }, {
                                clsName: "months",
                                navFnc: "y",
                                navStep: 1
                            }, {
                                clsName: "years",
                                navFnc: "y",
                                navStep: 10
                            }, {
                                clsName: "decades",
                                navFnc: "y",
                                navStep: 100
                            }],
                            M = ["days", "months", "years", "decades"],
                            y = ["top", "bottom", "auto"],
                            g = ["left", "right", "auto"],
                            L = ["default", "top", "bottom"],
                            Y = {
                                up: 38,
                                38: "up",
                                down: 40,
                                40: "down",
                                left: 37,
                                37: "left",
                                right: 39,
                                39: "right",
                                tab: 9,
                                9: "tab",
                                escape: 27,
                                27: "escape",
                                enter: 13,
                                13: "enter",
                                pageUp: 33,
                                33: "pageUp",
                                pageDown: 34,
                                34: "pageDown",
                                shift: 16,
                                16: "shift",
                                control: 17,
                                17: "control",
                                space: 32,
                                32: "space",
                                t: 84,
                                84: "t",
                                delete: 46,
                                46: "delete"
                            },
                            v = {},
                            w = function() {
                                return void 0 !== t.tz && void 0 !== n.timeZone && null !== n.timeZone && "" !== n.timeZone
                            },
                            k = function(e) {
                                var a;
                                return a = void 0 === e || null === e ? t() : t.isDate(e) || t.isMoment(e) ? t(e) : w() ? t.tz(e, l, n.useStrict, n.timeZone) : t(e, l, n.useStrict), w() && a.tz(n.timeZone), a
                            },
                            D = function(e) {
                                if ("string" != typeof e || e.length > 1) throw new TypeError("isEnabled expects a single character string parameter");
                                switch (e) {
                                    case "y":
                                        return -1 !== d.indexOf("Y");
                                    case "M":
                                        return -1 !== d.indexOf("M");
                                    case "d":
                                        return -1 !== d.toLowerCase().indexOf("d");
                                    case "h":
                                    case "H":
                                        return -1 !== d.toLowerCase().indexOf("h");
                                    case "m":
                                        return -1 !== d.indexOf("m");
                                    case "s":
                                        return -1 !== d.indexOf("s");
                                    default:
                                        return !1
                                }
                            },
                            b = function() {
                                return D("h") || D("m") || D("s")
                            },
                            T = function() {
                                return D("y") || D("M") || D("d")
                            },
                            S = function() {
                                var t = e("<div>").addClass("timepicker-hours").append(e("<table>").addClass("table-condensed")),
                                    a = e("<div>").addClass("timepicker-minutes").append(e("<table>").addClass("table-condensed")),
                                    s = e("<div>").addClass("timepicker-seconds").append(e("<table>").addClass("table-condensed")),
                                    i = [function() {
                                        var t = e("<tr>"),
                                            a = e("<tr>"),
                                            s = e("<tr>");
                                        return D("h") && (t.append(e("<td>").append(e("<a>").attr({
                                            href: "#",
                                            tabindex: "-1",
                                            title: n.tooltips.incrementHour
                                        }).addClass("btn").attr("data-action", "incrementHours").append(e("<span>").addClass(n.icons.up)))), a.append(e("<td>").append(e("<span>").addClass("timepicker-hour").attr({
                                            "data-time-component": "hours",
                                            title: n.tooltips.pickHour
                                        }).attr("data-action", "showHours"))), s.append(e("<td>").append(e("<a>").attr({
                                            href: "#",
                                            tabindex: "-1",
                                            title: n.tooltips.decrementHour
                                        }).addClass("btn").attr("data-action", "decrementHours").append(e("<span>").addClass(n.icons.down))))), D("m") && (D("h") && (t.append(e("<td>").addClass("separator")), a.append(e("<td>").addClass("separator").html(":")), s.append(e("<td>").addClass("separator"))), t.append(e("<td>").append(e("<a>").attr({
                                            href: "#",
                                            tabindex: "-1",
                                            title: n.tooltips.incrementMinute
                                        }).addClass("btn").attr("data-action", "incrementMinutes").append(e("<span>").addClass(n.icons.up)))), a.append(e("<td>").append(e("<span>").addClass("timepicker-minute").attr({
                                            "data-time-component": "minutes",
                                            title: n.tooltips.pickMinute
                                        }).attr("data-action", "showMinutes"))), s.append(e("<td>").append(e("<a>").attr({
                                            href: "#",
                                            tabindex: "-1",
                                            title: n.tooltips.decrementMinute
                                        }).addClass("btn").attr("data-action", "decrementMinutes").append(e("<span>").addClass(n.icons.down))))), D("s") && (D("m") && (t.append(e("<td>").addClass("separator")), a.append(e("<td>").addClass("separator").html(":")), s.append(e("<td>").addClass("separator"))), t.append(e("<td>").append(e("<a>").attr({
                                            href: "#",
                                            tabindex: "-1",
                                            title: n.tooltips.incrementSecond
                                        }).addClass("btn").attr("data-action", "incrementSeconds").append(e("<span>").addClass(n.icons.up)))), a.append(e("<td>").append(e("<span>").addClass("timepicker-second").attr({
                                            "data-time-component": "seconds",
                                            title: n.tooltips.pickSecond
                                        }).attr("data-action", "showSeconds"))), s.append(e("<td>").append(e("<a>").attr({
                                            href: "#",
                                            tabindex: "-1",
                                            title: n.tooltips.decrementSecond
                                        }).addClass("btn").attr("data-action", "decrementSeconds").append(e("<span>").addClass(n.icons.down))))), o || (t.append(e("<td>").addClass("separator")), a.append(e("<td>").append(e("<button>").addClass("btn btn-primary").attr({
                                            "data-action": "togglePeriod",
                                            tabindex: "-1",
                                            title: n.tooltips.togglePeriod
                                        }))), s.append(e("<td>").addClass("separator"))), e("<div>").addClass("timepicker-picker").append(e("<table>").addClass("table-condensed").append([t, a, s]))
                                    }()];
                                return D("h") && i.push(t), D("m") && i.push(a), D("s") && i.push(s), i
                            },
                            x = function() {
                                var t = e("<div>").addClass("bootstrap-datetimepicker-widget dropdown-menu"),
                                    a = e("<div>").addClass("datepicker").append(function() {
                                        var t = e("<thead>").append(e("<tr>").append(e("<th>").addClass("prev").attr("data-action", "previous").append(e("<span>").addClass(n.icons.previous))).append(e("<th>").addClass("picker-switch").attr("data-action", "pickerSwitch").attr("colspan", n.calendarWeeks ? "6" : "5")).append(e("<th>").addClass("next").attr("data-action", "next").append(e("<span>").addClass(n.icons.next)))),
                                            a = e("<tbody>").append(e("<tr>").append(e("<td>").attr("colspan", n.calendarWeeks ? "8" : "7")));
                                        return [e("<div>").addClass("datepicker-days").append(e("<table>").addClass("table-condensed").append(t).append(e("<tbody>"))), e("<div>").addClass("datepicker-months").append(e("<table>").addClass("table-condensed").append(t.clone()).append(a.clone())), e("<div>").addClass("datepicker-years").append(e("<table>").addClass("table-condensed").append(t.clone()).append(a.clone())), e("<div>").addClass("datepicker-decades").append(e("<table>").addClass("table-condensed").append(t.clone()).append(a.clone()))]
                                    }()),
                                    s = e("<div>").addClass("timepicker").append(S()),
                                    i = e("<ul>").addClass("list-unstyled"),
                                    r = e("<li>").addClass("picker-switch" + (n.collapse ? " accordion-toggle" : "")).append(function() {
                                        var t = [];
                                        return n.showTodayButton && t.push(e("<td>").append(e("<a>").attr({
                                            "data-action": "today",
                                            title: n.tooltips.today
                                        }).append(e("<span>").addClass(n.icons.today)))), !n.sideBySide && T() && b() && t.push(e("<td>").append(e("<a>").attr({
                                            "data-action": "togglePicker",
                                            href: "#",
                                            title: n.tooltips.selectTime
                                        }).append(e("<span>").addClass(n.icons.time)))), n.showClear && t.push(e("<td>").append(e("<a>").attr({
                                            "data-action": "clear",
                                            title: n.tooltips.clear
                                        }).append(e("<span>").addClass(n.icons.clear)))), n.showClose && t.push(e("<td>").append(e("<a>").attr({
                                            "data-action": "close",
                                            title: n.tooltips.close
                                        }).append(e("<span>").addClass(n.icons.close)))), e("<table>").addClass("table-condensed").append(e("<tbody>").append(e("<tr>").append(t)))
                                    }());
                                return n.inline && t.removeClass("dropdown-menu"), o && t.addClass("usetwentyfour"), D("s") && !o && t.addClass("wider"), n.sideBySide && T() && b() ? (t.addClass("timepicker-sbs"), "top" === n.toolbarPlacement && t.append(r), t.append(e("<div>").addClass("row").append(a.addClass("col-md-6")).append(s.addClass("col-md-6"))), "bottom" === n.toolbarPlacement && t.append(r), t) : ("top" === n.toolbarPlacement && i.append(r), T() && i.append(e("<li>").addClass(n.collapse && b() ? "collapse show" : "").append(a)), "default" === n.toolbarPlacement && i.append(r), b() && i.append(e("<li>").addClass(n.collapse && T() ? "collapse" : "").append(s)), "bottom" === n.toolbarPlacement && i.append(r), t.append(i))
                            },
                            H = function() {
                                var t, s = (c || a).position(),
                                    i = (c || a).offset(),
                                    r = n.widgetPositioning.vertical,
                                    o = n.widgetPositioning.horizontal;
                                if (n.widgetParent) t = n.widgetParent.append(h);
                                else if (a.is("input")) t = a.after(h).parent();
                                else {
                                    if (n.inline) return void(t = a.append(h));
                                    t = a, a.children().first().after(h)
                                }
                                if ("auto" === r && (r = i.top + 1.5 * h.height() >= e(window).height() + e(window).scrollTop() && h.height() + a.outerHeight() < i.top ? "top" : "bottom"), "auto" === o && (o = t.width() < i.left + h.outerWidth() / 2 && i.left + h.outerWidth() > e(window).width() ? "right" : "left"), "top" === r ? h.addClass("top").removeClass("bottom") : h.addClass("bottom").removeClass("top"), "right" === o ? h.addClass("pull-right") : h.removeClass("pull-right"), "static" === t.css("position") && (t = t.parents().filter(function() {
                                        return "static" !== e(this).css("position")
                                    }).first()), 0 === t.length) throw new Error("datetimepicker component should be placed within a non-static positioned container");
                                h.css({
                                    top: "top" === r ? "auto" : s.top + a.outerHeight(),
                                    bottom: "top" === r ? t.outerHeight() - (t === a ? 0 : s.top) : "auto",
                                    left: "left" === o ? t === a ? 0 : s.left : "auto",
                                    right: "left" === o ? "auto" : t.outerWidth() - a.outerWidth() - (t === a ? 0 : s.left)
                                })
                            },
                            j = function(e) {
                                "dp.change" === e.type && (e.date && e.date.isSame(e.oldDate) || !e.date && !e.oldDate) || a.trigger(e)
                            },
                            C = function(e) {
                                "y" === e && (e = "YYYY"), j({
                                    type: "dp.update",
                                    change: e,
                                    viewDate: i.clone()
                                })
                            },
                            P = function(e) {
                                h && (e && (u = Math.max(p, Math.min(3, u + e))), h.find(".datepicker > div").hide().filter(".datepicker-" + f[u].clsName).show())
                            },
                            O = function(t, a) {
                                if (!t.isValid()) return !1;
                                if (n.disabledDates && "d" === a && function(e) {
                                        return !0 === n.disabledDates[e.format("YYYY-MM-DD")]
                                    }(t)) return !1;
                                if (n.enabledDates && "d" === a && ! function(e) {
                                        return !0 === n.enabledDates[e.format("YYYY-MM-DD")]
                                    }(t)) return !1;
                                if (n.minDate && t.isBefore(n.minDate, a)) return !1;
                                if (n.maxDate && t.isAfter(n.maxDate, a)) return !1;
                                if (n.daysOfWeekDisabled && "d" === a && -1 !== n.daysOfWeekDisabled.indexOf(t.day())) return !1;
                                if (n.disabledHours && ("h" === a || "m" === a || "s" === a) && function(e) {
                                        return !0 === n.disabledHours[e.format("H")]
                                    }(t)) return !1;
                                if (n.enabledHours && ("h" === a || "m" === a || "s" === a) && ! function(e) {
                                        return !0 === n.enabledHours[e.format("H")]
                                    }(t)) return !1;
                                if (n.disabledTimeIntervals && ("h" === a || "m" === a || "s" === a)) {
                                    var s = !1;
                                    if (e.each(n.disabledTimeIntervals, function() {
                                            if (t.isBetween(this[0], this[1])) return s = !0, !1
                                        }), s) return !1
                                }
                                return !0
                            },
                            E = function() {
                                var a, r, o, d = h.find(".datepicker-days"),
                                    l = d.find("th"),
                                    u = [],
                                    _ = [];
                                if (T()) {
                                    for (l.eq(0).find("span").attr("title", n.tooltips.prevMonth), l.eq(1).attr("title", n.tooltips.selectMonth), l.eq(2).find("span").attr("title", n.tooltips.nextMonth), d.find(".disabled").removeClass("disabled"), l.eq(1).text(i.format(n.dayViewHeaderFormat)), O(i.clone().subtract(1, "M"), "M") || l.eq(0).addClass("disabled"), O(i.clone().add(1, "M"), "M") || l.eq(2).addClass("disabled"), a = i.clone().startOf("M").startOf("w").startOf("d"), o = 0; o < 42; o++) 0 === a.weekday() && (r = e("<tr>"), n.calendarWeeks && r.append('<td class="cw">' + a.week() + "</td>"), u.push(r)), _ = ["day"], a.isBefore(i, "M") && _.push("old"), a.isAfter(i, "M") && _.push("new"), a.isSame(s, "d") && !m && _.push("active"), O(a, "d") || _.push("disabled"), a.isSame(k(), "d") && _.push("today"), 0 !== a.day() && 6 !== a.day() || _.push("weekend"), j({
                                        type: "dp.classify",
                                        date: a,
                                        classNames: _
                                    }), r.append('<td data-action="selectDay" data-day="' + a.format("L") + '" class="' + _.join(" ") + '">' + a.date() + "</td>"), a.add(1, "d");
                                    d.find("tbody").empty().append(u),
                                        function() {
                                            var t = h.find(".datepicker-months"),
                                                a = t.find("th"),
                                                r = t.find("tbody").find("span");
                                            a.eq(0).find("span").attr("title", n.tooltips.prevYear), a.eq(1).attr("title", n.tooltips.selectYear), a.eq(2).find("span").attr("title", n.tooltips.nextYear), t.find(".disabled").removeClass("disabled"), O(i.clone().subtract(1, "y"), "y") || a.eq(0).addClass("disabled"), a.eq(1).text(i.year()), O(i.clone().add(1, "y"), "y") || a.eq(2).addClass("disabled"), r.removeClass("active"), s.isSame(i, "y") && !m && r.eq(s.month()).addClass("active"), r.each(function(t) {
                                                O(i.clone().month(t), "M") || e(this).addClass("disabled")
                                            })
                                        }(),
                                        function() {
                                            var e = h.find(".datepicker-years"),
                                                t = e.find("th"),
                                                a = i.clone().subtract(5, "y"),
                                                r = i.clone().add(6, "y"),
                                                o = "";
                                            for (t.eq(0).find("span").attr("title", n.tooltips.prevDecade), t.eq(1).attr("title", n.tooltips.selectDecade), t.eq(2).find("span").attr("title", n.tooltips.nextDecade), e.find(".disabled").removeClass("disabled"), n.minDate && n.minDate.isAfter(a, "y") && t.eq(0).addClass("disabled"), t.eq(1).text(a.year() + "-" + r.year()), n.maxDate && n.maxDate.isBefore(r, "y") && t.eq(2).addClass("disabled"); !a.isAfter(r, "y");) o += '<span data-action="selectYear" class="year' + (a.isSame(s, "y") && !m ? " active" : "") + (O(a, "y") ? "" : " disabled") + '">' + a.year() + "</span>", a.add(1, "y");
                                            e.find("td").html(o)
                                        }(),
                                        function() {
                                            var e, a = h.find(".datepicker-decades"),
                                                r = a.find("th"),
                                                o = t({
                                                    y: i.year() - i.year() % 100 - 1
                                                }),
                                                d = o.clone().add(100, "y"),
                                                l = o.clone(),
                                                u = !1,
                                                _ = !1,
                                                m = "";
                                            for (r.eq(0).find("span").attr("title", n.tooltips.prevCentury), r.eq(2).find("span").attr("title", n.tooltips.nextCentury), a.find(".disabled").removeClass("disabled"), (o.isSame(t({
                                                    y: 1900
                                                })) || n.minDate && n.minDate.isAfter(o, "y")) && r.eq(0).addClass("disabled"), r.eq(1).text(o.year() + "-" + d.year()), (o.isSame(t({
                                                    y: 2e3
                                                })) || n.maxDate && n.maxDate.isBefore(d, "y")) && r.eq(2).addClass("disabled"); !o.isAfter(d, "y");) e = o.year() + 12, u = n.minDate && n.minDate.isAfter(o, "y") && n.minDate.year() <= e, _ = n.maxDate && n.maxDate.isAfter(o, "y") && n.maxDate.year() <= e, m += '<span data-action="selectDecade" class="decade' + (s.isAfter(o) && s.year() <= e ? " active" : "") + (O(o, "y") || u || _ ? "" : " disabled") + '" data-selection="' + (o.year() + 6) + '">' + (o.year() + 1) + " - " + (o.year() + 12) + "</span>", o.add(12, "y");
                                            m += "<span></span><span></span><span></span>", a.find("td").html(m), r.eq(1).text(l.year() + 1 + "-" + o.year())
                                        }()
                                }
                            },
                            W = function() {
                                var t, a, r = h.find(".timepicker span[data-time-component]");
                                o || (t = h.find(".timepicker [data-action=togglePeriod]"), a = s.clone().add(s.hours() >= 12 ? -12 : 12, "h"), t.text(s.format("A")), O(a, "h") ? t.removeClass("disabled") : t.addClass("disabled")), r.filter("[data-time-component=hours]").text(s.format(o ? "HH" : "hh")), r.filter("[data-time-component=minutes]").text(s.format("mm")), r.filter("[data-time-component=seconds]").text(s.format("ss")),
                                    function() {
                                        var t = h.find(".timepicker-hours table"),
                                            a = i.clone().startOf("d"),
                                            n = [],
                                            s = e("<tr>");
                                        for (i.hour() > 11 && !o && a.hour(12); a.isSame(i, "d") && (o || i.hour() < 12 && a.hour() < 12 || i.hour() > 11);) a.hour() % 4 == 0 && (s = e("<tr>"), n.push(s)), s.append('<td data-action="selectHour" class="hour' + (O(a, "h") ? "" : " disabled") + '">' + a.format(o ? "HH" : "hh") + "</td>"), a.add(1, "h");
                                        t.empty().append(n)
                                    }(),
                                    function() {
                                        for (var t = h.find(".timepicker-minutes table"), a = i.clone().startOf("h"), s = [], r = e("<tr>"), o = 1 === n.stepping ? 5 : n.stepping; i.isSame(a, "h");) a.minute() % (4 * o) == 0 && (r = e("<tr>"), s.push(r)), r.append('<td data-action="selectMinute" class="minute' + (O(a, "m") ? "" : " disabled") + '">' + a.format("mm") + "</td>"), a.add(o, "m");
                                        t.empty().append(s)
                                    }(),
                                    function() {
                                        for (var t = h.find(".timepicker-seconds table"), a = i.clone().startOf("m"), n = [], s = e("<tr>"); i.isSame(a, "m");) a.second() % 20 == 0 && (s = e("<tr>"), n.push(s)), s.append('<td data-action="selectSecond" class="second' + (O(a, "s") ? "" : " disabled") + '">' + a.format("ss") + "</td>"), a.add(5, "s");
                                        t.empty().append(n)
                                    }()
                            },
                            A = function() {
                                h && (E(), W())
                            },
                            z = function(e) {
                                var t = m ? null : s;
                                if (!e) return m = !0, r.val(""), a.data("date", ""), j({
                                    type: "dp.change",
                                    date: !1,
                                    oldDate: t
                                }), void A();
                                if (e = e.clone().locale(n.locale), w() && e.tz(n.timeZone), 1 !== n.stepping)
                                    for (e.minutes(Math.round(e.minutes() / n.stepping) * n.stepping).seconds(0); n.minDate && e.isBefore(n.minDate);) e.add(n.stepping, "minutes");
                                O(e) ? (i = (s = e).clone(), r.val(s.format(d)), a.data("date", s.format(d)), m = !1, A(), j({
                                    type: "dp.change",
                                    date: s.clone(),
                                    oldDate: t
                                })) : (n.keepInvalid ? j({
                                    type: "dp.change",
                                    date: e,
                                    oldDate: t
                                }) : r.val(m ? "" : s.format(d)), j({
                                    type: "dp.error",
                                    date: e,
                                    oldDate: t
                                }))
                            },
                            F = function() {
                                var t = !1;
                                return h ? (h.find(".collapse").each(function() {
                                    var a = e(this).data("collapse");
                                    return !a || !a.transitioning || (t = !0, !1)
                                }), t ? _ : (c && c.hasClass("btn") && c.toggleClass("active"), h.hide(), e(window).off("resize", H), h.off("click", "[data-action]"), h.off("mousedown", !1), h.remove(), h = !1, j({
                                    type: "dp.hide",
                                    date: s.clone()
                                }), r.blur(), i = s.clone(), _)) : _
                            },
                            I = function() {
                                z(null)
                            },
                            $ = function(e) {
                                return void 0 === n.parseInputDate ? (!t.isMoment(e) || e instanceof Date) && (e = k(e)) : e = n.parseInputDate(e), e
                            },
                            N = {
                                next: function() {
                                    var e = f[u].navFnc;
                                    i.add(f[u].navStep, e), E(), C(e)
                                },
                                previous: function() {
                                    var e = f[u].navFnc;
                                    i.subtract(f[u].navStep, e), E(), C(e)
                                },
                                pickerSwitch: function() {
                                    P(1)
                                },
                                selectMonth: function(t) {
                                    var a = e(t.target).closest("tbody").find("span").index(e(t.target));
                                    i.month(a), u === p ? (z(s.clone().year(i.year()).month(i.month())), n.inline || F()) : (P(-1), E()), C("M")
                                },
                                selectYear: function(t) {
                                    var a = parseInt(e(t.target).text(), 10) || 0;
                                    i.year(a), u === p ? (z(s.clone().year(i.year())), n.inline || F()) : (P(-1), E()), C("YYYY")
                                },
                                selectDecade: function(t) {
                                    var a = parseInt(e(t.target).data("selection"), 10) || 0;
                                    i.year(a), u === p ? (z(s.clone().year(i.year())), n.inline || F()) : (P(-1), E()), C("YYYY")
                                },
                                selectDay: function(t) {
                                    var a = i.clone();
                                    e(t.target).is(".old") && a.subtract(1, "M"), e(t.target).is(".new") && a.add(1, "M"), z(a.date(parseInt(e(t.target).text(), 10))), b() || n.keepOpen || n.inline || F()
                                },
                                incrementHours: function() {
                                    var e = s.clone().add(1, "h");
                                    O(e, "h") && z(e)
                                },
                                incrementMinutes: function() {
                                    var e = s.clone().add(n.stepping, "m");
                                    O(e, "m") && z(e)
                                },
                                incrementSeconds: function() {
                                    var e = s.clone().add(1, "s");
                                    O(e, "s") && z(e)
                                },
                                decrementHours: function() {
                                    var e = s.clone().subtract(1, "h");
                                    O(e, "h") && z(e)
                                },
                                decrementMinutes: function() {
                                    var e = s.clone().subtract(n.stepping, "m");
                                    O(e, "m") && z(e)
                                },
                                decrementSeconds: function() {
                                    var e = s.clone().subtract(1, "s");
                                    O(e, "s") && z(e)
                                },
                                togglePeriod: function() {
                                    z(s.clone().add(s.hours() >= 12 ? -12 : 12, "h"))
                                },
                                togglePicker: function(t) {
                                    var a, s = e(t.target),
                                        i = s.closest("ul"),
                                        r = i.find(".show"),
                                        o = i.find(".collapse:not(.show)");
                                    if (r && r.length) {
                                        if ((a = r.data("collapse")) && a.transitioning) return;
                                        r.collapse ? (r.collapse("hide"), o.collapse("show")) : (r.removeClass("show"), o.addClass("show")), s.is("span") ? s.toggleClass(n.icons.time + " " + n.icons.date) : s.find("span").toggleClass(n.icons.time + " " + n.icons.date)
                                    }
                                },
                                showPicker: function() {
                                    h.find(".timepicker > div:not(.timepicker-picker)").hide(), h.find(".timepicker .timepicker-picker").show()
                                },
                                showHours: function() {
                                    h.find(".timepicker .timepicker-picker").hide(), h.find(".timepicker .timepicker-hours").show()
                                },
                                showMinutes: function() {
                                    h.find(".timepicker .timepicker-picker").hide(), h.find(".timepicker .timepicker-minutes").show()
                                },
                                showSeconds: function() {
                                    h.find(".timepicker .timepicker-picker").hide(), h.find(".timepicker .timepicker-seconds").show()
                                },
                                selectHour: function(t) {
                                    var a = parseInt(e(t.target).text(), 10);
                                    o || (s.hours() >= 12 ? 12 !== a && (a += 12) : 12 === a && (a = 0)), z(s.clone().hours(a)), N.showPicker.call(_)
                                },
                                selectMinute: function(t) {
                                    z(s.clone().minutes(parseInt(e(t.target).text(), 10))), N.showPicker.call(_)
                                },
                                selectSecond: function(t) {
                                    z(s.clone().seconds(parseInt(e(t.target).text(), 10))), N.showPicker.call(_)
                                },
                                clear: I,
                                today: function() {
                                    var e = k();
                                    O(e, "d") && z(e)
                                },
                                close: F
                            },
                            R = function(t) {
                                return !e(t.currentTarget).is(".disabled") && (N[e(t.currentTarget).data("action")].apply(_, arguments), !1)
                            },
                            J = function() {
                                var t;
                                return r.prop("disabled") || !n.ignoreReadonly && r.prop("readonly") || h ? _ : (void 0 !== r.val() && 0 !== r.val().trim().length ? z($(r.val().trim())) : m && n.useCurrent && (n.inline || r.is("input") && 0 === r.val().trim().length) && (t = k(), "string" == typeof n.useCurrent && (t = {
                                    year: function(e) {
                                        return e.month(0).date(1).hours(0).seconds(0).minutes(0)
                                    },
                                    month: function(e) {
                                        return e.date(1).hours(0).seconds(0).minutes(0)
                                    },
                                    day: function(e) {
                                        return e.hours(0).seconds(0).minutes(0)
                                    },
                                    hour: function(e) {
                                        return e.seconds(0).minutes(0)
                                    },
                                    minute: function(e) {
                                        return e.seconds(0)
                                    }
                                } [n.useCurrent](t)), z(t)), h = x(), function() {
                                    var t = e("<tr>"),
                                        a = i.clone().startOf("w").startOf("d");
                                    for (!0 === n.calendarWeeks && t.append(e("<th>").addClass("cw").text("#")); a.isBefore(i.clone().endOf("w"));) t.append(e("<th>").addClass("dow").text(a.format("dd"))), a.add(1, "d");
                                    h.find(".datepicker-days thead").append(t)
                                }(), function() {
                                    for (var t = [], a = i.clone().startOf("y").startOf("d"); a.isSame(i, "y");) t.push(e("<span>").attr("data-action", "selectMonth").addClass("month").text(a.format("MMM"))), a.add(1, "M");
                                    h.find(".datepicker-months td").empty().append(t)
                                }(), h.find(".timepicker-hours").hide(), h.find(".timepicker-minutes").hide(), h.find(".timepicker-seconds").hide(), A(), P(), e(window).on("resize", H), h.on("click", "[data-action]", R), h.on("mousedown", !1), c && c.hasClass("btn") && c.toggleClass("active"), H(), h.show(), n.focusOnShow && !r.is(":focus") && r.focus(), j({
                                    type: "dp.show"
                                }), _)
                            },
                            B = function() {
                                return h ? F() : J()
                            },
                            U = function(e) {
                                var t, a, s, i, r = null,
                                    o = [],
                                    d = {},
                                    l = e.which;
                                v[l] = "p";
                                for (t in v) v.hasOwnProperty(t) && "p" === v[t] && (o.push(t), parseInt(t, 10) !== l && (d[t] = !0));
                                for (t in n.keyBinds)
                                    if (n.keyBinds.hasOwnProperty(t) && "function" == typeof n.keyBinds[t] && (s = t.split(" ")).length === o.length && Y[l] === s[s.length - 1]) {
                                        for (i = !0, a = s.length - 2; a >= 0; a--)
                                            if (!(Y[s[a]] in d)) {
                                                i = !1;
                                                break
                                            }
                                        if (i) {
                                            r = n.keyBinds[t];
                                            break
                                        }
                                    }
                                r && (r.call(_, h), e.stopPropagation(), e.preventDefault())
                            },
                            V = function(e) {
                                v[e.which] = "r", e.stopPropagation(), e.preventDefault()
                            },
                            q = function(t) {
                                var a = e(t.target).val().trim(),
                                    n = a ? $(a) : null;
                                return z(n), t.stopImmediatePropagation(), !1
                            },
                            G = function(t) {
                                var a = {};
                                return e.each(t, function() {
                                    var e = $(this);
                                    e.isValid() && (a[e.format("YYYY-MM-DD")] = !0)
                                }), !!Object.keys(a).length && a
                            },
                            Z = function(t) {
                                var a = {};
                                return e.each(t, function() {
                                    a[this] = !0
                                }), !!Object.keys(a).length && a
                            },
                            K = function() {
                                var e = n.format || "L LT";
                                d = e.replace(/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, function(e) {
                                    return (s.localeData().longDateFormat(e) || e).replace(/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, function(e) {
                                        return s.localeData().longDateFormat(e) || e
                                    })
                                }), (l = n.extraFormats ? n.extraFormats.slice() : []).indexOf(e) < 0 && l.indexOf(d) < 0 && l.push(d), o = d.toLowerCase().indexOf("a") < 1 && d.replace(/\[.*?\]/g, "").indexOf("h") < 1, D("y") && (p = 2), D("M") && (p = 1), D("d") && (p = 0), u = Math.max(p, u), m || z(s)
                            };
                        if (_.destroy = function() {
                                F(), r.off({
                                    change: q,
                                    blur: blur,
                                    keydown: U,
                                    keyup: V,
                                    focus: n.allowInputToggle ? F : ""
                                }), a.is("input") ? r.off({
                                    focus: J
                                }) : c && (c.off("click", B), c.off("mousedown", !1)), a.removeData("DateTimePicker"), a.removeData("date")
                            }, _.toggle = B, _.show = J, _.hide = F, _.disable = function() {
                                return F(), c && c.hasClass("btn") && c.addClass("disabled"), r.prop("disabled", !0), _
                            }, _.enable = function() {
                                return c && c.hasClass("btn") && c.removeClass("disabled"), r.prop("disabled", !1), _
                            }, _.ignoreReadonly = function(e) {
                                if (0 === arguments.length) return n.ignoreReadonly;
                                if ("boolean" != typeof e) throw new TypeError("ignoreReadonly () expects a boolean parameter");
                                return n.ignoreReadonly = e, _
                            }, _.options = function(t) {
                                if (0 === arguments.length) return e.extend(!0, {}, n);
                                if (!(t instanceof Object)) throw new TypeError("options() options parameter should be an object");
                                return e.extend(!0, n, t), e.each(n, function(e, t) {
                                    if (void 0 === _[e]) throw new TypeError("option " + e + " is not recognized!");
                                    _[e](t)
                                }), _
                            }, _.date = function(e) {
                                if (0 === arguments.length) return m ? null : s.clone();
                                if (!(null === e || "string" == typeof e || t.isMoment(e) || e instanceof Date)) throw new TypeError("date() parameter must be one of [null, string, moment or Date]");
                                return z(null === e ? null : $(e)), _
                            }, _.format = function(e) {
                                if (0 === arguments.length) return n.format;
                                if ("string" != typeof e && ("boolean" != typeof e || !1 !== e)) throw new TypeError("format() expects a string or boolean:false parameter " + e);
                                return n.format = e, d && K(), _
                            }, _.timeZone = function(e) {
                                if (0 === arguments.length) return n.timeZone;
                                if ("string" != typeof e) throw new TypeError("newZone() expects a string parameter");
                                return n.timeZone = e, _
                            }, _.dayViewHeaderFormat = function(e) {
                                if (0 === arguments.length) return n.dayViewHeaderFormat;
                                if ("string" != typeof e) throw new TypeError("dayViewHeaderFormat() expects a string parameter");
                                return n.dayViewHeaderFormat = e, _
                            }, _.extraFormats = function(e) {
                                if (0 === arguments.length) return n.extraFormats;
                                if (!1 !== e && !(e instanceof Array)) throw new TypeError("extraFormats() expects an array or false parameter");
                                return n.extraFormats = e, l && K(), _
                            }, _.disabledDates = function(t) {
                                if (0 === arguments.length) return n.disabledDates ? e.extend({}, n.disabledDates) : n.disabledDates;
                                if (!t) return n.disabledDates = !1, A(), _;
                                if (!(t instanceof Array)) throw new TypeError("disabledDates() expects an array parameter");
                                return n.disabledDates = G(t), n.enabledDates = !1, A(), _
                            }, _.enabledDates = function(t) {
                                if (0 === arguments.length) return n.enabledDates ? e.extend({}, n.enabledDates) : n.enabledDates;
                                if (!t) return n.enabledDates = !1, A(), _;
                                if (!(t instanceof Array)) throw new TypeError("enabledDates() expects an array parameter");
                                return n.enabledDates = G(t), n.disabledDates = !1, A(), _
                            }, _.daysOfWeekDisabled = function(e) {
                                if (0 === arguments.length) return n.daysOfWeekDisabled.splice(0);
                                if ("boolean" == typeof e && !e) return n.daysOfWeekDisabled = !1, A(), _;
                                if (!(e instanceof Array)) throw new TypeError("daysOfWeekDisabled() expects an array parameter");
                                if (n.daysOfWeekDisabled = e.reduce(function(e, t) {
                                        return (t = parseInt(t, 10)) > 6 || t < 0 || isNaN(t) ? e : (-1 === e.indexOf(t) && e.push(t), e)
                                    }, []).sort(), n.useCurrent && !n.keepInvalid) {
                                    for (var t = 0; !O(s, "d");) {
                                        if (s.add(1, "d"), 31 === t) throw "Tried 31 times to find a valid date";
                                        t++
                                    }
                                    z(s)
                                }
                                return A(), _
                            }, _.maxDate = function(e) {
                                if (0 === arguments.length) return n.maxDate ? n.maxDate.clone() : n.maxDate;
                                if ("boolean" == typeof e && !1 === e) return n.maxDate = !1, A(), _;
                                "string" == typeof e && ("now" !== e && "moment" !== e || (e = k()));
                                var t = $(e);
                                if (!t.isValid()) throw new TypeError("maxDate() Could not parse date parameter: " + e);
                                if (n.minDate && t.isBefore(n.minDate)) throw new TypeError("maxDate() date parameter is before options.minDate: " + t.format(d));
                                return n.maxDate = t, n.useCurrent && !n.keepInvalid && s.isAfter(e) && z(n.maxDate), i.isAfter(t) && (i = t.clone().subtract(n.stepping, "m")), A(), _
                            }, _.minDate = function(e) {
                                if (0 === arguments.length) return n.minDate ? n.minDate.clone() : n.minDate;
                                if ("boolean" == typeof e && !1 === e) return n.minDate = !1, A(), _;
                                "string" == typeof e && ("now" !== e && "moment" !== e || (e = k()));
                                var t = $(e);
                                if (!t.isValid()) throw new TypeError("minDate() Could not parse date parameter: " + e);
                                if (n.maxDate && t.isAfter(n.maxDate)) throw new TypeError("minDate() date parameter is after options.maxDate: " + t.format(d));
                                return n.minDate = t, n.useCurrent && !n.keepInvalid && s.isBefore(e) && z(n.minDate), i.isBefore(t) && (i = t.clone().add(n.stepping, "m")), A(), _
                            }, _.defaultDate = function(e) {
                                if (0 === arguments.length) return n.defaultDate ? n.defaultDate.clone() : n.defaultDate;
                                if (!e) return n.defaultDate = !1, _;
                                "string" == typeof e && (e = "now" === e || "moment" === e ? k() : k(e));
                                var t = $(e);
                                if (!t.isValid()) throw new TypeError("defaultDate() Could not parse date parameter: " + e);
                                if (!O(t)) throw new TypeError("defaultDate() date passed is invalid according to component setup validations");
                                return n.defaultDate = t, (n.defaultDate && n.inline || "" === r.val().trim()) && z(n.defaultDate), _
                            }, _.locale = function(e) {
                                if (0 === arguments.length) return n.locale;
                                if (!t.localeData(e)) throw new TypeError("locale() locale " + e + " is not loaded from moment locales!");
                                return n.locale = e, s.locale(n.locale), i.locale(n.locale), d && K(), h && (F(), J()), _
                            }, _.stepping = function(e) {
                                return 0 === arguments.length ? n.stepping : (e = parseInt(e, 10), (isNaN(e) || e < 1) && (e = 1), n.stepping = e, _)
                            }, _.useCurrent = function(e) {
                                var t = ["year", "month", "day", "hour", "minute"];
                                if (0 === arguments.length) return n.useCurrent;
                                if ("boolean" != typeof e && "string" != typeof e) throw new TypeError("useCurrent() expects a boolean or string parameter");
                                if ("string" == typeof e && -1 === t.indexOf(e.toLowerCase())) throw new TypeError("useCurrent() expects a string parameter of " + t.join(", "));
                                return n.useCurrent = e, _
                            }, _.collapse = function(e) {
                                if (0 === arguments.length) return n.collapse;
                                if ("boolean" != typeof e) throw new TypeError("collapse() expects a boolean parameter");
                                return n.collapse === e ? _ : (n.collapse = e, h && (F(), J()), _)
                            }, _.icons = function(t) {
                                if (0 === arguments.length) return e.extend({}, n.icons);
                                if (!(t instanceof Object)) throw new TypeError("icons() expects parameter to be an Object");
                                return e.extend(n.icons, t), h && (F(), J()), _
                            }, _.tooltips = function(t) {
                                if (0 === arguments.length) return e.extend({}, n.tooltips);
                                if (!(t instanceof Object)) throw new TypeError("tooltips() expects parameter to be an Object");
                                return e.extend(n.tooltips, t), h && (F(), J()), _
                            }, _.useStrict = function(e) {
                                if (0 === arguments.length) return n.useStrict;
                                if ("boolean" != typeof e) throw new TypeError("useStrict() expects a boolean parameter");
                                return n.useStrict = e, _
                            }, _.sideBySide = function(e) {
                                if (0 === arguments.length) return n.sideBySide;
                                if ("boolean" != typeof e) throw new TypeError("sideBySide() expects a boolean parameter");
                                return n.sideBySide = e, h && (F(), J()), _
                            }, _.viewMode = function(e) {
                                if (0 === arguments.length) return n.viewMode;
                                if ("string" != typeof e) throw new TypeError("viewMode() expects a string parameter");
                                if (-1 === M.indexOf(e)) throw new TypeError("viewMode() parameter must be one of (" + M.join(", ") + ") value");
                                return n.viewMode = e, u = Math.max(M.indexOf(e), p), P(), _
                            }, _.toolbarPlacement = function(e) {
                                if (0 === arguments.length) return n.toolbarPlacement;
                                if ("string" != typeof e) throw new TypeError("toolbarPlacement() expects a string parameter");
                                if (-1 === L.indexOf(e)) throw new TypeError("toolbarPlacement() parameter must be one of (" + L.join(", ") + ") value");
                                return n.toolbarPlacement = e, h && (F(), J()), _
                            }, _.widgetPositioning = function(t) {
                                if (0 === arguments.length) return e.extend({}, n.widgetPositioning);
                                if ("[object Object]" !== {}.toString.call(t)) throw new TypeError("widgetPositioning() expects an object variable");
                                if (t.horizontal) {
                                    if ("string" != typeof t.horizontal) throw new TypeError("widgetPositioning() horizontal variable must be a string");
                                    if (t.horizontal = t.horizontal.toLowerCase(), -1 === g.indexOf(t.horizontal)) throw new TypeError("widgetPositioning() expects horizontal parameter to be one of (" + g.join(", ") + ")");
                                    n.widgetPositioning.horizontal = t.horizontal
                                }
                                if (t.vertical) {
                                    if ("string" != typeof t.vertical) throw new TypeError("widgetPositioning() vertical variable must be a string");
                                    if (t.vertical = t.vertical.toLowerCase(), -1 === y.indexOf(t.vertical)) throw new TypeError("widgetPositioning() expects vertical parameter to be one of (" + y.join(", ") + ")");
                                    n.widgetPositioning.vertical = t.vertical
                                }
                                return A(), _
                            }, _.calendarWeeks = function(e) {
                                if (0 === arguments.length) return n.calendarWeeks;
                                if ("boolean" != typeof e) throw new TypeError("calendarWeeks() expects parameter to be a boolean value");
                                return n.calendarWeeks = e, A(), _
                            }, _.showTodayButton = function(e) {
                                if (0 === arguments.length) return n.showTodayButton;
                                if ("boolean" != typeof e) throw new TypeError("showTodayButton() expects a boolean parameter");
                                return n.showTodayButton = e, h && (F(), J()), _
                            }, _.showClear = function(e) {
                                if (0 === arguments.length) return n.showClear;
                                if ("boolean" != typeof e) throw new TypeError("showClear() expects a boolean parameter");
                                return n.showClear = e, h && (F(), J()), _
                            }, _.widgetParent = function(t) {
                                if (0 === arguments.length) return n.widgetParent;
                                if ("string" == typeof t && (t = e(t)), null !== t && "string" != typeof t && !(t instanceof e)) throw new TypeError("widgetParent() expects a string or a jQuery object parameter");
                                return n.widgetParent = t, h && (F(), J()), _
                            }, _.keepOpen = function(e) {
                                if (0 === arguments.length) return n.keepOpen;
                                if ("boolean" != typeof e) throw new TypeError("keepOpen() expects a boolean parameter");
                                return n.keepOpen = e, _
                            }, _.focusOnShow = function(e) {
                                if (0 === arguments.length) return n.focusOnShow;
                                if ("boolean" != typeof e) throw new TypeError("focusOnShow() expects a boolean parameter");
                                return n.focusOnShow = e, _
                            }, _.inline = function(e) {
                                if (0 === arguments.length) return n.inline;
                                if ("boolean" != typeof e) throw new TypeError("inline() expects a boolean parameter");
                                return n.inline = e, _
                            }, _.clear = function() {
                                return I(), _
                            }, _.keyBinds = function(e) {
                                return 0 === arguments.length ? n.keyBinds : (n.keyBinds = e, _)
                            }, _.getMoment = function(e) {
                                return k(e)
                            }, _.debug = function(e) {
                                if ("boolean" != typeof e) throw new TypeError("debug() expects a boolean parameter");
                                return n.debug = e, _
                            }, _.allowInputToggle = function(e) {
                                if (0 === arguments.length) return n.allowInputToggle;
                                if ("boolean" != typeof e) throw new TypeError("allowInputToggle() expects a boolean parameter");
                                return n.allowInputToggle = e, _
                            }, _.showClose = function(e) {
                                if (0 === arguments.length) return n.showClose;
                                if ("boolean" != typeof e) throw new TypeError("showClose() expects a boolean parameter");
                                return n.showClose = e, _
                            }, _.keepInvalid = function(e) {
                                if (0 === arguments.length) return n.keepInvalid;
                                if ("boolean" != typeof e) throw new TypeError("keepInvalid() expects a boolean parameter");
                                return n.keepInvalid = e, _
                            }, _.datepickerInput = function(e) {
                                if (0 === arguments.length) return n.datepickerInput;
                                if ("string" != typeof e) throw new TypeError("datepickerInput() expects a string parameter");
                                return n.datepickerInput = e, _
                            }, _.parseInputDate = function(e) {
                                if (0 === arguments.length) return n.parseInputDate;
                                if ("function" != typeof e) throw new TypeError("parseInputDate() sholud be as function");
                                return n.parseInputDate = e, _
                            }, _.disabledTimeIntervals = function(t) {
                                if (0 === arguments.length) return n.disabledTimeIntervals ? e.extend({}, n.disabledTimeIntervals) : n.disabledTimeIntervals;
                                if (!t) return n.disabledTimeIntervals = !1, A(), _;
                                if (!(t instanceof Array)) throw new TypeError("disabledTimeIntervals() expects an array parameter");
                                return n.disabledTimeIntervals = t, A(), _
                            }, _.disabledHours = function(t) {
                                if (0 === arguments.length) return n.disabledHours ? e.extend({}, n.disabledHours) : n.disabledHours;
                                if (!t) return n.disabledHours = !1, A(), _;
                                if (!(t instanceof Array)) throw new TypeError("disabledHours() expects an array parameter");
                                if (n.disabledHours = Z(t), n.enabledHours = !1, n.useCurrent && !n.keepInvalid) {
                                    for (var a = 0; !O(s, "h");) {
                                        if (s.add(1, "h"), 24 === a) throw "Tried 24 times to find a valid date";
                                        a++
                                    }
                                    z(s)
                                }
                                return A(), _
                            }, _.enabledHours = function(t) {
                                if (0 === arguments.length) return n.enabledHours ? e.extend({}, n.enabledHours) : n.enabledHours;
                                if (!t) return n.enabledHours = !1, A(), _;
                                if (!(t instanceof Array)) throw new TypeError("enabledHours() expects an array parameter");
                                if (n.enabledHours = Z(t), n.disabledHours = !1, n.useCurrent && !n.keepInvalid) {
                                    for (var a = 0; !O(s, "h");) {
                                        if (s.add(1, "h"), 24 === a) throw "Tried 24 times to find a valid date";
                                        a++
                                    }
                                    z(s)
                                }
                                return A(), _
                            }, _.viewDate = function(e) {
                                if (0 === arguments.length) return i.clone();
                                if (!e) return i = s.clone(), _;
                                if (!("string" == typeof e || t.isMoment(e) || e instanceof Date)) throw new TypeError("viewDate() parameter must be one of [string, moment or Date]");
                                return i = $(e), C(), _
                            }, a.is("input")) r = a;
                        else if (0 === (r = a.find(n.datepickerInput)).length) r = a.find("input");
                        else if (!r.is("input")) throw new Error('CSS class "' + n.datepickerInput + '" cannot be applied to non input element');
                        if (a.hasClass("input-group") && (c = 0 === a.find(".datepickerbutton").length ? a.find(".input-group-addon") : a.find(".datepickerbutton")), !n.inline && !r.is("input")) throw new Error("Could not initialize DateTimePicker without an input element");
                        return s = k(), i = s.clone(), e.extend(!0, n, function() {
                            var t, s = {};
                            return (t = a.is("input") || n.inline ? a.data() : a.find("input").data()).dateOptions && t.dateOptions instanceof Object && (s = e.extend(!0, s, t.dateOptions)), e.each(n, function(e) {
                                var a = "date" + e.charAt(0).toUpperCase() + e.slice(1);
                                void 0 !== t[a] && (s[e] = t[a])
                            }), s
                        }()), _.options(n), K(), r.on({
                            change: q,
                            blur: n.debug ? "" : F,
                            keydown: U,
                            keyup: V,
                            focus: n.allowInputToggle ? J : ""
                        }), a.is("input") ? r.on({
                            focus: J
                        }) : c && (c.on("click", B), c.on("mousedown", !1)), r.prop("disabled") && _.disable(), r.is("input") && 0 !== r.val().trim().length ? z($(r.val().trim())) : n.defaultDate && void 0 === r.attr("placeholder") && z(n.defaultDate), n.inline && J(), _
                    };
                    return e.fn.datetimepicker = function(t) {
                        t = t || {};
                        var n, s = Array.prototype.slice.call(arguments, 1),
                            i = !0;
                        if ("object" == typeof t) return this.each(function() {
                            var n, s = e(this);
                            s.data("DateTimePicker") || (n = e.extend(!0, {}, e.fn.datetimepicker.defaults, t), s.data("DateTimePicker", a(s, n)))
                        });
                        if ("string" == typeof t) return this.each(function() {
                            var a = e(this).data("DateTimePicker");
                            if (!a) throw new Error('bootstrap-datetimepicker("' + t + '") method was called on an element that is not using DateTimePicker');
                            n = a[t].apply(a, s), i = n === a
                        }), i || e.inArray(t, ["destroy", "hide", "show", "toggle"]) > -1 ? this : n;
                        throw new TypeError("Invalid arguments for DateTimePicker: " + t)
                    }, e.fn.datetimepicker.defaults = {
                        timeZone: "",
                        format: !1,
                        dayViewHeaderFormat: "MMMM YYYY",
                        extraFormats: !1,
                        stepping: 1,
                        minDate: !1,
                        maxDate: !1,
                        useCurrent: !0,
                        collapse: !0,
                        locale: t.locale(),
                        defaultDate: !1,
                        disabledDates: !1,
                        enabledDates: !1,
                        icons: {
                            time: "iconfont icon-time",
                            date: "iconfont icon-rili",
                            up: "iconfont icon-up",
                            down: "iconfont icon-down",
                            previous: "iconfont icon-prev",
                            next: "iconfont icon-next",
                            today: "iconfont glyphicon-screenshot",
                            clear: "iconfont icon-trash",
                            close: "iconfont icon-remove"
                        },
                        tooltips: {
                            today: "Go to today",
                            clear: "Clear selection",
                            close: "Close the picker",
                            selectMonth: "Select Month",
                            prevMonth: "Previous Month",
                            nextMonth: "Next Month",
                            selectYear: "Select Year",
                            prevYear: "Previous Year",
                            nextYear: "Next Year",
                            selectDecade: "Select Decade",
                            prevDecade: "Previous Decade",
                            nextDecade: "Next Decade",
                            prevCentury: "Previous Century",
                            nextCentury: "Next Century",
                            pickHour: "Pick Hour",
                            incrementHour: "Increment Hour",
                            decrementHour: "Decrement Hour",
                            pickMinute: "Pick Minute",
                            incrementMinute: "Increment Minute",
                            decrementMinute: "Decrement Minute",
                            pickSecond: "Pick Second",
                            incrementSecond: "Increment Second",
                            decrementSecond: "Decrement Second",
                            togglePeriod: "Toggle Period",
                            selectTime: "Select Time"
                        },
                        useStrict: !1,
                        sideBySide: !1,
                        daysOfWeekDisabled: !1,
                        calendarWeeks: !1,
                        viewMode: "days",
                        toolbarPlacement: "default",
                        showTodayButton: !1,
                        showClear: !1,
                        showClose: !1,
                        widgetPositioning: {
                            horizontal: "auto",
                            vertical: "auto"
                        },
                        widgetParent: null,
                        ignoreReadonly: !1,
                        keepOpen: !1,
                        focusOnShow: !0,
                        inline: !1,
                        keepInvalid: !1,
                        datepickerInput: ".datepickerinput",
                        keyBinds: {
                            up: function(e) {
                                if (e) {
                                    var t = this.date() || this.getMoment();
                                    e.find(".datepicker").is(":visible") ? this.date(t.clone().subtract(7, "d")) : this.date(t.clone().add(this.stepping(), "m"))
                                }
                            },
                            down: function(e) {
                                if (e) {
                                    var t = this.date() || this.getMoment();
                                    e.find(".datepicker").is(":visible") ? this.date(t.clone().add(7, "d")) : this.date(t.clone().subtract(this.stepping(), "m"))
                                } else this.show()
                            },
                            "control up": function(e) {
                                if (e) {
                                    var t = this.date() || this.getMoment();
                                    e.find(".datepicker").is(":visible") ? this.date(t.clone().subtract(1, "y")) : this.date(t.clone().add(1, "h"))
                                }
                            },
                            "control down": function(e) {
                                if (e) {
                                    var t = this.date() || this.getMoment();
                                    e.find(".datepicker").is(":visible") ? this.date(t.clone().add(1, "y")) : this.date(t.clone().subtract(1, "h"))
                                }
                            },
                            left: function(e) {
                                if (e) {
                                    var t = this.date() || this.getMoment();
                                    e.find(".datepicker").is(":visible") && this.date(t.clone().subtract(1, "d"))
                                }
                            },
                            right: function(e) {
                                if (e) {
                                    var t = this.date() || this.getMoment();
                                    e.find(".datepicker").is(":visible") && this.date(t.clone().add(1, "d"))
                                }
                            },
                            pageUp: function(e) {
                                if (e) {
                                    var t = this.date() || this.getMoment();
                                    e.find(".datepicker").is(":visible") && this.date(t.clone().subtract(1, "M"))
                                }
                            },
                            pageDown: function(e) {
                                if (e) {
                                    var t = this.date() || this.getMoment();
                                    e.find(".datepicker").is(":visible") && this.date(t.clone().add(1, "M"))
                                }
                            },
                            enter: function() {
                                this.hide()
                            },
                            escape: function() {
                                this.hide()
                            },
                            "control space": function(e) {
                                e && e.find(".timepicker").is(":visible") && e.find('.btn[data-action="togglePeriod"]').click()
                            },
                            t: function() {
                                this.date(this.getMoment())
                            },
                            delete: function() {
                                this.clear()
                            }
                        },
                        debug: !1,
                        allowInputToggle: !1,
                        disabledTimeIntervals: !1,
                        disabledHours: !1,
                        enabledHours: !1,
                        viewDate: !1
                    }, e.fn.datetimepicker
                }), i("undefined" != typeof datetimepicker ? datetimepicker : window.datetimepicker)
            }).call(a, void 0, void 0, void 0, void 0, function(e) {
                t.exports = e
            })
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {}],
    2: [function(e, t, a) {
        ! function(e, t) {
            function a(a, n, s) {
                var i = a.children(n.headerTag),
                    r = a.children(n.bodyTag);
                i.length > r.length ? j(F, "contents") : i.length < r.length && j(F, "titles");
                var o = n.startIndex;
                if (s.stepCount = i.length, n.saveState && e.cookie) {
                    var d = e.cookie(O + _(a)),
                        u = parseInt(d, 0);
                    !isNaN(u) && u < s.stepCount && (o = u)
                }
                s.currentIndex = o, i.each(function(n) {
                    var s = e(this),
                        i = r.eq(n),
                        o = i.data("mode"),
                        d = null == o ? I.html : m(I, /^\s*$/.test(o) || isNaN(o) ? o : parseInt(o, 0)),
                        u = d === I.html || i.data("url") === t ? "" : i.data("url"),
                        _ = d !== I.html && "1" === i.data("loaded"),
                        c = e.extend({}, R, {
                            title: s.html(),
                            content: d === I.html ? i.html() : "",
                            contentUrl: u,
                            contentMode: d,
                            contentLoaded: _
                        });
                    ! function(e, t) {
                        l(e).push(t)
                    }(a, c)
                })
            }

            function n(e, t) {
                var a = e.find(".steps li").eq(t.currentIndex);
                e.triggerHandler("finishing", [t.currentIndex]) ? (a.addClass("done").removeClass("error"), e.triggerHandler("finished", [t.currentIndex])) : a.addClass("error")
            }

            function s(e) {
                var t = e.data("eventNamespace");
                return null == t && (t = "." + _(e), e.data("eventNamespace", t)), t
            }

            function i(e, t) {
                var a = _(e);
                return e.find("#" + a + E + t)
            }

            function r(e, t) {
                var a = _(e);
                return e.find("#" + a + W + t)
            }

            function o(e) {
                return e.data("options")
            }

            function d(e) {
                return e.data("state")
            }

            function l(e) {
                return e.data("steps")
            }

            function u(e, t) {
                var a = l(e);
                return (t < 0 || t >= a.length) && j(z), a[t]
            }

            function _(e) {
                var t = e.data("uid");
                return null == t && (null == (t = e._id()) && (t = "steps-uid-".concat(P), e._id(t)), P++, e.data("uid", t)), t
            }

            function m(e, a) {
                if (C("enumType", e), C("keyOrValue", a), "string" == typeof a) {
                    var n = e[a];
                    return n === t && j("The enum key '{0}' does not exist.", a), n
                }
                if ("number" == typeof a) {
                    for (var s in e)
                        if (e[s] === a) return a;
                    j("Invalid enum value '{0}'.", a)
                } else j("Invalid key or value type.")
            }

            function c(e, t, a) {
                return L(e, t, a, function(e, t) {
                    return e.currentIndex + t
                }(a, 1))
            }

            function h(e, t, a) {
                return L(e, t, a, function(e, t) {
                    return e.currentIndex - t
                }(a, 1))
            }

            function p(t, a, n, s) {
                if ((s < 0 || s >= n.stepCount) && j(z), !(a.forceMoveForward && s < n.currentIndex)) {
                    var i = n.currentIndex;
                    return t.triggerHandler("stepChanging", [n.currentIndex, s]) ? (n.currentIndex = s, x(t, a, n), w(t, a, n, i), v(t, a, n), g(t, a, n), function(t, a, n, s, i, r) {
                        var o = t.find(".content > .body"),
                            l = m(N, a.transitionEffect),
                            u = a.transitionEffectSpeed,
                            _ = o.eq(s),
                            c = o.eq(i);
                        switch (l) {
                            case N.fade:
                            case N.slide:
                                var h = l === N.fade ? "fadeOut" : "slideUp",
                                    p = l === N.fade ? "fadeIn" : "slideDown";
                                n.transitionElement = _, c[h](u, function() {
                                    var t = e(this)._showAria(!1).parent().parent(),
                                        a = d(t);
                                    a.transitionElement && (a.transitionElement[p](u, function() {
                                        e(this)._showAria()
                                    }).promise().done(r), a.transitionElement = null)
                                });
                                break;
                            case N.slideLeft:
                                var f = c.outerWidth(!0),
                                    M = s > i ? -f : f,
                                    y = s > i ? f : -f;
                                e.when(c.animate({
                                    left: M
                                }, u, function() {
                                    e(this)._showAria(!1)
                                }), _.css("left", y + "px")._showAria().animate({
                                    left: 0
                                }, u)).done(r);
                                break;
                            default:
                                e.when(c._showAria(!1), _._showAria()).done(r)
                        }
                    }(t, a, n, s, i, function() {
                        t.triggerHandler("stepChanged", [s, i])
                    })) : t.find(".steps li").eq(i).addClass("error"), !0
                }
            }

            function f(t) {
                var n = e.extend(!0, {}, J, t);
                return this.each(function() {
                    var t = e(this),
                        r = {
                            currentIndex: n.startIndex,
                            currentStep: null,
                            stepCount: 0,
                            transitionElement: null
                        };
                    t.data("options", n), t.data("state", r), t.data("steps", []), a(t, n, r),
                        function(t, a, n) {
                            var s = '<{0} class="{1}">{2}</{0}>',
                                i = m($, a.stepsOrientation) === $.vertical ? " vertical" : "",
                                r = e(s.format(a.contentContainerTag, "content " + a.clearFixCssClass, t.html())),
                                o = e(s.format(a.stepsContainerTag, "steps " + a.clearFixCssClass, '<ul role="tablist"></ul>')),
                                d = r.children(a.headerTag),
                                l = r.children(a.bodyTag);
                            t.attr("role", "application").empty().append(o).append(r).addClass(a.cssClass + " " + a.clearFixCssClass + i), l.each(function(a) {
                                    b(t, n, e(this), a)
                                }), d.each(function(s) {
                                    S(t, a, n, e(this), s)
                                }), w(t, a, n),
                                function(e, t, a) {
                                    if (t.enablePagination) {
                                        var n = '<li><a href="#{0}" role="menuitem">{1}</a></li>',
                                            s = "";
                                        t.forceMoveForward || (s += n.format("previous", t.labels.previous)), s += n.format("next", t.labels.next), t.enableFinishButton && (s += n.format("finish", t.labels.finish)), t.enableCancelButton && (s += n.format("cancel", t.labels.cancel)), e.append('<{0} class="actions {1}"><ul role="menu" aria-label="{2}">{3}</ul></{0}>'.format(t.actionContainerTag, t.clearFixCssClass, t.labels.pagination, s)), v(e, t, a), g(e, t, a)
                                    }
                                }(t, a, n)
                        }(t, n, r),
                        function(e, t) {
                            var a = s(e);
                            e.bind("canceled" + a, t.onCanceled), e.bind("contentLoaded" + a, t.onContentLoaded), e.bind("finishing" + a, t.onFinishing), e.bind("finished" + a, t.onFinished), e.bind("init" + a, t.onInit), e.bind("stepChanging" + a, t.onStepChanging), e.bind("stepChanged" + a, t.onStepChanged), t.enableKeyNavigation && e.bind("keyup" + a, y);
                            e.find(".actions a").bind("click" + a, Y)
                        }(t, n), n.autoFocus && 0 === P && i(t, n.startIndex).focus(), t.triggerHandler("init", [n.startIndex])
                })
            }

            function M(t, a, n, s, i) {
                (s < 0 || s > n.stepCount) && j(z),
                    function(e, t, a) {
                        l(e).splice(t, 0, a)
                    }(t, s, i = e.extend({}, R, i)), n.currentIndex !== n.stepCount && n.currentIndex >= s && (n.currentIndex++, x(t, a, n)), n.stepCount++;
                var o = t.find(".content"),
                    d = e("<{0}>{1}</{0}>".format(a.headerTag, i.title)),
                    u = e("<{0}></{0}>".format(a.bodyTag));
                return null != i.contentMode && i.contentMode !== I.html || u.html(i.content), 0 === s ? o.prepend(u).prepend(d) : r(t, s - 1).after(u).after(d), b(t, n, u, s), S(t, a, n, d, s), k(t, a, n, s), s === n.currentIndex && w(t, a, n), v(t, a, n), t
            }

            function y(t) {
                var a = e(this),
                    n = o(a),
                    s = d(a);
                if (n.suppressPaginationOnFocus && a.find(":focus").is(":input")) return t.preventDefault(), !1;
                var i = 37,
                    r = 39;
                t.keyCode === i ? (t.preventDefault(), h(a, n, s)) : t.keyCode === r && (t.preventDefault(), c(a, n, s))
            }

            function g(t, a, n) {
                if (n.stepCount > 0) {
                    var s = n.currentIndex,
                        i = u(t, s);
                    if (!a.enableContentCache || !i.contentLoaded) switch (m(I, i.contentMode)) {
                        case I.iframe:
                            t.find(".content > .body").eq(n.currentIndex).empty().html('<iframe src="' + i.contentUrl + '" frameborder="0" scrolling="no" />').data("loaded", "1");
                            break;
                        case I.async:
                            var o = r(t, s)._aria("busy", "true").empty().append(T(a.loadingTemplate, {
                                text: a.labels.loading
                            }));
                            e.ajax({
                                url: i.contentUrl,
                                cache: !1
                            }).done(function(e) {
                                o.empty().html(e)._aria("busy", "false").data("loaded", "1"), t.triggerHandler("contentLoaded", [s])
                            })
                    }
                }
            }

            function L(e, t, a, n) {
                var s = a.currentIndex;
                if (n >= 0 && n < a.stepCount && !(t.forceMoveForward && n < a.currentIndex)) {
                    var r = i(e, n),
                        o = r.parent(),
                        d = o.hasClass("disabled");
                    return o._enableAria(), r.click(), s !== a.currentIndex || !d || (o._enableAria(!1), !1)
                }
                return !1
            }

            function Y(t) {
                t.preventDefault();
                var a = e(this),
                    s = a.parent().parent().parent().parent(),
                    i = o(s),
                    r = d(s),
                    l = a.attr("href");
                switch (l.substring(l.lastIndexOf("#") + 1)) {
                    case "cancel":
                        ! function(e) {
                            e.triggerHandler("canceled")
                        }(s);
                        break;
                    case "finish":
                        n(s, r);
                        break;
                    case "next":
                        c(s, i, r);
                        break;
                    case "previous":
                        h(s, i, r)
                }
            }

            function v(e, t, a) {
                if (t.enablePagination) {
                    var n = e.find(".actions a[href$='#finish']").parent(),
                        s = e.find(".actions a[href$='#next']").parent();
                    if (!t.forceMoveForward) {
                        e.find(".actions a[href$='#previous']").parent()._enableAria(a.currentIndex > 0)
                    }
                    t.enableFinishButton && t.showFinishButtonAlways ? (n._enableAria(a.stepCount > 0), s._enableAria(a.stepCount > 1 && a.stepCount > a.currentIndex + 1)) : (n._showAria(t.enableFinishButton && a.stepCount === a.currentIndex + 1), s._showAria(0 === a.stepCount || a.stepCount > a.currentIndex + 1)._enableAria(a.stepCount > a.currentIndex + 1 || !t.enableFinishButton))
                }
            }

            function w(t, a, n, s) {
                var r = i(t, n.currentIndex),
                    o = e('<span class="current-info audible">' + a.labels.current + " </span>"),
                    d = t.find(".content > .title");
                if (null != s) {
                    var l = i(t, s);
                    l.parent().addClass("done").removeClass("error")._selectAria(!1), d.eq(s).removeClass("current").next(".body").removeClass("current"), o = l.find(".current-info"), r.focus()
                }
                r.prepend(o).parent()._selectAria().removeClass("done")._enableAria(), d.eq(n.currentIndex).addClass("current").next(".body").addClass("current")
            }

            function k(e, t, a, n) {
                for (var s = _(e), i = n; i < a.stepCount; i++) {
                    var r = s + E + i,
                        o = s + W + i,
                        d = s + A + i,
                        l = e.find(".title").eq(i)._id(d);
                    e.find(".steps a").eq(i)._id(r)._aria("controls", o).attr("href", "#" + d).html(T(t.titleTemplate, {
                        index: i + 1,
                        title: l.html()
                    })), e.find(".body").eq(i)._id(o)._aria("labelledby", d)
                }
            }

            function D(e, t, a, n) {
                return !(n < 0 || n >= a.stepCount || a.currentIndex === n) && (function(e, t) {
                    l(e).splice(t, 1)
                }(e, n), a.currentIndex > n && (a.currentIndex--, x(e, t, a)), a.stepCount--, function(e, t) {
                    var a = _(e);
                    return e.find("#" + a + A + t)
                }(e, n).remove(), r(e, n).remove(), i(e, n).parent().remove(), 0 === n && e.find(".steps li").first().addClass("first"), n === a.stepCount && e.find(".steps li").eq(n).addClass("last"), k(e, t, a, n), v(e, t, a), !0)
            }

            function b(e, t, a, n) {
                var s = _(e),
                    i = s + W + n,
                    r = s + A + n;
                a._id(i).attr("role", "tabpanel")._aria("labelledby", r).addClass("body")._showAria(t.currentIndex === n)
            }

            function T(e, a) {
                for (var n = e.match(/#([a-z]*)#/gi), s = 0; s < n.length; s++) {
                    var i = n[s],
                        r = i.substring(1, i.length - 1);
                    a[r] === t && j("The key '{0}' does not exist in the substitute collection!", r), e = e.replace(i, a[r])
                }
                return e
            }

            function S(t, a, n, i, r) {
                var o = _(t),
                    d = o + E + r,
                    l = o + W + r,
                    u = o + A + r,
                    m = t.find(".steps > ul"),
                    c = T(a.titleTemplate, {
                        index: r + 1,
                        title: i.html()
                    }),
                    h = e('<li role="tab"><a id="' + d + '" href="#' + u + '" aria-controls="' + l + '">' + c + "</a></li>");
                h._enableAria(a.enableAllSteps || n.currentIndex > r), n.currentIndex > r && h.addClass("done"), i._id(u).attr("tabindex", "-1").addClass("title"), 0 === r ? m.prepend(h) : m.find("li").eq(r - 1).after(h), 0 === r && m.find("li").removeClass("first").eq(r).addClass("first"), r === n.stepCount - 1 && m.find("li").removeClass("last").eq(r).addClass("last"), h.children("a").bind("click" + s(t), H)
            }

            function x(t, a, n) {
                a.saveState && e.cookie && e.cookie(O + _(t), n.currentIndex)
            }

            function H(t) {
                t.preventDefault();
                var a = e(this),
                    n = a.parent().parent().parent().parent(),
                    s = o(n),
                    r = d(n),
                    l = r.currentIndex;
                if (a.parent().is(":not(.disabled):not(.current)")) {
                    var u = a.attr("href");
                    p(n, s, r, parseInt(u.substring(u.lastIndexOf("-") + 1), 0))
                }
                if (l === r.currentIndex) return i(n, l).focus(), !1
            }

            function j(e) {
                throw arguments.length > 1 && (e = e.format(Array.prototype.slice.call(arguments, 1))), new Error(e)
            }

            function C(e, t) {
                null == t && j("The argument '{0}' is null or undefined.", e)
            }
            e.fn.extend({
                _aria: function(e, t) {
                    return this.attr("aria-" + e, t)
                },
                _removeAria: function(e) {
                    return this.removeAttr("aria-" + e)
                },
                _enableAria: function(e) {
                    return null == e || e ? this.removeClass("disabled")._aria("disabled", "false") : this.addClass("disabled")._aria("disabled", "true")
                },
                _showAria: function(e) {
                    return null == e || e ? this.show()._aria("hidden", "false") : this.hide()._aria("hidden", "true")
                },
                _selectAria: function(e) {
                    return null == e || e ? this.addClass("current")._aria("selected", "true") : this.removeClass("current")._aria("selected", "false")
                },
                _id: function(e) {
                    return e ? this.attr("id", e) : this.attr("id")
                }
            }), String.prototype.format || (String.prototype.format = function() {
                for (var t = 1 === arguments.length && e.isArray(arguments[0]) ? arguments[0] : arguments, a = this, n = 0; n < t.length; n++) {
                    var s = new RegExp("\\{" + n + "\\}", "gm");
                    a = a.replace(s, t[n])
                }
                return a
            });
            var P = 0,
                O = "jQu3ry_5teps_St@te_",
                E = "-t-",
                W = "-p-",
                A = "-h-",
                z = "Index out of range.",
                F = "One or more corresponding step {0} are missing.";
            e.fn.steps = function(t) {
                return e.fn.steps[t] ? e.fn.steps[t].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof t && t ? void e.error("Method " + t + " does not exist on jQuery.steps") : f.apply(this, arguments)
            }, e.fn.steps.add = function(e) {
                var t = d(this);
                return M(this, o(this), t, t.stepCount, e)
            }, e.fn.steps.destroy = function() {
                return function(t, a) {
                    var n = s(t);
                    t.unbind(n).removeData("uid").removeData("options").removeData("state").removeData("steps").removeData("eventNamespace").find(".actions a").unbind(n), t.removeClass(a.clearFixCssClass + " vertical");
                    var i = t.find(".content > *");
                    i.removeData("loaded").removeData("mode").removeData("url"), i.removeAttr("id").removeAttr("role").removeAttr("tabindex").removeAttr("class").removeAttr("style")._removeAria("labelledby")._removeAria("hidden"), t.find(".content > [data-mode='async'],.content > [data-mode='iframe']").empty();
                    var r = e('<{0} class="{1}"></{0}>'.format(t.get(0).tagName, t.attr("class"))),
                        o = t._id();
                    return null != o && "" !== o && r._id(o), r.html(t.find(".content").html()), t.after(r), t.remove(), r
                }(this, o(this))
            }, e.fn.steps.finish = function() {
                n(this, d(this))
            }, e.fn.steps.getCurrentIndex = function() {
                return d(this).currentIndex
            }, e.fn.steps.getCurrentStep = function() {
                return u(this, d(this).currentIndex)
            }, e.fn.steps.getStep = function(e) {
                return u(this, e)
            }, e.fn.steps.insert = function(e, t) {
                return M(this, o(this), d(this), e, t)
            }, e.fn.steps.next = function() {
                return c(this, o(this), d(this))
            }, e.fn.steps.previous = function() {
                return h(this, o(this), d(this))
            }, e.fn.steps.remove = function(e) {
                return D(this, o(this), d(this), e)
            }, e.fn.steps.setStep = function(e, t) {
                throw new Error("Not yet implemented!")
            }, e.fn.steps.skip = function(e) {
                throw new Error("Not yet implemented!")
            };
            var I = e.fn.steps.contentMode = {
                    html: 0,
                    iframe: 1,
                    async: 2
                },
                $ = e.fn.steps.stepsOrientation = {
                    horizontal: 0,
                    vertical: 1
                },
                N = e.fn.steps.transitionEffect = {
                    none: 0,
                    fade: 1,
                    slide: 2,
                    slideLeft: 3
                },
                R = e.fn.steps.stepModel = {
                    title: "",
                    content: "",
                    contentUrl: "",
                    contentMode: I.html,
                    contentLoaded: !1
                },
                J = e.fn.steps.defaults = {
                    headerTag: "h1",
                    bodyTag: "div",
                    contentContainerTag: "div",
                    actionContainerTag: "div",
                    stepsContainerTag: "div",
                    cssClass: "wizard",
                    clearFixCssClass: "clearfix",
                    stepsOrientation: $.horizontal,
                    titleTemplate: '<span class="number">#index#.</span> #title#',
                    loadingTemplate: '<span class="spinner"></span> #text#',
                    autoFocus: !1,
                    enableAllSteps: !1,
                    enableKeyNavigation: !0,
                    enablePagination: !0,
                    suppressPaginationOnFocus: !0,
                    enableContentCache: !0,
                    enableCancelButton: !1,
                    enableFinishButton: !0,
                    preloadContent: !1,
                    showFinishButtonAlways: !1,
                    forceMoveForward: !1,
                    saveState: !1,
                    startIndex: 0,
                    transitionEffect: N.none,
                    transitionEffectSpeed: 200,
                    onStepChanging: function(e, t, a) {
                        return !0
                    },
                    onStepChanged: function(e, t, a) {},
                    onCanceled: function(e) {},
                    onFinishing: function(e, t) {
                        return !0
                    },
                    onFinished: function(e, t) {},
                    onContentLoaded: function(e, t) {},
                    onInit: function(e, t) {},
                    labels: {
                        cancel: "Cancel",
                        current: "current step:",
                        pagination: "Pagination",
                        finish: "Finish",
                        next: "Next",
                        previous: "Previous",
                        loading: "Loading ..."
                    }
                }
        }(jQuery)
    }, {}],
    3: [function(e, t, a) {
        (function(a) {
            var n = e;
            (function(e, t, a, s, i) {
                ! function(e) {
                    "function" == typeof s && s.amd ? s(["jquery"], e) : e("object" == typeof t ? n("jquery") : window.jQuery || window.Zepto)
                }(function(e) {
                    var t, a, n, s, i, r, o = "mfp-prevent-close",
                        d = function() {},
                        l = !!window.jQuery,
                        u = e(window),
                        _ = function(e, a) {
                            t.ev.on("mfp" + e + ".mfp", a)
                        },
                        m = function(t, a, n, s) {
                            var i = document.createElement("div");
                            return i.className = "mfp-" + t, n && (i.innerHTML = n), s ? a && a.appendChild(i) : (i = e(i), a && i.appendTo(a)), i
                        },
                        c = function(a, n) {
                            t.ev.triggerHandler("mfp" + a, n), t.st.callbacks && (a = a.charAt(0).toLowerCase() + a.slice(1), t.st.callbacks[a] && t.st.callbacks[a].apply(t, e.isArray(n) ? n : [n]))
                        },
                        h = function(a) {
                            return a === r && t.currTemplate.closeBtn || (t.currTemplate.closeBtn = e(t.st.closeMarkup.replace("%title%", t.st.tClose)), r = a), t.currTemplate.closeBtn
                        },
                        p = function() {
                            e.magnificPopup.instance || ((t = new d).init(), e.magnificPopup.instance = t)
                        };
                    d.prototype = {
                        constructor: d,
                        init: function() {
                            var a = navigator.appVersion;
                            t.isLowIE = t.isIE8 = document.all && !document.addEventListener, t.isAndroid = /android/gi.test(a), t.isIOS = /iphone|ipad|ipod/gi.test(a), t.supportsTransition = function() {
                                var e = document.createElement("p").style,
                                    t = ["ms", "O", "Moz", "Webkit"];
                                if (void 0 !== e.transition) return !0;
                                for (; t.length;)
                                    if (t.pop() + "Transition" in e) return !0;
                                return !1
                            }(), t.probablyMobile = t.isAndroid || t.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), n = e(document), t.popupsCache = {}
                        },
                        open: function(a) {
                            var s;
                            if (!1 === a.isObj) {
                                t.items = a.items.toArray(), t.index = 0;
                                var r, o = a.items;
                                for (s = 0; s < o.length; s++)
                                    if ((r = o[s]).parsed && (r = r.el[0]), r === a.el[0]) {
                                        t.index = s;
                                        break
                                    }
                            } else t.items = e.isArray(a.items) ? a.items : [a.items], t.index = a.index || 0; {
                                if (!t.isOpen) {
                                    t.types = [], i = "", a.mainEl && a.mainEl.length ? t.ev = a.mainEl.eq(0) : t.ev = n, a.key ? (t.popupsCache[a.key] || (t.popupsCache[a.key] = {}), t.currTemplate = t.popupsCache[a.key]) : t.currTemplate = {}, t.st = e.extend(!0, {}, e.magnificPopup.defaults, a), t.fixedContentPos = "auto" === t.st.fixedContentPos ? !t.probablyMobile : t.st.fixedContentPos, t.st.modal && (t.st.closeOnContentClick = !1, t.st.closeOnBgClick = !1, t.st.showCloseBtn = !1, t.st.enableEscapeKey = !1), t.bgOverlay || (t.bgOverlay = m("bg").on("click.mfp", function() {
                                        t.close()
                                    }), t.wrap = m("wrap").attr("tabindex", -1).on("click.mfp", function(e) {
                                        t._checkIfClose(e.target) && t.close()
                                    }), t.container = m("container", t.wrap)), t.contentContainer = m("content"), t.st.preloader && (t.preloader = m("preloader", t.container, t.st.tLoading));
                                    var d = e.magnificPopup.modules;
                                    for (s = 0; s < d.length; s++) {
                                        var l = d[s];
                                        l = l.charAt(0).toUpperCase() + l.slice(1), t["init" + l].call(t)
                                    }
                                    c("BeforeOpen"), t.st.showCloseBtn && (t.st.closeBtnInside ? (_("MarkupParse", function(e, t, a, n) {
                                        a.close_replaceWith = h(n.type)
                                    }), i += " mfp-close-btn-in") : t.wrap.append(h())), t.st.alignTop && (i += " mfp-align-top"), t.fixedContentPos ? t.wrap.css({
                                        overflow: t.st.overflowY,
                                        overflowX: "hidden",
                                        overflowY: t.st.overflowY
                                    }) : t.wrap.css({
                                        top: u.scrollTop(),
                                        position: "absolute"
                                    }), (!1 === t.st.fixedBgPos || "auto" === t.st.fixedBgPos && !t.fixedContentPos) && t.bgOverlay.css({
                                        height: n.height(),
                                        position: "absolute"
                                    }), t.st.enableEscapeKey && n.on("keyup.mfp", function(e) {
                                        27 === e.keyCode && t.close()
                                    }), u.on("resize.mfp", function() {
                                        t.updateSize()
                                    }), t.st.closeOnContentClick || (i += " mfp-auto-cursor"), i && t.wrap.addClass(i);
                                    var p = t.wH = u.height(),
                                        f = {};
                                    if (t.fixedContentPos && t._hasScrollBar(p)) {
                                        var M = t._getScrollbarSize();
                                        M && (f.marginRight = M)
                                    }
                                    t.fixedContentPos && (t.isIE7 ? e("body, html").css("overflow", "hidden") : f.overflow = "hidden");
                                    var y = t.st.mainClass;
                                    return t.isIE7 && (y += " mfp-ie7"), y && t._addClassToMFP(y), t.updateItemHTML(), c("BuildControls"), e("html").css(f), t.bgOverlay.add(t.wrap).prependTo(t.st.prependTo || e(document.body)), t._lastFocusedEl = document.activeElement, setTimeout(function() {
                                        t.content ? (t._addClassToMFP("mfp-ready"), t._setFocus()) : t.bgOverlay.addClass("mfp-ready"), n.on("focusin.mfp", t._onFocusIn)
                                    }, 16), t.isOpen = !0, t.updateSize(p), c("Open"), a
                                }
                                t.updateItemHTML()
                            }
                        },
                        close: function() {
                            t.isOpen && (c("BeforeClose"), t.isOpen = !1, t.st.removalDelay && !t.isLowIE && t.supportsTransition ? (t._addClassToMFP("mfp-removing"), setTimeout(function() {
                                t._close()
                            }, t.st.removalDelay)) : t._close())
                        },
                        _close: function() {
                            c("Close");
                            var a = "mfp-removing mfp-ready ";
                            if (t.bgOverlay.detach(), t.wrap.detach(), t.container.empty(), t.st.mainClass && (a += t.st.mainClass + " "), t._removeClassFromMFP(a), t.fixedContentPos) {
                                var s = {
                                    marginRight: ""
                                };
                                t.isIE7 ? e("body, html").css("overflow", "") : s.overflow = "", e("html").css(s)
                            }
                            n.off("keyup.mfp focusin.mfp"), t.ev.off(".mfp"), t.wrap.attr("class", "mfp-wrap").removeAttr("style"), t.bgOverlay.attr("class", "mfp-bg"), t.container.attr("class", "mfp-container"), !t.st.showCloseBtn || t.st.closeBtnInside && !0 !== t.currTemplate[t.currItem.type] || t.currTemplate.closeBtn && t.currTemplate.closeBtn.detach(), t.st.autoFocusLast && t._lastFocusedEl && e(t._lastFocusedEl).focus(), t.currItem = null, t.content = null, t.currTemplate = null, t.prevHeight = 0, c("AfterClose")
                        },
                        updateSize: function(e) {
                            if (t.isIOS) {
                                var a = document.documentElement.clientWidth / window.innerWidth,
                                    n = window.innerHeight * a;
                                t.wrap.css("height", n), t.wH = n
                            } else t.wH = e || u.height();
                            t.fixedContentPos || t.wrap.css("height", t.wH), c("Resize")
                        },
                        updateItemHTML: function() {
                            var a = t.items[t.index];
                            t.contentContainer.detach(), t.content && t.content.detach(), a.parsed || (a = t.parseEl(t.index));
                            var n = a.type;
                            if (c("BeforeChange", [t.currItem ? t.currItem.type : "", n]), t.currItem = a, !t.currTemplate[n]) {
                                var i = !!t.st[n] && t.st[n].markup;
                                c("FirstMarkupParse", i), t.currTemplate[n] = !i || e(i)
                            }
                            s && s !== a.type && t.container.removeClass("mfp-" + s + "-holder");
                            var r = t["get" + n.charAt(0).toUpperCase() + n.slice(1)](a, t.currTemplate[n]);
                            t.appendContent(r, n), a.preloaded = !0, c("Change", a), s = a.type, t.container.prepend(t.contentContainer), c("AfterChange")
                        },
                        appendContent: function(e, a) {
                            t.content = e, e ? t.st.showCloseBtn && t.st.closeBtnInside && !0 === t.currTemplate[a] ? t.content.find(".mfp-close").length || t.content.append(h()) : t.content = e : t.content = "", c("BeforeAppend"), t.container.addClass("mfp-" + a + "-holder"), t.contentContainer.append(t.content)
                        },
                        parseEl: function(a) {
                            var n, s = t.items[a];
                            if (s.tagName ? s = {
                                    el: e(s)
                                } : (n = s.type, s = {
                                    data: s,
                                    src: s.src
                                }), s.el) {
                                for (var i = t.types, r = 0; r < i.length; r++)
                                    if (s.el.hasClass("mfp-" + i[r])) {
                                        n = i[r];
                                        break
                                    }
                                s.src = s.el.attr("data-mfp-src"), s.src || (s.src = s.el.attr("href"))
                            }
                            return s.type = n || t.st.type || "inline", s.index = a, s.parsed = !0, t.items[a] = s, c("ElementParse", s), t.items[a]
                        },
                        addGroup: function(e, a) {
                            var n = function(n) {
                                n.mfpEl = this, t._openClick(n, e, a)
                            };
                            a || (a = {});
                            var s = "click.magnificPopup";
                            a.mainEl = e, a.items ? (a.isObj = !0, e.off(s).on(s, n)) : (a.isObj = !1, a.delegate ? e.off(s).on(s, a.delegate, n) : (a.items = e, e.off(s).on(s, n)))
                        },
                        _openClick: function(a, n, s) {
                            if ((void 0 !== s.midClick ? s.midClick : e.magnificPopup.defaults.midClick) || !(2 === a.which || a.ctrlKey || a.metaKey || a.altKey || a.shiftKey)) {
                                var i = void 0 !== s.disableOn ? s.disableOn : e.magnificPopup.defaults.disableOn;
                                if (i)
                                    if (e.isFunction(i)) {
                                        if (!i.call(t)) return !0
                                    } else if (u.width() < i) return !0;
                                a.type && (a.preventDefault(), t.isOpen && a.stopPropagation()), s.el = e(a.mfpEl), s.delegate && (s.items = n.find(s.delegate)), t.open(s)
                            }
                        },
                        updateStatus: function(e, n) {
                            if (t.preloader) {
                                a !== e && t.container.removeClass("mfp-s-" + a), n || "loading" !== e || (n = t.st.tLoading);
                                var s = {
                                    status: e,
                                    text: n
                                };
                                c("UpdateStatus", s), e = s.status, n = s.text, t.preloader.html(n), t.preloader.find("a").on("click", function(e) {
                                    e.stopImmediatePropagation()
                                }), t.container.addClass("mfp-s-" + e), a = e
                            }
                        },
                        _checkIfClose: function(a) {
                            if (!e(a).hasClass(o)) {
                                var n = t.st.closeOnContentClick,
                                    s = t.st.closeOnBgClick;
                                if (n && s) return !0;
                                if (!t.content || e(a).hasClass("mfp-close") || t.preloader && a === t.preloader[0]) return !0;
                                if (a === t.content[0] || e.contains(t.content[0], a)) {
                                    if (n) return !0
                                } else if (s && e.contains(document, a)) return !0;
                                return !1
                            }
                        },
                        _addClassToMFP: function(e) {
                            t.bgOverlay.addClass(e), t.wrap.addClass(e)
                        },
                        _removeClassFromMFP: function(e) {
                            this.bgOverlay.removeClass(e), t.wrap.removeClass(e)
                        },
                        _hasScrollBar: function(e) {
                            return (t.isIE7 ? n.height() : document.body.scrollHeight) > (e || u.height())
                        },
                        _setFocus: function() {
                            (t.st.focus ? t.content.find(t.st.focus).eq(0) : t.wrap).focus()
                        },
                        _onFocusIn: function(a) {
                            if (a.target !== t.wrap[0] && !e.contains(t.wrap[0], a.target)) return t._setFocus(), !1
                        },
                        _parseMarkup: function(t, a, n) {
                            var s;
                            n.data && (a = e.extend(n.data, a)), c("MarkupParse", [t, a, n]), e.each(a, function(a, n) {
                                if (void 0 === n || !1 === n) return !0;
                                if ((s = a.split("_")).length > 1) {
                                    var i = t.find(".mfp-" + s[0]);
                                    if (i.length > 0) {
                                        var r = s[1];
                                        "replaceWith" === r ? i[0] !== n[0] && i.replaceWith(n) : "img" === r ? i.is("img") ? i.attr("src", n) : i.replaceWith(e("<img>").attr("src", n).attr("class", i.attr("class"))) : i.attr(s[1], n)
                                    }
                                } else t.find(".mfp-" + a).html(n)
                            })
                        },
                        _getScrollbarSize: function() {
                            if (void 0 === t.scrollbarSize) {
                                var e = document.createElement("div");
                                e.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(e), t.scrollbarSize = e.offsetWidth - e.clientWidth, document.body.removeChild(e)
                            }
                            return t.scrollbarSize
                        }
                    }, e.magnificPopup = {
                        instance: null,
                        proto: d.prototype,
                        modules: [],
                        open: function(t, a) {
                            return p(), t = t ? e.extend(!0, {}, t) : {}, t.isObj = !0, t.index = a || 0, this.instance.open(t)
                        },
                        close: function() {
                            return e.magnificPopup.instance && e.magnificPopup.instance.close()
                        },
                        registerModule: function(t, a) {
                            a.options && (e.magnificPopup.defaults[t] = a.options), e.extend(this.proto, a.proto), this.modules.push(t)
                        },
                        defaults: {
                            disableOn: 0,
                            key: null,
                            midClick: !1,
                            mainClass: "",
                            preloader: !0,
                            focus: "",
                            closeOnContentClick: !1,
                            closeOnBgClick: !0,
                            closeBtnInside: !0,
                            showCloseBtn: !0,
                            enableEscapeKey: !0,
                            modal: !1,
                            alignTop: !1,
                            removalDelay: 0,
                            prependTo: null,
                            fixedContentPos: "auto",
                            fixedBgPos: "auto",
                            overflowY: "auto",
                            closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
                            tClose: "Close (Esc)",
                            tLoading: "Loading...",
                            autoFocusLast: !0
                        }
                    }, e.fn.magnificPopup = function(a) {
                        p();
                        var n = e(this);
                        if ("string" == typeof a)
                            if ("open" === a) {
                                var s, i = l ? n.data("magnificPopup") : n[0].magnificPopup,
                                    r = parseInt(arguments[1], 10) || 0;
                                i.items ? s = i.items[r] : (s = n, i.delegate && (s = s.find(i.delegate)), s = s.eq(r)), t._openClick({
                                    mfpEl: s
                                }, n, i)
                            } else t.isOpen && t[a].apply(t, Array.prototype.slice.call(arguments, 1));
                        else a = e.extend(!0, {}, a), l ? n.data("magnificPopup", a) : n[0].magnificPopup = a, t.addGroup(n, a);
                        return n
                    };
                    var f, M, y, g = function() {
                        y && (M.after(y.addClass(f)).detach(), y = null)
                    };
                    e.magnificPopup.registerModule("inline", {
                        options: {
                            hiddenClass: "hide",
                            markup: "",
                            tNotFound: "Content not found"
                        },
                        proto: {
                            initInline: function() {
                                t.types.push("inline"), _("Close.inline", function() {
                                    g()
                                })
                            },
                            getInline: function(a, n) {
                                if (g(), a.src) {
                                    var s = t.st.inline,
                                        i = e(a.src);
                                    if (i.length) {
                                        var r = i[0].parentNode;
                                        r && r.tagName && (M || (f = s.hiddenClass, M = m(f), f = "mfp-" + f), y = i.after(M).detach().removeClass(f)), t.updateStatus("ready")
                                    } else t.updateStatus("error", s.tNotFound), i = e("<div>");
                                    return a.inlineElement = i, i
                                }
                                return t.updateStatus("ready"), t._parseMarkup(n, {}, a), n
                            }
                        }
                    });
                    var L, Y = function() {
                            L && e(document.body).removeClass(L)
                        },
                        v = function() {
                            Y(), t.req && t.req.abort()
                        };
                    e.magnificPopup.registerModule("ajax", {
                        options: {
                            settings: null,
                            cursor: "mfp-ajax-cur",
                            tError: '<a href="%url%">The content</a> could not be loaded.'
                        },
                        proto: {
                            initAjax: function() {
                                t.types.push("ajax"), L = t.st.ajax.cursor, _("Close.ajax", v), _("BeforeChange.ajax", v)
                            },
                            getAjax: function(a) {
                                L && e(document.body).addClass(L), t.updateStatus("loading");
                                var n = e.extend({
                                    url: a.src,
                                    success: function(n, s, i) {
                                        var r = {
                                            data: n,
                                            xhr: i
                                        };
                                        c("ParseAjax", r), t.appendContent(e(r.data), "ajax"), a.finished = !0, Y(), t._setFocus(), setTimeout(function() {
                                            t.wrap.addClass("mfp-ready")
                                        }, 16), t.updateStatus("ready"), c("AjaxContentAdded")
                                    },
                                    error: function() {
                                        Y(), a.finished = a.loadError = !0, t.updateStatus("error", t.st.ajax.tError.replace("%url%", a.src))
                                    }
                                }, t.st.ajax.settings);
                                return t.req = e.ajax(n), ""
                            }
                        }
                    });
                    var w;
                    e.magnificPopup.registerModule("image", {
                        options: {
                            markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
                            cursor: "mfp-zoom-out-cur",
                            titleSrc: "title",
                            verticalFit: !0,
                            tError: '<a href="%url%">The image</a> could not be loaded.'
                        },
                        proto: {
                            initImage: function() {
                                var a = t.st.image;
                                t.types.push("image"), _("Open.image", function() {
                                    "image" === t.currItem.type && a.cursor && e(document.body).addClass(a.cursor)
                                }), _("Close.image", function() {
                                    a.cursor && e(document.body).removeClass(a.cursor), u.off("resize.mfp")
                                }), _("Resize.image", t.resizeImage), t.isLowIE && _("AfterChange", t.resizeImage)
                            },
                            resizeImage: function() {
                                var e = t.currItem;
                                if (e && e.img && t.st.image.verticalFit) {
                                    var a = 0;
                                    t.isLowIE && (a = parseInt(e.img.css("padding-top"), 10) + parseInt(e.img.css("padding-bottom"), 10)), e.img.css("max-height", t.wH - a)
                                }
                            },
                            _onImageHasSize: function(e) {
                                e.img && (e.hasSize = !0, w && clearInterval(w), e.isCheckingImgSize = !1, c("ImageHasSize", e), e.imgHidden && (t.content && t.content.removeClass("mfp-loading"), e.imgHidden = !1))
                            },
                            findImageSize: function(e) {
                                var a = 0,
                                    n = e.img[0],
                                    s = function(i) {
                                        w && clearInterval(w), w = setInterval(function() {
                                            n.naturalWidth > 0 ? t._onImageHasSize(e) : (a > 200 && clearInterval(w), 3 === ++a ? s(10) : 40 === a ? s(50) : 100 === a && s(500))
                                        }, i)
                                    };
                                s(1)
                            },
                            getImage: function(a, n) {
                                var s = 0,
                                    i = function() {
                                        a && (a.img[0].complete ? (a.img.off(".mfploader"), a === t.currItem && (t._onImageHasSize(a), t.updateStatus("ready")), a.hasSize = !0, a.loaded = !0, c("ImageLoadComplete")) : ++s < 200 ? setTimeout(i, 100) : r())
                                    },
                                    r = function() {
                                        a && (a.img.off(".mfploader"), a === t.currItem && (t._onImageHasSize(a), t.updateStatus("error", o.tError.replace("%url%", a.src))), a.hasSize = !0, a.loaded = !0, a.loadError = !0)
                                    },
                                    o = t.st.image,
                                    d = n.find(".mfp-img");
                                if (d.length) {
                                    var l = document.createElement("img");
                                    l.className = "mfp-img", a.el && a.el.find("img").length && (l.alt = a.el.find("img").attr("alt")), a.img = e(l).on("load.mfploader", i).on("error.mfploader", r), l.src = a.src, d.is("img") && (a.img = a.img.clone()), (l = a.img[0]).naturalWidth > 0 ? a.hasSize = !0 : l.width || (a.hasSize = !1)
                                }
                                return t._parseMarkup(n, {
                                    title: function(a) {
                                        if (a.data && void 0 !== a.data.title) return a.data.title;
                                        var n = t.st.image.titleSrc;
                                        if (n) {
                                            if (e.isFunction(n)) return n.call(t, a);
                                            if (a.el) return a.el.attr(n) || ""
                                        }
                                        return ""
                                    }(a),
                                    img_replaceWith: a.img
                                }, a), t.resizeImage(), a.hasSize ? (w && clearInterval(w), a.loadError ? (n.addClass("mfp-loading"), t.updateStatus("error", o.tError.replace("%url%", a.src))) : (n.removeClass("mfp-loading"), t.updateStatus("ready")), n) : (t.updateStatus("loading"), a.loading = !0, a.hasSize || (a.imgHidden = !0, n.addClass("mfp-loading"), t.findImageSize(a)), n)
                            }
                        }
                    });
                    var k;
                    e.magnificPopup.registerModule("zoom", {
                        options: {
                            enabled: !1,
                            easing: "ease-in-out",
                            duration: 300,
                            opener: function(e) {
                                return e.is("img") ? e : e.find("img")
                            }
                        },
                        proto: {
                            initZoom: function() {
                                var e, a = t.st.zoom;
                                if (a.enabled && t.supportsTransition) {
                                    var n, s, i = a.duration,
                                        r = function(e) {
                                            var t = e.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
                                                n = "all " + a.duration / 1e3 + "s " + a.easing,
                                                s = {
                                                    position: "fixed",
                                                    zIndex: 9999,
                                                    left: 0,
                                                    top: 0,
                                                    "-webkit-backface-visibility": "hidden"
                                                };
                                            return s["-webkit-transition"] = s["-moz-transition"] = s["-o-transition"] = s.transition = n, t.css(s), t
                                        },
                                        o = function() {
                                            t.content.css("visibility", "visible")
                                        };
                                    _("BuildControls.zoom", function() {
                                        if (t._allowZoom()) {
                                            if (clearTimeout(n), t.content.css("visibility", "hidden"), !(e = t._getItemToZoom())) return void o();
                                            (s = r(e)).css(t._getOffset()), t.wrap.append(s), n = setTimeout(function() {
                                                s.css(t._getOffset(!0)), n = setTimeout(function() {
                                                    o(), setTimeout(function() {
                                                        s.remove(), e = s = null, c("ZoomAnimationEnded")
                                                    }, 16)
                                                }, i)
                                            }, 16)
                                        }
                                    }), _("BeforeClose.zoom", function() {
                                        if (t._allowZoom()) {
                                            if (clearTimeout(n), t.st.removalDelay = i, !e) {
                                                if (!(e = t._getItemToZoom())) return;
                                                s = r(e)
                                            }
                                            s.css(t._getOffset(!0)), t.wrap.append(s), t.content.css("visibility", "hidden"), setTimeout(function() {
                                                s.css(t._getOffset())
                                            }, 16)
                                        }
                                    }), _("Close.zoom", function() {
                                        t._allowZoom() && (o(), s && s.remove(), e = null)
                                    })
                                }
                            },
                            _allowZoom: function() {
                                return "image" === t.currItem.type
                            },
                            _getItemToZoom: function() {
                                return !!t.currItem.hasSize && t.currItem.img
                            },
                            _getOffset: function(a) {
                                var n, s = (n = a ? t.currItem.img : t.st.zoom.opener(t.currItem.el || t.currItem)).offset(),
                                    i = parseInt(n.css("padding-top"), 10),
                                    r = parseInt(n.css("padding-bottom"), 10);
                                s.top -= e(window).scrollTop() - i;
                                var o = {
                                    width: n.width(),
                                    height: (l ? n.innerHeight() : n[0].offsetHeight) - r - i
                                };
                                return void 0 === k && (k = void 0 !== document.createElement("p").style.MozTransform), k ? o["-moz-transform"] = o.transform = "translate(" + s.left + "px," + s.top + "px)" : (o.left = s.left, o.top = s.top), o
                            }
                        }
                    });
                    var D = function(e) {
                        if (t.currTemplate.iframe) {
                            var a = t.currTemplate.iframe.find("iframe");
                            a.length && (e || (a[0].src = "//about:blank"), t.isIE8 && a.css("display", e ? "block" : "none"))
                        }
                    };
                    e.magnificPopup.registerModule("iframe", {
                        options: {
                            markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
                            srcAction: "iframe_src",
                            patterns: {
                                youtube: {
                                    index: "youtube.com",
                                    id: "v=",
                                    src: "//www.youtube.com/embed/%id%?autoplay=1"
                                },
                                vimeo: {
                                    index: "vimeo.com/",
                                    id: "/",
                                    src: "//player.vimeo.com/video/%id%?autoplay=1"
                                },
                                gmaps: {
                                    index: "//maps.google.",
                                    src: "%id%&output=embed"
                                }
                            }
                        },
                        proto: {
                            initIframe: function() {
                                t.types.push("iframe"), _("BeforeChange", function(e, t, a) {
                                    t !== a && ("iframe" === t ? D() : "iframe" === a && D(!0))
                                }), _("Close.iframe", function() {
                                    D()
                                })
                            },
                            getIframe: function(a, n) {
                                var s = a.src,
                                    i = t.st.iframe;
                                e.each(i.patterns, function() {
                                    if (s.indexOf(this.index) > -1) return this.id && (s = "string" == typeof this.id ? s.substr(s.lastIndexOf(this.id) + this.id.length, s.length) : this.id.call(this, s)), s = this.src.replace("%id%", s), !1
                                });
                                var r = {};
                                return i.srcAction && (r[i.srcAction] = s), t._parseMarkup(n, r, a), t.updateStatus("ready"), n
                            }
                        }
                    });
                    var b = function(e) {
                            var a = t.items.length;
                            return e > a - 1 ? e - a : e < 0 ? a + e : e
                        },
                        T = function(e, t, a) {
                            return e.replace(/%curr%/gi, t + 1).replace(/%total%/gi, a)
                        };
                    e.magnificPopup.registerModule("gallery", {
                        options: {
                            enabled: !1,
                            arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                            preload: [0, 2],
                            navigateByImgClick: !0,
                            arrows: !0,
                            tPrev: "Previous (Left arrow key)",
                            tNext: "Next (Right arrow key)",
                            tCounter: "%curr% of %total%"
                        },
                        proto: {
                            initGallery: function() {
                                var a = t.st.gallery,
                                    s = ".mfp-gallery";
                                if (t.direction = !0, !a || !a.enabled) return !1;
                                i += " mfp-gallery", _("Open" + s, function() {
                                    a.navigateByImgClick && t.wrap.on("click" + s, ".mfp-img", function() {
                                        if (t.items.length > 1) return t.next(), !1
                                    }), n.on("keydown" + s, function(e) {
                                        37 === e.keyCode ? t.prev() : 39 === e.keyCode && t.next()
                                    })
                                }), _("UpdateStatus" + s, function(e, a) {
                                    a.text && (a.text = T(a.text, t.currItem.index, t.items.length))
                                }), _("MarkupParse" + s, function(e, n, s, i) {
                                    var r = t.items.length;
                                    s.counter = r > 1 ? T(a.tCounter, i.index, r) : ""
                                }), _("BuildControls" + s, function() {
                                    if (t.items.length > 1 && a.arrows && !t.arrowLeft) {
                                        var n = a.arrowMarkup,
                                            s = t.arrowLeft = e(n.replace(/%title%/gi, a.tPrev).replace(/%dir%/gi, "left")).addClass(o),
                                            i = t.arrowRight = e(n.replace(/%title%/gi, a.tNext).replace(/%dir%/gi, "right")).addClass(o);
                                        s.click(function() {
                                            t.prev()
                                        }), i.click(function() {
                                            t.next()
                                        }), t.container.append(s.add(i))
                                    }
                                }), _("Change" + s, function() {
                                    t._preloadTimeout && clearTimeout(t._preloadTimeout), t._preloadTimeout = setTimeout(function() {
                                        t.preloadNearbyImages(), t._preloadTimeout = null
                                    }, 16)
                                }), _("Close" + s, function() {
                                    n.off(s), t.wrap.off("click" + s), t.arrowRight = t.arrowLeft = null
                                })
                            },
                            next: function() {
                                t.direction = !0, t.index = b(t.index + 1), t.updateItemHTML()
                            },
                            prev: function() {
                                t.direction = !1, t.index = b(t.index - 1), t.updateItemHTML()
                            },
                            goTo: function(e) {
                                t.direction = e >= t.index, t.index = e, t.updateItemHTML()
                            },
                            preloadNearbyImages: function() {
                                var e, a = t.st.gallery.preload,
                                    n = Math.min(a[0], t.items.length),
                                    s = Math.min(a[1], t.items.length);
                                for (e = 1; e <= (t.direction ? s : n); e++) t._preloadItem(t.index + e);
                                for (e = 1; e <= (t.direction ? n : s); e++) t._preloadItem(t.index - e)
                            },
                            _preloadItem: function(a) {
                                if (a = b(a), !t.items[a].preloaded) {
                                    var n = t.items[a];
                                    n.parsed || (n = t.parseEl(a)), c("LazyLoad", n), "image" === n.type && (n.img = e('<img class="mfp-img" />').on("load.mfploader", function() {
                                        n.hasSize = !0
                                    }).on("error.mfploader", function() {
                                        n.hasSize = !0, n.loadError = !0, c("LazyLoadError", n)
                                    }).attr("src", n.src)), n.preloaded = !0
                                }
                            }
                        }
                    });
                    e.magnificPopup.registerModule("retina", {
                        options: {
                            replaceSrc: function(e) {
                                return e.src.replace(/\.\w+$/, function(e) {
                                    return "@2x" + e
                                })
                            },
                            ratio: 1
                        },
                        proto: {
                            initRetina: function() {
                                if (window.devicePixelRatio > 1) {
                                    var e = t.st.retina,
                                        a = e.ratio;
                                    (a = isNaN(a) ? a() : a) > 1 && (_("ImageHasSize.retina", function(e, t) {
                                        t.img.css({
                                            "max-width": t.img[0].naturalWidth / a,
                                            width: "100%"
                                        })
                                    }), _("ElementParse.retina", function(t, n) {
                                        n.src = e.replaceSrc(n, a)
                                    }))
                                }
                            }
                        }
                    }), p()
                }), i("undefined" != typeof magnificPopup ? magnificPopup : window.magnificPopup)
            }).call(a, void 0, void 0, void 0, void 0, function(e) {
                t.exports = e
            })
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {}],
    4: [function(e, t, a) {
        ! function(n, s) {
            "object" == typeof a && void 0 !== t && "function" == typeof e ? s(e("../moment")) : "function" == typeof define && define.amd ? define(["../moment"], s) : s(n.moment)
        }(this, function(e) {
            "use strict";

            function t(e, t, a) {
                return "m" === a ? t ? "хвіліна" : "хвіліну" : "h" === a ? t ? "гадзіна" : "гадзіну" : e + " " + function(e, t) {
                    var a = e.split("_");
                    return t % 10 == 1 && t % 100 != 11 ? a[0] : t % 10 >= 2 && t % 10 <= 4 && (t % 100 < 10 || t % 100 >= 20) ? a[1] : a[2]
                }({
                    ss: t ? "секунда_секунды_секунд" : "секунду_секунды_секунд",
                    mm: t ? "хвіліна_хвіліны_хвілін" : "хвіліну_хвіліны_хвілін",
                    hh: t ? "гадзіна_гадзіны_гадзін" : "гадзіну_гадзіны_гадзін",
                    dd: "дзень_дні_дзён",
                    MM: "месяц_месяцы_месяцаў",
                    yy: "год_гады_гадоў"
                } [a], +e)
            }

            function a(e, t, a) {
                return e + " " + function(e, t) {
                    if (2 === t) return function(e) {
                        var t = {
                            m: "v",
                            b: "v",
                            d: "z"
                        };
                        if (void 0 === t[e.charAt(0)]) return e;
                        return t[e.charAt(0)] + e.substring(1)
                    }(e);
                    return e
                }({
                    mm: "munutenn",
                    MM: "miz",
                    dd: "devezh"
                } [a], e)
            }

            function n(e) {
                return e > 9 ? n(e % 10) : e
            }

            function s(e, t, a) {
                var n = e + " ";
                switch (a) {
                    case "ss":
                        return n += 1 === e ? "sekunda" : 2 === e || 3 === e || 4 === e ? "sekunde" : "sekundi";
                    case "m":
                        return t ? "jedna minuta" : "jedne minute";
                    case "mm":
                        return n += 1 === e ? "minuta" : 2 === e || 3 === e || 4 === e ? "minute" : "minuta";
                    case "h":
                        return t ? "jedan sat" : "jednog sata";
                    case "hh":
                        return n += 1 === e ? "sat" : 2 === e || 3 === e || 4 === e ? "sata" : "sati";
                    case "dd":
                        return n += 1 === e ? "dan" : "dana";
                    case "MM":
                        return n += 1 === e ? "mjesec" : 2 === e || 3 === e || 4 === e ? "mjeseca" : "mjeseci";
                    case "yy":
                        return n += 1 === e ? "godina" : 2 === e || 3 === e || 4 === e ? "godine" : "godina"
                }
            }

            function i(e) {
                return e > 1 && e < 5 && 1 != ~~(e / 10)
            }

            function r(e, t, a, n) {
                var s = e + " ";
                switch (a) {
                    case "s":
                        return t || n ? "pár sekund" : "pár sekundami";
                    case "ss":
                        return t || n ? s + (i(e) ? "sekundy" : "sekund") : s + "sekundami";
                    case "m":
                        return t ? "minuta" : n ? "minutu" : "minutou";
                    case "mm":
                        return t || n ? s + (i(e) ? "minuty" : "minut") : s + "minutami";
                    case "h":
                        return t ? "hodina" : n ? "hodinu" : "hodinou";
                    case "hh":
                        return t || n ? s + (i(e) ? "hodiny" : "hodin") : s + "hodinami";
                    case "d":
                        return t || n ? "den" : "dnem";
                    case "dd":
                        return t || n ? s + (i(e) ? "dny" : "dní") : s + "dny";
                    case "M":
                        return t || n ? "měsíc" : "měsícem";
                    case "MM":
                        return t || n ? s + (i(e) ? "měsíce" : "měsíců") : s + "měsíci";
                    case "y":
                        return t || n ? "rok" : "rokem";
                    case "yy":
                        return t || n ? s + (i(e) ? "roky" : "let") : s + "lety"
                }
            }

            function o(e, t, a, n) {
                var s = {
                    m: ["eine Minute", "einer Minute"],
                    h: ["eine Stunde", "einer Stunde"],
                    d: ["ein Tag", "einem Tag"],
                    dd: [e + " Tage", e + " Tagen"],
                    M: ["ein Monat", "einem Monat"],
                    MM: [e + " Monate", e + " Monaten"],
                    y: ["ein Jahr", "einem Jahr"],
                    yy: [e + " Jahre", e + " Jahren"]
                };
                return t ? s[a][0] : s[a][1]
            }

            function d(e, t, a, n) {
                var s = {
                    m: ["eine Minute", "einer Minute"],
                    h: ["eine Stunde", "einer Stunde"],
                    d: ["ein Tag", "einem Tag"],
                    dd: [e + " Tage", e + " Tagen"],
                    M: ["ein Monat", "einem Monat"],
                    MM: [e + " Monate", e + " Monaten"],
                    y: ["ein Jahr", "einem Jahr"],
                    yy: [e + " Jahre", e + " Jahren"]
                };
                return t ? s[a][0] : s[a][1]
            }

            function l(e, t, a, n) {
                var s = {
                    m: ["eine Minute", "einer Minute"],
                    h: ["eine Stunde", "einer Stunde"],
                    d: ["ein Tag", "einem Tag"],
                    dd: [e + " Tage", e + " Tagen"],
                    M: ["ein Monat", "einem Monat"],
                    MM: [e + " Monate", e + " Monaten"],
                    y: ["ein Jahr", "einem Jahr"],
                    yy: [e + " Jahre", e + " Jahren"]
                };
                return t ? s[a][0] : s[a][1]
            }

            function u(e, t, a, n) {
                var s = {
                    s: ["mõne sekundi", "mõni sekund", "paar sekundit"],
                    ss: [e + "sekundi", e + "sekundit"],
                    m: ["ühe minuti", "üks minut"],
                    mm: [e + " minuti", e + " minutit"],
                    h: ["ühe tunni", "tund aega", "üks tund"],
                    hh: [e + " tunni", e + " tundi"],
                    d: ["ühe päeva", "üks päev"],
                    M: ["kuu aja", "kuu aega", "üks kuu"],
                    MM: [e + " kuu", e + " kuud"],
                    y: ["ühe aasta", "aasta", "üks aasta"],
                    yy: [e + " aasta", e + " aastat"]
                };
                return t ? s[a][2] ? s[a][2] : s[a][1] : n ? s[a][0] : s[a][1]
            }

            function _(e, t, a, n) {
                var s = "";
                switch (a) {
                    case "s":
                        return n ? "muutaman sekunnin" : "muutama sekunti";
                    case "ss":
                        return n ? "sekunnin" : "sekuntia";
                    case "m":
                        return n ? "minuutin" : "minuutti";
                    case "mm":
                        s = n ? "minuutin" : "minuuttia";
                        break;
                    case "h":
                        return n ? "tunnin" : "tunti";
                    case "hh":
                        s = n ? "tunnin" : "tuntia";
                        break;
                    case "d":
                        return n ? "päivän" : "päivä";
                    case "dd":
                        s = n ? "päivän" : "päivää";
                        break;
                    case "M":
                        return n ? "kuukauden" : "kuukausi";
                    case "MM":
                        s = n ? "kuukauden" : "kuukautta";
                        break;
                    case "y":
                        return n ? "vuoden" : "vuosi";
                    case "yy":
                        s = n ? "vuoden" : "vuotta"
                }
                return s = function(e, t) {
                    return e < 10 ? t ? Ye[e] : Le[e] : e
                }(e, n) + " " + s
            }

            function m(e, t, a, n) {
                var s = {
                    s: ["thodde secondanim", "thodde second"],
                    ss: [e + " secondanim", e + " second"],
                    m: ["eka mintan", "ek minute"],
                    mm: [e + " mintanim", e + " mintam"],
                    h: ["eka horan", "ek hor"],
                    hh: [e + " horanim", e + " horam"],
                    d: ["eka disan", "ek dis"],
                    dd: [e + " disanim", e + " dis"],
                    M: ["eka mhoinean", "ek mhoino"],
                    MM: [e + " mhoineanim", e + " mhoine"],
                    y: ["eka vorsan", "ek voros"],
                    yy: [e + " vorsanim", e + " vorsam"]
                };
                return t ? s[a][0] : s[a][1]
            }

            function c(e, t, a) {
                var n = e + " ";
                switch (a) {
                    case "ss":
                        return n += 1 === e ? "sekunda" : 2 === e || 3 === e || 4 === e ? "sekunde" : "sekundi";
                    case "m":
                        return t ? "jedna minuta" : "jedne minute";
                    case "mm":
                        return n += 1 === e ? "minuta" : 2 === e || 3 === e || 4 === e ? "minute" : "minuta";
                    case "h":
                        return t ? "jedan sat" : "jednog sata";
                    case "hh":
                        return n += 1 === e ? "sat" : 2 === e || 3 === e || 4 === e ? "sata" : "sati";
                    case "dd":
                        return n += 1 === e ? "dan" : "dana";
                    case "MM":
                        return n += 1 === e ? "mjesec" : 2 === e || 3 === e || 4 === e ? "mjeseca" : "mjeseci";
                    case "yy":
                        return n += 1 === e ? "godina" : 2 === e || 3 === e || 4 === e ? "godine" : "godina"
                }
            }

            function h(e, t, a, n) {
                var s = e;
                switch (a) {
                    case "s":
                        return n || t ? "néhány másodperc" : "néhány másodperce";
                    case "ss":
                        return s + (n || t) ? " másodperc" : " másodperce";
                    case "m":
                        return "egy" + (n || t ? " perc" : " perce");
                    case "mm":
                        return s + (n || t ? " perc" : " perce");
                    case "h":
                        return "egy" + (n || t ? " óra" : " órája");
                    case "hh":
                        return s + (n || t ? " óra" : " órája");
                    case "d":
                        return "egy" + (n || t ? " nap" : " napja");
                    case "dd":
                        return s + (n || t ? " nap" : " napja");
                    case "M":
                        return "egy" + (n || t ? " hónap" : " hónapja");
                    case "MM":
                        return s + (n || t ? " hónap" : " hónapja");
                    case "y":
                        return "egy" + (n || t ? " év" : " éve");
                    case "yy":
                        return s + (n || t ? " év" : " éve")
                }
                return ""
            }

            function p(e) {
                return (e ? "" : "[múlt] ") + "[" + Se[this.day()] + "] LT[-kor]"
            }

            function f(e) {
                return e % 100 == 11 || e % 10 != 1
            }

            function M(e, t, a, n) {
                var s = e + " ";
                switch (a) {
                    case "s":
                        return t || n ? "nokkrar sekúndur" : "nokkrum sekúndum";
                    case "ss":
                        return f(e) ? s + (t || n ? "sekúndur" : "sekúndum") : s + "sekúnda";
                    case "m":
                        return t ? "mínúta" : "mínútu";
                    case "mm":
                        return f(e) ? s + (t || n ? "mínútur" : "mínútum") : t ? s + "mínúta" : s + "mínútu";
                    case "hh":
                        return f(e) ? s + (t || n ? "klukkustundir" : "klukkustundum") : s + "klukkustund";
                    case "d":
                        return t ? "dagur" : n ? "dag" : "degi";
                    case "dd":
                        return f(e) ? t ? s + "dagar" : s + (n ? "daga" : "dögum") : t ? s + "dagur" : s + (n ? "dag" : "degi");
                    case "M":
                        return t ? "mánuður" : n ? "mánuð" : "mánuði";
                    case "MM":
                        return f(e) ? t ? s + "mánuðir" : s + (n ? "mánuði" : "mánuðum") : t ? s + "mánuður" : s + (n ? "mánuð" : "mánuði");
                    case "y":
                        return t || n ? "ár" : "ári";
                    case "yy":
                        return f(e) ? s + (t || n ? "ár" : "árum") : s + (t || n ? "ár" : "ári")
                }
            }

            function y(e, t, a, n) {
                var s = {
                    m: ["eng Minutt", "enger Minutt"],
                    h: ["eng Stonn", "enger Stonn"],
                    d: ["een Dag", "engem Dag"],
                    M: ["ee Mount", "engem Mount"],
                    y: ["ee Joer", "engem Joer"]
                };
                return t ? s[a][0] : s[a][1]
            }

            function g(e) {
                if (e = parseInt(e, 10), isNaN(e)) return !1;
                if (e < 0) return !0;
                if (e < 10) return 4 <= e && e <= 7;
                if (e < 100) {
                    var t = e % 10;
                    return g(0 === t ? e / 10 : t)
                }
                if (e < 1e4) {
                    for (; e >= 10;) e /= 10;
                    return g(e)
                }
                return e /= 1e3, g(e)
            }

            function L(e, t, a, n) {
                return t ? v(a)[0] : n ? v(a)[1] : v(a)[2]
            }

            function Y(e) {
                return e % 10 == 0 || e > 10 && e < 20
            }

            function v(e) {
                return Ee[e].split("_")
            }

            function w(e, t, a, n) {
                var s = e + " ";
                return 1 === e ? s + L(0, t, a[0], n) : t ? s + (Y(e) ? v(a)[1] : v(a)[0]) : n ? s + v(a)[1] : s + (Y(e) ? v(a)[1] : v(a)[2])
            }

            function k(e, t, a) {
                return a ? t % 10 == 1 && t % 100 != 11 ? e[2] : e[3] : t % 10 == 1 && t % 100 != 11 ? e[0] : e[1]
            }

            function D(e, t, a) {
                return e + " " + k(We[a], e, t)
            }

            function b(e, t, a) {
                return k(We[a], e, t)
            }

            function T(e, t, a, n) {
                switch (a) {
                    case "s":
                        return t ? "хэдхэн секунд" : "хэдхэн секундын";
                    case "ss":
                        return e + (t ? " секунд" : " секундын");
                    case "m":
                    case "mm":
                        return e + (t ? " минут" : " минутын");
                    case "h":
                    case "hh":
                        return e + (t ? " цаг" : " цагийн");
                    case "d":
                    case "dd":
                        return e + (t ? " өдөр" : " өдрийн");
                    case "M":
                    case "MM":
                        return e + (t ? " сар" : " сарын");
                    case "y":
                    case "yy":
                        return e + (t ? " жил" : " жилийн");
                    default:
                        return e
                }
            }

            function S(e, t, a, n) {
                var s = "";
                if (t) switch (a) {
                    case "s":
                        s = "काही सेकंद";
                        break;
                    case "ss":
                        s = "%d सेकंद";
                        break;
                    case "m":
                        s = "एक मिनिट";
                        break;
                    case "mm":
                        s = "%d मिनिटे";
                        break;
                    case "h":
                        s = "एक तास";
                        break;
                    case "hh":
                        s = "%d तास";
                        break;
                    case "d":
                        s = "एक दिवस";
                        break;
                    case "dd":
                        s = "%d दिवस";
                        break;
                    case "M":
                        s = "एक महिना";
                        break;
                    case "MM":
                        s = "%d महिने";
                        break;
                    case "y":
                        s = "एक वर्ष";
                        break;
                    case "yy":
                        s = "%d वर्षे"
                } else switch (a) {
                    case "s":
                        s = "काही सेकंदां";
                        break;
                    case "ss":
                        s = "%d सेकंदां";
                        break;
                    case "m":
                        s = "एका मिनिटा";
                        break;
                    case "mm":
                        s = "%d मिनिटां";
                        break;
                    case "h":
                        s = "एका तासा";
                        break;
                    case "hh":
                        s = "%d तासां";
                        break;
                    case "d":
                        s = "एका दिवसा";
                        break;
                    case "dd":
                        s = "%d दिवसां";
                        break;
                    case "M":
                        s = "एका महिन्या";
                        break;
                    case "MM":
                        s = "%d महिन्यां";
                        break;
                    case "y":
                        s = "एका वर्षा";
                        break;
                    case "yy":
                        s = "%d वर्षां"
                }
                return s.replace(/%d/i, e)
            }

            function x(e) {
                return e % 10 < 5 && e % 10 > 1 && ~~(e / 10) % 10 != 1
            }

            function H(e, t, a) {
                var n = e + " ";
                switch (a) {
                    case "ss":
                        return n + (x(e) ? "sekundy" : "sekund");
                    case "m":
                        return t ? "minuta" : "minutę";
                    case "mm":
                        return n + (x(e) ? "minuty" : "minut");
                    case "h":
                        return t ? "godzina" : "godzinę";
                    case "hh":
                        return n + (x(e) ? "godziny" : "godzin");
                    case "MM":
                        return n + (x(e) ? "miesiące" : "miesięcy");
                    case "yy":
                        return n + (x(e) ? "lata" : "lat")
                }
            }

            function j(e, t, a) {
                var n = " ";
                return (e % 100 >= 20 || e >= 100 && e % 100 == 0) && (n = " de "), e + n + {
                    ss: "secunde",
                    mm: "minute",
                    hh: "ore",
                    dd: "zile",
                    MM: "luni",
                    yy: "ani"
                } [a]
            }

            function C(e, t, a) {
                return "m" === a ? t ? "минута" : "минуту" : e + " " + function(e, t) {
                    var a = e.split("_");
                    return t % 10 == 1 && t % 100 != 11 ? a[0] : t % 10 >= 2 && t % 10 <= 4 && (t % 100 < 10 || t % 100 >= 20) ? a[1] : a[2]
                }({
                    ss: t ? "секунда_секунды_секунд" : "секунду_секунды_секунд",
                    mm: t ? "минута_минуты_минут" : "минуту_минуты_минут",
                    hh: "час_часа_часов",
                    dd: "день_дня_дней",
                    MM: "месяц_месяца_месяцев",
                    yy: "год_года_лет"
                } [a], +e)
            }

            function P(e) {
                return e > 1 && e < 5
            }

            function O(e, t, a, n) {
                var s = e + " ";
                switch (a) {
                    case "s":
                        return t || n ? "pár sekúnd" : "pár sekundami";
                    case "ss":
                        return t || n ? s + (P(e) ? "sekundy" : "sekúnd") : s + "sekundami";
                    case "m":
                        return t ? "minúta" : n ? "minútu" : "minútou";
                    case "mm":
                        return t || n ? s + (P(e) ? "minúty" : "minút") : s + "minútami";
                    case "h":
                        return t ? "hodina" : n ? "hodinu" : "hodinou";
                    case "hh":
                        return t || n ? s + (P(e) ? "hodiny" : "hodín") : s + "hodinami";
                    case "d":
                        return t || n ? "deň" : "dňom";
                    case "dd":
                        return t || n ? s + (P(e) ? "dni" : "dní") : s + "dňami";
                    case "M":
                        return t || n ? "mesiac" : "mesiacom";
                    case "MM":
                        return t || n ? s + (P(e) ? "mesiace" : "mesiacov") : s + "mesiacmi";
                    case "y":
                        return t || n ? "rok" : "rokom";
                    case "yy":
                        return t || n ? s + (P(e) ? "roky" : "rokov") : s + "rokmi"
                }
            }

            function E(e, t, a, n) {
                var s = e + " ";
                switch (a) {
                    case "s":
                        return t || n ? "nekaj sekund" : "nekaj sekundami";
                    case "ss":
                        return s += 1 === e ? t ? "sekundo" : "sekundi" : 2 === e ? t || n ? "sekundi" : "sekundah" : e < 5 ? t || n ? "sekunde" : "sekundah" : "sekund";
                    case "m":
                        return t ? "ena minuta" : "eno minuto";
                    case "mm":
                        return s += 1 === e ? t ? "minuta" : "minuto" : 2 === e ? t || n ? "minuti" : "minutama" : e < 5 ? t || n ? "minute" : "minutami" : t || n ? "minut" : "minutami";
                    case "h":
                        return t ? "ena ura" : "eno uro";
                    case "hh":
                        return s += 1 === e ? t ? "ura" : "uro" : 2 === e ? t || n ? "uri" : "urama" : e < 5 ? t || n ? "ure" : "urami" : t || n ? "ur" : "urami";
                    case "d":
                        return t || n ? "en dan" : "enim dnem";
                    case "dd":
                        return s += 1 === e ? t || n ? "dan" : "dnem" : 2 === e ? t || n ? "dni" : "dnevoma" : t || n ? "dni" : "dnevi";
                    case "M":
                        return t || n ? "en mesec" : "enim mesecem";
                    case "MM":
                        return s += 1 === e ? t || n ? "mesec" : "mesecem" : 2 === e ? t || n ? "meseca" : "mesecema" : e < 5 ? t || n ? "mesece" : "meseci" : t || n ? "mesecev" : "meseci";
                    case "y":
                        return t || n ? "eno leto" : "enim letom";
                    case "yy":
                        return s += 1 === e ? t || n ? "leto" : "letom" : 2 === e ? t || n ? "leti" : "letoma" : e < 5 ? t || n ? "leta" : "leti" : t || n ? "let" : "leti"
                }
            }

            function W(e, t, a, n) {
                var s = function(e) {
                    var t = Math.floor(e % 1e3 / 100),
                        a = Math.floor(e % 100 / 10),
                        n = e % 10,
                        s = "";
                    t > 0 && (s += mt[t] + "vatlh");
                    a > 0 && (s += ("" !== s ? " " : "") + mt[a] + "maH");
                    n > 0 && (s += ("" !== s ? " " : "") + mt[n]);
                    return "" === s ? "pagh" : s
                }(e);
                switch (a) {
                    case "ss":
                        return s + " lup";
                    case "mm":
                        return s + " tup";
                    case "hh":
                        return s + " rep";
                    case "dd":
                        return s + " jaj";
                    case "MM":
                        return s + " jar";
                    case "yy":
                        return s + " DIS"
                }
            }

            function A(e, t, a, n) {
                var s = {
                    s: ["viensas secunds", "'iensas secunds"],
                    ss: [e + " secunds", e + " secunds"],
                    m: ["'n míut", "'iens míut"],
                    mm: [e + " míuts", e + " míuts"],
                    h: ["'n þora", "'iensa þora"],
                    hh: [e + " þoras", e + " þoras"],
                    d: ["'n ziua", "'iensa ziua"],
                    dd: [e + " ziuas", e + " ziuas"],
                    M: ["'n mes", "'iens mes"],
                    MM: [e + " mesen", e + " mesen"],
                    y: ["'n ar", "'iens ar"],
                    yy: [e + " ars", e + " ars"]
                };
                return n ? s[a][0] : t ? s[a][0] : s[a][1]
            }

            function z(e, t, a) {
                return "m" === a ? t ? "хвилина" : "хвилину" : "h" === a ? t ? "година" : "годину" : e + " " + function(e, t) {
                    var a = e.split("_");
                    return t % 10 == 1 && t % 100 != 11 ? a[0] : t % 10 >= 2 && t % 10 <= 4 && (t % 100 < 10 || t % 100 >= 20) ? a[1] : a[2]
                }({
                    ss: t ? "секунда_секунди_секунд" : "секунду_секунди_секунд",
                    mm: t ? "хвилина_хвилини_хвилин" : "хвилину_хвилини_хвилин",
                    hh: t ? "година_години_годин" : "годину_години_годин",
                    dd: "день_дні_днів",
                    MM: "місяць_місяці_місяців",
                    yy: "рік_роки_років"
                } [a], +e)
            }

            function F(e) {
                return function() {
                    return e + "о" + (11 === this.hours() ? "б" : "") + "] LT"
                }
            }
            e.defineLocale("af", {
                months: "Januarie_Februarie_Maart_April_Mei_Junie_Julie_Augustus_September_Oktober_November_Desember".split("_"),
                monthsShort: "Jan_Feb_Mrt_Apr_Mei_Jun_Jul_Aug_Sep_Okt_Nov_Des".split("_"),
                weekdays: "Sondag_Maandag_Dinsdag_Woensdag_Donderdag_Vrydag_Saterdag".split("_"),
                weekdaysShort: "Son_Maa_Din_Woe_Don_Vry_Sat".split("_"),
                weekdaysMin: "So_Ma_Di_Wo_Do_Vr_Sa".split("_"),
                meridiemParse: /vm|nm/i,
                isPM: function(e) {
                    return /^nm$/i.test(e)
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? a ? "vm" : "VM" : a ? "nm" : "NM"
                },
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Vandag om] LT",
                    nextDay: "[Môre om] LT",
                    nextWeek: "dddd [om] LT",
                    lastDay: "[Gister om] LT",
                    lastWeek: "[Laas] dddd [om] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "oor %s",
                    past: "%s gelede",
                    s: "'n paar sekondes",
                    ss: "%d sekondes",
                    m: "'n minuut",
                    mm: "%d minute",
                    h: "'n uur",
                    hh: "%d ure",
                    d: "'n dag",
                    dd: "%d dae",
                    M: "'n maand",
                    MM: "%d maande",
                    y: "'n jaar",
                    yy: "%d jaar"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(ste|de)/,
                ordinal: function(e) {
                    return e + (1 === e || 8 === e || e >= 20 ? "ste" : "de")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("ar-dz", {
                months: "جانفي_فيفري_مارس_أفريل_ماي_جوان_جويلية_أوت_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),
                monthsShort: "جانفي_فيفري_مارس_أفريل_ماي_جوان_جويلية_أوت_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),
                weekdays: "الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),
                weekdaysShort: "احد_اثنين_ثلاثاء_اربعاء_خميس_جمعة_سبت".split("_"),
                weekdaysMin: "أح_إث_ثلا_أر_خم_جم_سب".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[اليوم على الساعة] LT",
                    nextDay: "[غدا على الساعة] LT",
                    nextWeek: "dddd [على الساعة] LT",
                    lastDay: "[أمس على الساعة] LT",
                    lastWeek: "dddd [على الساعة] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "في %s",
                    past: "منذ %s",
                    s: "ثوان",
                    ss: "%d ثانية",
                    m: "دقيقة",
                    mm: "%d دقائق",
                    h: "ساعة",
                    hh: "%d ساعات",
                    d: "يوم",
                    dd: "%d أيام",
                    M: "شهر",
                    MM: "%d أشهر",
                    y: "سنة",
                    yy: "%d سنوات"
                },
                week: {
                    dow: 0,
                    doy: 4
                }
            }), e.defineLocale("ar-kw", {
                months: "يناير_فبراير_مارس_أبريل_ماي_يونيو_يوليوز_غشت_شتنبر_أكتوبر_نونبر_دجنبر".split("_"),
                monthsShort: "يناير_فبراير_مارس_أبريل_ماي_يونيو_يوليوز_غشت_شتنبر_أكتوبر_نونبر_دجنبر".split("_"),
                weekdays: "الأحد_الإتنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),
                weekdaysShort: "احد_اتنين_ثلاثاء_اربعاء_خميس_جمعة_سبت".split("_"),
                weekdaysMin: "ح_ن_ث_ر_خ_ج_س".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[اليوم على الساعة] LT",
                    nextDay: "[غدا على الساعة] LT",
                    nextWeek: "dddd [على الساعة] LT",
                    lastDay: "[أمس على الساعة] LT",
                    lastWeek: "dddd [على الساعة] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "في %s",
                    past: "منذ %s",
                    s: "ثوان",
                    ss: "%d ثانية",
                    m: "دقيقة",
                    mm: "%d دقائق",
                    h: "ساعة",
                    hh: "%d ساعات",
                    d: "يوم",
                    dd: "%d أيام",
                    M: "شهر",
                    MM: "%d أشهر",
                    y: "سنة",
                    yy: "%d سنوات"
                },
                week: {
                    dow: 0,
                    doy: 12
                }
            });
            var I = {
                    1: "1",
                    2: "2",
                    3: "3",
                    4: "4",
                    5: "5",
                    6: "6",
                    7: "7",
                    8: "8",
                    9: "9",
                    0: "0"
                },
                $ = function(e) {
                    return 0 === e ? 0 : 1 === e ? 1 : 2 === e ? 2 : e % 100 >= 3 && e % 100 <= 10 ? 3 : e % 100 >= 11 ? 4 : 5
                },
                N = {
                    s: ["أقل من ثانية", "ثانية واحدة", ["ثانيتان", "ثانيتين"], "%d ثوان", "%d ثانية", "%d ثانية"],
                    m: ["أقل من دقيقة", "دقيقة واحدة", ["دقيقتان", "دقيقتين"], "%d دقائق", "%d دقيقة", "%d دقيقة"],
                    h: ["أقل من ساعة", "ساعة واحدة", ["ساعتان", "ساعتين"], "%d ساعات", "%d ساعة", "%d ساعة"],
                    d: ["أقل من يوم", "يوم واحد", ["يومان", "يومين"], "%d أيام", "%d يومًا", "%d يوم"],
                    M: ["أقل من شهر", "شهر واحد", ["شهران", "شهرين"], "%d أشهر", "%d شهرا", "%d شهر"],
                    y: ["أقل من عام", "عام واحد", ["عامان", "عامين"], "%d أعوام", "%d عامًا", "%d عام"]
                },
                R = function(e) {
                    return function(t, a, n, s) {
                        var i = $(t),
                            r = N[e][$(t)];
                        return 2 === i && (r = r[a ? 0 : 1]), r.replace(/%d/i, t)
                    }
                },
                J = ["يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
            e.defineLocale("ar-ly", {
                months: J,
                monthsShort: J,
                weekdays: "الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),
                weekdaysShort: "أحد_إثنين_ثلاثاء_أربعاء_خميس_جمعة_سبت".split("_"),
                weekdaysMin: "ح_ن_ث_ر_خ_ج_س".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "D/‏M/‏YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                meridiemParse: /ص|م/,
                isPM: function(e) {
                    return "م" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "ص" : "م"
                },
                calendar: {
                    sameDay: "[اليوم عند الساعة] LT",
                    nextDay: "[غدًا عند الساعة] LT",
                    nextWeek: "dddd [عند الساعة] LT",
                    lastDay: "[أمس عند الساعة] LT",
                    lastWeek: "dddd [عند الساعة] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "بعد %s",
                    past: "منذ %s",
                    s: R("s"),
                    ss: R("s"),
                    m: R("m"),
                    mm: R("m"),
                    h: R("h"),
                    hh: R("h"),
                    d: R("d"),
                    dd: R("d"),
                    M: R("M"),
                    MM: R("M"),
                    y: R("y"),
                    yy: R("y")
                },
                preparse: function(e) {
                    return e.replace(/،/g, ",")
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return I[e]
                    }).replace(/,/g, "،")
                },
                week: {
                    dow: 6,
                    doy: 12
                }
            }), e.defineLocale("ar-ma", {
                months: "يناير_فبراير_مارس_أبريل_ماي_يونيو_يوليوز_غشت_شتنبر_أكتوبر_نونبر_دجنبر".split("_"),
                monthsShort: "يناير_فبراير_مارس_أبريل_ماي_يونيو_يوليوز_غشت_شتنبر_أكتوبر_نونبر_دجنبر".split("_"),
                weekdays: "الأحد_الإتنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),
                weekdaysShort: "احد_اتنين_ثلاثاء_اربعاء_خميس_جمعة_سبت".split("_"),
                weekdaysMin: "ح_ن_ث_ر_خ_ج_س".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[اليوم على الساعة] LT",
                    nextDay: "[غدا على الساعة] LT",
                    nextWeek: "dddd [على الساعة] LT",
                    lastDay: "[أمس على الساعة] LT",
                    lastWeek: "dddd [على الساعة] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "في %s",
                    past: "منذ %s",
                    s: "ثوان",
                    ss: "%d ثانية",
                    m: "دقيقة",
                    mm: "%d دقائق",
                    h: "ساعة",
                    hh: "%d ساعات",
                    d: "يوم",
                    dd: "%d أيام",
                    M: "شهر",
                    MM: "%d أشهر",
                    y: "سنة",
                    yy: "%d سنوات"
                },
                week: {
                    dow: 6,
                    doy: 12
                }
            });
            var B = {
                    1: "١",
                    2: "٢",
                    3: "٣",
                    4: "٤",
                    5: "٥",
                    6: "٦",
                    7: "٧",
                    8: "٨",
                    9: "٩",
                    0: "٠"
                },
                U = {
                    "١": "1",
                    "٢": "2",
                    "٣": "3",
                    "٤": "4",
                    "٥": "5",
                    "٦": "6",
                    "٧": "7",
                    "٨": "8",
                    "٩": "9",
                    "٠": "0"
                };
            e.defineLocale("ar-sa", {
                months: "يناير_فبراير_مارس_أبريل_مايو_يونيو_يوليو_أغسطس_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),
                monthsShort: "يناير_فبراير_مارس_أبريل_مايو_يونيو_يوليو_أغسطس_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),
                weekdays: "الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),
                weekdaysShort: "أحد_إثنين_ثلاثاء_أربعاء_خميس_جمعة_سبت".split("_"),
                weekdaysMin: "ح_ن_ث_ر_خ_ج_س".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                meridiemParse: /ص|م/,
                isPM: function(e) {
                    return "م" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "ص" : "م"
                },
                calendar: {
                    sameDay: "[اليوم على الساعة] LT",
                    nextDay: "[غدا على الساعة] LT",
                    nextWeek: "dddd [على الساعة] LT",
                    lastDay: "[أمس على الساعة] LT",
                    lastWeek: "dddd [على الساعة] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "في %s",
                    past: "منذ %s",
                    s: "ثوان",
                    ss: "%d ثانية",
                    m: "دقيقة",
                    mm: "%d دقائق",
                    h: "ساعة",
                    hh: "%d ساعات",
                    d: "يوم",
                    dd: "%d أيام",
                    M: "شهر",
                    MM: "%d أشهر",
                    y: "سنة",
                    yy: "%d سنوات"
                },
                preparse: function(e) {
                    return e.replace(/[١٢٣٤٥٦٧٨٩٠]/g, function(e) {
                        return U[e]
                    }).replace(/،/g, ",")
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return B[e]
                    }).replace(/,/g, "،")
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            }), e.defineLocale("ar-tn", {
                months: "جانفي_فيفري_مارس_أفريل_ماي_جوان_جويلية_أوت_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),
                monthsShort: "جانفي_فيفري_مارس_أفريل_ماي_جوان_جويلية_أوت_سبتمبر_أكتوبر_نوفمبر_ديسمبر".split("_"),
                weekdays: "الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),
                weekdaysShort: "أحد_إثنين_ثلاثاء_أربعاء_خميس_جمعة_سبت".split("_"),
                weekdaysMin: "ح_ن_ث_ر_خ_ج_س".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[اليوم على الساعة] LT",
                    nextDay: "[غدا على الساعة] LT",
                    nextWeek: "dddd [على الساعة] LT",
                    lastDay: "[أمس على الساعة] LT",
                    lastWeek: "dddd [على الساعة] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "في %s",
                    past: "منذ %s",
                    s: "ثوان",
                    ss: "%d ثانية",
                    m: "دقيقة",
                    mm: "%d دقائق",
                    h: "ساعة",
                    hh: "%d ساعات",
                    d: "يوم",
                    dd: "%d أيام",
                    M: "شهر",
                    MM: "%d أشهر",
                    y: "سنة",
                    yy: "%d سنوات"
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var V = {
                    1: "١",
                    2: "٢",
                    3: "٣",
                    4: "٤",
                    5: "٥",
                    6: "٦",
                    7: "٧",
                    8: "٨",
                    9: "٩",
                    0: "٠"
                },
                q = {
                    "١": "1",
                    "٢": "2",
                    "٣": "3",
                    "٤": "4",
                    "٥": "5",
                    "٦": "6",
                    "٧": "7",
                    "٨": "8",
                    "٩": "9",
                    "٠": "0"
                },
                G = function(e) {
                    return 0 === e ? 0 : 1 === e ? 1 : 2 === e ? 2 : e % 100 >= 3 && e % 100 <= 10 ? 3 : e % 100 >= 11 ? 4 : 5
                },
                Z = {
                    s: ["أقل من ثانية", "ثانية واحدة", ["ثانيتان", "ثانيتين"], "%d ثوان", "%d ثانية", "%d ثانية"],
                    m: ["أقل من دقيقة", "دقيقة واحدة", ["دقيقتان", "دقيقتين"], "%d دقائق", "%d دقيقة", "%d دقيقة"],
                    h: ["أقل من ساعة", "ساعة واحدة", ["ساعتان", "ساعتين"], "%d ساعات", "%d ساعة", "%d ساعة"],
                    d: ["أقل من يوم", "يوم واحد", ["يومان", "يومين"], "%d أيام", "%d يومًا", "%d يوم"],
                    M: ["أقل من شهر", "شهر واحد", ["شهران", "شهرين"], "%d أشهر", "%d شهرا", "%d شهر"],
                    y: ["أقل من عام", "عام واحد", ["عامان", "عامين"], "%d أعوام", "%d عامًا", "%d عام"]
                },
                K = function(e) {
                    return function(t, a, n, s) {
                        var i = G(t),
                            r = Z[e][G(t)];
                        return 2 === i && (r = r[a ? 0 : 1]), r.replace(/%d/i, t)
                    }
                },
                Q = ["يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
            e.defineLocale("ar", {
                months: Q,
                monthsShort: Q,
                weekdays: "الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),
                weekdaysShort: "أحد_إثنين_ثلاثاء_أربعاء_خميس_جمعة_سبت".split("_"),
                weekdaysMin: "ح_ن_ث_ر_خ_ج_س".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "D/‏M/‏YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                meridiemParse: /ص|م/,
                isPM: function(e) {
                    return "م" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "ص" : "م"
                },
                calendar: {
                    sameDay: "[اليوم عند الساعة] LT",
                    nextDay: "[غدًا عند الساعة] LT",
                    nextWeek: "dddd [عند الساعة] LT",
                    lastDay: "[أمس عند الساعة] LT",
                    lastWeek: "dddd [عند الساعة] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "بعد %s",
                    past: "منذ %s",
                    s: K("s"),
                    ss: K("s"),
                    m: K("m"),
                    mm: K("m"),
                    h: K("h"),
                    hh: K("h"),
                    d: K("d"),
                    dd: K("d"),
                    M: K("M"),
                    MM: K("M"),
                    y: K("y"),
                    yy: K("y")
                },
                preparse: function(e) {
                    return e.replace(/[١٢٣٤٥٦٧٨٩٠]/g, function(e) {
                        return q[e]
                    }).replace(/،/g, ",")
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return V[e]
                    }).replace(/,/g, "،")
                },
                week: {
                    dow: 6,
                    doy: 12
                }
            });
            var X = {
                1: "-inci",
                5: "-inci",
                8: "-inci",
                70: "-inci",
                80: "-inci",
                2: "-nci",
                7: "-nci",
                20: "-nci",
                50: "-nci",
                3: "-üncü",
                4: "-üncü",
                100: "-üncü",
                6: "-ncı",
                9: "-uncu",
                10: "-uncu",
                30: "-uncu",
                60: "-ıncı",
                90: "-ıncı"
            };
            e.defineLocale("az", {
                months: "yanvar_fevral_mart_aprel_may_iyun_iyul_avqust_sentyabr_oktyabr_noyabr_dekabr".split("_"),
                monthsShort: "yan_fev_mar_apr_may_iyn_iyl_avq_sen_okt_noy_dek".split("_"),
                weekdays: "Bazar_Bazar ertəsi_Çərşənbə axşamı_Çərşənbə_Cümə axşamı_Cümə_Şənbə".split("_"),
                weekdaysShort: "Baz_BzE_ÇAx_Çər_CAx_Cüm_Şən".split("_"),
                weekdaysMin: "Bz_BE_ÇA_Çə_CA_Cü_Şə".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[bugün saat] LT",
                    nextDay: "[sabah saat] LT",
                    nextWeek: "[gələn həftə] dddd [saat] LT",
                    lastDay: "[dünən] LT",
                    lastWeek: "[keçən həftə] dddd [saat] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s sonra",
                    past: "%s əvvəl",
                    s: "birneçə saniyə",
                    ss: "%d saniyə",
                    m: "bir dəqiqə",
                    mm: "%d dəqiqə",
                    h: "bir saat",
                    hh: "%d saat",
                    d: "bir gün",
                    dd: "%d gün",
                    M: "bir ay",
                    MM: "%d ay",
                    y: "bir il",
                    yy: "%d il"
                },
                meridiemParse: /gecə|səhər|gündüz|axşam/,
                isPM: function(e) {
                    return /^(gündüz|axşam)$/.test(e)
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "gecə" : e < 12 ? "səhər" : e < 17 ? "gündüz" : "axşam"
                },
                dayOfMonthOrdinalParse: /\d{1,2}-(ıncı|inci|nci|üncü|ncı|uncu)/,
                ordinal: function(e) {
                    if (0 === e) return e + "-ıncı";
                    var t = e % 10;
                    return e + (X[t] || X[e % 100 - t] || X[e >= 100 ? 100 : null])
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("be", {
                months: {
                    format: "студзеня_лютага_сакавіка_красавіка_траўня_чэрвеня_ліпеня_жніўня_верасня_кастрычніка_лістапада_снежня".split("_"),
                    standalone: "студзень_люты_сакавік_красавік_травень_чэрвень_ліпень_жнівень_верасень_кастрычнік_лістапад_снежань".split("_")
                },
                monthsShort: "студ_лют_сак_крас_трав_чэрв_ліп_жнів_вер_каст_ліст_снеж".split("_"),
                weekdays: {
                    format: "нядзелю_панядзелак_аўторак_сераду_чацвер_пятніцу_суботу".split("_"),
                    standalone: "нядзеля_панядзелак_аўторак_серада_чацвер_пятніца_субота".split("_"),
                    isFormat: /\[ ?[Ууў] ?(?:мінулую|наступную)? ?\] ?dddd/
                },
                weekdaysShort: "нд_пн_ат_ср_чц_пт_сб".split("_"),
                weekdaysMin: "нд_пн_ат_ср_чц_пт_сб".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY г.",
                    LLL: "D MMMM YYYY г., HH:mm",
                    LLLL: "dddd, D MMMM YYYY г., HH:mm"
                },
                calendar: {
                    sameDay: "[Сёння ў] LT",
                    nextDay: "[Заўтра ў] LT",
                    lastDay: "[Учора ў] LT",
                    nextWeek: function() {
                        return "[У] dddd [ў] LT"
                    },
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                            case 3:
                            case 5:
                            case 6:
                                return "[У мінулую] dddd [ў] LT";
                            case 1:
                            case 2:
                            case 4:
                                return "[У мінулы] dddd [ў] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "праз %s",
                    past: "%s таму",
                    s: "некалькі секунд",
                    m: t,
                    mm: t,
                    h: t,
                    hh: t,
                    d: "дзень",
                    dd: t,
                    M: "месяц",
                    MM: t,
                    y: "год",
                    yy: t
                },
                meridiemParse: /ночы|раніцы|дня|вечара/,
                isPM: function(e) {
                    return /^(дня|вечара)$/.test(e)
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "ночы" : e < 12 ? "раніцы" : e < 17 ? "дня" : "вечара"
                },
                dayOfMonthOrdinalParse: /\d{1,2}-(і|ы|га)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "M":
                        case "d":
                        case "DDD":
                        case "w":
                        case "W":
                            return e % 10 != 2 && e % 10 != 3 || e % 100 == 12 || e % 100 == 13 ? e + "-ы" : e + "-і";
                        case "D":
                            return e + "-га";
                        default:
                            return e
                    }
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("bg", {
                months: "януари_февруари_март_април_май_юни_юли_август_септември_октомври_ноември_декември".split("_"),
                monthsShort: "янр_фев_мар_апр_май_юни_юли_авг_сеп_окт_ное_дек".split("_"),
                weekdays: "неделя_понеделник_вторник_сряда_четвъртък_петък_събота".split("_"),
                weekdaysShort: "нед_пон_вто_сря_чет_пет_съб".split("_"),
                weekdaysMin: "нд_пн_вт_ср_чт_пт_сб".split("_"),
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "D.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY H:mm",
                    LLLL: "dddd, D MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[Днес в] LT",
                    nextDay: "[Утре в] LT",
                    nextWeek: "dddd [в] LT",
                    lastDay: "[Вчера в] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                            case 3:
                            case 6:
                                return "[В изминалата] dddd [в] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[В изминалия] dddd [в] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "след %s",
                    past: "преди %s",
                    s: "няколко секунди",
                    ss: "%d секунди",
                    m: "минута",
                    mm: "%d минути",
                    h: "час",
                    hh: "%d часа",
                    d: "ден",
                    dd: "%d дни",
                    M: "месец",
                    MM: "%d месеца",
                    y: "година",
                    yy: "%d години"
                },
                dayOfMonthOrdinalParse: /\d{1,2}-(ев|ен|ти|ви|ри|ми)/,
                ordinal: function(e) {
                    var t = e % 10,
                        a = e % 100;
                    return 0 === e ? e + "-ев" : 0 === a ? e + "-ен" : a > 10 && a < 20 ? e + "-ти" : 1 === t ? e + "-ви" : 2 === t ? e + "-ри" : 7 === t || 8 === t ? e + "-ми" : e + "-ти"
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("bm", {
                months: "Zanwuyekalo_Fewuruyekalo_Marisikalo_Awirilikalo_Mɛkalo_Zuwɛnkalo_Zuluyekalo_Utikalo_Sɛtanburukalo_ɔkutɔburukalo_Nowanburukalo_Desanburukalo".split("_"),
                monthsShort: "Zan_Few_Mar_Awi_Mɛ_Zuw_Zul_Uti_Sɛt_ɔku_Now_Des".split("_"),
                weekdays: "Kari_Ntɛnɛn_Tarata_Araba_Alamisa_Juma_Sibiri".split("_"),
                weekdaysShort: "Kar_Ntɛ_Tar_Ara_Ala_Jum_Sib".split("_"),
                weekdaysMin: "Ka_Nt_Ta_Ar_Al_Ju_Si".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "MMMM [tile] D [san] YYYY",
                    LLL: "MMMM [tile] D [san] YYYY [lɛrɛ] HH:mm",
                    LLLL: "dddd MMMM [tile] D [san] YYYY [lɛrɛ] HH:mm"
                },
                calendar: {
                    sameDay: "[Bi lɛrɛ] LT",
                    nextDay: "[Sini lɛrɛ] LT",
                    nextWeek: "dddd [don lɛrɛ] LT",
                    lastDay: "[Kunu lɛrɛ] LT",
                    lastWeek: "dddd [tɛmɛnen lɛrɛ] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s kɔnɔ",
                    past: "a bɛ %s bɔ",
                    s: "sanga dama dama",
                    ss: "sekondi %d",
                    m: "miniti kelen",
                    mm: "miniti %d",
                    h: "lɛrɛ kelen",
                    hh: "lɛrɛ %d",
                    d: "tile kelen",
                    dd: "tile %d",
                    M: "kalo kelen",
                    MM: "kalo %d",
                    y: "san kelen",
                    yy: "san %d"
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var ee = {
                    1: "১",
                    2: "২",
                    3: "৩",
                    4: "৪",
                    5: "৫",
                    6: "৬",
                    7: "৭",
                    8: "৮",
                    9: "৯",
                    0: "০"
                },
                te = {
                    "১": "1",
                    "২": "2",
                    "৩": "3",
                    "৪": "4",
                    "৫": "5",
                    "৬": "6",
                    "৭": "7",
                    "৮": "8",
                    "৯": "9",
                    "০": "0"
                };
            e.defineLocale("bn", {
                months: "জানুয়ারী_ফেব্রুয়ারি_মার্চ_এপ্রিল_মে_জুন_জুলাই_আগস্ট_সেপ্টেম্বর_অক্টোবর_নভেম্বর_ডিসেম্বর".split("_"),
                monthsShort: "জানু_ফেব_মার্চ_এপ্র_মে_জুন_জুল_আগ_সেপ্ট_অক্টো_নভে_ডিসে".split("_"),
                weekdays: "রবিবার_সোমবার_মঙ্গলবার_বুধবার_বৃহস্পতিবার_শুক্রবার_শনিবার".split("_"),
                weekdaysShort: "রবি_সোম_মঙ্গল_বুধ_বৃহস্পতি_শুক্র_শনি".split("_"),
                weekdaysMin: "রবি_সোম_মঙ্গ_বুধ_বৃহঃ_শুক্র_শনি".split("_"),
                longDateFormat: {
                    LT: "A h:mm সময়",
                    LTS: "A h:mm:ss সময়",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, A h:mm সময়",
                    LLLL: "dddd, D MMMM YYYY, A h:mm সময়"
                },
                calendar: {
                    sameDay: "[আজ] LT",
                    nextDay: "[আগামীকাল] LT",
                    nextWeek: "dddd, LT",
                    lastDay: "[গতকাল] LT",
                    lastWeek: "[গত] dddd, LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s পরে",
                    past: "%s আগে",
                    s: "কয়েক সেকেন্ড",
                    ss: "%d সেকেন্ড",
                    m: "এক মিনিট",
                    mm: "%d মিনিট",
                    h: "এক ঘন্টা",
                    hh: "%d ঘন্টা",
                    d: "এক দিন",
                    dd: "%d দিন",
                    M: "এক মাস",
                    MM: "%d মাস",
                    y: "এক বছর",
                    yy: "%d বছর"
                },
                preparse: function(e) {
                    return e.replace(/[১২৩৪৫৬৭৮৯০]/g, function(e) {
                        return te[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return ee[e]
                    })
                },
                meridiemParse: /রাত|সকাল|দুপুর|বিকাল|রাত/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "রাত" === t && e >= 4 || "দুপুর" === t && e < 5 || "বিকাল" === t ? e + 12 : e
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "রাত" : e < 10 ? "সকাল" : e < 17 ? "দুপুর" : e < 20 ? "বিকাল" : "রাত"
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            });
            var ae = {
                    1: "༡",
                    2: "༢",
                    3: "༣",
                    4: "༤",
                    5: "༥",
                    6: "༦",
                    7: "༧",
                    8: "༨",
                    9: "༩",
                    0: "༠"
                },
                ne = {
                    "༡": "1",
                    "༢": "2",
                    "༣": "3",
                    "༤": "4",
                    "༥": "5",
                    "༦": "6",
                    "༧": "7",
                    "༨": "8",
                    "༩": "9",
                    "༠": "0"
                };
            e.defineLocale("bo", {
                months: "ཟླ་བ་དང་པོ_ཟླ་བ་གཉིས་པ_ཟླ་བ་གསུམ་པ_ཟླ་བ་བཞི་པ_ཟླ་བ་ལྔ་པ_ཟླ་བ་དྲུག་པ_ཟླ་བ་བདུན་པ_ཟླ་བ་བརྒྱད་པ_ཟླ་བ་དགུ་པ_ཟླ་བ་བཅུ་པ_ཟླ་བ་བཅུ་གཅིག་པ_ཟླ་བ་བཅུ་གཉིས་པ".split("_"),
                monthsShort: "ཟླ་བ་དང་པོ_ཟླ་བ་གཉིས་པ_ཟླ་བ་གསུམ་པ_ཟླ་བ་བཞི་པ_ཟླ་བ་ལྔ་པ_ཟླ་བ་དྲུག་པ_ཟླ་བ་བདུན་པ_ཟླ་བ་བརྒྱད་པ_ཟླ་བ་དགུ་པ_ཟླ་བ་བཅུ་པ_ཟླ་བ་བཅུ་གཅིག་པ_ཟླ་བ་བཅུ་གཉིས་པ".split("_"),
                weekdays: "གཟའ་ཉི་མ་_གཟའ་ཟླ་བ་_གཟའ་མིག་དམར་_གཟའ་ལྷག་པ་_གཟའ་ཕུར་བུ_གཟའ་པ་སངས་_གཟའ་སྤེན་པ་".split("_"),
                weekdaysShort: "ཉི་མ་_ཟླ་བ་_མིག་དམར་_ལྷག་པ་_ཕུར་བུ_པ་སངས་_སྤེན་པ་".split("_"),
                weekdaysMin: "ཉི་མ་_ཟླ་བ་_མིག་དམར་_ལྷག་པ་_ཕུར་བུ_པ་སངས་_སྤེན་པ་".split("_"),
                longDateFormat: {
                    LT: "A h:mm",
                    LTS: "A h:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, A h:mm",
                    LLLL: "dddd, D MMMM YYYY, A h:mm"
                },
                calendar: {
                    sameDay: "[དི་རིང] LT",
                    nextDay: "[སང་ཉིན] LT",
                    nextWeek: "[བདུན་ཕྲག་རྗེས་མ], LT",
                    lastDay: "[ཁ་སང] LT",
                    lastWeek: "[བདུན་ཕྲག་མཐའ་མ] dddd, LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s ལ་",
                    past: "%s སྔན་ལ",
                    s: "ལམ་སང",
                    ss: "%d སྐར་ཆ།",
                    m: "སྐར་མ་གཅིག",
                    mm: "%d སྐར་མ",
                    h: "ཆུ་ཚོད་གཅིག",
                    hh: "%d ཆུ་ཚོད",
                    d: "ཉིན་གཅིག",
                    dd: "%d ཉིན་",
                    M: "ཟླ་བ་གཅིག",
                    MM: "%d ཟླ་བ",
                    y: "ལོ་གཅིག",
                    yy: "%d ལོ"
                },
                preparse: function(e) {
                    return e.replace(/[༡༢༣༤༥༦༧༨༩༠]/g, function(e) {
                        return ne[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return ae[e]
                    })
                },
                meridiemParse: /མཚན་མོ|ཞོགས་ཀས|ཉིན་གུང|དགོང་དག|མཚན་མོ/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "མཚན་མོ" === t && e >= 4 || "ཉིན་གུང" === t && e < 5 || "དགོང་དག" === t ? e + 12 : e
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "མཚན་མོ" : e < 10 ? "ཞོགས་ཀས" : e < 17 ? "ཉིན་གུང" : e < 20 ? "དགོང་དག" : "མཚན་མོ"
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            }), e.defineLocale("br", {
                months: "Genver_C'hwevrer_Meurzh_Ebrel_Mae_Mezheven_Gouere_Eost_Gwengolo_Here_Du_Kerzu".split("_"),
                monthsShort: "Gen_C'hwe_Meu_Ebr_Mae_Eve_Gou_Eos_Gwe_Her_Du_Ker".split("_"),
                weekdays: "Sul_Lun_Meurzh_Merc'her_Yaou_Gwener_Sadorn".split("_"),
                weekdaysShort: "Sul_Lun_Meu_Mer_Yao_Gwe_Sad".split("_"),
                weekdaysMin: "Su_Lu_Me_Mer_Ya_Gw_Sa".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "h[e]mm A",
                    LTS: "h[e]mm:ss A",
                    L: "DD/MM/YYYY",
                    LL: "D [a viz] MMMM YYYY",
                    LLL: "D [a viz] MMMM YYYY h[e]mm A",
                    LLLL: "dddd, D [a viz] MMMM YYYY h[e]mm A"
                },
                calendar: {
                    sameDay: "[Hiziv da] LT",
                    nextDay: "[Warc'hoazh da] LT",
                    nextWeek: "dddd [da] LT",
                    lastDay: "[Dec'h da] LT",
                    lastWeek: "dddd [paset da] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "a-benn %s",
                    past: "%s 'zo",
                    s: "un nebeud segondennoù",
                    ss: "%d eilenn",
                    m: "ur vunutenn",
                    mm: a,
                    h: "un eur",
                    hh: "%d eur",
                    d: "un devezh",
                    dd: a,
                    M: "ur miz",
                    MM: a,
                    y: "ur bloaz",
                    yy: function(e) {
                        switch (n(e)) {
                            case 1:
                            case 3:
                            case 4:
                            case 5:
                            case 9:
                                return e + " bloaz";
                            default:
                                return e + " vloaz"
                        }
                    }
                },
                dayOfMonthOrdinalParse: /\d{1,2}(añ|vet)/,
                ordinal: function(e) {
                    return e + (1 === e ? "añ" : "vet")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("bs", {
                months: "januar_februar_mart_april_maj_juni_juli_august_septembar_oktobar_novembar_decembar".split("_"),
                monthsShort: "jan._feb._mar._apr._maj._jun._jul._aug._sep._okt._nov._dec.".split("_"),
                monthsParseExact: !0,
                weekdays: "nedjelja_ponedjeljak_utorak_srijeda_četvrtak_petak_subota".split("_"),
                weekdaysShort: "ned._pon._uto._sri._čet._pet._sub.".split("_"),
                weekdaysMin: "ne_po_ut_sr_če_pe_su".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY H:mm",
                    LLLL: "dddd, D. MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[danas u] LT",
                    nextDay: "[sutra u] LT",
                    nextWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[u] [nedjelju] [u] LT";
                            case 3:
                                return "[u] [srijedu] [u] LT";
                            case 6:
                                return "[u] [subotu] [u] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[u] dddd [u] LT"
                        }
                    },
                    lastDay: "[jučer u] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                            case 3:
                                return "[prošlu] dddd [u] LT";
                            case 6:
                                return "[prošle] [subote] [u] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[prošli] dddd [u] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "za %s",
                    past: "prije %s",
                    s: "par sekundi",
                    ss: s,
                    m: s,
                    mm: s,
                    h: s,
                    hh: s,
                    d: "dan",
                    dd: s,
                    M: "mjesec",
                    MM: s,
                    y: "godinu",
                    yy: s
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("ca", {
                months: {
                    standalone: "gener_febrer_març_abril_maig_juny_juliol_agost_setembre_octubre_novembre_desembre".split("_"),
                    format: "de gener_de febrer_de març_d'abril_de maig_de juny_de juliol_d'agost_de setembre_d'octubre_de novembre_de desembre".split("_"),
                    isFormat: /D[oD]?(\s)+MMMM/
                },
                monthsShort: "gen._febr._març_abr._maig_juny_jul._ag._set._oct._nov._des.".split("_"),
                monthsParseExact: !0,
                weekdays: "diumenge_dilluns_dimarts_dimecres_dijous_divendres_dissabte".split("_"),
                weekdaysShort: "dg._dl._dt._dc._dj._dv._ds.".split("_"),
                weekdaysMin: "dg_dl_dt_dc_dj_dv_ds".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM [de] YYYY",
                    ll: "D MMM YYYY",
                    LLL: "D MMMM [de] YYYY [a les] H:mm",
                    lll: "D MMM YYYY, H:mm",
                    LLLL: "dddd D MMMM [de] YYYY [a les] H:mm",
                    llll: "ddd D MMM YYYY, H:mm"
                },
                calendar: {
                    sameDay: function() {
                        return "[avui a " + (1 !== this.hours() ? "les" : "la") + "] LT"
                    },
                    nextDay: function() {
                        return "[demà a " + (1 !== this.hours() ? "les" : "la") + "] LT"
                    },
                    nextWeek: function() {
                        return "dddd [a " + (1 !== this.hours() ? "les" : "la") + "] LT"
                    },
                    lastDay: function() {
                        return "[ahir a " + (1 !== this.hours() ? "les" : "la") + "] LT"
                    },
                    lastWeek: function() {
                        return "[el] dddd [passat a " + (1 !== this.hours() ? "les" : "la") + "] LT"
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "d'aquí %s",
                    past: "fa %s",
                    s: "uns segons",
                    ss: "%d segons",
                    m: "un minut",
                    mm: "%d minuts",
                    h: "una hora",
                    hh: "%d hores",
                    d: "un dia",
                    dd: "%d dies",
                    M: "un mes",
                    MM: "%d mesos",
                    y: "un any",
                    yy: "%d anys"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(r|n|t|è|a)/,
                ordinal: function(e, t) {
                    var a = 1 === e ? "r" : 2 === e ? "n" : 3 === e ? "r" : 4 === e ? "t" : "è";
                    return "w" !== t && "W" !== t || (a = "a"), e + a
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var se = "leden_únor_březen_duben_květen_červen_červenec_srpen_září_říjen_listopad_prosinec".split("_"),
                ie = "led_úno_bře_dub_kvě_čvn_čvc_srp_zář_říj_lis_pro".split("_");
            e.defineLocale("cs", {
                months: se,
                monthsShort: ie,
                monthsParse: function(e, t) {
                    var a, n = [];
                    for (a = 0; a < 12; a++) n[a] = new RegExp("^" + e[a] + "$|^" + t[a] + "$", "i");
                    return n
                }(se, ie),
                shortMonthsParse: function(e) {
                    var t, a = [];
                    for (t = 0; t < 12; t++) a[t] = new RegExp("^" + e[t] + "$", "i");
                    return a
                }(ie),
                longMonthsParse: function(e) {
                    var t, a = [];
                    for (t = 0; t < 12; t++) a[t] = new RegExp("^" + e[t] + "$", "i");
                    return a
                }(se),
                weekdays: "neděle_pondělí_úterý_středa_čtvrtek_pátek_sobota".split("_"),
                weekdaysShort: "ne_po_út_st_čt_pá_so".split("_"),
                weekdaysMin: "ne_po_út_st_čt_pá_so".split("_"),
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY H:mm",
                    LLLL: "dddd D. MMMM YYYY H:mm",
                    l: "D. M. YYYY"
                },
                calendar: {
                    sameDay: "[dnes v] LT",
                    nextDay: "[zítra v] LT",
                    nextWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[v neděli v] LT";
                            case 1:
                            case 2:
                                return "[v] dddd [v] LT";
                            case 3:
                                return "[ve středu v] LT";
                            case 4:
                                return "[ve čtvrtek v] LT";
                            case 5:
                                return "[v pátek v] LT";
                            case 6:
                                return "[v sobotu v] LT"
                        }
                    },
                    lastDay: "[včera v] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[minulou neděli v] LT";
                            case 1:
                            case 2:
                                return "[minulé] dddd [v] LT";
                            case 3:
                                return "[minulou středu v] LT";
                            case 4:
                            case 5:
                                return "[minulý] dddd [v] LT";
                            case 6:
                                return "[minulou sobotu v] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "za %s",
                    past: "před %s",
                    s: r,
                    ss: r,
                    m: r,
                    mm: r,
                    h: r,
                    hh: r,
                    d: r,
                    dd: r,
                    M: r,
                    MM: r,
                    y: r,
                    yy: r
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("cv", {
                months: "кӑрлач_нарӑс_пуш_ака_май_ҫӗртме_утӑ_ҫурла_авӑн_юпа_чӳк_раштав".split("_"),
                monthsShort: "кӑр_нар_пуш_ака_май_ҫӗр_утӑ_ҫур_авн_юпа_чӳк_раш".split("_"),
                weekdays: "вырсарникун_тунтикун_ытларикун_юнкун_кӗҫнерникун_эрнекун_шӑматкун".split("_"),
                weekdaysShort: "выр_тун_ытл_юн_кӗҫ_эрн_шӑм".split("_"),
                weekdaysMin: "вр_тн_ыт_юн_кҫ_эр_шм".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD-MM-YYYY",
                    LL: "YYYY [ҫулхи] MMMM [уйӑхӗн] D[-мӗшӗ]",
                    LLL: "YYYY [ҫулхи] MMMM [уйӑхӗн] D[-мӗшӗ], HH:mm",
                    LLLL: "dddd, YYYY [ҫулхи] MMMM [уйӑхӗн] D[-мӗшӗ], HH:mm"
                },
                calendar: {
                    sameDay: "[Паян] LT [сехетре]",
                    nextDay: "[Ыран] LT [сехетре]",
                    lastDay: "[Ӗнер] LT [сехетре]",
                    nextWeek: "[Ҫитес] dddd LT [сехетре]",
                    lastWeek: "[Иртнӗ] dddd LT [сехетре]",
                    sameElse: "L"
                },
                relativeTime: {
                    future: function(e) {
                        return e + (/сехет$/i.exec(e) ? "рен" : /ҫул$/i.exec(e) ? "тан" : "ран")
                    },
                    past: "%s каялла",
                    s: "пӗр-ик ҫеккунт",
                    ss: "%d ҫеккунт",
                    m: "пӗр минут",
                    mm: "%d минут",
                    h: "пӗр сехет",
                    hh: "%d сехет",
                    d: "пӗр кун",
                    dd: "%d кун",
                    M: "пӗр уйӑх",
                    MM: "%d уйӑх",
                    y: "пӗр ҫул",
                    yy: "%d ҫул"
                },
                dayOfMonthOrdinalParse: /\d{1,2}-мӗш/,
                ordinal: "%d-мӗш",
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("cy", {
                months: "Ionawr_Chwefror_Mawrth_Ebrill_Mai_Mehefin_Gorffennaf_Awst_Medi_Hydref_Tachwedd_Rhagfyr".split("_"),
                monthsShort: "Ion_Chwe_Maw_Ebr_Mai_Meh_Gor_Aws_Med_Hyd_Tach_Rhag".split("_"),
                weekdays: "Dydd Sul_Dydd Llun_Dydd Mawrth_Dydd Mercher_Dydd Iau_Dydd Gwener_Dydd Sadwrn".split("_"),
                weekdaysShort: "Sul_Llun_Maw_Mer_Iau_Gwe_Sad".split("_"),
                weekdaysMin: "Su_Ll_Ma_Me_Ia_Gw_Sa".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Heddiw am] LT",
                    nextDay: "[Yfory am] LT",
                    nextWeek: "dddd [am] LT",
                    lastDay: "[Ddoe am] LT",
                    lastWeek: "dddd [diwethaf am] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "mewn %s",
                    past: "%s yn ôl",
                    s: "ychydig eiliadau",
                    ss: "%d eiliad",
                    m: "munud",
                    mm: "%d munud",
                    h: "awr",
                    hh: "%d awr",
                    d: "diwrnod",
                    dd: "%d diwrnod",
                    M: "mis",
                    MM: "%d mis",
                    y: "blwyddyn",
                    yy: "%d flynedd"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(fed|ain|af|il|ydd|ed|eg)/,
                ordinal: function(e) {
                    var t = "";
                    return e > 20 ? t = 40 === e || 50 === e || 60 === e || 80 === e || 100 === e ? "fed" : "ain" : e > 0 && (t = ["", "af", "il", "ydd", "ydd", "ed", "ed", "ed", "fed", "fed", "fed", "eg", "fed", "eg", "eg", "fed", "eg", "eg", "fed", "eg", "fed"][e]), e + t
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("da", {
                months: "januar_februar_marts_april_maj_juni_juli_august_september_oktober_november_december".split("_"),
                monthsShort: "jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec".split("_"),
                weekdays: "søndag_mandag_tirsdag_onsdag_torsdag_fredag_lørdag".split("_"),
                weekdaysShort: "søn_man_tir_ons_tor_fre_lør".split("_"),
                weekdaysMin: "sø_ma_ti_on_to_fr_lø".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY HH:mm",
                    LLLL: "dddd [d.] D. MMMM YYYY [kl.] HH:mm"
                },
                calendar: {
                    sameDay: "[i dag kl.] LT",
                    nextDay: "[i morgen kl.] LT",
                    nextWeek: "på dddd [kl.] LT",
                    lastDay: "[i går kl.] LT",
                    lastWeek: "[i] dddd[s kl.] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "om %s",
                    past: "%s siden",
                    s: "få sekunder",
                    ss: "%d sekunder",
                    m: "et minut",
                    mm: "%d minutter",
                    h: "en time",
                    hh: "%d timer",
                    d: "en dag",
                    dd: "%d dage",
                    M: "en måned",
                    MM: "%d måneder",
                    y: "et år",
                    yy: "%d år"
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("de-at", {
                months: "Jänner_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),
                monthsShort: "Jän._Feb._März_Apr._Mai_Juni_Juli_Aug._Sep._Okt._Nov._Dez.".split("_"),
                monthsParseExact: !0,
                weekdays: "Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag".split("_"),
                weekdaysShort: "So._Mo._Di._Mi._Do._Fr._Sa.".split("_"),
                weekdaysMin: "So_Mo_Di_Mi_Do_Fr_Sa".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY HH:mm",
                    LLLL: "dddd, D. MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[heute um] LT [Uhr]",
                    sameElse: "L",
                    nextDay: "[morgen um] LT [Uhr]",
                    nextWeek: "dddd [um] LT [Uhr]",
                    lastDay: "[gestern um] LT [Uhr]",
                    lastWeek: "[letzten] dddd [um] LT [Uhr]"
                },
                relativeTime: {
                    future: "in %s",
                    past: "vor %s",
                    s: "ein paar Sekunden",
                    ss: "%d Sekunden",
                    m: o,
                    mm: "%d Minuten",
                    h: o,
                    hh: "%d Stunden",
                    d: o,
                    dd: o,
                    M: o,
                    MM: o,
                    y: o,
                    yy: o
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("de-ch", {
                months: "Januar_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),
                monthsShort: "Jan._Feb._März_Apr._Mai_Juni_Juli_Aug._Sep._Okt._Nov._Dez.".split("_"),
                monthsParseExact: !0,
                weekdays: "Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag".split("_"),
                weekdaysShort: "So_Mo_Di_Mi_Do_Fr_Sa".split("_"),
                weekdaysMin: "So_Mo_Di_Mi_Do_Fr_Sa".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY HH:mm",
                    LLLL: "dddd, D. MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[heute um] LT [Uhr]",
                    sameElse: "L",
                    nextDay: "[morgen um] LT [Uhr]",
                    nextWeek: "dddd [um] LT [Uhr]",
                    lastDay: "[gestern um] LT [Uhr]",
                    lastWeek: "[letzten] dddd [um] LT [Uhr]"
                },
                relativeTime: {
                    future: "in %s",
                    past: "vor %s",
                    s: "ein paar Sekunden",
                    ss: "%d Sekunden",
                    m: d,
                    mm: "%d Minuten",
                    h: d,
                    hh: "%d Stunden",
                    d: d,
                    dd: d,
                    M: d,
                    MM: d,
                    y: d,
                    yy: d
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("de", {
                months: "Januar_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),
                monthsShort: "Jan._Feb._März_Apr._Mai_Juni_Juli_Aug._Sep._Okt._Nov._Dez.".split("_"),
                monthsParseExact: !0,
                weekdays: "Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag".split("_"),
                weekdaysShort: "So._Mo._Di._Mi._Do._Fr._Sa.".split("_"),
                weekdaysMin: "So_Mo_Di_Mi_Do_Fr_Sa".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY HH:mm",
                    LLLL: "dddd, D. MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[heute um] LT [Uhr]",
                    sameElse: "L",
                    nextDay: "[morgen um] LT [Uhr]",
                    nextWeek: "dddd [um] LT [Uhr]",
                    lastDay: "[gestern um] LT [Uhr]",
                    lastWeek: "[letzten] dddd [um] LT [Uhr]"
                },
                relativeTime: {
                    future: "in %s",
                    past: "vor %s",
                    s: "ein paar Sekunden",
                    ss: "%d Sekunden",
                    m: l,
                    mm: "%d Minuten",
                    h: l,
                    hh: "%d Stunden",
                    d: l,
                    dd: l,
                    M: l,
                    MM: l,
                    y: l,
                    yy: l
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var re = ["ޖެނުއަރީ", "ފެބްރުއަރީ", "މާރިޗު", "އޭޕްރީލު", "މޭ", "ޖޫން", "ޖުލައި", "އޯގަސްޓު", "ސެޕްޓެމްބަރު", "އޮކްޓޯބަރު", "ނޮވެމްބަރު", "ޑިސެމްބަރު"],
                oe = ["އާދިއްތަ", "ހޯމަ", "އަންގާރަ", "ބުދަ", "ބުރާސްފަތި", "ހުކުރު", "ހޮނިހިރު"];
            e.defineLocale("dv", {
                months: re,
                monthsShort: re,
                weekdays: oe,
                weekdaysShort: oe,
                weekdaysMin: "އާދި_ހޯމަ_އަން_ބުދަ_ބުރާ_ހުކު_ހޮނި".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "D/M/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                meridiemParse: /މކ|މފ/,
                isPM: function(e) {
                    return "މފ" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "މކ" : "މފ"
                },
                calendar: {
                    sameDay: "[މިއަދު] LT",
                    nextDay: "[މާދަމާ] LT",
                    nextWeek: "dddd LT",
                    lastDay: "[އިއްޔެ] LT",
                    lastWeek: "[ފާއިތުވި] dddd LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "ތެރޭގައި %s",
                    past: "ކުރިން %s",
                    s: "ސިކުންތުކޮޅެއް",
                    ss: "d% ސިކުންތު",
                    m: "މިނިޓެއް",
                    mm: "މިނިޓު %d",
                    h: "ގަޑިއިރެއް",
                    hh: "ގަޑިއިރު %d",
                    d: "ދުވަހެއް",
                    dd: "ދުވަސް %d",
                    M: "މަހެއް",
                    MM: "މަސް %d",
                    y: "އަހަރެއް",
                    yy: "އަހަރު %d"
                },
                preparse: function(e) {
                    return e.replace(/،/g, ",")
                },
                postformat: function(e) {
                    return e.replace(/,/g, "،")
                },
                week: {
                    dow: 7,
                    doy: 12
                }
            }), e.defineLocale("el", {
                monthsNominativeEl: "Ιανουάριος_Φεβρουάριος_Μάρτιος_Απρίλιος_Μάιος_Ιούνιος_Ιούλιος_Αύγουστος_Σεπτέμβριος_Οκτώβριος_Νοέμβριος_Δεκέμβριος".split("_"),
                monthsGenitiveEl: "Ιανουαρίου_Φεβρουαρίου_Μαρτίου_Απριλίου_Μαΐου_Ιουνίου_Ιουλίου_Αυγούστου_Σεπτεμβρίου_Οκτωβρίου_Νοεμβρίου_Δεκεμβρίου".split("_"),
                months: function(e, t) {
                    return e ? "string" == typeof t && /D/.test(t.substring(0, t.indexOf("MMMM"))) ? this._monthsGenitiveEl[e.month()] : this._monthsNominativeEl[e.month()] : this._monthsNominativeEl
                },
                monthsShort: "Ιαν_Φεβ_Μαρ_Απρ_Μαϊ_Ιουν_Ιουλ_Αυγ_Σεπ_Οκτ_Νοε_Δεκ".split("_"),
                weekdays: "Κυριακή_Δευτέρα_Τρίτη_Τετάρτη_Πέμπτη_Παρασκευή_Σάββατο".split("_"),
                weekdaysShort: "Κυρ_Δευ_Τρι_Τετ_Πεμ_Παρ_Σαβ".split("_"),
                weekdaysMin: "Κυ_Δε_Τρ_Τε_Πε_Πα_Σα".split("_"),
                meridiem: function(e, t, a) {
                    return e > 11 ? a ? "μμ" : "ΜΜ" : a ? "πμ" : "ΠΜ"
                },
                isPM: function(e) {
                    return "μ" === (e + "").toLowerCase()[0]
                },
                meridiemParse: /[ΠΜ]\.?Μ?\.?/i,
                longDateFormat: {
                    LT: "h:mm A",
                    LTS: "h:mm:ss A",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY h:mm A",
                    LLLL: "dddd, D MMMM YYYY h:mm A"
                },
                calendarEl: {
                    sameDay: "[Σήμερα {}] LT",
                    nextDay: "[Αύριο {}] LT",
                    nextWeek: "dddd [{}] LT",
                    lastDay: "[Χθες {}] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 6:
                                return "[το προηγούμενο] dddd [{}] LT";
                            default:
                                return "[την προηγούμενη] dddd [{}] LT"
                        }
                    },
                    sameElse: "L"
                },
                calendar: function(e, t) {
                    var a = this._calendarEl[e],
                        n = t && t.hours();
                    return function(e) {
                        return e instanceof Function || "[object Function]" === Object.prototype.toString.call(e)
                    }(a) && (a = a.apply(t)), a.replace("{}", n % 12 == 1 ? "στη" : "στις")
                },
                relativeTime: {
                    future: "σε %s",
                    past: "%s πριν",
                    s: "λίγα δευτερόλεπτα",
                    ss: "%d δευτερόλεπτα",
                    m: "ένα λεπτό",
                    mm: "%d λεπτά",
                    h: "μία ώρα",
                    hh: "%d ώρες",
                    d: "μία μέρα",
                    dd: "%d μέρες",
                    M: "ένας μήνας",
                    MM: "%d μήνες",
                    y: "ένας χρόνος",
                    yy: "%d χρόνια"
                },
                dayOfMonthOrdinalParse: /\d{1,2}η/,
                ordinal: "%dη",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("en-au", {
                months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
                monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
                weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
                weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
                weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
                longDateFormat: {
                    LT: "h:mm A",
                    LTS: "h:mm:ss A",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY h:mm A",
                    LLLL: "dddd, D MMMM YYYY h:mm A"
                },
                calendar: {
                    sameDay: "[Today at] LT",
                    nextDay: "[Tomorrow at] LT",
                    nextWeek: "dddd [at] LT",
                    lastDay: "[Yesterday at] LT",
                    lastWeek: "[Last] dddd [at] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "in %s",
                    past: "%s ago",
                    s: "a few seconds",
                    ss: "%d seconds",
                    m: "a minute",
                    mm: "%d minutes",
                    h: "an hour",
                    hh: "%d hours",
                    d: "a day",
                    dd: "%d days",
                    M: "a month",
                    MM: "%d months",
                    y: "a year",
                    yy: "%d years"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(st|nd|rd|th)/,
                ordinal: function(e) {
                    var t = e % 10;
                    return e + (1 == ~~(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("en-ca", {
                months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
                monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
                weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
                weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
                weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
                longDateFormat: {
                    LT: "h:mm A",
                    LTS: "h:mm:ss A",
                    L: "YYYY-MM-DD",
                    LL: "MMMM D, YYYY",
                    LLL: "MMMM D, YYYY h:mm A",
                    LLLL: "dddd, MMMM D, YYYY h:mm A"
                },
                calendar: {
                    sameDay: "[Today at] LT",
                    nextDay: "[Tomorrow at] LT",
                    nextWeek: "dddd [at] LT",
                    lastDay: "[Yesterday at] LT",
                    lastWeek: "[Last] dddd [at] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "in %s",
                    past: "%s ago",
                    s: "a few seconds",
                    ss: "%d seconds",
                    m: "a minute",
                    mm: "%d minutes",
                    h: "an hour",
                    hh: "%d hours",
                    d: "a day",
                    dd: "%d days",
                    M: "a month",
                    MM: "%d months",
                    y: "a year",
                    yy: "%d years"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(st|nd|rd|th)/,
                ordinal: function(e) {
                    var t = e % 10;
                    return e + (1 == ~~(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th")
                }
            }), e.defineLocale("en-gb", {
                months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
                monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
                weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
                weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
                weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Today at] LT",
                    nextDay: "[Tomorrow at] LT",
                    nextWeek: "dddd [at] LT",
                    lastDay: "[Yesterday at] LT",
                    lastWeek: "[Last] dddd [at] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "in %s",
                    past: "%s ago",
                    s: "a few seconds",
                    ss: "%d seconds",
                    m: "a minute",
                    mm: "%d minutes",
                    h: "an hour",
                    hh: "%d hours",
                    d: "a day",
                    dd: "%d days",
                    M: "a month",
                    MM: "%d months",
                    y: "a year",
                    yy: "%d years"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(st|nd|rd|th)/,
                ordinal: function(e) {
                    var t = e % 10;
                    return e + (1 == ~~(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("en-ie", {
                months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
                monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
                weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
                weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
                weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD-MM-YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Today at] LT",
                    nextDay: "[Tomorrow at] LT",
                    nextWeek: "dddd [at] LT",
                    lastDay: "[Yesterday at] LT",
                    lastWeek: "[Last] dddd [at] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "in %s",
                    past: "%s ago",
                    s: "a few seconds",
                    ss: "%d seconds",
                    m: "a minute",
                    mm: "%d minutes",
                    h: "an hour",
                    hh: "%d hours",
                    d: "a day",
                    dd: "%d days",
                    M: "a month",
                    MM: "%d months",
                    y: "a year",
                    yy: "%d years"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(st|nd|rd|th)/,
                ordinal: function(e) {
                    var t = e % 10;
                    return e + (1 == ~~(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("en-il", {
                months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
                monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
                weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
                weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
                weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Today at] LT",
                    nextDay: "[Tomorrow at] LT",
                    nextWeek: "dddd [at] LT",
                    lastDay: "[Yesterday at] LT",
                    lastWeek: "[Last] dddd [at] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "in %s",
                    past: "%s ago",
                    s: "a few seconds",
                    m: "a minute",
                    mm: "%d minutes",
                    h: "an hour",
                    hh: "%d hours",
                    d: "a day",
                    dd: "%d days",
                    M: "a month",
                    MM: "%d months",
                    y: "a year",
                    yy: "%d years"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(st|nd|rd|th)/,
                ordinal: function(e) {
                    var t = e % 10;
                    return e + (1 == ~~(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th")
                }
            }), e.defineLocale("en-nz", {
                months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
                monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
                weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
                weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
                weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
                longDateFormat: {
                    LT: "h:mm A",
                    LTS: "h:mm:ss A",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY h:mm A",
                    LLLL: "dddd, D MMMM YYYY h:mm A"
                },
                calendar: {
                    sameDay: "[Today at] LT",
                    nextDay: "[Tomorrow at] LT",
                    nextWeek: "dddd [at] LT",
                    lastDay: "[Yesterday at] LT",
                    lastWeek: "[Last] dddd [at] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "in %s",
                    past: "%s ago",
                    s: "a few seconds",
                    ss: "%d seconds",
                    m: "a minute",
                    mm: "%d minutes",
                    h: "an hour",
                    hh: "%d hours",
                    d: "a day",
                    dd: "%d days",
                    M: "a month",
                    MM: "%d months",
                    y: "a year",
                    yy: "%d years"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(st|nd|rd|th)/,
                ordinal: function(e) {
                    var t = e % 10;
                    return e + (1 == ~~(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("eo", {
                months: "januaro_februaro_marto_aprilo_majo_junio_julio_aŭgusto_septembro_oktobro_novembro_decembro".split("_"),
                monthsShort: "jan_feb_mar_apr_maj_jun_jul_aŭg_sep_okt_nov_dec".split("_"),
                weekdays: "dimanĉo_lundo_mardo_merkredo_ĵaŭdo_vendredo_sabato".split("_"),
                weekdaysShort: "dim_lun_mard_merk_ĵaŭ_ven_sab".split("_"),
                weekdaysMin: "di_lu_ma_me_ĵa_ve_sa".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY-MM-DD",
                    LL: "D[-a de] MMMM, YYYY",
                    LLL: "D[-a de] MMMM, YYYY HH:mm",
                    LLLL: "dddd, [la] D[-a de] MMMM, YYYY HH:mm"
                },
                meridiemParse: /[ap]\.t\.m/i,
                isPM: function(e) {
                    return "p" === e.charAt(0).toLowerCase()
                },
                meridiem: function(e, t, a) {
                    return e > 11 ? a ? "p.t.m." : "P.T.M." : a ? "a.t.m." : "A.T.M."
                },
                calendar: {
                    sameDay: "[Hodiaŭ je] LT",
                    nextDay: "[Morgaŭ je] LT",
                    nextWeek: "dddd [je] LT",
                    lastDay: "[Hieraŭ je] LT",
                    lastWeek: "[pasinta] dddd [je] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "post %s",
                    past: "antaŭ %s",
                    s: "sekundoj",
                    ss: "%d sekundoj",
                    m: "minuto",
                    mm: "%d minutoj",
                    h: "horo",
                    hh: "%d horoj",
                    d: "tago",
                    dd: "%d tagoj",
                    M: "monato",
                    MM: "%d monatoj",
                    y: "jaro",
                    yy: "%d jaroj"
                },
                dayOfMonthOrdinalParse: /\d{1,2}a/,
                ordinal: "%da",
                week: {
                    dow: 1,
                    doy: 7
                }
            });
            var de = "ene._feb._mar._abr._may._jun._jul._ago._sep._oct._nov._dic.".split("_"),
                le = "ene_feb_mar_abr_may_jun_jul_ago_sep_oct_nov_dic".split("_"),
                ue = [/^ene/i, /^feb/i, /^mar/i, /^abr/i, /^may/i, /^jun/i, /^jul/i, /^ago/i, /^sep/i, /^oct/i, /^nov/i, /^dic/i],
                _e = /^(enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre|ene\.?|feb\.?|mar\.?|abr\.?|may\.?|jun\.?|jul\.?|ago\.?|sep\.?|oct\.?|nov\.?|dic\.?)/i;
            e.defineLocale("es-do", {
                months: "enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre".split("_"),
                monthsShort: function(e, t) {
                    return e ? /-MMM-/.test(t) ? le[e.month()] : de[e.month()] : de
                },
                monthsRegex: _e,
                monthsShortRegex: _e,
                monthsStrictRegex: /^(enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre)/i,
                monthsShortStrictRegex: /^(ene\.?|feb\.?|mar\.?|abr\.?|may\.?|jun\.?|jul\.?|ago\.?|sep\.?|oct\.?|nov\.?|dic\.?)/i,
                monthsParse: ue,
                longMonthsParse: ue,
                shortMonthsParse: ue,
                weekdays: "domingo_lunes_martes_miércoles_jueves_viernes_sábado".split("_"),
                weekdaysShort: "dom._lun._mar._mié._jue._vie._sáb.".split("_"),
                weekdaysMin: "do_lu_ma_mi_ju_vi_sá".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "h:mm A",
                    LTS: "h:mm:ss A",
                    L: "DD/MM/YYYY",
                    LL: "D [de] MMMM [de] YYYY",
                    LLL: "D [de] MMMM [de] YYYY h:mm A",
                    LLLL: "dddd, D [de] MMMM [de] YYYY h:mm A"
                },
                calendar: {
                    sameDay: function() {
                        return "[hoy a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    nextDay: function() {
                        return "[mañana a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    nextWeek: function() {
                        return "dddd [a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    lastDay: function() {
                        return "[ayer a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    lastWeek: function() {
                        return "[el] dddd [pasado a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "en %s",
                    past: "hace %s",
                    s: "unos segundos",
                    ss: "%d segundos",
                    m: "un minuto",
                    mm: "%d minutos",
                    h: "una hora",
                    hh: "%d horas",
                    d: "un día",
                    dd: "%d días",
                    M: "un mes",
                    MM: "%d meses",
                    y: "un año",
                    yy: "%d años"
                },
                dayOfMonthOrdinalParse: /\d{1,2}º/,
                ordinal: "%dº",
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var me = "ene._feb._mar._abr._may._jun._jul._ago._sep._oct._nov._dic.".split("_"),
                ce = "ene_feb_mar_abr_may_jun_jul_ago_sep_oct_nov_dic".split("_");
            e.defineLocale("es-us", {
                months: "enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre".split("_"),
                monthsShort: function(e, t) {
                    return e ? /-MMM-/.test(t) ? ce[e.month()] : me[e.month()] : me
                },
                monthsParseExact: !0,
                weekdays: "domingo_lunes_martes_miércoles_jueves_viernes_sábado".split("_"),
                weekdaysShort: "dom._lun._mar._mié._jue._vie._sáb.".split("_"),
                weekdaysMin: "do_lu_ma_mi_ju_vi_sá".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "h:mm A",
                    LTS: "h:mm:ss A",
                    L: "MM/DD/YYYY",
                    LL: "MMMM [de] D [de] YYYY",
                    LLL: "MMMM [de] D [de] YYYY h:mm A",
                    LLLL: "dddd, MMMM [de] D [de] YYYY h:mm A"
                },
                calendar: {
                    sameDay: function() {
                        return "[hoy a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    nextDay: function() {
                        return "[mañana a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    nextWeek: function() {
                        return "dddd [a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    lastDay: function() {
                        return "[ayer a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    lastWeek: function() {
                        return "[el] dddd [pasado a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "en %s",
                    past: "hace %s",
                    s: "unos segundos",
                    ss: "%d segundos",
                    m: "un minuto",
                    mm: "%d minutos",
                    h: "una hora",
                    hh: "%d horas",
                    d: "un día",
                    dd: "%d días",
                    M: "un mes",
                    MM: "%d meses",
                    y: "un año",
                    yy: "%d años"
                },
                dayOfMonthOrdinalParse: /\d{1,2}º/,
                ordinal: "%dº",
                week: {
                    dow: 0,
                    doy: 6
                }
            });
            var he = "ene._feb._mar._abr._may._jun._jul._ago._sep._oct._nov._dic.".split("_"),
                pe = "ene_feb_mar_abr_may_jun_jul_ago_sep_oct_nov_dic".split("_"),
                fe = [/^ene/i, /^feb/i, /^mar/i, /^abr/i, /^may/i, /^jun/i, /^jul/i, /^ago/i, /^sep/i, /^oct/i, /^nov/i, /^dic/i],
                Me = /^(enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre|ene\.?|feb\.?|mar\.?|abr\.?|may\.?|jun\.?|jul\.?|ago\.?|sep\.?|oct\.?|nov\.?|dic\.?)/i;
            e.defineLocale("es", {
                months: "enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre".split("_"),
                monthsShort: function(e, t) {
                    return e ? /-MMM-/.test(t) ? pe[e.month()] : he[e.month()] : he
                },
                monthsRegex: Me,
                monthsShortRegex: Me,
                monthsStrictRegex: /^(enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre)/i,
                monthsShortStrictRegex: /^(ene\.?|feb\.?|mar\.?|abr\.?|may\.?|jun\.?|jul\.?|ago\.?|sep\.?|oct\.?|nov\.?|dic\.?)/i,
                monthsParse: fe,
                longMonthsParse: fe,
                shortMonthsParse: fe,
                weekdays: "domingo_lunes_martes_miércoles_jueves_viernes_sábado".split("_"),
                weekdaysShort: "dom._lun._mar._mié._jue._vie._sáb.".split("_"),
                weekdaysMin: "do_lu_ma_mi_ju_vi_sá".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D [de] MMMM [de] YYYY",
                    LLL: "D [de] MMMM [de] YYYY H:mm",
                    LLLL: "dddd, D [de] MMMM [de] YYYY H:mm"
                },
                calendar: {
                    sameDay: function() {
                        return "[hoy a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    nextDay: function() {
                        return "[mañana a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    nextWeek: function() {
                        return "dddd [a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    lastDay: function() {
                        return "[ayer a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    lastWeek: function() {
                        return "[el] dddd [pasado a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "en %s",
                    past: "hace %s",
                    s: "unos segundos",
                    ss: "%d segundos",
                    m: "un minuto",
                    mm: "%d minutos",
                    h: "una hora",
                    hh: "%d horas",
                    d: "un día",
                    dd: "%d días",
                    M: "un mes",
                    MM: "%d meses",
                    y: "un año",
                    yy: "%d años"
                },
                dayOfMonthOrdinalParse: /\d{1,2}º/,
                ordinal: "%dº",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("et", {
                months: "jaanuar_veebruar_märts_aprill_mai_juuni_juuli_august_september_oktoober_november_detsember".split("_"),
                monthsShort: "jaan_veebr_märts_apr_mai_juuni_juuli_aug_sept_okt_nov_dets".split("_"),
                weekdays: "pühapäev_esmaspäev_teisipäev_kolmapäev_neljapäev_reede_laupäev".split("_"),
                weekdaysShort: "P_E_T_K_N_R_L".split("_"),
                weekdaysMin: "P_E_T_K_N_R_L".split("_"),
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY H:mm",
                    LLLL: "dddd, D. MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[Täna,] LT",
                    nextDay: "[Homme,] LT",
                    nextWeek: "[Järgmine] dddd LT",
                    lastDay: "[Eile,] LT",
                    lastWeek: "[Eelmine] dddd LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s pärast",
                    past: "%s tagasi",
                    s: u,
                    ss: u,
                    m: u,
                    mm: u,
                    h: u,
                    hh: u,
                    d: u,
                    dd: "%d päeva",
                    M: u,
                    MM: u,
                    y: u,
                    yy: u
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("eu", {
                months: "urtarrila_otsaila_martxoa_apirila_maiatza_ekaina_uztaila_abuztua_iraila_urria_azaroa_abendua".split("_"),
                monthsShort: "urt._ots._mar._api._mai._eka._uzt._abu._ira._urr._aza._abe.".split("_"),
                monthsParseExact: !0,
                weekdays: "igandea_astelehena_asteartea_asteazkena_osteguna_ostirala_larunbata".split("_"),
                weekdaysShort: "ig._al._ar._az._og._ol._lr.".split("_"),
                weekdaysMin: "ig_al_ar_az_og_ol_lr".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY-MM-DD",
                    LL: "YYYY[ko] MMMM[ren] D[a]",
                    LLL: "YYYY[ko] MMMM[ren] D[a] HH:mm",
                    LLLL: "dddd, YYYY[ko] MMMM[ren] D[a] HH:mm",
                    l: "YYYY-M-D",
                    ll: "YYYY[ko] MMM D[a]",
                    lll: "YYYY[ko] MMM D[a] HH:mm",
                    llll: "ddd, YYYY[ko] MMM D[a] HH:mm"
                },
                calendar: {
                    sameDay: "[gaur] LT[etan]",
                    nextDay: "[bihar] LT[etan]",
                    nextWeek: "dddd LT[etan]",
                    lastDay: "[atzo] LT[etan]",
                    lastWeek: "[aurreko] dddd LT[etan]",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s barru",
                    past: "duela %s",
                    s: "segundo batzuk",
                    ss: "%d segundo",
                    m: "minutu bat",
                    mm: "%d minutu",
                    h: "ordu bat",
                    hh: "%d ordu",
                    d: "egun bat",
                    dd: "%d egun",
                    M: "hilabete bat",
                    MM: "%d hilabete",
                    y: "urte bat",
                    yy: "%d urte"
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 7
                }
            });
            var ye = {
                    1: "۱",
                    2: "۲",
                    3: "۳",
                    4: "۴",
                    5: "۵",
                    6: "۶",
                    7: "۷",
                    8: "۸",
                    9: "۹",
                    0: "۰"
                },
                ge = {
                    "۱": "1",
                    "۲": "2",
                    "۳": "3",
                    "۴": "4",
                    "۵": "5",
                    "۶": "6",
                    "۷": "7",
                    "۸": "8",
                    "۹": "9",
                    "۰": "0"
                };
            e.defineLocale("fa", {
                months: "ژانویه_فوریه_مارس_آوریل_مه_ژوئن_ژوئیه_اوت_سپتامبر_اکتبر_نوامبر_دسامبر".split("_"),
                monthsShort: "ژانویه_فوریه_مارس_آوریل_مه_ژوئن_ژوئیه_اوت_سپتامبر_اکتبر_نوامبر_دسامبر".split("_"),
                weekdays: "یک‌شنبه_دوشنبه_سه‌شنبه_چهارشنبه_پنج‌شنبه_جمعه_شنبه".split("_"),
                weekdaysShort: "یک‌شنبه_دوشنبه_سه‌شنبه_چهارشنبه_پنج‌شنبه_جمعه_شنبه".split("_"),
                weekdaysMin: "ی_د_س_چ_پ_ج_ش".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                meridiemParse: /قبل از ظهر|بعد از ظهر/,
                isPM: function(e) {
                    return /بعد از ظهر/.test(e)
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "قبل از ظهر" : "بعد از ظهر"
                },
                calendar: {
                    sameDay: "[امروز ساعت] LT",
                    nextDay: "[فردا ساعت] LT",
                    nextWeek: "dddd [ساعت] LT",
                    lastDay: "[دیروز ساعت] LT",
                    lastWeek: "dddd [پیش] [ساعت] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "در %s",
                    past: "%s پیش",
                    s: "چند ثانیه",
                    ss: "ثانیه d%",
                    m: "یک دقیقه",
                    mm: "%d دقیقه",
                    h: "یک ساعت",
                    hh: "%d ساعت",
                    d: "یک روز",
                    dd: "%d روز",
                    M: "یک ماه",
                    MM: "%d ماه",
                    y: "یک سال",
                    yy: "%d سال"
                },
                preparse: function(e) {
                    return e.replace(/[۰-۹]/g, function(e) {
                        return ge[e]
                    }).replace(/،/g, ",")
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return ye[e]
                    }).replace(/,/g, "،")
                },
                dayOfMonthOrdinalParse: /\d{1,2}م/,
                ordinal: "%dم",
                week: {
                    dow: 6,
                    doy: 12
                }
            });
            var Le = "nolla yksi kaksi kolme neljä viisi kuusi seitsemän kahdeksan yhdeksän".split(" "),
                Ye = ["nolla", "yhden", "kahden", "kolmen", "neljän", "viiden", "kuuden", Le[7], Le[8], Le[9]];
            e.defineLocale("fi", {
                months: "tammikuu_helmikuu_maaliskuu_huhtikuu_toukokuu_kesäkuu_heinäkuu_elokuu_syyskuu_lokakuu_marraskuu_joulukuu".split("_"),
                monthsShort: "tammi_helmi_maalis_huhti_touko_kesä_heinä_elo_syys_loka_marras_joulu".split("_"),
                weekdays: "sunnuntai_maanantai_tiistai_keskiviikko_torstai_perjantai_lauantai".split("_"),
                weekdaysShort: "su_ma_ti_ke_to_pe_la".split("_"),
                weekdaysMin: "su_ma_ti_ke_to_pe_la".split("_"),
                longDateFormat: {
                    LT: "HH.mm",
                    LTS: "HH.mm.ss",
                    L: "DD.MM.YYYY",
                    LL: "Do MMMM[ta] YYYY",
                    LLL: "Do MMMM[ta] YYYY, [klo] HH.mm",
                    LLLL: "dddd, Do MMMM[ta] YYYY, [klo] HH.mm",
                    l: "D.M.YYYY",
                    ll: "Do MMM YYYY",
                    lll: "Do MMM YYYY, [klo] HH.mm",
                    llll: "ddd, Do MMM YYYY, [klo] HH.mm"
                },
                calendar: {
                    sameDay: "[tänään] [klo] LT",
                    nextDay: "[huomenna] [klo] LT",
                    nextWeek: "dddd [klo] LT",
                    lastDay: "[eilen] [klo] LT",
                    lastWeek: "[viime] dddd[na] [klo] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s päästä",
                    past: "%s sitten",
                    s: _,
                    ss: _,
                    m: _,
                    mm: _,
                    h: _,
                    hh: _,
                    d: _,
                    dd: _,
                    M: _,
                    MM: _,
                    y: _,
                    yy: _
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("fo", {
                months: "januar_februar_mars_apríl_mai_juni_juli_august_september_oktober_november_desember".split("_"),
                monthsShort: "jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_"),
                weekdays: "sunnudagur_mánadagur_týsdagur_mikudagur_hósdagur_fríggjadagur_leygardagur".split("_"),
                weekdaysShort: "sun_mán_týs_mik_hós_frí_ley".split("_"),
                weekdaysMin: "su_má_tý_mi_hó_fr_le".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D. MMMM, YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Í dag kl.] LT",
                    nextDay: "[Í morgin kl.] LT",
                    nextWeek: "dddd [kl.] LT",
                    lastDay: "[Í gjár kl.] LT",
                    lastWeek: "[síðstu] dddd [kl] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "um %s",
                    past: "%s síðani",
                    s: "fá sekund",
                    ss: "%d sekundir",
                    m: "ein minutt",
                    mm: "%d minuttir",
                    h: "ein tími",
                    hh: "%d tímar",
                    d: "ein dagur",
                    dd: "%d dagar",
                    M: "ein mánaði",
                    MM: "%d mánaðir",
                    y: "eitt ár",
                    yy: "%d ár"
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("fr-ca", {
                months: "janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),
                monthsShort: "janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),
                monthsParseExact: !0,
                weekdays: "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
                weekdaysShort: "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
                weekdaysMin: "di_lu_ma_me_je_ve_sa".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY-MM-DD",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Aujourd’hui à] LT",
                    nextDay: "[Demain à] LT",
                    nextWeek: "dddd [à] LT",
                    lastDay: "[Hier à] LT",
                    lastWeek: "dddd [dernier à] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "dans %s",
                    past: "il y a %s",
                    s: "quelques secondes",
                    ss: "%d secondes",
                    m: "une minute",
                    mm: "%d minutes",
                    h: "une heure",
                    hh: "%d heures",
                    d: "un jour",
                    dd: "%d jours",
                    M: "un mois",
                    MM: "%d mois",
                    y: "un an",
                    yy: "%d ans"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(er|e)/,
                ordinal: function(e, t) {
                    switch (t) {
                        default:
                            case "M":
                            case "Q":
                            case "D":
                            case "DDD":
                            case "d":
                            return e + (1 === e ? "er" : "e");
                        case "w":
                                case "W":
                                return e + (1 === e ? "re" : "e")
                    }
                }
            }), e.defineLocale("fr-ch", {
                months: "janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),
                monthsShort: "janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),
                monthsParseExact: !0,
                weekdays: "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
                weekdaysShort: "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
                weekdaysMin: "di_lu_ma_me_je_ve_sa".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Aujourd’hui à] LT",
                    nextDay: "[Demain à] LT",
                    nextWeek: "dddd [à] LT",
                    lastDay: "[Hier à] LT",
                    lastWeek: "dddd [dernier à] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "dans %s",
                    past: "il y a %s",
                    s: "quelques secondes",
                    ss: "%d secondes",
                    m: "une minute",
                    mm: "%d minutes",
                    h: "une heure",
                    hh: "%d heures",
                    d: "un jour",
                    dd: "%d jours",
                    M: "un mois",
                    MM: "%d mois",
                    y: "un an",
                    yy: "%d ans"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(er|e)/,
                ordinal: function(e, t) {
                    switch (t) {
                        default:
                            case "M":
                            case "Q":
                            case "D":
                            case "DDD":
                            case "d":
                            return e + (1 === e ? "er" : "e");
                        case "w":
                                case "W":
                                return e + (1 === e ? "re" : "e")
                    }
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("fr", {
                months: "janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),
                monthsShort: "janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),
                monthsParseExact: !0,
                weekdays: "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
                weekdaysShort: "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
                weekdaysMin: "di_lu_ma_me_je_ve_sa".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Aujourd’hui à] LT",
                    nextDay: "[Demain à] LT",
                    nextWeek: "dddd [à] LT",
                    lastDay: "[Hier à] LT",
                    lastWeek: "dddd [dernier à] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "dans %s",
                    past: "il y a %s",
                    s: "quelques secondes",
                    ss: "%d secondes",
                    m: "une minute",
                    mm: "%d minutes",
                    h: "une heure",
                    hh: "%d heures",
                    d: "un jour",
                    dd: "%d jours",
                    M: "un mois",
                    MM: "%d mois",
                    y: "un an",
                    yy: "%d ans"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(er|)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "D":
                            return e + (1 === e ? "er" : "");
                        default:
                        case "M":
                        case "Q":
                        case "DDD":
                        case "d":
                            return e + (1 === e ? "er" : "e");
                        case "w":
                        case "W":
                            return e + (1 === e ? "re" : "e")
                    }
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var ve = "jan._feb._mrt._apr._mai_jun._jul._aug._sep._okt._nov._des.".split("_"),
                we = "jan_feb_mrt_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_");
            e.defineLocale("fy", {
                months: "jannewaris_febrewaris_maart_april_maaie_juny_july_augustus_septimber_oktober_novimber_desimber".split("_"),
                monthsShort: function(e, t) {
                    return e ? /-MMM-/.test(t) ? we[e.month()] : ve[e.month()] : ve
                },
                monthsParseExact: !0,
                weekdays: "snein_moandei_tiisdei_woansdei_tongersdei_freed_sneon".split("_"),
                weekdaysShort: "si._mo._ti._wo._to._fr._so.".split("_"),
                weekdaysMin: "Si_Mo_Ti_Wo_To_Fr_So".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD-MM-YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[hjoed om] LT",
                    nextDay: "[moarn om] LT",
                    nextWeek: "dddd [om] LT",
                    lastDay: "[juster om] LT",
                    lastWeek: "[ôfrûne] dddd [om] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "oer %s",
                    past: "%s lyn",
                    s: "in pear sekonden",
                    ss: "%d sekonden",
                    m: "ien minút",
                    mm: "%d minuten",
                    h: "ien oere",
                    hh: "%d oeren",
                    d: "ien dei",
                    dd: "%d dagen",
                    M: "ien moanne",
                    MM: "%d moannen",
                    y: "ien jier",
                    yy: "%d jierren"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(ste|de)/,
                ordinal: function(e) {
                    return e + (1 === e || 8 === e || e >= 20 ? "ste" : "de")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            e.defineLocale("gd", {
                months: ["Am Faoilleach", "An Gearran", "Am Màrt", "An Giblean", "An Cèitean", "An t-Ògmhios", "An t-Iuchar", "An Lùnastal", "An t-Sultain", "An Dàmhair", "An t-Samhain", "An Dùbhlachd"],
                monthsShort: ["Faoi", "Gear", "Màrt", "Gibl", "Cèit", "Ògmh", "Iuch", "Lùn", "Sult", "Dàmh", "Samh", "Dùbh"],
                monthsParseExact: !0,
                weekdays: ["Didòmhnaich", "Diluain", "Dimàirt", "Diciadain", "Diardaoin", "Dihaoine", "Disathairne"],
                weekdaysShort: ["Did", "Dil", "Dim", "Dic", "Dia", "Dih", "Dis"],
                weekdaysMin: ["Dò", "Lu", "Mà", "Ci", "Ar", "Ha", "Sa"],
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[An-diugh aig] LT",
                    nextDay: "[A-màireach aig] LT",
                    nextWeek: "dddd [aig] LT",
                    lastDay: "[An-dè aig] LT",
                    lastWeek: "dddd [seo chaidh] [aig] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "ann an %s",
                    past: "bho chionn %s",
                    s: "beagan diogan",
                    ss: "%d diogan",
                    m: "mionaid",
                    mm: "%d mionaidean",
                    h: "uair",
                    hh: "%d uairean",
                    d: "latha",
                    dd: "%d latha",
                    M: "mìos",
                    MM: "%d mìosan",
                    y: "bliadhna",
                    yy: "%d bliadhna"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(d|na|mh)/,
                ordinal: function(e) {
                    return e + (1 === e ? "d" : e % 10 == 2 ? "na" : "mh")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("gl", {
                months: "xaneiro_febreiro_marzo_abril_maio_xuño_xullo_agosto_setembro_outubro_novembro_decembro".split("_"),
                monthsShort: "xan._feb._mar._abr._mai._xuñ._xul._ago._set._out._nov._dec.".split("_"),
                monthsParseExact: !0,
                weekdays: "domingo_luns_martes_mércores_xoves_venres_sábado".split("_"),
                weekdaysShort: "dom._lun._mar._mér._xov._ven._sáb.".split("_"),
                weekdaysMin: "do_lu_ma_mé_xo_ve_sá".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D [de] MMMM [de] YYYY",
                    LLL: "D [de] MMMM [de] YYYY H:mm",
                    LLLL: "dddd, D [de] MMMM [de] YYYY H:mm"
                },
                calendar: {
                    sameDay: function() {
                        return "[hoxe " + (1 !== this.hours() ? "ás" : "á") + "] LT"
                    },
                    nextDay: function() {
                        return "[mañá " + (1 !== this.hours() ? "ás" : "á") + "] LT"
                    },
                    nextWeek: function() {
                        return "dddd [" + (1 !== this.hours() ? "ás" : "a") + "] LT"
                    },
                    lastDay: function() {
                        return "[onte " + (1 !== this.hours() ? "á" : "a") + "] LT"
                    },
                    lastWeek: function() {
                        return "[o] dddd [pasado " + (1 !== this.hours() ? "ás" : "a") + "] LT"
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: function(e) {
                        return 0 === e.indexOf("un") ? "n" + e : "en " + e
                    },
                    past: "hai %s",
                    s: "uns segundos",
                    ss: "%d segundos",
                    m: "un minuto",
                    mm: "%d minutos",
                    h: "unha hora",
                    hh: "%d horas",
                    d: "un día",
                    dd: "%d días",
                    M: "un mes",
                    MM: "%d meses",
                    y: "un ano",
                    yy: "%d anos"
                },
                dayOfMonthOrdinalParse: /\d{1,2}º/,
                ordinal: "%dº",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("gom-latn", {
                months: "Janer_Febrer_Mars_Abril_Mai_Jun_Julai_Agost_Setembr_Otubr_Novembr_Dezembr".split("_"),
                monthsShort: "Jan._Feb._Mars_Abr._Mai_Jun_Jul._Ago._Set._Otu._Nov._Dez.".split("_"),
                monthsParseExact: !0,
                weekdays: "Aitar_Somar_Mongllar_Budvar_Brestar_Sukrar_Son'var".split("_"),
                weekdaysShort: "Ait._Som._Mon._Bud._Bre._Suk._Son.".split("_"),
                weekdaysMin: "Ai_Sm_Mo_Bu_Br_Su_Sn".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "A h:mm [vazta]",
                    LTS: "A h:mm:ss [vazta]",
                    L: "DD-MM-YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY A h:mm [vazta]",
                    LLLL: "dddd, MMMM[achea] Do, YYYY, A h:mm [vazta]",
                    llll: "ddd, D MMM YYYY, A h:mm [vazta]"
                },
                calendar: {
                    sameDay: "[Aiz] LT",
                    nextDay: "[Faleam] LT",
                    nextWeek: "[Ieta to] dddd[,] LT",
                    lastDay: "[Kal] LT",
                    lastWeek: "[Fatlo] dddd[,] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s",
                    past: "%s adim",
                    s: m,
                    ss: m,
                    m: m,
                    mm: m,
                    h: m,
                    hh: m,
                    d: m,
                    dd: m,
                    M: m,
                    MM: m,
                    y: m,
                    yy: m
                },
                dayOfMonthOrdinalParse: /\d{1,2}(er)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "D":
                            return e + "er";
                        default:
                        case "M":
                        case "Q":
                        case "DDD":
                        case "d":
                        case "w":
                        case "W":
                            return e
                    }
                },
                week: {
                    dow: 1,
                    doy: 4
                },
                meridiemParse: /rati|sokalli|donparam|sanje/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "rati" === t ? e < 4 ? e : e + 12 : "sokalli" === t ? e : "donparam" === t ? e > 12 ? e : e + 12 : "sanje" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "rati" : e < 12 ? "sokalli" : e < 16 ? "donparam" : e < 20 ? "sanje" : "rati"
                }
            });
            var ke = {
                    1: "૧",
                    2: "૨",
                    3: "૩",
                    4: "૪",
                    5: "૫",
                    6: "૬",
                    7: "૭",
                    8: "૮",
                    9: "૯",
                    0: "૦"
                },
                De = {
                    "૧": "1",
                    "૨": "2",
                    "૩": "3",
                    "૪": "4",
                    "૫": "5",
                    "૬": "6",
                    "૭": "7",
                    "૮": "8",
                    "૯": "9",
                    "૦": "0"
                };
            e.defineLocale("gu", {
                months: "જાન્યુઆરી_ફેબ્રુઆરી_માર્ચ_એપ્રિલ_મે_જૂન_જુલાઈ_ઑગસ્ટ_સપ્ટેમ્બર_ઑક્ટ્બર_નવેમ્બર_ડિસેમ્બર".split("_"),
                monthsShort: "જાન્યુ._ફેબ્રુ._માર્ચ_એપ્રિ._મે_જૂન_જુલા._ઑગ._સપ્ટે._ઑક્ટ્._નવે._ડિસે.".split("_"),
                monthsParseExact: !0,
                weekdays: "રવિવાર_સોમવાર_મંગળવાર_બુધ્વાર_ગુરુવાર_શુક્રવાર_શનિવાર".split("_"),
                weekdaysShort: "રવિ_સોમ_મંગળ_બુધ્_ગુરુ_શુક્ર_શનિ".split("_"),
                weekdaysMin: "ર_સો_મં_બુ_ગુ_શુ_શ".split("_"),
                longDateFormat: {
                    LT: "A h:mm વાગ્યે",
                    LTS: "A h:mm:ss વાગ્યે",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, A h:mm વાગ્યે",
                    LLLL: "dddd, D MMMM YYYY, A h:mm વાગ્યે"
                },
                calendar: {
                    sameDay: "[આજ] LT",
                    nextDay: "[કાલે] LT",
                    nextWeek: "dddd, LT",
                    lastDay: "[ગઇકાલે] LT",
                    lastWeek: "[પાછલા] dddd, LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s મા",
                    past: "%s પેહલા",
                    s: "અમુક પળો",
                    ss: "%d સેકંડ",
                    m: "એક મિનિટ",
                    mm: "%d મિનિટ",
                    h: "એક કલાક",
                    hh: "%d કલાક",
                    d: "એક દિવસ",
                    dd: "%d દિવસ",
                    M: "એક મહિનો",
                    MM: "%d મહિનો",
                    y: "એક વર્ષ",
                    yy: "%d વર્ષ"
                },
                preparse: function(e) {
                    return e.replace(/[૧૨૩૪૫૬૭૮૯૦]/g, function(e) {
                        return De[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return ke[e]
                    })
                },
                meridiemParse: /રાત|બપોર|સવાર|સાંજ/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "રાત" === t ? e < 4 ? e : e + 12 : "સવાર" === t ? e : "બપોર" === t ? e >= 10 ? e : e + 12 : "સાંજ" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "રાત" : e < 10 ? "સવાર" : e < 17 ? "બપોર" : e < 20 ? "સાંજ" : "રાત"
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            }), e.defineLocale("he", {
                months: "ינואר_פברואר_מרץ_אפריל_מאי_יוני_יולי_אוגוסט_ספטמבר_אוקטובר_נובמבר_דצמבר".split("_"),
                monthsShort: "ינו׳_פבר׳_מרץ_אפר׳_מאי_יוני_יולי_אוג׳_ספט׳_אוק׳_נוב׳_דצמ׳".split("_"),
                weekdays: "ראשון_שני_שלישי_רביעי_חמישי_שישי_שבת".split("_"),
                weekdaysShort: "א׳_ב׳_ג׳_ד׳_ה׳_ו׳_ש׳".split("_"),
                weekdaysMin: "א_ב_ג_ד_ה_ו_ש".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D [ב]MMMM YYYY",
                    LLL: "D [ב]MMMM YYYY HH:mm",
                    LLLL: "dddd, D [ב]MMMM YYYY HH:mm",
                    l: "D/M/YYYY",
                    ll: "D MMM YYYY",
                    lll: "D MMM YYYY HH:mm",
                    llll: "ddd, D MMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[היום ב־]LT",
                    nextDay: "[מחר ב־]LT",
                    nextWeek: "dddd [בשעה] LT",
                    lastDay: "[אתמול ב־]LT",
                    lastWeek: "[ביום] dddd [האחרון בשעה] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "בעוד %s",
                    past: "לפני %s",
                    s: "מספר שניות",
                    ss: "%d שניות",
                    m: "דקה",
                    mm: "%d דקות",
                    h: "שעה",
                    hh: function(e) {
                        return 2 === e ? "שעתיים" : e + " שעות"
                    },
                    d: "יום",
                    dd: function(e) {
                        return 2 === e ? "יומיים" : e + " ימים"
                    },
                    M: "חודש",
                    MM: function(e) {
                        return 2 === e ? "חודשיים" : e + " חודשים"
                    },
                    y: "שנה",
                    yy: function(e) {
                        return 2 === e ? "שנתיים" : e % 10 == 0 && 10 !== e ? e + " שנה" : e + " שנים"
                    }
                },
                meridiemParse: /אחה"צ|לפנה"צ|אחרי הצהריים|לפני הצהריים|לפנות בוקר|בבוקר|בערב/i,
                isPM: function(e) {
                    return /^(אחה"צ|אחרי הצהריים|בערב)$/.test(e)
                },
                meridiem: function(e, t, a) {
                    return e < 5 ? "לפנות בוקר" : e < 10 ? "בבוקר" : e < 12 ? a ? 'לפנה"צ' : "לפני הצהריים" : e < 18 ? a ? 'אחה"צ' : "אחרי הצהריים" : "בערב"
                }
            });
            var be = {
                    1: "१",
                    2: "२",
                    3: "३",
                    4: "४",
                    5: "५",
                    6: "६",
                    7: "७",
                    8: "८",
                    9: "९",
                    0: "०"
                },
                Te = {
                    "१": "1",
                    "२": "2",
                    "३": "3",
                    "४": "4",
                    "५": "5",
                    "६": "6",
                    "७": "7",
                    "८": "8",
                    "९": "9",
                    "०": "0"
                };
            e.defineLocale("hi", {
                months: "जनवरी_फ़रवरी_मार्च_अप्रैल_मई_जून_जुलाई_अगस्त_सितम्बर_अक्टूबर_नवम्बर_दिसम्बर".split("_"),
                monthsShort: "जन._फ़र._मार्च_अप्रै._मई_जून_जुल._अग._सित._अक्टू._नव._दिस.".split("_"),
                monthsParseExact: !0,
                weekdays: "रविवार_सोमवार_मंगलवार_बुधवार_गुरूवार_शुक्रवार_शनिवार".split("_"),
                weekdaysShort: "रवि_सोम_मंगल_बुध_गुरू_शुक्र_शनि".split("_"),
                weekdaysMin: "र_सो_मं_बु_गु_शु_श".split("_"),
                longDateFormat: {
                    LT: "A h:mm बजे",
                    LTS: "A h:mm:ss बजे",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, A h:mm बजे",
                    LLLL: "dddd, D MMMM YYYY, A h:mm बजे"
                },
                calendar: {
                    sameDay: "[आज] LT",
                    nextDay: "[कल] LT",
                    nextWeek: "dddd, LT",
                    lastDay: "[कल] LT",
                    lastWeek: "[पिछले] dddd, LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s में",
                    past: "%s पहले",
                    s: "कुछ ही क्षण",
                    ss: "%d सेकंड",
                    m: "एक मिनट",
                    mm: "%d मिनट",
                    h: "एक घंटा",
                    hh: "%d घंटे",
                    d: "एक दिन",
                    dd: "%d दिन",
                    M: "एक महीने",
                    MM: "%d महीने",
                    y: "एक वर्ष",
                    yy: "%d वर्ष"
                },
                preparse: function(e) {
                    return e.replace(/[१२३४५६७८९०]/g, function(e) {
                        return Te[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return be[e]
                    })
                },
                meridiemParse: /रात|सुबह|दोपहर|शाम/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "रात" === t ? e < 4 ? e : e + 12 : "सुबह" === t ? e : "दोपहर" === t ? e >= 10 ? e : e + 12 : "शाम" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "रात" : e < 10 ? "सुबह" : e < 17 ? "दोपहर" : e < 20 ? "शाम" : "रात"
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            }), e.defineLocale("hr", {
                months: {
                    format: "siječnja_veljače_ožujka_travnja_svibnja_lipnja_srpnja_kolovoza_rujna_listopada_studenoga_prosinca".split("_"),
                    standalone: "siječanj_veljača_ožujak_travanj_svibanj_lipanj_srpanj_kolovoz_rujan_listopad_studeni_prosinac".split("_")
                },
                monthsShort: "sij._velj._ožu._tra._svi._lip._srp._kol._ruj._lis._stu._pro.".split("_"),
                monthsParseExact: !0,
                weekdays: "nedjelja_ponedjeljak_utorak_srijeda_četvrtak_petak_subota".split("_"),
                weekdaysShort: "ned._pon._uto._sri._čet._pet._sub.".split("_"),
                weekdaysMin: "ne_po_ut_sr_če_pe_su".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY H:mm",
                    LLLL: "dddd, D. MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[danas u] LT",
                    nextDay: "[sutra u] LT",
                    nextWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[u] [nedjelju] [u] LT";
                            case 3:
                                return "[u] [srijedu] [u] LT";
                            case 6:
                                return "[u] [subotu] [u] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[u] dddd [u] LT"
                        }
                    },
                    lastDay: "[jučer u] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                            case 3:
                                return "[prošlu] dddd [u] LT";
                            case 6:
                                return "[prošle] [subote] [u] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[prošli] dddd [u] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "za %s",
                    past: "prije %s",
                    s: "par sekundi",
                    ss: c,
                    m: c,
                    mm: c,
                    h: c,
                    hh: c,
                    d: "dan",
                    dd: c,
                    M: "mjesec",
                    MM: c,
                    y: "godinu",
                    yy: c
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 7
                }
            });
            var Se = "vasárnap hétfőn kedden szerdán csütörtökön pénteken szombaton".split(" ");
            e.defineLocale("hu", {
                months: "január_február_március_április_május_június_július_augusztus_szeptember_október_november_december".split("_"),
                monthsShort: "jan_feb_márc_ápr_máj_jún_júl_aug_szept_okt_nov_dec".split("_"),
                weekdays: "vasárnap_hétfő_kedd_szerda_csütörtök_péntek_szombat".split("_"),
                weekdaysShort: "vas_hét_kedd_sze_csüt_pén_szo".split("_"),
                weekdaysMin: "v_h_k_sze_cs_p_szo".split("_"),
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "YYYY.MM.DD.",
                    LL: "YYYY. MMMM D.",
                    LLL: "YYYY. MMMM D. H:mm",
                    LLLL: "YYYY. MMMM D., dddd H:mm"
                },
                meridiemParse: /de|du/i,
                isPM: function(e) {
                    return "u" === e.charAt(1).toLowerCase()
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? !0 === a ? "de" : "DE" : !0 === a ? "du" : "DU"
                },
                calendar: {
                    sameDay: "[ma] LT[-kor]",
                    nextDay: "[holnap] LT[-kor]",
                    nextWeek: function() {
                        return p.call(this, !0)
                    },
                    lastDay: "[tegnap] LT[-kor]",
                    lastWeek: function() {
                        return p.call(this, !1)
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s múlva",
                    past: "%s",
                    s: h,
                    ss: h,
                    m: h,
                    mm: h,
                    h: h,
                    hh: h,
                    d: h,
                    dd: h,
                    M: h,
                    MM: h,
                    y: h,
                    yy: h
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("hy-am", {
                months: {
                    format: "հունվարի_փետրվարի_մարտի_ապրիլի_մայիսի_հունիսի_հուլիսի_օգոստոսի_սեպտեմբերի_հոկտեմբերի_նոյեմբերի_դեկտեմբերի".split("_"),
                    standalone: "հունվար_փետրվար_մարտ_ապրիլ_մայիս_հունիս_հուլիս_օգոստոս_սեպտեմբեր_հոկտեմբեր_նոյեմբեր_դեկտեմբեր".split("_")
                },
                monthsShort: "հնվ_փտր_մրտ_ապր_մյս_հնս_հլս_օգս_սպտ_հկտ_նմբ_դկտ".split("_"),
                weekdays: "կիրակի_երկուշաբթի_երեքշաբթի_չորեքշաբթի_հինգշաբթի_ուրբաթ_շաբաթ".split("_"),
                weekdaysShort: "կրկ_երկ_երք_չրք_հնգ_ուրբ_շբթ".split("_"),
                weekdaysMin: "կրկ_երկ_երք_չրք_հնգ_ուրբ_շբթ".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY թ.",
                    LLL: "D MMMM YYYY թ., HH:mm",
                    LLLL: "dddd, D MMMM YYYY թ., HH:mm"
                },
                calendar: {
                    sameDay: "[այսօր] LT",
                    nextDay: "[վաղը] LT",
                    lastDay: "[երեկ] LT",
                    nextWeek: function() {
                        return "dddd [օրը ժամը] LT"
                    },
                    lastWeek: function() {
                        return "[անցած] dddd [օրը ժամը] LT"
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s հետո",
                    past: "%s առաջ",
                    s: "մի քանի վայրկյան",
                    ss: "%d վայրկյան",
                    m: "րոպե",
                    mm: "%d րոպե",
                    h: "ժամ",
                    hh: "%d ժամ",
                    d: "օր",
                    dd: "%d օր",
                    M: "ամիս",
                    MM: "%d ամիս",
                    y: "տարի",
                    yy: "%d տարի"
                },
                meridiemParse: /գիշերվա|առավոտվա|ցերեկվա|երեկոյան/,
                isPM: function(e) {
                    return /^(ցերեկվա|երեկոյան)$/.test(e)
                },
                meridiem: function(e) {
                    return e < 4 ? "գիշերվա" : e < 12 ? "առավոտվա" : e < 17 ? "ցերեկվա" : "երեկոյան"
                },
                dayOfMonthOrdinalParse: /\d{1,2}|\d{1,2}-(ին|րդ)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "DDD":
                        case "w":
                        case "W":
                        case "DDDo":
                            return 1 === e ? e + "-ին" : e + "-րդ";
                        default:
                            return e
                    }
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("id", {
                months: "Januari_Februari_Maret_April_Mei_Juni_Juli_Agustus_September_Oktober_November_Desember".split("_"),
                monthsShort: "Jan_Feb_Mar_Apr_Mei_Jun_Jul_Agt_Sep_Okt_Nov_Des".split("_"),
                weekdays: "Minggu_Senin_Selasa_Rabu_Kamis_Jumat_Sabtu".split("_"),
                weekdaysShort: "Min_Sen_Sel_Rab_Kam_Jum_Sab".split("_"),
                weekdaysMin: "Mg_Sn_Sl_Rb_Km_Jm_Sb".split("_"),
                longDateFormat: {
                    LT: "HH.mm",
                    LTS: "HH.mm.ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY [pukul] HH.mm",
                    LLLL: "dddd, D MMMM YYYY [pukul] HH.mm"
                },
                meridiemParse: /pagi|siang|sore|malam/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "pagi" === t ? e : "siang" === t ? e >= 11 ? e : e + 12 : "sore" === t || "malam" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 11 ? "pagi" : e < 15 ? "siang" : e < 19 ? "sore" : "malam"
                },
                calendar: {
                    sameDay: "[Hari ini pukul] LT",
                    nextDay: "[Besok pukul] LT",
                    nextWeek: "dddd [pukul] LT",
                    lastDay: "[Kemarin pukul] LT",
                    lastWeek: "dddd [lalu pukul] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "dalam %s",
                    past: "%s yang lalu",
                    s: "beberapa detik",
                    ss: "%d detik",
                    m: "semenit",
                    mm: "%d menit",
                    h: "sejam",
                    hh: "%d jam",
                    d: "sehari",
                    dd: "%d hari",
                    M: "sebulan",
                    MM: "%d bulan",
                    y: "setahun",
                    yy: "%d tahun"
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("is", {
                months: "janúar_febrúar_mars_apríl_maí_júní_júlí_ágúst_september_október_nóvember_desember".split("_"),
                monthsShort: "jan_feb_mar_apr_maí_jún_júl_ágú_sep_okt_nóv_des".split("_"),
                weekdays: "sunnudagur_mánudagur_þriðjudagur_miðvikudagur_fimmtudagur_föstudagur_laugardagur".split("_"),
                weekdaysShort: "sun_mán_þri_mið_fim_fös_lau".split("_"),
                weekdaysMin: "Su_Má_Þr_Mi_Fi_Fö_La".split("_"),
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY [kl.] H:mm",
                    LLLL: "dddd, D. MMMM YYYY [kl.] H:mm"
                },
                calendar: {
                    sameDay: "[í dag kl.] LT",
                    nextDay: "[á morgun kl.] LT",
                    nextWeek: "dddd [kl.] LT",
                    lastDay: "[í gær kl.] LT",
                    lastWeek: "[síðasta] dddd [kl.] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "eftir %s",
                    past: "fyrir %s síðan",
                    s: M,
                    ss: M,
                    m: M,
                    mm: M,
                    h: "klukkustund",
                    hh: M,
                    d: M,
                    dd: M,
                    M: M,
                    MM: M,
                    y: M,
                    yy: M
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("it", {
                months: "gennaio_febbraio_marzo_aprile_maggio_giugno_luglio_agosto_settembre_ottobre_novembre_dicembre".split("_"),
                monthsShort: "gen_feb_mar_apr_mag_giu_lug_ago_set_ott_nov_dic".split("_"),
                weekdays: "domenica_lunedì_martedì_mercoledì_giovedì_venerdì_sabato".split("_"),
                weekdaysShort: "dom_lun_mar_mer_gio_ven_sab".split("_"),
                weekdaysMin: "do_lu_ma_me_gi_ve_sa".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Oggi alle] LT",
                    nextDay: "[Domani alle] LT",
                    nextWeek: "dddd [alle] LT",
                    lastDay: "[Ieri alle] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[la scorsa] dddd [alle] LT";
                            default:
                                return "[lo scorso] dddd [alle] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: function(e) {
                        return (/^[0-9].+$/.test(e) ? "tra" : "in") + " " + e
                    },
                    past: "%s fa",
                    s: "alcuni secondi",
                    ss: "%d secondi",
                    m: "un minuto",
                    mm: "%d minuti",
                    h: "un'ora",
                    hh: "%d ore",
                    d: "un giorno",
                    dd: "%d giorni",
                    M: "un mese",
                    MM: "%d mesi",
                    y: "un anno",
                    yy: "%d anni"
                },
                dayOfMonthOrdinalParse: /\d{1,2}º/,
                ordinal: "%dº",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("ja", {
                months: "1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),
                monthsShort: "1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),
                weekdays: "日曜日_月曜日_火曜日_水曜日_木曜日_金曜日_土曜日".split("_"),
                weekdaysShort: "日_月_火_水_木_金_土".split("_"),
                weekdaysMin: "日_月_火_水_木_金_土".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY/MM/DD",
                    LL: "YYYY年M月D日",
                    LLL: "YYYY年M月D日 HH:mm",
                    LLLL: "YYYY年M月D日 dddd HH:mm",
                    l: "YYYY/MM/DD",
                    ll: "YYYY年M月D日",
                    lll: "YYYY年M月D日 HH:mm",
                    llll: "YYYY年M月D日(ddd) HH:mm"
                },
                meridiemParse: /午前|午後/i,
                isPM: function(e) {
                    return "午後" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "午前" : "午後"
                },
                calendar: {
                    sameDay: "[今日] LT",
                    nextDay: "[明日] LT",
                    nextWeek: function(e) {
                        return e.week() < this.week() ? "[来週]dddd LT" : "dddd LT"
                    },
                    lastDay: "[昨日] LT",
                    lastWeek: function(e) {
                        return this.week() < e.week() ? "[先週]dddd LT" : "dddd LT"
                    },
                    sameElse: "L"
                },
                dayOfMonthOrdinalParse: /\d{1,2}日/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "d":
                        case "D":
                        case "DDD":
                            return e + "日";
                        default:
                            return e
                    }
                },
                relativeTime: {
                    future: "%s後",
                    past: "%s前",
                    s: "数秒",
                    ss: "%d秒",
                    m: "1分",
                    mm: "%d分",
                    h: "1時間",
                    hh: "%d時間",
                    d: "1日",
                    dd: "%d日",
                    M: "1ヶ月",
                    MM: "%dヶ月",
                    y: "1年",
                    yy: "%d年"
                }
            }), e.defineLocale("jv", {
                months: "Januari_Februari_Maret_April_Mei_Juni_Juli_Agustus_September_Oktober_Nopember_Desember".split("_"),
                monthsShort: "Jan_Feb_Mar_Apr_Mei_Jun_Jul_Ags_Sep_Okt_Nop_Des".split("_"),
                weekdays: "Minggu_Senen_Seloso_Rebu_Kemis_Jemuwah_Septu".split("_"),
                weekdaysShort: "Min_Sen_Sel_Reb_Kem_Jem_Sep".split("_"),
                weekdaysMin: "Mg_Sn_Sl_Rb_Km_Jm_Sp".split("_"),
                longDateFormat: {
                    LT: "HH.mm",
                    LTS: "HH.mm.ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY [pukul] HH.mm",
                    LLLL: "dddd, D MMMM YYYY [pukul] HH.mm"
                },
                meridiemParse: /enjing|siyang|sonten|ndalu/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "enjing" === t ? e : "siyang" === t ? e >= 11 ? e : e + 12 : "sonten" === t || "ndalu" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 11 ? "enjing" : e < 15 ? "siyang" : e < 19 ? "sonten" : "ndalu"
                },
                calendar: {
                    sameDay: "[Dinten puniko pukul] LT",
                    nextDay: "[Mbenjang pukul] LT",
                    nextWeek: "dddd [pukul] LT",
                    lastDay: "[Kala wingi pukul] LT",
                    lastWeek: "dddd [kepengker pukul] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "wonten ing %s",
                    past: "%s ingkang kepengker",
                    s: "sawetawis detik",
                    ss: "%d detik",
                    m: "setunggal menit",
                    mm: "%d menit",
                    h: "setunggal jam",
                    hh: "%d jam",
                    d: "sedinten",
                    dd: "%d dinten",
                    M: "sewulan",
                    MM: "%d wulan",
                    y: "setaun",
                    yy: "%d taun"
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("ka", {
                months: {
                    standalone: "იანვარი_თებერვალი_მარტი_აპრილი_მაისი_ივნისი_ივლისი_აგვისტო_სექტემბერი_ოქტომბერი_ნოემბერი_დეკემბერი".split("_"),
                    format: "იანვარს_თებერვალს_მარტს_აპრილის_მაისს_ივნისს_ივლისს_აგვისტს_სექტემბერს_ოქტომბერს_ნოემბერს_დეკემბერს".split("_")
                },
                monthsShort: "იან_თებ_მარ_აპრ_მაი_ივნ_ივლ_აგვ_სექ_ოქტ_ნოე_დეკ".split("_"),
                weekdays: {
                    standalone: "კვირა_ორშაბათი_სამშაბათი_ოთხშაბათი_ხუთშაბათი_პარასკევი_შაბათი".split("_"),
                    format: "კვირას_ორშაბათს_სამშაბათს_ოთხშაბათს_ხუთშაბათს_პარასკევს_შაბათს".split("_"),
                    isFormat: /(წინა|შემდეგ)/
                },
                weekdaysShort: "კვი_ორშ_სამ_ოთხ_ხუთ_პარ_შაბ".split("_"),
                weekdaysMin: "კვ_ორ_სა_ოთ_ხუ_პა_შა".split("_"),
                longDateFormat: {
                    LT: "h:mm A",
                    LTS: "h:mm:ss A",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY h:mm A",
                    LLLL: "dddd, D MMMM YYYY h:mm A"
                },
                calendar: {
                    sameDay: "[დღეს] LT[-ზე]",
                    nextDay: "[ხვალ] LT[-ზე]",
                    lastDay: "[გუშინ] LT[-ზე]",
                    nextWeek: "[შემდეგ] dddd LT[-ზე]",
                    lastWeek: "[წინა] dddd LT-ზე",
                    sameElse: "L"
                },
                relativeTime: {
                    future: function(e) {
                        return /(წამი|წუთი|საათი|წელი)/.test(e) ? e.replace(/ი$/, "ში") : e + "ში"
                    },
                    past: function(e) {
                        return /(წამი|წუთი|საათი|დღე|თვე)/.test(e) ? e.replace(/(ი|ე)$/, "ის წინ") : /წელი/.test(e) ? e.replace(/წელი$/, "წლის წინ") : void 0
                    },
                    s: "რამდენიმე წამი",
                    ss: "%d წამი",
                    m: "წუთი",
                    mm: "%d წუთი",
                    h: "საათი",
                    hh: "%d საათი",
                    d: "დღე",
                    dd: "%d დღე",
                    M: "თვე",
                    MM: "%d თვე",
                    y: "წელი",
                    yy: "%d წელი"
                },
                dayOfMonthOrdinalParse: /0|1-ლი|მე-\d{1,2}|\d{1,2}-ე/,
                ordinal: function(e) {
                    return 0 === e ? e : 1 === e ? e + "-ლი" : e < 20 || e <= 100 && e % 20 == 0 || e % 100 == 0 ? "მე-" + e : e + "-ე"
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            });
            var xe = {
                0: "-ші",
                1: "-ші",
                2: "-ші",
                3: "-ші",
                4: "-ші",
                5: "-ші",
                6: "-шы",
                7: "-ші",
                8: "-ші",
                9: "-шы",
                10: "-шы",
                20: "-шы",
                30: "-шы",
                40: "-шы",
                50: "-ші",
                60: "-шы",
                70: "-ші",
                80: "-ші",
                90: "-шы",
                100: "-ші"
            };
            e.defineLocale("kk", {
                months: "қаңтар_ақпан_наурыз_сәуір_мамыр_маусым_шілде_тамыз_қыркүйек_қазан_қараша_желтоқсан".split("_"),
                monthsShort: "қаң_ақп_нау_сәу_мам_мау_шіл_там_қыр_қаз_қар_жел".split("_"),
                weekdays: "жексенбі_дүйсенбі_сейсенбі_сәрсенбі_бейсенбі_жұма_сенбі".split("_"),
                weekdaysShort: "жек_дүй_сей_сәр_бей_жұм_сен".split("_"),
                weekdaysMin: "жк_дй_сй_ср_бй_жм_сн".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Бүгін сағат] LT",
                    nextDay: "[Ертең сағат] LT",
                    nextWeek: "dddd [сағат] LT",
                    lastDay: "[Кеше сағат] LT",
                    lastWeek: "[Өткен аптаның] dddd [сағат] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s ішінде",
                    past: "%s бұрын",
                    s: "бірнеше секунд",
                    ss: "%d секунд",
                    m: "бір минут",
                    mm: "%d минут",
                    h: "бір сағат",
                    hh: "%d сағат",
                    d: "бір күн",
                    dd: "%d күн",
                    M: "бір ай",
                    MM: "%d ай",
                    y: "бір жыл",
                    yy: "%d жыл"
                },
                dayOfMonthOrdinalParse: /\d{1,2}-(ші|шы)/,
                ordinal: function(e) {
                    return e + (xe[e] || xe[e % 10] || xe[e >= 100 ? 100 : null])
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            });
            var He = {
                    1: "១",
                    2: "២",
                    3: "៣",
                    4: "៤",
                    5: "៥",
                    6: "៦",
                    7: "៧",
                    8: "៨",
                    9: "៩",
                    0: "០"
                },
                je = {
                    "១": "1",
                    "២": "2",
                    "៣": "3",
                    "៤": "4",
                    "៥": "5",
                    "៦": "6",
                    "៧": "7",
                    "៨": "8",
                    "៩": "9",
                    "០": "0"
                };
            e.defineLocale("km", {
                months: "មករា_កុម្ភៈ_មីនា_មេសា_ឧសភា_មិថុនា_កក្កដា_សីហា_កញ្ញា_តុលា_វិច្ឆិកា_ធ្នូ".split("_"),
                monthsShort: "មករា_កុម្ភៈ_មីនា_មេសា_ឧសភា_មិថុនា_កក្កដា_សីហា_កញ្ញា_តុលា_វិច្ឆិកា_ធ្នូ".split("_"),
                weekdays: "អាទិត្យ_ច័ន្ទ_អង្គារ_ពុធ_ព្រហស្បតិ៍_សុក្រ_សៅរ៍".split("_"),
                weekdaysShort: "អា_ច_អ_ព_ព្រ_សុ_ស".split("_"),
                weekdaysMin: "អា_ច_អ_ព_ព្រ_សុ_ស".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                meridiemParse: /ព្រឹក|ល្ងាច/,
                isPM: function(e) {
                    return "ល្ងាច" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "ព្រឹក" : "ល្ងាច"
                },
                calendar: {
                    sameDay: "[ថ្ងៃនេះ ម៉ោង] LT",
                    nextDay: "[ស្អែក ម៉ោង] LT",
                    nextWeek: "dddd [ម៉ោង] LT",
                    lastDay: "[ម្សិលមិញ ម៉ោង] LT",
                    lastWeek: "dddd [សប្តាហ៍មុន] [ម៉ោង] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%sទៀត",
                    past: "%sមុន",
                    s: "ប៉ុន្មានវិនាទី",
                    ss: "%d វិនាទី",
                    m: "មួយនាទី",
                    mm: "%d នាទី",
                    h: "មួយម៉ោង",
                    hh: "%d ម៉ោង",
                    d: "មួយថ្ងៃ",
                    dd: "%d ថ្ងៃ",
                    M: "មួយខែ",
                    MM: "%d ខែ",
                    y: "មួយឆ្នាំ",
                    yy: "%d ឆ្នាំ"
                },
                dayOfMonthOrdinalParse: /ទី\d{1,2}/,
                ordinal: "ទី%d",
                preparse: function(e) {
                    return e.replace(/[១២៣៤៥៦៧៨៩០]/g, function(e) {
                        return je[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return He[e]
                    })
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var Ce = {
                    1: "೧",
                    2: "೨",
                    3: "೩",
                    4: "೪",
                    5: "೫",
                    6: "೬",
                    7: "೭",
                    8: "೮",
                    9: "೯",
                    0: "೦"
                },
                Pe = {
                    "೧": "1",
                    "೨": "2",
                    "೩": "3",
                    "೪": "4",
                    "೫": "5",
                    "೬": "6",
                    "೭": "7",
                    "೮": "8",
                    "೯": "9",
                    "೦": "0"
                };
            e.defineLocale("kn", {
                months: "ಜನವರಿ_ಫೆಬ್ರವರಿ_ಮಾರ್ಚ್_ಏಪ್ರಿಲ್_ಮೇ_ಜೂನ್_ಜುಲೈ_ಆಗಸ್ಟ್_ಸೆಪ್ಟೆಂಬರ್_ಅಕ್ಟೋಬರ್_ನವೆಂಬರ್_ಡಿಸೆಂಬರ್".split("_"),
                monthsShort: "ಜನ_ಫೆಬ್ರ_ಮಾರ್ಚ್_ಏಪ್ರಿಲ್_ಮೇ_ಜೂನ್_ಜುಲೈ_ಆಗಸ್ಟ್_ಸೆಪ್ಟೆಂ_ಅಕ್ಟೋ_ನವೆಂ_ಡಿಸೆಂ".split("_"),
                monthsParseExact: !0,
                weekdays: "ಭಾನುವಾರ_ಸೋಮವಾರ_ಮಂಗಳವಾರ_ಬುಧವಾರ_ಗುರುವಾರ_ಶುಕ್ರವಾರ_ಶನಿವಾರ".split("_"),
                weekdaysShort: "ಭಾನು_ಸೋಮ_ಮಂಗಳ_ಬುಧ_ಗುರು_ಶುಕ್ರ_ಶನಿ".split("_"),
                weekdaysMin: "ಭಾ_ಸೋ_ಮಂ_ಬು_ಗು_ಶು_ಶ".split("_"),
                longDateFormat: {
                    LT: "A h:mm",
                    LTS: "A h:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, A h:mm",
                    LLLL: "dddd, D MMMM YYYY, A h:mm"
                },
                calendar: {
                    sameDay: "[ಇಂದು] LT",
                    nextDay: "[ನಾಳೆ] LT",
                    nextWeek: "dddd, LT",
                    lastDay: "[ನಿನ್ನೆ] LT",
                    lastWeek: "[ಕೊನೆಯ] dddd, LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s ನಂತರ",
                    past: "%s ಹಿಂದೆ",
                    s: "ಕೆಲವು ಕ್ಷಣಗಳು",
                    ss: "%d ಸೆಕೆಂಡುಗಳು",
                    m: "ಒಂದು ನಿಮಿಷ",
                    mm: "%d ನಿಮಿಷ",
                    h: "ಒಂದು ಗಂಟೆ",
                    hh: "%d ಗಂಟೆ",
                    d: "ಒಂದು ದಿನ",
                    dd: "%d ದಿನ",
                    M: "ಒಂದು ತಿಂಗಳು",
                    MM: "%d ತಿಂಗಳು",
                    y: "ಒಂದು ವರ್ಷ",
                    yy: "%d ವರ್ಷ"
                },
                preparse: function(e) {
                    return e.replace(/[೧೨೩೪೫೬೭೮೯೦]/g, function(e) {
                        return Pe[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return Ce[e]
                    })
                },
                meridiemParse: /ರಾತ್ರಿ|ಬೆಳಿಗ್ಗೆ|ಮಧ್ಯಾಹ್ನ|ಸಂಜೆ/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "ರಾತ್ರಿ" === t ? e < 4 ? e : e + 12 : "ಬೆಳಿಗ್ಗೆ" === t ? e : "ಮಧ್ಯಾಹ್ನ" === t ? e >= 10 ? e : e + 12 : "ಸಂಜೆ" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "ರಾತ್ರಿ" : e < 10 ? "ಬೆಳಿಗ್ಗೆ" : e < 17 ? "ಮಧ್ಯಾಹ್ನ" : e < 20 ? "ಸಂಜೆ" : "ರಾತ್ರಿ"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(ನೇ)/,
                ordinal: function(e) {
                    return e + "ನೇ"
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            }), e.defineLocale("ko", {
                months: "1월_2월_3월_4월_5월_6월_7월_8월_9월_10월_11월_12월".split("_"),
                monthsShort: "1월_2월_3월_4월_5월_6월_7월_8월_9월_10월_11월_12월".split("_"),
                weekdays: "일요일_월요일_화요일_수요일_목요일_금요일_토요일".split("_"),
                weekdaysShort: "일_월_화_수_목_금_토".split("_"),
                weekdaysMin: "일_월_화_수_목_금_토".split("_"),
                longDateFormat: {
                    LT: "A h:mm",
                    LTS: "A h:mm:ss",
                    L: "YYYY.MM.DD.",
                    LL: "YYYY년 MMMM D일",
                    LLL: "YYYY년 MMMM D일 A h:mm",
                    LLLL: "YYYY년 MMMM D일 dddd A h:mm",
                    l: "YYYY.MM.DD.",
                    ll: "YYYY년 MMMM D일",
                    lll: "YYYY년 MMMM D일 A h:mm",
                    llll: "YYYY년 MMMM D일 dddd A h:mm"
                },
                calendar: {
                    sameDay: "오늘 LT",
                    nextDay: "내일 LT",
                    nextWeek: "dddd LT",
                    lastDay: "어제 LT",
                    lastWeek: "지난주 dddd LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s 후",
                    past: "%s 전",
                    s: "몇 초",
                    ss: "%d초",
                    m: "1분",
                    mm: "%d분",
                    h: "한 시간",
                    hh: "%d시간",
                    d: "하루",
                    dd: "%d일",
                    M: "한 달",
                    MM: "%d달",
                    y: "일 년",
                    yy: "%d년"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(일|월|주)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "d":
                        case "D":
                        case "DDD":
                            return e + "일";
                        case "M":
                            return e + "월";
                        case "w":
                        case "W":
                            return e + "주";
                        default:
                            return e
                    }
                },
                meridiemParse: /오전|오후/,
                isPM: function(e) {
                    return "오후" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "오전" : "오후"
                }
            });
            var Oe = {
                0: "-чү",
                1: "-чи",
                2: "-чи",
                3: "-чү",
                4: "-чү",
                5: "-чи",
                6: "-чы",
                7: "-чи",
                8: "-чи",
                9: "-чу",
                10: "-чу",
                20: "-чы",
                30: "-чу",
                40: "-чы",
                50: "-чү",
                60: "-чы",
                70: "-чи",
                80: "-чи",
                90: "-чу",
                100: "-чү"
            };
            e.defineLocale("ky", {
                months: "январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь".split("_"),
                monthsShort: "янв_фев_март_апр_май_июнь_июль_авг_сен_окт_ноя_дек".split("_"),
                weekdays: "Жекшемби_Дүйшөмбү_Шейшемби_Шаршемби_Бейшемби_Жума_Ишемби".split("_"),
                weekdaysShort: "Жек_Дүй_Шей_Шар_Бей_Жум_Ише".split("_"),
                weekdaysMin: "Жк_Дй_Шй_Шр_Бй_Жм_Иш".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Бүгүн саат] LT",
                    nextDay: "[Эртең саат] LT",
                    nextWeek: "dddd [саат] LT",
                    lastDay: "[Кече саат] LT",
                    lastWeek: "[Өткен аптанын] dddd [күнү] [саат] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s ичинде",
                    past: "%s мурун",
                    s: "бирнече секунд",
                    ss: "%d секунд",
                    m: "бир мүнөт",
                    mm: "%d мүнөт",
                    h: "бир саат",
                    hh: "%d саат",
                    d: "бир күн",
                    dd: "%d күн",
                    M: "бир ай",
                    MM: "%d ай",
                    y: "бир жыл",
                    yy: "%d жыл"
                },
                dayOfMonthOrdinalParse: /\d{1,2}-(чи|чы|чү|чу)/,
                ordinal: function(e) {
                    return e + (Oe[e] || Oe[e % 10] || Oe[e >= 100 ? 100 : null])
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("lb", {
                months: "Januar_Februar_Mäerz_Abrëll_Mee_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),
                monthsShort: "Jan._Febr._Mrz._Abr._Mee_Jun._Jul._Aug._Sept._Okt._Nov._Dez.".split("_"),
                monthsParseExact: !0,
                weekdays: "Sonndeg_Méindeg_Dënschdeg_Mëttwoch_Donneschdeg_Freideg_Samschdeg".split("_"),
                weekdaysShort: "So._Mé._Dë._Më._Do._Fr._Sa.".split("_"),
                weekdaysMin: "So_Mé_Dë_Më_Do_Fr_Sa".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm [Auer]",
                    LTS: "H:mm:ss [Auer]",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY H:mm [Auer]",
                    LLLL: "dddd, D. MMMM YYYY H:mm [Auer]"
                },
                calendar: {
                    sameDay: "[Haut um] LT",
                    sameElse: "L",
                    nextDay: "[Muer um] LT",
                    nextWeek: "dddd [um] LT",
                    lastDay: "[Gëschter um] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 2:
                            case 4:
                                return "[Leschten] dddd [um] LT";
                            default:
                                return "[Leschte] dddd [um] LT"
                        }
                    }
                },
                relativeTime: {
                    future: function(e) {
                        return g(e.substr(0, e.indexOf(" "))) ? "a " + e : "an " + e
                    },
                    past: function(e) {
                        return g(e.substr(0, e.indexOf(" "))) ? "viru " + e : "virun " + e
                    },
                    s: "e puer Sekonnen",
                    ss: "%d Sekonnen",
                    m: y,
                    mm: "%d Minutten",
                    h: y,
                    hh: "%d Stonnen",
                    d: y,
                    dd: "%d Deeg",
                    M: y,
                    MM: "%d Méint",
                    y: y,
                    yy: "%d Joer"
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("lo", {
                months: "ມັງກອນ_ກຸມພາ_ມີນາ_ເມສາ_ພຶດສະພາ_ມິຖຸນາ_ກໍລະກົດ_ສິງຫາ_ກັນຍາ_ຕຸລາ_ພະຈິກ_ທັນວາ".split("_"),
                monthsShort: "ມັງກອນ_ກຸມພາ_ມີນາ_ເມສາ_ພຶດສະພາ_ມິຖຸນາ_ກໍລະກົດ_ສິງຫາ_ກັນຍາ_ຕຸລາ_ພະຈິກ_ທັນວາ".split("_"),
                weekdays: "ອາທິດ_ຈັນ_ອັງຄານ_ພຸດ_ພະຫັດ_ສຸກ_ເສົາ".split("_"),
                weekdaysShort: "ທິດ_ຈັນ_ອັງຄານ_ພຸດ_ພະຫັດ_ສຸກ_ເສົາ".split("_"),
                weekdaysMin: "ທ_ຈ_ອຄ_ພ_ພຫ_ສກ_ສ".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "ວັນdddd D MMMM YYYY HH:mm"
                },
                meridiemParse: /ຕອນເຊົ້າ|ຕອນແລງ/,
                isPM: function(e) {
                    return "ຕອນແລງ" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "ຕອນເຊົ້າ" : "ຕອນແລງ"
                },
                calendar: {
                    sameDay: "[ມື້ນີ້ເວລາ] LT",
                    nextDay: "[ມື້ອື່ນເວລາ] LT",
                    nextWeek: "[ວັນ]dddd[ໜ້າເວລາ] LT",
                    lastDay: "[ມື້ວານນີ້ເວລາ] LT",
                    lastWeek: "[ວັນ]dddd[ແລ້ວນີ້ເວລາ] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "ອີກ %s",
                    past: "%sຜ່ານມາ",
                    s: "ບໍ່ເທົ່າໃດວິນາທີ",
                    ss: "%d ວິນາທີ",
                    m: "1 ນາທີ",
                    mm: "%d ນາທີ",
                    h: "1 ຊົ່ວໂມງ",
                    hh: "%d ຊົ່ວໂມງ",
                    d: "1 ມື້",
                    dd: "%d ມື້",
                    M: "1 ເດືອນ",
                    MM: "%d ເດືອນ",
                    y: "1 ປີ",
                    yy: "%d ປີ"
                },
                dayOfMonthOrdinalParse: /(ທີ່)\d{1,2}/,
                ordinal: function(e) {
                    return "ທີ່" + e
                }
            });
            var Ee = {
                ss: "sekundė_sekundžių_sekundes",
                m: "minutė_minutės_minutę",
                mm: "minutės_minučių_minutes",
                h: "valanda_valandos_valandą",
                hh: "valandos_valandų_valandas",
                d: "diena_dienos_dieną",
                dd: "dienos_dienų_dienas",
                M: "mėnuo_mėnesio_mėnesį",
                MM: "mėnesiai_mėnesių_mėnesius",
                y: "metai_metų_metus",
                yy: "metai_metų_metus"
            };
            e.defineLocale("lt", {
                months: {
                    format: "sausio_vasario_kovo_balandžio_gegužės_birželio_liepos_rugpjūčio_rugsėjo_spalio_lapkričio_gruodžio".split("_"),
                    standalone: "sausis_vasaris_kovas_balandis_gegužė_birželis_liepa_rugpjūtis_rugsėjis_spalis_lapkritis_gruodis".split("_"),
                    isFormat: /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?|MMMM?(\[[^\[\]]*\]|\s)+D[oD]?/
                },
                monthsShort: "sau_vas_kov_bal_geg_bir_lie_rgp_rgs_spa_lap_grd".split("_"),
                weekdays: {
                    format: "sekmadienį_pirmadienį_antradienį_trečiadienį_ketvirtadienį_penktadienį_šeštadienį".split("_"),
                    standalone: "sekmadienis_pirmadienis_antradienis_trečiadienis_ketvirtadienis_penktadienis_šeštadienis".split("_"),
                    isFormat: /dddd HH:mm/
                },
                weekdaysShort: "Sek_Pir_Ant_Tre_Ket_Pen_Šeš".split("_"),
                weekdaysMin: "S_P_A_T_K_Pn_Š".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY-MM-DD",
                    LL: "YYYY [m.] MMMM D [d.]",
                    LLL: "YYYY [m.] MMMM D [d.], HH:mm [val.]",
                    LLLL: "YYYY [m.] MMMM D [d.], dddd, HH:mm [val.]",
                    l: "YYYY-MM-DD",
                    ll: "YYYY [m.] MMMM D [d.]",
                    lll: "YYYY [m.] MMMM D [d.], HH:mm [val.]",
                    llll: "YYYY [m.] MMMM D [d.], ddd, HH:mm [val.]"
                },
                calendar: {
                    sameDay: "[Šiandien] LT",
                    nextDay: "[Rytoj] LT",
                    nextWeek: "dddd LT",
                    lastDay: "[Vakar] LT",
                    lastWeek: "[Praėjusį] dddd LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "po %s",
                    past: "prieš %s",
                    s: function(e, t, a, n) {
                        return t ? "kelios sekundės" : n ? "kelių sekundžių" : "kelias sekundes"
                    },
                    ss: w,
                    m: L,
                    mm: w,
                    h: L,
                    hh: w,
                    d: L,
                    dd: w,
                    M: L,
                    MM: w,
                    y: L,
                    yy: w
                },
                dayOfMonthOrdinalParse: /\d{1,2}-oji/,
                ordinal: function(e) {
                    return e + "-oji"
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var We = {
                ss: "sekundes_sekundēm_sekunde_sekundes".split("_"),
                m: "minūtes_minūtēm_minūte_minūtes".split("_"),
                mm: "minūtes_minūtēm_minūte_minūtes".split("_"),
                h: "stundas_stundām_stunda_stundas".split("_"),
                hh: "stundas_stundām_stunda_stundas".split("_"),
                d: "dienas_dienām_diena_dienas".split("_"),
                dd: "dienas_dienām_diena_dienas".split("_"),
                M: "mēneša_mēnešiem_mēnesis_mēneši".split("_"),
                MM: "mēneša_mēnešiem_mēnesis_mēneši".split("_"),
                y: "gada_gadiem_gads_gadi".split("_"),
                yy: "gada_gadiem_gads_gadi".split("_")
            };
            e.defineLocale("lv", {
                months: "janvāris_februāris_marts_aprīlis_maijs_jūnijs_jūlijs_augusts_septembris_oktobris_novembris_decembris".split("_"),
                monthsShort: "jan_feb_mar_apr_mai_jūn_jūl_aug_sep_okt_nov_dec".split("_"),
                weekdays: "svētdiena_pirmdiena_otrdiena_trešdiena_ceturtdiena_piektdiena_sestdiena".split("_"),
                weekdaysShort: "Sv_P_O_T_C_Pk_S".split("_"),
                weekdaysMin: "Sv_P_O_T_C_Pk_S".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY.",
                    LL: "YYYY. [gada] D. MMMM",
                    LLL: "YYYY. [gada] D. MMMM, HH:mm",
                    LLLL: "YYYY. [gada] D. MMMM, dddd, HH:mm"
                },
                calendar: {
                    sameDay: "[Šodien pulksten] LT",
                    nextDay: "[Rīt pulksten] LT",
                    nextWeek: "dddd [pulksten] LT",
                    lastDay: "[Vakar pulksten] LT",
                    lastWeek: "[Pagājušā] dddd [pulksten] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "pēc %s",
                    past: "pirms %s",
                    s: function(e, t) {
                        return t ? "dažas sekundes" : "dažām sekundēm"
                    },
                    ss: D,
                    m: b,
                    mm: D,
                    h: b,
                    hh: D,
                    d: b,
                    dd: D,
                    M: b,
                    MM: D,
                    y: b,
                    yy: D
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var Ae = {
                words: {
                    ss: ["sekund", "sekunda", "sekundi"],
                    m: ["jedan minut", "jednog minuta"],
                    mm: ["minut", "minuta", "minuta"],
                    h: ["jedan sat", "jednog sata"],
                    hh: ["sat", "sata", "sati"],
                    dd: ["dan", "dana", "dana"],
                    MM: ["mjesec", "mjeseca", "mjeseci"],
                    yy: ["godina", "godine", "godina"]
                },
                correctGrammaticalCase: function(e, t) {
                    return 1 === e ? t[0] : e >= 2 && e <= 4 ? t[1] : t[2]
                },
                translate: function(e, t, a) {
                    var n = Ae.words[a];
                    return 1 === a.length ? t ? n[0] : n[1] : e + " " + Ae.correctGrammaticalCase(e, n)
                }
            };
            e.defineLocale("me", {
                months: "januar_februar_mart_april_maj_jun_jul_avgust_septembar_oktobar_novembar_decembar".split("_"),
                monthsShort: "jan._feb._mar._apr._maj_jun_jul_avg._sep._okt._nov._dec.".split("_"),
                monthsParseExact: !0,
                weekdays: "nedjelja_ponedjeljak_utorak_srijeda_četvrtak_petak_subota".split("_"),
                weekdaysShort: "ned._pon._uto._sri._čet._pet._sub.".split("_"),
                weekdaysMin: "ne_po_ut_sr_če_pe_su".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY H:mm",
                    LLLL: "dddd, D. MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[danas u] LT",
                    nextDay: "[sjutra u] LT",
                    nextWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[u] [nedjelju] [u] LT";
                            case 3:
                                return "[u] [srijedu] [u] LT";
                            case 6:
                                return "[u] [subotu] [u] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[u] dddd [u] LT"
                        }
                    },
                    lastDay: "[juče u] LT",
                    lastWeek: function() {
                        return ["[prošle] [nedjelje] [u] LT", "[prošlog] [ponedjeljka] [u] LT", "[prošlog] [utorka] [u] LT", "[prošle] [srijede] [u] LT", "[prošlog] [četvrtka] [u] LT", "[prošlog] [petka] [u] LT", "[prošle] [subote] [u] LT"][this.day()]
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "za %s",
                    past: "prije %s",
                    s: "nekoliko sekundi",
                    ss: Ae.translate,
                    m: Ae.translate,
                    mm: Ae.translate,
                    h: Ae.translate,
                    hh: Ae.translate,
                    d: "dan",
                    dd: Ae.translate,
                    M: "mjesec",
                    MM: Ae.translate,
                    y: "godinu",
                    yy: Ae.translate
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("mi", {
                months: "Kohi-tāte_Hui-tanguru_Poutū-te-rangi_Paenga-whāwhā_Haratua_Pipiri_Hōngoingoi_Here-turi-kōkā_Mahuru_Whiringa-ā-nuku_Whiringa-ā-rangi_Hakihea".split("_"),
                monthsShort: "Kohi_Hui_Pou_Pae_Hara_Pipi_Hōngoi_Here_Mahu_Whi-nu_Whi-ra_Haki".split("_"),
                monthsRegex: /(?:['a-z\u0101\u014D\u016B]+\-?){1,3}/i,
                monthsStrictRegex: /(?:['a-z\u0101\u014D\u016B]+\-?){1,3}/i,
                monthsShortRegex: /(?:['a-z\u0101\u014D\u016B]+\-?){1,3}/i,
                monthsShortStrictRegex: /(?:['a-z\u0101\u014D\u016B]+\-?){1,2}/i,
                weekdays: "Rātapu_Mane_Tūrei_Wenerei_Tāite_Paraire_Hātarei".split("_"),
                weekdaysShort: "Ta_Ma_Tū_We_Tāi_Pa_Hā".split("_"),
                weekdaysMin: "Ta_Ma_Tū_We_Tāi_Pa_Hā".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY [i] HH:mm",
                    LLLL: "dddd, D MMMM YYYY [i] HH:mm"
                },
                calendar: {
                    sameDay: "[i teie mahana, i] LT",
                    nextDay: "[apopo i] LT",
                    nextWeek: "dddd [i] LT",
                    lastDay: "[inanahi i] LT",
                    lastWeek: "dddd [whakamutunga i] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "i roto i %s",
                    past: "%s i mua",
                    s: "te hēkona ruarua",
                    ss: "%d hēkona",
                    m: "he meneti",
                    mm: "%d meneti",
                    h: "te haora",
                    hh: "%d haora",
                    d: "he ra",
                    dd: "%d ra",
                    M: "he marama",
                    MM: "%d marama",
                    y: "he tau",
                    yy: "%d tau"
                },
                dayOfMonthOrdinalParse: /\d{1,2}º/,
                ordinal: "%dº",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("mk", {
                months: "јануари_февруари_март_април_мај_јуни_јули_август_септември_октомври_ноември_декември".split("_"),
                monthsShort: "јан_фев_мар_апр_мај_јун_јул_авг_сеп_окт_ное_дек".split("_"),
                weekdays: "недела_понеделник_вторник_среда_четврток_петок_сабота".split("_"),
                weekdaysShort: "нед_пон_вто_сре_чет_пет_саб".split("_"),
                weekdaysMin: "нe_пo_вт_ср_че_пе_сa".split("_"),
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "D.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY H:mm",
                    LLLL: "dddd, D MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[Денес во] LT",
                    nextDay: "[Утре во] LT",
                    nextWeek: "[Во] dddd [во] LT",
                    lastDay: "[Вчера во] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                            case 3:
                            case 6:
                                return "[Изминатата] dddd [во] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[Изминатиот] dddd [во] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "после %s",
                    past: "пред %s",
                    s: "неколку секунди",
                    ss: "%d секунди",
                    m: "минута",
                    mm: "%d минути",
                    h: "час",
                    hh: "%d часа",
                    d: "ден",
                    dd: "%d дена",
                    M: "месец",
                    MM: "%d месеци",
                    y: "година",
                    yy: "%d години"
                },
                dayOfMonthOrdinalParse: /\d{1,2}-(ев|ен|ти|ви|ри|ми)/,
                ordinal: function(e) {
                    var t = e % 10,
                        a = e % 100;
                    return 0 === e ? e + "-ев" : 0 === a ? e + "-ен" : a > 10 && a < 20 ? e + "-ти" : 1 === t ? e + "-ви" : 2 === t ? e + "-ри" : 7 === t || 8 === t ? e + "-ми" : e + "-ти"
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("ml", {
                months: "ജനുവരി_ഫെബ്രുവരി_മാർച്ച്_ഏപ്രിൽ_മേയ്_ജൂൺ_ജൂലൈ_ഓഗസ്റ്റ്_സെപ്റ്റംബർ_ഒക്ടോബർ_നവംബർ_ഡിസംബർ".split("_"),
                monthsShort: "ജനു._ഫെബ്രു._മാർ._ഏപ്രി._മേയ്_ജൂൺ_ജൂലൈ._ഓഗ._സെപ്റ്റ._ഒക്ടോ._നവം._ഡിസം.".split("_"),
                monthsParseExact: !0,
                weekdays: "ഞായറാഴ്ച_തിങ്കളാഴ്ച_ചൊവ്വാഴ്ച_ബുധനാഴ്ച_വ്യാഴാഴ്ച_വെള്ളിയാഴ്ച_ശനിയാഴ്ച".split("_"),
                weekdaysShort: "ഞായർ_തിങ്കൾ_ചൊവ്വ_ബുധൻ_വ്യാഴം_വെള്ളി_ശനി".split("_"),
                weekdaysMin: "ഞാ_തി_ചൊ_ബു_വ്യാ_വെ_ശ".split("_"),
                longDateFormat: {
                    LT: "A h:mm -നു",
                    LTS: "A h:mm:ss -നു",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, A h:mm -നു",
                    LLLL: "dddd, D MMMM YYYY, A h:mm -നു"
                },
                calendar: {
                    sameDay: "[ഇന്ന്] LT",
                    nextDay: "[നാളെ] LT",
                    nextWeek: "dddd, LT",
                    lastDay: "[ഇന്നലെ] LT",
                    lastWeek: "[കഴിഞ്ഞ] dddd, LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s കഴിഞ്ഞ്",
                    past: "%s മുൻപ്",
                    s: "അൽപ നിമിഷങ്ങൾ",
                    ss: "%d സെക്കൻഡ്",
                    m: "ഒരു മിനിറ്റ്",
                    mm: "%d മിനിറ്റ്",
                    h: "ഒരു മണിക്കൂർ",
                    hh: "%d മണിക്കൂർ",
                    d: "ഒരു ദിവസം",
                    dd: "%d ദിവസം",
                    M: "ഒരു മാസം",
                    MM: "%d മാസം",
                    y: "ഒരു വർഷം",
                    yy: "%d വർഷം"
                },
                meridiemParse: /രാത്രി|രാവിലെ|ഉച്ച കഴിഞ്ഞ്|വൈകുന്നേരം|രാത്രി/i,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "രാത്രി" === t && e >= 4 || "ഉച്ച കഴിഞ്ഞ്" === t || "വൈകുന്നേരം" === t ? e + 12 : e
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "രാത്രി" : e < 12 ? "രാവിലെ" : e < 17 ? "ഉച്ച കഴിഞ്ഞ്" : e < 20 ? "വൈകുന്നേരം" : "രാത്രി"
                }
            }), e.defineLocale("mn", {
                months: "Нэгдүгээр сар_Хоёрдугаар сар_Гуравдугаар сар_Дөрөвдүгээр сар_Тавдугаар сар_Зургадугаар сар_Долдугаар сар_Наймдугаар сар_Есдүгээр сар_Аравдугаар сар_Арван нэгдүгээр сар_Арван хоёрдугаар сар".split("_"),
                monthsShort: "1 сар_2 сар_3 сар_4 сар_5 сар_6 сар_7 сар_8 сар_9 сар_10 сар_11 сар_12 сар".split("_"),
                monthsParseExact: !0,
                weekdays: "Ням_Даваа_Мягмар_Лхагва_Пүрэв_Баасан_Бямба".split("_"),
                weekdaysShort: "Ням_Дав_Мяг_Лха_Пүр_Баа_Бям".split("_"),
                weekdaysMin: "Ня_Да_Мя_Лх_Пү_Ба_Бя".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY-MM-DD",
                    LL: "YYYY оны MMMMын D",
                    LLL: "YYYY оны MMMMын D HH:mm",
                    LLLL: "dddd, YYYY оны MMMMын D HH:mm"
                },
                meridiemParse: /ҮӨ|ҮХ/i,
                isPM: function(e) {
                    return "ҮХ" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "ҮӨ" : "ҮХ"
                },
                calendar: {
                    sameDay: "[Өнөөдөр] LT",
                    nextDay: "[Маргааш] LT",
                    nextWeek: "[Ирэх] dddd LT",
                    lastDay: "[Өчигдөр] LT",
                    lastWeek: "[Өнгөрсөн] dddd LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s дараа",
                    past: "%s өмнө",
                    s: T,
                    ss: T,
                    m: T,
                    mm: T,
                    h: T,
                    hh: T,
                    d: T,
                    dd: T,
                    M: T,
                    MM: T,
                    y: T,
                    yy: T
                },
                dayOfMonthOrdinalParse: /\d{1,2} өдөр/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "d":
                        case "D":
                        case "DDD":
                            return e + " өдөр";
                        default:
                            return e
                    }
                }
            });
            var ze = {
                    1: "१",
                    2: "२",
                    3: "३",
                    4: "४",
                    5: "५",
                    6: "६",
                    7: "७",
                    8: "८",
                    9: "९",
                    0: "०"
                },
                Fe = {
                    "१": "1",
                    "२": "2",
                    "३": "3",
                    "४": "4",
                    "५": "5",
                    "६": "6",
                    "७": "7",
                    "८": "8",
                    "९": "9",
                    "०": "0"
                };
            e.defineLocale("mr", {
                months: "जानेवारी_फेब्रुवारी_मार्च_एप्रिल_मे_जून_जुलै_ऑगस्ट_सप्टेंबर_ऑक्टोबर_नोव्हेंबर_डिसेंबर".split("_"),
                monthsShort: "जाने._फेब्रु._मार्च._एप्रि._मे._जून._जुलै._ऑग._सप्टें._ऑक्टो._नोव्हें._डिसें.".split("_"),
                monthsParseExact: !0,
                weekdays: "रविवार_सोमवार_मंगळवार_बुधवार_गुरूवार_शुक्रवार_शनिवार".split("_"),
                weekdaysShort: "रवि_सोम_मंगळ_बुध_गुरू_शुक्र_शनि".split("_"),
                weekdaysMin: "र_सो_मं_बु_गु_शु_श".split("_"),
                longDateFormat: {
                    LT: "A h:mm वाजता",
                    LTS: "A h:mm:ss वाजता",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, A h:mm वाजता",
                    LLLL: "dddd, D MMMM YYYY, A h:mm वाजता"
                },
                calendar: {
                    sameDay: "[आज] LT",
                    nextDay: "[उद्या] LT",
                    nextWeek: "dddd, LT",
                    lastDay: "[काल] LT",
                    lastWeek: "[मागील] dddd, LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%sमध्ये",
                    past: "%sपूर्वी",
                    s: S,
                    ss: S,
                    m: S,
                    mm: S,
                    h: S,
                    hh: S,
                    d: S,
                    dd: S,
                    M: S,
                    MM: S,
                    y: S,
                    yy: S
                },
                preparse: function(e) {
                    return e.replace(/[१२३४५६७८९०]/g, function(e) {
                        return Fe[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return ze[e]
                    })
                },
                meridiemParse: /रात्री|सकाळी|दुपारी|सायंकाळी/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "रात्री" === t ? e < 4 ? e : e + 12 : "सकाळी" === t ? e : "दुपारी" === t ? e >= 10 ? e : e + 12 : "सायंकाळी" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "रात्री" : e < 10 ? "सकाळी" : e < 17 ? "दुपारी" : e < 20 ? "सायंकाळी" : "रात्री"
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            }), e.defineLocale("ms-my", {
                months: "Januari_Februari_Mac_April_Mei_Jun_Julai_Ogos_September_Oktober_November_Disember".split("_"),
                monthsShort: "Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ogs_Sep_Okt_Nov_Dis".split("_"),
                weekdays: "Ahad_Isnin_Selasa_Rabu_Khamis_Jumaat_Sabtu".split("_"),
                weekdaysShort: "Ahd_Isn_Sel_Rab_Kha_Jum_Sab".split("_"),
                weekdaysMin: "Ah_Is_Sl_Rb_Km_Jm_Sb".split("_"),
                longDateFormat: {
                    LT: "HH.mm",
                    LTS: "HH.mm.ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY [pukul] HH.mm",
                    LLLL: "dddd, D MMMM YYYY [pukul] HH.mm"
                },
                meridiemParse: /pagi|tengahari|petang|malam/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "pagi" === t ? e : "tengahari" === t ? e >= 11 ? e : e + 12 : "petang" === t || "malam" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 11 ? "pagi" : e < 15 ? "tengahari" : e < 19 ? "petang" : "malam"
                },
                calendar: {
                    sameDay: "[Hari ini pukul] LT",
                    nextDay: "[Esok pukul] LT",
                    nextWeek: "dddd [pukul] LT",
                    lastDay: "[Kelmarin pukul] LT",
                    lastWeek: "dddd [lepas pukul] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "dalam %s",
                    past: "%s yang lepas",
                    s: "beberapa saat",
                    ss: "%d saat",
                    m: "seminit",
                    mm: "%d minit",
                    h: "sejam",
                    hh: "%d jam",
                    d: "sehari",
                    dd: "%d hari",
                    M: "sebulan",
                    MM: "%d bulan",
                    y: "setahun",
                    yy: "%d tahun"
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("ms", {
                months: "Januari_Februari_Mac_April_Mei_Jun_Julai_Ogos_September_Oktober_November_Disember".split("_"),
                monthsShort: "Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ogs_Sep_Okt_Nov_Dis".split("_"),
                weekdays: "Ahad_Isnin_Selasa_Rabu_Khamis_Jumaat_Sabtu".split("_"),
                weekdaysShort: "Ahd_Isn_Sel_Rab_Kha_Jum_Sab".split("_"),
                weekdaysMin: "Ah_Is_Sl_Rb_Km_Jm_Sb".split("_"),
                longDateFormat: {
                    LT: "HH.mm",
                    LTS: "HH.mm.ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY [pukul] HH.mm",
                    LLLL: "dddd, D MMMM YYYY [pukul] HH.mm"
                },
                meridiemParse: /pagi|tengahari|petang|malam/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "pagi" === t ? e : "tengahari" === t ? e >= 11 ? e : e + 12 : "petang" === t || "malam" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 11 ? "pagi" : e < 15 ? "tengahari" : e < 19 ? "petang" : "malam"
                },
                calendar: {
                    sameDay: "[Hari ini pukul] LT",
                    nextDay: "[Esok pukul] LT",
                    nextWeek: "dddd [pukul] LT",
                    lastDay: "[Kelmarin pukul] LT",
                    lastWeek: "dddd [lepas pukul] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "dalam %s",
                    past: "%s yang lepas",
                    s: "beberapa saat",
                    ss: "%d saat",
                    m: "seminit",
                    mm: "%d minit",
                    h: "sejam",
                    hh: "%d jam",
                    d: "sehari",
                    dd: "%d hari",
                    M: "sebulan",
                    MM: "%d bulan",
                    y: "setahun",
                    yy: "%d tahun"
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("mt", {
                months: "Jannar_Frar_Marzu_April_Mejju_Ġunju_Lulju_Awwissu_Settembru_Ottubru_Novembru_Diċembru".split("_"),
                monthsShort: "Jan_Fra_Mar_Apr_Mej_Ġun_Lul_Aww_Set_Ott_Nov_Diċ".split("_"),
                weekdays: "Il-Ħadd_It-Tnejn_It-Tlieta_L-Erbgħa_Il-Ħamis_Il-Ġimgħa_Is-Sibt".split("_"),
                weekdaysShort: "Ħad_Tne_Tli_Erb_Ħam_Ġim_Sib".split("_"),
                weekdaysMin: "Ħa_Tn_Tl_Er_Ħa_Ġi_Si".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Illum fil-]LT",
                    nextDay: "[Għada fil-]LT",
                    nextWeek: "dddd [fil-]LT",
                    lastDay: "[Il-bieraħ fil-]LT",
                    lastWeek: "dddd [li għadda] [fil-]LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "f’ %s",
                    past: "%s ilu",
                    s: "ftit sekondi",
                    ss: "%d sekondi",
                    m: "minuta",
                    mm: "%d minuti",
                    h: "siegħa",
                    hh: "%d siegħat",
                    d: "ġurnata",
                    dd: "%d ġranet",
                    M: "xahar",
                    MM: "%d xhur",
                    y: "sena",
                    yy: "%d sni"
                },
                dayOfMonthOrdinalParse: /\d{1,2}º/,
                ordinal: "%dº",
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var Ie = {
                    1: "၁",
                    2: "၂",
                    3: "၃",
                    4: "၄",
                    5: "၅",
                    6: "၆",
                    7: "၇",
                    8: "၈",
                    9: "၉",
                    0: "၀"
                },
                $e = {
                    "၁": "1",
                    "၂": "2",
                    "၃": "3",
                    "၄": "4",
                    "၅": "5",
                    "၆": "6",
                    "၇": "7",
                    "၈": "8",
                    "၉": "9",
                    "၀": "0"
                };
            e.defineLocale("my", {
                months: "ဇန်နဝါရီ_ဖေဖော်ဝါရီ_မတ်_ဧပြီ_မေ_ဇွန်_ဇူလိုင်_သြဂုတ်_စက်တင်ဘာ_အောက်တိုဘာ_နိုဝင်ဘာ_ဒီဇင်ဘာ".split("_"),
                monthsShort: "ဇန်_ဖေ_မတ်_ပြီ_မေ_ဇွန်_လိုင်_သြ_စက်_အောက်_နို_ဒီ".split("_"),
                weekdays: "တနင်္ဂနွေ_တနင်္လာ_အင်္ဂါ_ဗုဒ္ဓဟူး_ကြာသပတေး_သောကြာ_စနေ".split("_"),
                weekdaysShort: "နွေ_လာ_ဂါ_ဟူး_ကြာ_သော_နေ".split("_"),
                weekdaysMin: "နွေ_လာ_ဂါ_ဟူး_ကြာ_သော_နေ".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[ယနေ.] LT [မှာ]",
                    nextDay: "[မနက်ဖြန်] LT [မှာ]",
                    nextWeek: "dddd LT [မှာ]",
                    lastDay: "[မနေ.က] LT [မှာ]",
                    lastWeek: "[ပြီးခဲ့သော] dddd LT [မှာ]",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "လာမည့် %s မှာ",
                    past: "လွန်ခဲ့သော %s က",
                    s: "စက္ကန်.အနည်းငယ်",
                    ss: "%d စက္ကန့်",
                    m: "တစ်မိနစ်",
                    mm: "%d မိနစ်",
                    h: "တစ်နာရီ",
                    hh: "%d နာရီ",
                    d: "တစ်ရက်",
                    dd: "%d ရက်",
                    M: "တစ်လ",
                    MM: "%d လ",
                    y: "တစ်နှစ်",
                    yy: "%d နှစ်"
                },
                preparse: function(e) {
                    return e.replace(/[၁၂၃၄၅၆၇၈၉၀]/g, function(e) {
                        return $e[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return Ie[e]
                    })
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("nb", {
                months: "januar_februar_mars_april_mai_juni_juli_august_september_oktober_november_desember".split("_"),
                monthsShort: "jan._feb._mars_april_mai_juni_juli_aug._sep._okt._nov._des.".split("_"),
                monthsParseExact: !0,
                weekdays: "søndag_mandag_tirsdag_onsdag_torsdag_fredag_lørdag".split("_"),
                weekdaysShort: "sø._ma._ti._on._to._fr._lø.".split("_"),
                weekdaysMin: "sø_ma_ti_on_to_fr_lø".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY [kl.] HH:mm",
                    LLLL: "dddd D. MMMM YYYY [kl.] HH:mm"
                },
                calendar: {
                    sameDay: "[i dag kl.] LT",
                    nextDay: "[i morgen kl.] LT",
                    nextWeek: "dddd [kl.] LT",
                    lastDay: "[i går kl.] LT",
                    lastWeek: "[forrige] dddd [kl.] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "om %s",
                    past: "%s siden",
                    s: "noen sekunder",
                    ss: "%d sekunder",
                    m: "ett minutt",
                    mm: "%d minutter",
                    h: "en time",
                    hh: "%d timer",
                    d: "en dag",
                    dd: "%d dager",
                    M: "en måned",
                    MM: "%d måneder",
                    y: "ett år",
                    yy: "%d år"
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var Ne = {
                    1: "१",
                    2: "२",
                    3: "३",
                    4: "४",
                    5: "५",
                    6: "६",
                    7: "७",
                    8: "८",
                    9: "९",
                    0: "०"
                },
                Re = {
                    "१": "1",
                    "२": "2",
                    "३": "3",
                    "४": "4",
                    "५": "5",
                    "६": "6",
                    "७": "7",
                    "८": "8",
                    "९": "9",
                    "०": "0"
                };
            e.defineLocale("ne", {
                months: "जनवरी_फेब्रुवरी_मार्च_अप्रिल_मई_जुन_जुलाई_अगष्ट_सेप्टेम्बर_अक्टोबर_नोभेम्बर_डिसेम्बर".split("_"),
                monthsShort: "जन._फेब्रु._मार्च_अप्रि._मई_जुन_जुलाई._अग._सेप्ट._अक्टो._नोभे._डिसे.".split("_"),
                monthsParseExact: !0,
                weekdays: "आइतबार_सोमबार_मङ्गलबार_बुधबार_बिहिबार_शुक्रबार_शनिबार".split("_"),
                weekdaysShort: "आइत._सोम._मङ्गल._बुध._बिहि._शुक्र._शनि.".split("_"),
                weekdaysMin: "आ._सो._मं._बु._बि._शु._श.".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "Aको h:mm बजे",
                    LTS: "Aको h:mm:ss बजे",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, Aको h:mm बजे",
                    LLLL: "dddd, D MMMM YYYY, Aको h:mm बजे"
                },
                preparse: function(e) {
                    return e.replace(/[१२३४५६७८९०]/g, function(e) {
                        return Re[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return Ne[e]
                    })
                },
                meridiemParse: /राति|बिहान|दिउँसो|साँझ/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "राति" === t ? e < 4 ? e : e + 12 : "बिहान" === t ? e : "दिउँसो" === t ? e >= 10 ? e : e + 12 : "साँझ" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 3 ? "राति" : e < 12 ? "बिहान" : e < 16 ? "दिउँसो" : e < 20 ? "साँझ" : "राति"
                },
                calendar: {
                    sameDay: "[आज] LT",
                    nextDay: "[भोलि] LT",
                    nextWeek: "[आउँदो] dddd[,] LT",
                    lastDay: "[हिजो] LT",
                    lastWeek: "[गएको] dddd[,] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%sमा",
                    past: "%s अगाडि",
                    s: "केही क्षण",
                    ss: "%d सेकेण्ड",
                    m: "एक मिनेट",
                    mm: "%d मिनेट",
                    h: "एक घण्टा",
                    hh: "%d घण्टा",
                    d: "एक दिन",
                    dd: "%d दिन",
                    M: "एक महिना",
                    MM: "%d महिना",
                    y: "एक बर्ष",
                    yy: "%d बर्ष"
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            });
            var Je = "jan._feb._mrt._apr._mei_jun._jul._aug._sep._okt._nov._dec.".split("_"),
                Be = "jan_feb_mrt_apr_mei_jun_jul_aug_sep_okt_nov_dec".split("_"),
                Ue = [/^jan/i, /^feb/i, /^maart|mrt.?$/i, /^apr/i, /^mei$/i, /^jun[i.]?$/i, /^jul[i.]?$/i, /^aug/i, /^sep/i, /^okt/i, /^nov/i, /^dec/i],
                Ve = /^(januari|februari|maart|april|mei|april|ju[nl]i|augustus|september|oktober|november|december|jan\.?|feb\.?|mrt\.?|apr\.?|ju[nl]\.?|aug\.?|sep\.?|okt\.?|nov\.?|dec\.?)/i;
            e.defineLocale("nl-be", {
                months: "januari_februari_maart_april_mei_juni_juli_augustus_september_oktober_november_december".split("_"),
                monthsShort: function(e, t) {
                    return e ? /-MMM-/.test(t) ? Be[e.month()] : Je[e.month()] : Je
                },
                monthsRegex: Ve,
                monthsShortRegex: Ve,
                monthsStrictRegex: /^(januari|februari|maart|mei|ju[nl]i|april|augustus|september|oktober|november|december)/i,
                monthsShortStrictRegex: /^(jan\.?|feb\.?|mrt\.?|apr\.?|mei|ju[nl]\.?|aug\.?|sep\.?|okt\.?|nov\.?|dec\.?)/i,
                monthsParse: Ue,
                longMonthsParse: Ue,
                shortMonthsParse: Ue,
                weekdays: "zondag_maandag_dinsdag_woensdag_donderdag_vrijdag_zaterdag".split("_"),
                weekdaysShort: "zo._ma._di._wo._do._vr._za.".split("_"),
                weekdaysMin: "zo_ma_di_wo_do_vr_za".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[vandaag om] LT",
                    nextDay: "[morgen om] LT",
                    nextWeek: "dddd [om] LT",
                    lastDay: "[gisteren om] LT",
                    lastWeek: "[afgelopen] dddd [om] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "over %s",
                    past: "%s geleden",
                    s: "een paar seconden",
                    ss: "%d seconden",
                    m: "één minuut",
                    mm: "%d minuten",
                    h: "één uur",
                    hh: "%d uur",
                    d: "één dag",
                    dd: "%d dagen",
                    M: "één maand",
                    MM: "%d maanden",
                    y: "één jaar",
                    yy: "%d jaar"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(ste|de)/,
                ordinal: function(e) {
                    return e + (1 === e || 8 === e || e >= 20 ? "ste" : "de")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var qe = "jan._feb._mrt._apr._mei_jun._jul._aug._sep._okt._nov._dec.".split("_"),
                Ge = "jan_feb_mrt_apr_mei_jun_jul_aug_sep_okt_nov_dec".split("_"),
                Ze = [/^jan/i, /^feb/i, /^maart|mrt.?$/i, /^apr/i, /^mei$/i, /^jun[i.]?$/i, /^jul[i.]?$/i, /^aug/i, /^sep/i, /^okt/i, /^nov/i, /^dec/i],
                Ke = /^(januari|februari|maart|april|mei|april|ju[nl]i|augustus|september|oktober|november|december|jan\.?|feb\.?|mrt\.?|apr\.?|ju[nl]\.?|aug\.?|sep\.?|okt\.?|nov\.?|dec\.?)/i;
            e.defineLocale("nl", {
                months: "januari_februari_maart_april_mei_juni_juli_augustus_september_oktober_november_december".split("_"),
                monthsShort: function(e, t) {
                    return e ? /-MMM-/.test(t) ? Ge[e.month()] : qe[e.month()] : qe
                },
                monthsRegex: Ke,
                monthsShortRegex: Ke,
                monthsStrictRegex: /^(januari|februari|maart|mei|ju[nl]i|april|augustus|september|oktober|november|december)/i,
                monthsShortStrictRegex: /^(jan\.?|feb\.?|mrt\.?|apr\.?|mei|ju[nl]\.?|aug\.?|sep\.?|okt\.?|nov\.?|dec\.?)/i,
                monthsParse: Ze,
                longMonthsParse: Ze,
                shortMonthsParse: Ze,
                weekdays: "zondag_maandag_dinsdag_woensdag_donderdag_vrijdag_zaterdag".split("_"),
                weekdaysShort: "zo._ma._di._wo._do._vr._za.".split("_"),
                weekdaysMin: "zo_ma_di_wo_do_vr_za".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD-MM-YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[vandaag om] LT",
                    nextDay: "[morgen om] LT",
                    nextWeek: "dddd [om] LT",
                    lastDay: "[gisteren om] LT",
                    lastWeek: "[afgelopen] dddd [om] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "over %s",
                    past: "%s geleden",
                    s: "een paar seconden",
                    ss: "%d seconden",
                    m: "één minuut",
                    mm: "%d minuten",
                    h: "één uur",
                    hh: "%d uur",
                    d: "één dag",
                    dd: "%d dagen",
                    M: "één maand",
                    MM: "%d maanden",
                    y: "één jaar",
                    yy: "%d jaar"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(ste|de)/,
                ordinal: function(e) {
                    return e + (1 === e || 8 === e || e >= 20 ? "ste" : "de")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("nn", {
                months: "januar_februar_mars_april_mai_juni_juli_august_september_oktober_november_desember".split("_"),
                monthsShort: "jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_"),
                weekdays: "sundag_måndag_tysdag_onsdag_torsdag_fredag_laurdag".split("_"),
                weekdaysShort: "sun_mån_tys_ons_tor_fre_lau".split("_"),
                weekdaysMin: "su_må_ty_on_to_fr_lø".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY [kl.] H:mm",
                    LLLL: "dddd D. MMMM YYYY [kl.] HH:mm"
                },
                calendar: {
                    sameDay: "[I dag klokka] LT",
                    nextDay: "[I morgon klokka] LT",
                    nextWeek: "dddd [klokka] LT",
                    lastDay: "[I går klokka] LT",
                    lastWeek: "[Føregåande] dddd [klokka] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "om %s",
                    past: "%s sidan",
                    s: "nokre sekund",
                    ss: "%d sekund",
                    m: "eit minutt",
                    mm: "%d minutt",
                    h: "ein time",
                    hh: "%d timar",
                    d: "ein dag",
                    dd: "%d dagar",
                    M: "ein månad",
                    MM: "%d månader",
                    y: "eit år",
                    yy: "%d år"
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var Qe = {
                    1: "੧",
                    2: "੨",
                    3: "੩",
                    4: "੪",
                    5: "੫",
                    6: "੬",
                    7: "੭",
                    8: "੮",
                    9: "੯",
                    0: "੦"
                },
                Xe = {
                    "੧": "1",
                    "੨": "2",
                    "੩": "3",
                    "੪": "4",
                    "੫": "5",
                    "੬": "6",
                    "੭": "7",
                    "੮": "8",
                    "੯": "9",
                    "੦": "0"
                };
            e.defineLocale("pa-in", {
                months: "ਜਨਵਰੀ_ਫ਼ਰਵਰੀ_ਮਾਰਚ_ਅਪ੍ਰੈਲ_ਮਈ_ਜੂਨ_ਜੁਲਾਈ_ਅਗਸਤ_ਸਤੰਬਰ_ਅਕਤੂਬਰ_ਨਵੰਬਰ_ਦਸੰਬਰ".split("_"),
                monthsShort: "ਜਨਵਰੀ_ਫ਼ਰਵਰੀ_ਮਾਰਚ_ਅਪ੍ਰੈਲ_ਮਈ_ਜੂਨ_ਜੁਲਾਈ_ਅਗਸਤ_ਸਤੰਬਰ_ਅਕਤੂਬਰ_ਨਵੰਬਰ_ਦਸੰਬਰ".split("_"),
                weekdays: "ਐਤਵਾਰ_ਸੋਮਵਾਰ_ਮੰਗਲਵਾਰ_ਬੁਧਵਾਰ_ਵੀਰਵਾਰ_ਸ਼ੁੱਕਰਵਾਰ_ਸ਼ਨੀਚਰਵਾਰ".split("_"),
                weekdaysShort: "ਐਤ_ਸੋਮ_ਮੰਗਲ_ਬੁਧ_ਵੀਰ_ਸ਼ੁਕਰ_ਸ਼ਨੀ".split("_"),
                weekdaysMin: "ਐਤ_ਸੋਮ_ਮੰਗਲ_ਬੁਧ_ਵੀਰ_ਸ਼ੁਕਰ_ਸ਼ਨੀ".split("_"),
                longDateFormat: {
                    LT: "A h:mm ਵਜੇ",
                    LTS: "A h:mm:ss ਵਜੇ",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, A h:mm ਵਜੇ",
                    LLLL: "dddd, D MMMM YYYY, A h:mm ਵਜੇ"
                },
                calendar: {
                    sameDay: "[ਅਜ] LT",
                    nextDay: "[ਕਲ] LT",
                    nextWeek: "[ਅਗਲਾ] dddd, LT",
                    lastDay: "[ਕਲ] LT",
                    lastWeek: "[ਪਿਛਲੇ] dddd, LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s ਵਿੱਚ",
                    past: "%s ਪਿਛਲੇ",
                    s: "ਕੁਝ ਸਕਿੰਟ",
                    ss: "%d ਸਕਿੰਟ",
                    m: "ਇਕ ਮਿੰਟ",
                    mm: "%d ਮਿੰਟ",
                    h: "ਇੱਕ ਘੰਟਾ",
                    hh: "%d ਘੰਟੇ",
                    d: "ਇੱਕ ਦਿਨ",
                    dd: "%d ਦਿਨ",
                    M: "ਇੱਕ ਮਹੀਨਾ",
                    MM: "%d ਮਹੀਨੇ",
                    y: "ਇੱਕ ਸਾਲ",
                    yy: "%d ਸਾਲ"
                },
                preparse: function(e) {
                    return e.replace(/[੧੨੩੪੫੬੭੮੯੦]/g, function(e) {
                        return Xe[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return Qe[e]
                    })
                },
                meridiemParse: /ਰਾਤ|ਸਵੇਰ|ਦੁਪਹਿਰ|ਸ਼ਾਮ/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "ਰਾਤ" === t ? e < 4 ? e : e + 12 : "ਸਵੇਰ" === t ? e : "ਦੁਪਹਿਰ" === t ? e >= 10 ? e : e + 12 : "ਸ਼ਾਮ" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "ਰਾਤ" : e < 10 ? "ਸਵੇਰ" : e < 17 ? "ਦੁਪਹਿਰ" : e < 20 ? "ਸ਼ਾਮ" : "ਰਾਤ"
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            });
            var et = "styczeń_luty_marzec_kwiecień_maj_czerwiec_lipiec_sierpień_wrzesień_październik_listopad_grudzień".split("_"),
                tt = "stycznia_lutego_marca_kwietnia_maja_czerwca_lipca_sierpnia_września_października_listopada_grudnia".split("_");
            e.defineLocale("pl", {
                months: function(e, t) {
                    return e ? "" === t ? "(" + tt[e.month()] + "|" + et[e.month()] + ")" : /D MMMM/.test(t) ? tt[e.month()] : et[e.month()] : et
                },
                monthsShort: "sty_lut_mar_kwi_maj_cze_lip_sie_wrz_paź_lis_gru".split("_"),
                weekdays: "niedziela_poniedziałek_wtorek_środa_czwartek_piątek_sobota".split("_"),
                weekdaysShort: "ndz_pon_wt_śr_czw_pt_sob".split("_"),
                weekdaysMin: "Nd_Pn_Wt_Śr_Cz_Pt_So".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Dziś o] LT",
                    nextDay: "[Jutro o] LT",
                    nextWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[W niedzielę o] LT";
                            case 2:
                                return "[We wtorek o] LT";
                            case 3:
                                return "[W środę o] LT";
                            case 6:
                                return "[W sobotę o] LT";
                            default:
                                return "[W] dddd [o] LT"
                        }
                    },
                    lastDay: "[Wczoraj o] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[W zeszłą niedzielę o] LT";
                            case 3:
                                return "[W zeszłą środę o] LT";
                            case 6:
                                return "[W zeszłą sobotę o] LT";
                            default:
                                return "[W zeszły] dddd [o] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "za %s",
                    past: "%s temu",
                    s: "kilka sekund",
                    ss: H,
                    m: H,
                    mm: H,
                    h: H,
                    hh: H,
                    d: "1 dzień",
                    dd: "%d dni",
                    M: "miesiąc",
                    MM: H,
                    y: "rok",
                    yy: H
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("pt-br", {
                months: "janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro".split("_"),
                monthsShort: "jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez".split("_"),
                weekdays: "Domingo_Segunda-feira_Terça-feira_Quarta-feira_Quinta-feira_Sexta-feira_Sábado".split("_"),
                weekdaysShort: "Dom_Seg_Ter_Qua_Qui_Sex_Sáb".split("_"),
                weekdaysMin: "Do_2ª_3ª_4ª_5ª_6ª_Sá".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D [de] MMMM [de] YYYY",
                    LLL: "D [de] MMMM [de] YYYY [às] HH:mm",
                    LLLL: "dddd, D [de] MMMM [de] YYYY [às] HH:mm"
                },
                calendar: {
                    sameDay: "[Hoje às] LT",
                    nextDay: "[Amanhã às] LT",
                    nextWeek: "dddd [às] LT",
                    lastDay: "[Ontem às] LT",
                    lastWeek: function() {
                        return 0 === this.day() || 6 === this.day() ? "[Último] dddd [às] LT" : "[Última] dddd [às] LT"
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "em %s",
                    past: "há %s",
                    s: "poucos segundos",
                    ss: "%d segundos",
                    m: "um minuto",
                    mm: "%d minutos",
                    h: "uma hora",
                    hh: "%d horas",
                    d: "um dia",
                    dd: "%d dias",
                    M: "um mês",
                    MM: "%d meses",
                    y: "um ano",
                    yy: "%d anos"
                },
                dayOfMonthOrdinalParse: /\d{1,2}º/,
                ordinal: "%dº"
            }), e.defineLocale("pt", {
                months: "janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro".split("_"),
                monthsShort: "jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez".split("_"),
                weekdays: "Domingo_Segunda-feira_Terça-feira_Quarta-feira_Quinta-feira_Sexta-feira_Sábado".split("_"),
                weekdaysShort: "Dom_Seg_Ter_Qua_Qui_Sex_Sáb".split("_"),
                weekdaysMin: "Do_2ª_3ª_4ª_5ª_6ª_Sá".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D [de] MMMM [de] YYYY",
                    LLL: "D [de] MMMM [de] YYYY HH:mm",
                    LLLL: "dddd, D [de] MMMM [de] YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Hoje às] LT",
                    nextDay: "[Amanhã às] LT",
                    nextWeek: "dddd [às] LT",
                    lastDay: "[Ontem às] LT",
                    lastWeek: function() {
                        return 0 === this.day() || 6 === this.day() ? "[Último] dddd [às] LT" : "[Última] dddd [às] LT"
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "em %s",
                    past: "há %s",
                    s: "segundos",
                    ss: "%d segundos",
                    m: "um minuto",
                    mm: "%d minutos",
                    h: "uma hora",
                    hh: "%d horas",
                    d: "um dia",
                    dd: "%d dias",
                    M: "um mês",
                    MM: "%d meses",
                    y: "um ano",
                    yy: "%d anos"
                },
                dayOfMonthOrdinalParse: /\d{1,2}º/,
                ordinal: "%dº",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("ro", {
                months: "ianuarie_februarie_martie_aprilie_mai_iunie_iulie_august_septembrie_octombrie_noiembrie_decembrie".split("_"),
                monthsShort: "ian._febr._mart._apr._mai_iun._iul._aug._sept._oct._nov._dec.".split("_"),
                monthsParseExact: !0,
                weekdays: "duminică_luni_marți_miercuri_joi_vineri_sâmbătă".split("_"),
                weekdaysShort: "Dum_Lun_Mar_Mie_Joi_Vin_Sâm".split("_"),
                weekdaysMin: "Du_Lu_Ma_Mi_Jo_Vi_Sâ".split("_"),
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY H:mm",
                    LLLL: "dddd, D MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[azi la] LT",
                    nextDay: "[mâine la] LT",
                    nextWeek: "dddd [la] LT",
                    lastDay: "[ieri la] LT",
                    lastWeek: "[fosta] dddd [la] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "peste %s",
                    past: "%s în urmă",
                    s: "câteva secunde",
                    ss: j,
                    m: "un minut",
                    mm: j,
                    h: "o oră",
                    hh: j,
                    d: "o zi",
                    dd: j,
                    M: "o lună",
                    MM: j,
                    y: "un an",
                    yy: j
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            });
            var at = [/^янв/i, /^фев/i, /^мар/i, /^апр/i, /^ма[йя]/i, /^июн/i, /^июл/i, /^авг/i, /^сен/i, /^окт/i, /^ноя/i, /^дек/i];
            e.defineLocale("ru", {
                months: {
                    format: "января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря".split("_"),
                    standalone: "январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь".split("_")
                },
                monthsShort: {
                    format: "янв._февр._мар._апр._мая_июня_июля_авг._сент._окт._нояб._дек.".split("_"),
                    standalone: "янв._февр._март_апр._май_июнь_июль_авг._сент._окт._нояб._дек.".split("_")
                },
                weekdays: {
                    standalone: "воскресенье_понедельник_вторник_среда_четверг_пятница_суббота".split("_"),
                    format: "воскресенье_понедельник_вторник_среду_четверг_пятницу_субботу".split("_"),
                    isFormat: /\[ ?[Вв] ?(?:прошлую|следующую|эту)? ?\] ?dddd/
                },
                weekdaysShort: "вс_пн_вт_ср_чт_пт_сб".split("_"),
                weekdaysMin: "вс_пн_вт_ср_чт_пт_сб".split("_"),
                monthsParse: at,
                longMonthsParse: at,
                shortMonthsParse: at,
                monthsRegex: /^(январ[ья]|янв\.?|феврал[ья]|февр?\.?|марта?|мар\.?|апрел[ья]|апр\.?|ма[йя]|июн[ья]|июн\.?|июл[ья]|июл\.?|августа?|авг\.?|сентябр[ья]|сент?\.?|октябр[ья]|окт\.?|ноябр[ья]|нояб?\.?|декабр[ья]|дек\.?)/i,
                monthsShortRegex: /^(январ[ья]|янв\.?|феврал[ья]|февр?\.?|марта?|мар\.?|апрел[ья]|апр\.?|ма[йя]|июн[ья]|июн\.?|июл[ья]|июл\.?|августа?|авг\.?|сентябр[ья]|сент?\.?|октябр[ья]|окт\.?|ноябр[ья]|нояб?\.?|декабр[ья]|дек\.?)/i,
                monthsStrictRegex: /^(январ[яь]|феврал[яь]|марта?|апрел[яь]|ма[яй]|июн[яь]|июл[яь]|августа?|сентябр[яь]|октябр[яь]|ноябр[яь]|декабр[яь])/i,
                monthsShortStrictRegex: /^(янв\.|февр?\.|мар[т.]|апр\.|ма[яй]|июн[ья.]|июл[ья.]|авг\.|сент?\.|окт\.|нояб?\.|дек\.)/i,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY г.",
                    LLL: "D MMMM YYYY г., H:mm",
                    LLLL: "dddd, D MMMM YYYY г., H:mm"
                },
                calendar: {
                    sameDay: "[Сегодня, в] LT",
                    nextDay: "[Завтра, в] LT",
                    lastDay: "[Вчера, в] LT",
                    nextWeek: function(e) {
                        if (e.week() === this.week()) return 2 === this.day() ? "[Во] dddd, [в] LT" : "[В] dddd, [в] LT";
                        switch (this.day()) {
                            case 0:
                                return "[В следующее] dddd, [в] LT";
                            case 1:
                            case 2:
                            case 4:
                                return "[В следующий] dddd, [в] LT";
                            case 3:
                            case 5:
                            case 6:
                                return "[В следующую] dddd, [в] LT"
                        }
                    },
                    lastWeek: function(e) {
                        if (e.week() === this.week()) return 2 === this.day() ? "[Во] dddd, [в] LT" : "[В] dddd, [в] LT";
                        switch (this.day()) {
                            case 0:
                                return "[В прошлое] dddd, [в] LT";
                            case 1:
                            case 2:
                            case 4:
                                return "[В прошлый] dddd, [в] LT";
                            case 3:
                            case 5:
                            case 6:
                                return "[В прошлую] dddd, [в] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "через %s",
                    past: "%s назад",
                    s: "несколько секунд",
                    ss: C,
                    m: C,
                    mm: C,
                    h: "час",
                    hh: C,
                    d: "день",
                    dd: C,
                    M: "месяц",
                    MM: C,
                    y: "год",
                    yy: C
                },
                meridiemParse: /ночи|утра|дня|вечера/i,
                isPM: function(e) {
                    return /^(дня|вечера)$/.test(e)
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "ночи" : e < 12 ? "утра" : e < 17 ? "дня" : "вечера"
                },
                dayOfMonthOrdinalParse: /\d{1,2}-(й|го|я)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "M":
                        case "d":
                        case "DDD":
                            return e + "-й";
                        case "D":
                            return e + "-го";
                        case "w":
                        case "W":
                            return e + "-я";
                        default:
                            return e
                    }
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var nt = ["جنوري", "فيبروري", "مارچ", "اپريل", "مئي", "جون", "جولاءِ", "آگسٽ", "سيپٽمبر", "آڪٽوبر", "نومبر", "ڊسمبر"],
                st = ["آچر", "سومر", "اڱارو", "اربع", "خميس", "جمع", "ڇنڇر"];
            e.defineLocale("sd", {
                months: nt,
                monthsShort: nt,
                weekdays: st,
                weekdaysShort: st,
                weekdaysMin: st,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd، D MMMM YYYY HH:mm"
                },
                meridiemParse: /صبح|شام/,
                isPM: function(e) {
                    return "شام" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "صبح" : "شام"
                },
                calendar: {
                    sameDay: "[اڄ] LT",
                    nextDay: "[سڀاڻي] LT",
                    nextWeek: "dddd [اڳين هفتي تي] LT",
                    lastDay: "[ڪالهه] LT",
                    lastWeek: "[گزريل هفتي] dddd [تي] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s پوء",
                    past: "%s اڳ",
                    s: "چند سيڪنڊ",
                    ss: "%d سيڪنڊ",
                    m: "هڪ منٽ",
                    mm: "%d منٽ",
                    h: "هڪ ڪلاڪ",
                    hh: "%d ڪلاڪ",
                    d: "هڪ ڏينهن",
                    dd: "%d ڏينهن",
                    M: "هڪ مهينو",
                    MM: "%d مهينا",
                    y: "هڪ سال",
                    yy: "%d سال"
                },
                preparse: function(e) {
                    return e.replace(/،/g, ",")
                },
                postformat: function(e) {
                    return e.replace(/,/g, "،")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("se", {
                months: "ođđajagemánnu_guovvamánnu_njukčamánnu_cuoŋománnu_miessemánnu_geassemánnu_suoidnemánnu_borgemánnu_čakčamánnu_golggotmánnu_skábmamánnu_juovlamánnu".split("_"),
                monthsShort: "ođđj_guov_njuk_cuo_mies_geas_suoi_borg_čakč_golg_skáb_juov".split("_"),
                weekdays: "sotnabeaivi_vuossárga_maŋŋebárga_gaskavahkku_duorastat_bearjadat_lávvardat".split("_"),
                weekdaysShort: "sotn_vuos_maŋ_gask_duor_bear_láv".split("_"),
                weekdaysMin: "s_v_m_g_d_b_L".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "MMMM D. [b.] YYYY",
                    LLL: "MMMM D. [b.] YYYY [ti.] HH:mm",
                    LLLL: "dddd, MMMM D. [b.] YYYY [ti.] HH:mm"
                },
                calendar: {
                    sameDay: "[otne ti] LT",
                    nextDay: "[ihttin ti] LT",
                    nextWeek: "dddd [ti] LT",
                    lastDay: "[ikte ti] LT",
                    lastWeek: "[ovddit] dddd [ti] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s geažes",
                    past: "maŋit %s",
                    s: "moadde sekunddat",
                    ss: "%d sekunddat",
                    m: "okta minuhta",
                    mm: "%d minuhtat",
                    h: "okta diimmu",
                    hh: "%d diimmut",
                    d: "okta beaivi",
                    dd: "%d beaivvit",
                    M: "okta mánnu",
                    MM: "%d mánut",
                    y: "okta jahki",
                    yy: "%d jagit"
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("si", {
                months: "ජනවාරි_පෙබරවාරි_මාර්තු_අප්‍රේල්_මැයි_ජූනි_ජූලි_අගෝස්තු_සැප්තැම්බර්_ඔක්තෝබර්_නොවැම්බර්_දෙසැම්බර්".split("_"),
                monthsShort: "ජන_පෙබ_මාර්_අප්_මැයි_ජූනි_ජූලි_අගෝ_සැප්_ඔක්_නොවැ_දෙසැ".split("_"),
                weekdays: "ඉරිදා_සඳුදා_අඟහරුවාදා_බදාදා_බ්‍රහස්පතින්දා_සිකුරාදා_සෙනසුරාදා".split("_"),
                weekdaysShort: "ඉරි_සඳු_අඟ_බදා_බ්‍රහ_සිකු_සෙන".split("_"),
                weekdaysMin: "ඉ_ස_අ_බ_බ්‍ර_සි_සෙ".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "a h:mm",
                    LTS: "a h:mm:ss",
                    L: "YYYY/MM/DD",
                    LL: "YYYY MMMM D",
                    LLL: "YYYY MMMM D, a h:mm",
                    LLLL: "YYYY MMMM D [වැනි] dddd, a h:mm:ss"
                },
                calendar: {
                    sameDay: "[අද] LT[ට]",
                    nextDay: "[හෙට] LT[ට]",
                    nextWeek: "dddd LT[ට]",
                    lastDay: "[ඊයේ] LT[ට]",
                    lastWeek: "[පසුගිය] dddd LT[ට]",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%sකින්",
                    past: "%sකට පෙර",
                    s: "තත්පර කිහිපය",
                    ss: "තත්පර %d",
                    m: "මිනිත්තුව",
                    mm: "මිනිත්තු %d",
                    h: "පැය",
                    hh: "පැය %d",
                    d: "දිනය",
                    dd: "දින %d",
                    M: "මාසය",
                    MM: "මාස %d",
                    y: "වසර",
                    yy: "වසර %d"
                },
                dayOfMonthOrdinalParse: /\d{1,2} වැනි/,
                ordinal: function(e) {
                    return e + " වැනි"
                },
                meridiemParse: /පෙර වරු|පස් වරු|පෙ.ව|ප.ව./,
                isPM: function(e) {
                    return "ප.ව." === e || "පස් වරු" === e
                },
                meridiem: function(e, t, a) {
                    return e > 11 ? a ? "ප.ව." : "පස් වරු" : a ? "පෙ.ව." : "පෙර වරු"
                }
            });
            var it = "január_február_marec_apríl_máj_jún_júl_august_september_október_november_december".split("_"),
                rt = "jan_feb_mar_apr_máj_jún_júl_aug_sep_okt_nov_dec".split("_");
            e.defineLocale("sk", {
                months: it,
                monthsShort: rt,
                weekdays: "nedeľa_pondelok_utorok_streda_štvrtok_piatok_sobota".split("_"),
                weekdaysShort: "ne_po_ut_st_št_pi_so".split("_"),
                weekdaysMin: "ne_po_ut_st_št_pi_so".split("_"),
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY H:mm",
                    LLLL: "dddd D. MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[dnes o] LT",
                    nextDay: "[zajtra o] LT",
                    nextWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[v nedeľu o] LT";
                            case 1:
                            case 2:
                                return "[v] dddd [o] LT";
                            case 3:
                                return "[v stredu o] LT";
                            case 4:
                                return "[vo štvrtok o] LT";
                            case 5:
                                return "[v piatok o] LT";
                            case 6:
                                return "[v sobotu o] LT"
                        }
                    },
                    lastDay: "[včera o] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[minulú nedeľu o] LT";
                            case 1:
                            case 2:
                                return "[minulý] dddd [o] LT";
                            case 3:
                                return "[minulú stredu o] LT";
                            case 4:
                            case 5:
                                return "[minulý] dddd [o] LT";
                            case 6:
                                return "[minulú sobotu o] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "za %s",
                    past: "pred %s",
                    s: O,
                    ss: O,
                    m: O,
                    mm: O,
                    h: O,
                    hh: O,
                    d: O,
                    dd: O,
                    M: O,
                    MM: O,
                    y: O,
                    yy: O
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("sl", {
                months: "januar_februar_marec_april_maj_junij_julij_avgust_september_oktober_november_december".split("_"),
                monthsShort: "jan._feb._mar._apr._maj._jun._jul._avg._sep._okt._nov._dec.".split("_"),
                monthsParseExact: !0,
                weekdays: "nedelja_ponedeljek_torek_sreda_četrtek_petek_sobota".split("_"),
                weekdaysShort: "ned._pon._tor._sre._čet._pet._sob.".split("_"),
                weekdaysMin: "ne_po_to_sr_če_pe_so".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY H:mm",
                    LLLL: "dddd, D. MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[danes ob] LT",
                    nextDay: "[jutri ob] LT",
                    nextWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[v] [nedeljo] [ob] LT";
                            case 3:
                                return "[v] [sredo] [ob] LT";
                            case 6:
                                return "[v] [soboto] [ob] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[v] dddd [ob] LT"
                        }
                    },
                    lastDay: "[včeraj ob] LT",
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[prejšnjo] [nedeljo] [ob] LT";
                            case 3:
                                return "[prejšnjo] [sredo] [ob] LT";
                            case 6:
                                return "[prejšnjo] [soboto] [ob] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[prejšnji] dddd [ob] LT"
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "čez %s",
                    past: "pred %s",
                    s: E,
                    ss: E,
                    m: E,
                    mm: E,
                    h: E,
                    hh: E,
                    d: E,
                    dd: E,
                    M: E,
                    MM: E,
                    y: E,
                    yy: E
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("sq", {
                months: "Janar_Shkurt_Mars_Prill_Maj_Qershor_Korrik_Gusht_Shtator_Tetor_Nëntor_Dhjetor".split("_"),
                monthsShort: "Jan_Shk_Mar_Pri_Maj_Qer_Kor_Gus_Sht_Tet_Nën_Dhj".split("_"),
                weekdays: "E Diel_E Hënë_E Martë_E Mërkurë_E Enjte_E Premte_E Shtunë".split("_"),
                weekdaysShort: "Die_Hën_Mar_Mër_Enj_Pre_Sht".split("_"),
                weekdaysMin: "D_H_Ma_Më_E_P_Sh".split("_"),
                weekdaysParseExact: !0,
                meridiemParse: /PD|MD/,
                isPM: function(e) {
                    return "M" === e.charAt(0)
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "PD" : "MD"
                },
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Sot në] LT",
                    nextDay: "[Nesër në] LT",
                    nextWeek: "dddd [në] LT",
                    lastDay: "[Dje në] LT",
                    lastWeek: "dddd [e kaluar në] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "në %s",
                    past: "%s më parë",
                    s: "disa sekonda",
                    ss: "%d sekonda",
                    m: "një minutë",
                    mm: "%d minuta",
                    h: "një orë",
                    hh: "%d orë",
                    d: "një ditë",
                    dd: "%d ditë",
                    M: "një muaj",
                    MM: "%d muaj",
                    y: "një vit",
                    yy: "%d vite"
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var ot = {
                words: {
                    ss: ["секунда", "секунде", "секунди"],
                    m: ["један минут", "једне минуте"],
                    mm: ["минут", "минуте", "минута"],
                    h: ["један сат", "једног сата"],
                    hh: ["сат", "сата", "сати"],
                    dd: ["дан", "дана", "дана"],
                    MM: ["месец", "месеца", "месеци"],
                    yy: ["година", "године", "година"]
                },
                correctGrammaticalCase: function(e, t) {
                    return 1 === e ? t[0] : e >= 2 && e <= 4 ? t[1] : t[2]
                },
                translate: function(e, t, a) {
                    var n = ot.words[a];
                    return 1 === a.length ? t ? n[0] : n[1] : e + " " + ot.correctGrammaticalCase(e, n)
                }
            };
            e.defineLocale("sr-cyrl", {
                months: "јануар_фебруар_март_април_мај_јун_јул_август_септембар_октобар_новембар_децембар".split("_"),
                monthsShort: "јан._феб._мар._апр._мај_јун_јул_авг._сеп._окт._нов._дец.".split("_"),
                monthsParseExact: !0,
                weekdays: "недеља_понедељак_уторак_среда_четвртак_петак_субота".split("_"),
                weekdaysShort: "нед._пон._уто._сре._чет._пет._суб.".split("_"),
                weekdaysMin: "не_по_ут_ср_че_пе_су".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY H:mm",
                    LLLL: "dddd, D. MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[данас у] LT",
                    nextDay: "[сутра у] LT",
                    nextWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[у] [недељу] [у] LT";
                            case 3:
                                return "[у] [среду] [у] LT";
                            case 6:
                                return "[у] [суботу] [у] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[у] dddd [у] LT"
                        }
                    },
                    lastDay: "[јуче у] LT",
                    lastWeek: function() {
                        return ["[прошле] [недеље] [у] LT", "[прошлог] [понедељка] [у] LT", "[прошлог] [уторка] [у] LT", "[прошле] [среде] [у] LT", "[прошлог] [четвртка] [у] LT", "[прошлог] [петка] [у] LT", "[прошле] [суботе] [у] LT"][this.day()]
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "за %s",
                    past: "пре %s",
                    s: "неколико секунди",
                    ss: ot.translate,
                    m: ot.translate,
                    mm: ot.translate,
                    h: ot.translate,
                    hh: ot.translate,
                    d: "дан",
                    dd: ot.translate,
                    M: "месец",
                    MM: ot.translate,
                    y: "годину",
                    yy: ot.translate
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 7
                }
            });
            var dt = {
                words: {
                    ss: ["sekunda", "sekunde", "sekundi"],
                    m: ["jedan minut", "jedne minute"],
                    mm: ["minut", "minute", "minuta"],
                    h: ["jedan sat", "jednog sata"],
                    hh: ["sat", "sata", "sati"],
                    dd: ["dan", "dana", "dana"],
                    MM: ["mesec", "meseca", "meseci"],
                    yy: ["godina", "godine", "godina"]
                },
                correctGrammaticalCase: function(e, t) {
                    return 1 === e ? t[0] : e >= 2 && e <= 4 ? t[1] : t[2]
                },
                translate: function(e, t, a) {
                    var n = dt.words[a];
                    return 1 === a.length ? t ? n[0] : n[1] : e + " " + dt.correctGrammaticalCase(e, n)
                }
            };
            e.defineLocale("sr", {
                months: "januar_februar_mart_april_maj_jun_jul_avgust_septembar_oktobar_novembar_decembar".split("_"),
                monthsShort: "jan._feb._mar._apr._maj_jun_jul_avg._sep._okt._nov._dec.".split("_"),
                monthsParseExact: !0,
                weekdays: "nedelja_ponedeljak_utorak_sreda_četvrtak_petak_subota".split("_"),
                weekdaysShort: "ned._pon._uto._sre._čet._pet._sub.".split("_"),
                weekdaysMin: "ne_po_ut_sr_če_pe_su".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM YYYY",
                    LLL: "D. MMMM YYYY H:mm",
                    LLLL: "dddd, D. MMMM YYYY H:mm"
                },
                calendar: {
                    sameDay: "[danas u] LT",
                    nextDay: "[sutra u] LT",
                    nextWeek: function() {
                        switch (this.day()) {
                            case 0:
                                return "[u] [nedelju] [u] LT";
                            case 3:
                                return "[u] [sredu] [u] LT";
                            case 6:
                                return "[u] [subotu] [u] LT";
                            case 1:
                            case 2:
                            case 4:
                            case 5:
                                return "[u] dddd [u] LT"
                        }
                    },
                    lastDay: "[juče u] LT",
                    lastWeek: function() {
                        return ["[prošle] [nedelje] [u] LT", "[prošlog] [ponedeljka] [u] LT", "[prošlog] [utorka] [u] LT", "[prošle] [srede] [u] LT", "[prošlog] [četvrtka] [u] LT", "[prošlog] [petka] [u] LT", "[prošle] [subote] [u] LT"][this.day()]
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "za %s",
                    past: "pre %s",
                    s: "nekoliko sekundi",
                    ss: dt.translate,
                    m: dt.translate,
                    mm: dt.translate,
                    h: dt.translate,
                    hh: dt.translate,
                    d: "dan",
                    dd: dt.translate,
                    M: "mesec",
                    MM: dt.translate,
                    y: "godinu",
                    yy: dt.translate
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("ss", {
                months: "Bhimbidvwane_Indlovana_Indlov'lenkhulu_Mabasa_Inkhwekhweti_Inhlaba_Kholwane_Ingci_Inyoni_Imphala_Lweti_Ingongoni".split("_"),
                monthsShort: "Bhi_Ina_Inu_Mab_Ink_Inh_Kho_Igc_Iny_Imp_Lwe_Igo".split("_"),
                weekdays: "Lisontfo_Umsombuluko_Lesibili_Lesitsatfu_Lesine_Lesihlanu_Umgcibelo".split("_"),
                weekdaysShort: "Lis_Umb_Lsb_Les_Lsi_Lsh_Umg".split("_"),
                weekdaysMin: "Li_Us_Lb_Lt_Ls_Lh_Ug".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "h:mm A",
                    LTS: "h:mm:ss A",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY h:mm A",
                    LLLL: "dddd, D MMMM YYYY h:mm A"
                },
                calendar: {
                    sameDay: "[Namuhla nga] LT",
                    nextDay: "[Kusasa nga] LT",
                    nextWeek: "dddd [nga] LT",
                    lastDay: "[Itolo nga] LT",
                    lastWeek: "dddd [leliphelile] [nga] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "nga %s",
                    past: "wenteka nga %s",
                    s: "emizuzwana lomcane",
                    ss: "%d mzuzwana",
                    m: "umzuzu",
                    mm: "%d emizuzu",
                    h: "lihora",
                    hh: "%d emahora",
                    d: "lilanga",
                    dd: "%d emalanga",
                    M: "inyanga",
                    MM: "%d tinyanga",
                    y: "umnyaka",
                    yy: "%d iminyaka"
                },
                meridiemParse: /ekuseni|emini|entsambama|ebusuku/,
                meridiem: function(e, t, a) {
                    return e < 11 ? "ekuseni" : e < 15 ? "emini" : e < 19 ? "entsambama" : "ebusuku"
                },
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "ekuseni" === t ? e : "emini" === t ? e >= 11 ? e : e + 12 : "entsambama" === t || "ebusuku" === t ? 0 === e ? 0 : e + 12 : void 0
                },
                dayOfMonthOrdinalParse: /\d{1,2}/,
                ordinal: "%d",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("sv", {
                months: "januari_februari_mars_april_maj_juni_juli_augusti_september_oktober_november_december".split("_"),
                monthsShort: "jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec".split("_"),
                weekdays: "söndag_måndag_tisdag_onsdag_torsdag_fredag_lördag".split("_"),
                weekdaysShort: "sön_mån_tis_ons_tor_fre_lör".split("_"),
                weekdaysMin: "sö_må_ti_on_to_fr_lö".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY-MM-DD",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY [kl.] HH:mm",
                    LLLL: "dddd D MMMM YYYY [kl.] HH:mm",
                    lll: "D MMM YYYY HH:mm",
                    llll: "ddd D MMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Idag] LT",
                    nextDay: "[Imorgon] LT",
                    lastDay: "[Igår] LT",
                    nextWeek: "[På] dddd LT",
                    lastWeek: "[I] dddd[s] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "om %s",
                    past: "för %s sedan",
                    s: "några sekunder",
                    ss: "%d sekunder",
                    m: "en minut",
                    mm: "%d minuter",
                    h: "en timme",
                    hh: "%d timmar",
                    d: "en dag",
                    dd: "%d dagar",
                    M: "en månad",
                    MM: "%d månader",
                    y: "ett år",
                    yy: "%d år"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(e|a)/,
                ordinal: function(e) {
                    var t = e % 10;
                    return e + (1 == ~~(e % 100 / 10) ? "e" : 1 === t ? "a" : 2 === t ? "a" : "e")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("sw", {
                months: "Januari_Februari_Machi_Aprili_Mei_Juni_Julai_Agosti_Septemba_Oktoba_Novemba_Desemba".split("_"),
                monthsShort: "Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ago_Sep_Okt_Nov_Des".split("_"),
                weekdays: "Jumapili_Jumatatu_Jumanne_Jumatano_Alhamisi_Ijumaa_Jumamosi".split("_"),
                weekdaysShort: "Jpl_Jtat_Jnne_Jtan_Alh_Ijm_Jmos".split("_"),
                weekdaysMin: "J2_J3_J4_J5_Al_Ij_J1".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[leo saa] LT",
                    nextDay: "[kesho saa] LT",
                    nextWeek: "[wiki ijayo] dddd [saat] LT",
                    lastDay: "[jana] LT",
                    lastWeek: "[wiki iliyopita] dddd [saat] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s baadaye",
                    past: "tokea %s",
                    s: "hivi punde",
                    ss: "sekunde %d",
                    m: "dakika moja",
                    mm: "dakika %d",
                    h: "saa limoja",
                    hh: "masaa %d",
                    d: "siku moja",
                    dd: "masiku %d",
                    M: "mwezi mmoja",
                    MM: "miezi %d",
                    y: "mwaka mmoja",
                    yy: "miaka %d"
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            });
            var lt = {
                    1: "௧",
                    2: "௨",
                    3: "௩",
                    4: "௪",
                    5: "௫",
                    6: "௬",
                    7: "௭",
                    8: "௮",
                    9: "௯",
                    0: "௦"
                },
                ut = {
                    "௧": "1",
                    "௨": "2",
                    "௩": "3",
                    "௪": "4",
                    "௫": "5",
                    "௬": "6",
                    "௭": "7",
                    "௮": "8",
                    "௯": "9",
                    "௦": "0"
                };
            e.defineLocale("ta", {
                months: "ஜனவரி_பிப்ரவரி_மார்ச்_ஏப்ரல்_மே_ஜூன்_ஜூலை_ஆகஸ்ட்_செப்டெம்பர்_அக்டோபர்_நவம்பர்_டிசம்பர்".split("_"),
                monthsShort: "ஜனவரி_பிப்ரவரி_மார்ச்_ஏப்ரல்_மே_ஜூன்_ஜூலை_ஆகஸ்ட்_செப்டெம்பர்_அக்டோபர்_நவம்பர்_டிசம்பர்".split("_"),
                weekdays: "ஞாயிற்றுக்கிழமை_திங்கட்கிழமை_செவ்வாய்கிழமை_புதன்கிழமை_வியாழக்கிழமை_வெள்ளிக்கிழமை_சனிக்கிழமை".split("_"),
                weekdaysShort: "ஞாயிறு_திங்கள்_செவ்வாய்_புதன்_வியாழன்_வெள்ளி_சனி".split("_"),
                weekdaysMin: "ஞா_தி_செ_பு_வி_வெ_ச".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, HH:mm",
                    LLLL: "dddd, D MMMM YYYY, HH:mm"
                },
                calendar: {
                    sameDay: "[இன்று] LT",
                    nextDay: "[நாளை] LT",
                    nextWeek: "dddd, LT",
                    lastDay: "[நேற்று] LT",
                    lastWeek: "[கடந்த வாரம்] dddd, LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s இல்",
                    past: "%s முன்",
                    s: "ஒரு சில விநாடிகள்",
                    ss: "%d விநாடிகள்",
                    m: "ஒரு நிமிடம்",
                    mm: "%d நிமிடங்கள்",
                    h: "ஒரு மணி நேரம்",
                    hh: "%d மணி நேரம்",
                    d: "ஒரு நாள்",
                    dd: "%d நாட்கள்",
                    M: "ஒரு மாதம்",
                    MM: "%d மாதங்கள்",
                    y: "ஒரு வருடம்",
                    yy: "%d ஆண்டுகள்"
                },
                dayOfMonthOrdinalParse: /\d{1,2}வது/,
                ordinal: function(e) {
                    return e + "வது"
                },
                preparse: function(e) {
                    return e.replace(/[௧௨௩௪௫௬௭௮௯௦]/g, function(e) {
                        return ut[e]
                    })
                },
                postformat: function(e) {
                    return e.replace(/\d/g, function(e) {
                        return lt[e]
                    })
                },
                meridiemParse: /யாமம்|வைகறை|காலை|நண்பகல்|எற்பாடு|மாலை/,
                meridiem: function(e, t, a) {
                    return e < 2 ? " யாமம்" : e < 6 ? " வைகறை" : e < 10 ? " காலை" : e < 14 ? " நண்பகல்" : e < 18 ? " எற்பாடு" : e < 22 ? " மாலை" : " யாமம்"
                },
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "யாமம்" === t ? e < 2 ? e : e + 12 : "வைகறை" === t || "காலை" === t ? e : "நண்பகல்" === t && e >= 10 ? e : e + 12
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            }), e.defineLocale("te", {
                months: "జనవరి_ఫిబ్రవరి_మార్చి_ఏప్రిల్_మే_జూన్_జూలై_ఆగస్టు_సెప్టెంబర్_అక్టోబర్_నవంబర్_డిసెంబర్".split("_"),
                monthsShort: "జన._ఫిబ్ర._మార్చి_ఏప్రి._మే_జూన్_జూలై_ఆగ._సెప్._అక్టో._నవ._డిసె.".split("_"),
                monthsParseExact: !0,
                weekdays: "ఆదివారం_సోమవారం_మంగళవారం_బుధవారం_గురువారం_శుక్రవారం_శనివారం".split("_"),
                weekdaysShort: "ఆది_సోమ_మంగళ_బుధ_గురు_శుక్ర_శని".split("_"),
                weekdaysMin: "ఆ_సో_మం_బు_గు_శు_శ".split("_"),
                longDateFormat: {
                    LT: "A h:mm",
                    LTS: "A h:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY, A h:mm",
                    LLLL: "dddd, D MMMM YYYY, A h:mm"
                },
                calendar: {
                    sameDay: "[నేడు] LT",
                    nextDay: "[రేపు] LT",
                    nextWeek: "dddd, LT",
                    lastDay: "[నిన్న] LT",
                    lastWeek: "[గత] dddd, LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s లో",
                    past: "%s క్రితం",
                    s: "కొన్ని క్షణాలు",
                    ss: "%d సెకన్లు",
                    m: "ఒక నిమిషం",
                    mm: "%d నిమిషాలు",
                    h: "ఒక గంట",
                    hh: "%d గంటలు",
                    d: "ఒక రోజు",
                    dd: "%d రోజులు",
                    M: "ఒక నెల",
                    MM: "%d నెలలు",
                    y: "ఒక సంవత్సరం",
                    yy: "%d సంవత్సరాలు"
                },
                dayOfMonthOrdinalParse: /\d{1,2}వ/,
                ordinal: "%dవ",
                meridiemParse: /రాత్రి|ఉదయం|మధ్యాహ్నం|సాయంత్రం/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "రాత్రి" === t ? e < 4 ? e : e + 12 : "ఉదయం" === t ? e : "మధ్యాహ్నం" === t ? e >= 10 ? e : e + 12 : "సాయంత్రం" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "రాత్రి" : e < 10 ? "ఉదయం" : e < 17 ? "మధ్యాహ్నం" : e < 20 ? "సాయంత్రం" : "రాత్రి"
                },
                week: {
                    dow: 0,
                    doy: 6
                }
            }), e.defineLocale("tet", {
                months: "Janeiru_Fevereiru_Marsu_Abril_Maiu_Juñu_Jullu_Agustu_Setembru_Outubru_Novembru_Dezembru".split("_"),
                monthsShort: "Jan_Fev_Mar_Abr_Mai_Jun_Jul_Ago_Set_Out_Nov_Dez".split("_"),
                weekdays: "Domingu_Segunda_Tersa_Kuarta_Kinta_Sesta_Sabadu".split("_"),
                weekdaysShort: "Dom_Seg_Ters_Kua_Kint_Sest_Sab".split("_"),
                weekdaysMin: "Do_Seg_Te_Ku_Ki_Ses_Sa".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Ohin iha] LT",
                    nextDay: "[Aban iha] LT",
                    nextWeek: "dddd [iha] LT",
                    lastDay: "[Horiseik iha] LT",
                    lastWeek: "dddd [semana kotuk] [iha] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "iha %s",
                    past: "%s liuba",
                    s: "minutu balun",
                    ss: "minutu %d",
                    m: "minutu ida",
                    mm: "minutu %d",
                    h: "oras ida",
                    hh: "oras %d",
                    d: "loron ida",
                    dd: "loron %d",
                    M: "fulan ida",
                    MM: "fulan %d",
                    y: "tinan ida",
                    yy: "tinan %d"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(st|nd|rd|th)/,
                ordinal: function(e) {
                    var t = e % 10;
                    return e + (1 == ~~(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var _t = {
                0: "-ум",
                1: "-ум",
                2: "-юм",
                3: "-юм",
                4: "-ум",
                5: "-ум",
                6: "-ум",
                7: "-ум",
                8: "-ум",
                9: "-ум",
                10: "-ум",
                12: "-ум",
                13: "-ум",
                20: "-ум",
                30: "-юм",
                40: "-ум",
                50: "-ум",
                60: "-ум",
                70: "-ум",
                80: "-ум",
                90: "-ум",
                100: "-ум"
            };
            e.defineLocale("tg", {
                months: "январ_феврал_март_апрел_май_июн_июл_август_сентябр_октябр_ноябр_декабр".split("_"),
                monthsShort: "янв_фев_мар_апр_май_июн_июл_авг_сен_окт_ноя_дек".split("_"),
                weekdays: "якшанбе_душанбе_сешанбе_чоршанбе_панҷшанбе_ҷумъа_шанбе".split("_"),
                weekdaysShort: "яшб_дшб_сшб_чшб_пшб_ҷум_шнб".split("_"),
                weekdaysMin: "яш_дш_сш_чш_пш_ҷм_шб".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Имрӯз соати] LT",
                    nextDay: "[Пагоҳ соати] LT",
                    lastDay: "[Дирӯз соати] LT",
                    nextWeek: "dddd[и] [ҳафтаи оянда соати] LT",
                    lastWeek: "dddd[и] [ҳафтаи гузашта соати] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "баъди %s",
                    past: "%s пеш",
                    s: "якчанд сония",
                    m: "як дақиқа",
                    mm: "%d дақиқа",
                    h: "як соат",
                    hh: "%d соат",
                    d: "як рӯз",
                    dd: "%d рӯз",
                    M: "як моҳ",
                    MM: "%d моҳ",
                    y: "як сол",
                    yy: "%d сол"
                },
                meridiemParse: /шаб|субҳ|рӯз|бегоҳ/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "шаб" === t ? e < 4 ? e : e + 12 : "субҳ" === t ? e : "рӯз" === t ? e >= 11 ? e : e + 12 : "бегоҳ" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "шаб" : e < 11 ? "субҳ" : e < 16 ? "рӯз" : e < 19 ? "бегоҳ" : "шаб"
                },
                dayOfMonthOrdinalParse: /\d{1,2}-(ум|юм)/,
                ordinal: function(e) {
                    return e + (_t[e] || _t[e % 10] || _t[e >= 100 ? 100 : null])
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("th", {
                months: "มกราคม_กุมภาพันธ์_มีนาคม_เมษายน_พฤษภาคม_มิถุนายน_กรกฎาคม_สิงหาคม_กันยายน_ตุลาคม_พฤศจิกายน_ธันวาคม".split("_"),
                monthsShort: "ม.ค._ก.พ._มี.ค._เม.ย._พ.ค._มิ.ย._ก.ค._ส.ค._ก.ย._ต.ค._พ.ย._ธ.ค.".split("_"),
                monthsParseExact: !0,
                weekdays: "อาทิตย์_จันทร์_อังคาร_พุธ_พฤหัสบดี_ศุกร์_เสาร์".split("_"),
                weekdaysShort: "อาทิตย์_จันทร์_อังคาร_พุธ_พฤหัส_ศุกร์_เสาร์".split("_"),
                weekdaysMin: "อา._จ._อ._พ._พฤ._ศ._ส.".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "H:mm",
                    LTS: "H:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY เวลา H:mm",
                    LLLL: "วันddddที่ D MMMM YYYY เวลา H:mm"
                },
                meridiemParse: /ก่อนเที่ยง|หลังเที่ยง/,
                isPM: function(e) {
                    return "หลังเที่ยง" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "ก่อนเที่ยง" : "หลังเที่ยง"
                },
                calendar: {
                    sameDay: "[วันนี้ เวลา] LT",
                    nextDay: "[พรุ่งนี้ เวลา] LT",
                    nextWeek: "dddd[หน้า เวลา] LT",
                    lastDay: "[เมื่อวานนี้ เวลา] LT",
                    lastWeek: "[วัน]dddd[ที่แล้ว เวลา] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "อีก %s",
                    past: "%sที่แล้ว",
                    s: "ไม่กี่วินาที",
                    ss: "%d วินาที",
                    m: "1 นาที",
                    mm: "%d นาที",
                    h: "1 ชั่วโมง",
                    hh: "%d ชั่วโมง",
                    d: "1 วัน",
                    dd: "%d วัน",
                    M: "1 เดือน",
                    MM: "%d เดือน",
                    y: "1 ปี",
                    yy: "%d ปี"
                }
            }), e.defineLocale("tl-ph", {
                months: "Enero_Pebrero_Marso_Abril_Mayo_Hunyo_Hulyo_Agosto_Setyembre_Oktubre_Nobyembre_Disyembre".split("_"),
                monthsShort: "Ene_Peb_Mar_Abr_May_Hun_Hul_Ago_Set_Okt_Nob_Dis".split("_"),
                weekdays: "Linggo_Lunes_Martes_Miyerkules_Huwebes_Biyernes_Sabado".split("_"),
                weekdaysShort: "Lin_Lun_Mar_Miy_Huw_Biy_Sab".split("_"),
                weekdaysMin: "Li_Lu_Ma_Mi_Hu_Bi_Sab".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "MM/D/YYYY",
                    LL: "MMMM D, YYYY",
                    LLL: "MMMM D, YYYY HH:mm",
                    LLLL: "dddd, MMMM DD, YYYY HH:mm"
                },
                calendar: {
                    sameDay: "LT [ngayong araw]",
                    nextDay: "[Bukas ng] LT",
                    nextWeek: "LT [sa susunod na] dddd",
                    lastDay: "LT [kahapon]",
                    lastWeek: "LT [noong nakaraang] dddd",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "sa loob ng %s",
                    past: "%s ang nakalipas",
                    s: "ilang segundo",
                    ss: "%d segundo",
                    m: "isang minuto",
                    mm: "%d minuto",
                    h: "isang oras",
                    hh: "%d oras",
                    d: "isang araw",
                    dd: "%d araw",
                    M: "isang buwan",
                    MM: "%d buwan",
                    y: "isang taon",
                    yy: "%d taon"
                },
                dayOfMonthOrdinalParse: /\d{1,2}/,
                ordinal: function(e) {
                    return e
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var mt = "pagh_wa’_cha’_wej_loS_vagh_jav_Soch_chorgh_Hut".split("_");
            e.defineLocale("tlh", {
                months: "tera’ jar wa’_tera’ jar cha’_tera’ jar wej_tera’ jar loS_tera’ jar vagh_tera’ jar jav_tera’ jar Soch_tera’ jar chorgh_tera’ jar Hut_tera’ jar wa’maH_tera’ jar wa’maH wa’_tera’ jar wa’maH cha’".split("_"),
                monthsShort: "jar wa’_jar cha’_jar wej_jar loS_jar vagh_jar jav_jar Soch_jar chorgh_jar Hut_jar wa’maH_jar wa’maH wa’_jar wa’maH cha’".split("_"),
                monthsParseExact: !0,
                weekdays: "lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj".split("_"),
                weekdaysShort: "lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj".split("_"),
                weekdaysMin: "lojmItjaj_DaSjaj_povjaj_ghItlhjaj_loghjaj_buqjaj_ghInjaj".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[DaHjaj] LT",
                    nextDay: "[wa’leS] LT",
                    nextWeek: "LLL",
                    lastDay: "[wa’Hu’] LT",
                    lastWeek: "LLL",
                    sameElse: "L"
                },
                relativeTime: {
                    future: function(e) {
                        var t = e;
                        return t = -1 !== e.indexOf("jaj") ? t.slice(0, -3) + "leS" : -1 !== e.indexOf("jar") ? t.slice(0, -3) + "waQ" : -1 !== e.indexOf("DIS") ? t.slice(0, -3) + "nem" : t + " pIq"
                    },
                    past: function(e) {
                        var t = e;
                        return t = -1 !== e.indexOf("jaj") ? t.slice(0, -3) + "Hu’" : -1 !== e.indexOf("jar") ? t.slice(0, -3) + "wen" : -1 !== e.indexOf("DIS") ? t.slice(0, -3) + "ben" : t + " ret"
                    },
                    s: "puS lup",
                    ss: W,
                    m: "wa’ tup",
                    mm: W,
                    h: "wa’ rep",
                    hh: W,
                    d: "wa’ jaj",
                    dd: W,
                    M: "wa’ jar",
                    MM: W,
                    y: "wa’ DIS",
                    yy: W
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            });
            var ct = {
                1: "'inci",
                5: "'inci",
                8: "'inci",
                70: "'inci",
                80: "'inci",
                2: "'nci",
                7: "'nci",
                20: "'nci",
                50: "'nci",
                3: "'üncü",
                4: "'üncü",
                100: "'üncü",
                6: "'ncı",
                9: "'uncu",
                10: "'uncu",
                30: "'uncu",
                60: "'ıncı",
                90: "'ıncı"
            };
            e.defineLocale("tr", {
                months: "Ocak_Şubat_Mart_Nisan_Mayıs_Haziran_Temmuz_Ağustos_Eylül_Ekim_Kasım_Aralık".split("_"),
                monthsShort: "Oca_Şub_Mar_Nis_May_Haz_Tem_Ağu_Eyl_Eki_Kas_Ara".split("_"),
                weekdays: "Pazar_Pazartesi_Salı_Çarşamba_Perşembe_Cuma_Cumartesi".split("_"),
                weekdaysShort: "Paz_Pts_Sal_Çar_Per_Cum_Cts".split("_"),
                weekdaysMin: "Pz_Pt_Sa_Ça_Pe_Cu_Ct".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[bugün saat] LT",
                    nextDay: "[yarın saat] LT",
                    nextWeek: "[gelecek] dddd [saat] LT",
                    lastDay: "[dün] LT",
                    lastWeek: "[geçen] dddd [saat] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s sonra",
                    past: "%s önce",
                    s: "birkaç saniye",
                    ss: "%d saniye",
                    m: "bir dakika",
                    mm: "%d dakika",
                    h: "bir saat",
                    hh: "%d saat",
                    d: "bir gün",
                    dd: "%d gün",
                    M: "bir ay",
                    MM: "%d ay",
                    y: "bir yıl",
                    yy: "%d yıl"
                },
                ordinal: function(e, t) {
                    switch (t) {
                        case "d":
                        case "D":
                        case "Do":
                        case "DD":
                            return e;
                        default:
                            if (0 === e) return e + "'ıncı";
                            var a = e % 10;
                            return e + (ct[a] || ct[e % 100 - a] || ct[e >= 100 ? 100 : null])
                    }
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("tzl", {
                months: "Januar_Fevraglh_Març_Avrïu_Mai_Gün_Julia_Guscht_Setemvar_Listopäts_Noemvar_Zecemvar".split("_"),
                monthsShort: "Jan_Fev_Mar_Avr_Mai_Gün_Jul_Gus_Set_Lis_Noe_Zec".split("_"),
                weekdays: "Súladi_Lúneçi_Maitzi_Márcuri_Xhúadi_Viénerçi_Sáturi".split("_"),
                weekdaysShort: "Súl_Lún_Mai_Már_Xhú_Vié_Sát".split("_"),
                weekdaysMin: "Sú_Lú_Ma_Má_Xh_Vi_Sá".split("_"),
                longDateFormat: {
                    LT: "HH.mm",
                    LTS: "HH.mm.ss",
                    L: "DD.MM.YYYY",
                    LL: "D. MMMM [dallas] YYYY",
                    LLL: "D. MMMM [dallas] YYYY HH.mm",
                    LLLL: "dddd, [li] D. MMMM [dallas] YYYY HH.mm"
                },
                meridiemParse: /d\'o|d\'a/i,
                isPM: function(e) {
                    return "d'o" === e.toLowerCase()
                },
                meridiem: function(e, t, a) {
                    return e > 11 ? a ? "d'o" : "D'O" : a ? "d'a" : "D'A"
                },
                calendar: {
                    sameDay: "[oxhi à] LT",
                    nextDay: "[demà à] LT",
                    nextWeek: "dddd [à] LT",
                    lastDay: "[ieiri à] LT",
                    lastWeek: "[sür el] dddd [lasteu à] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "osprei %s",
                    past: "ja%s",
                    s: A,
                    ss: A,
                    m: A,
                    mm: A,
                    h: A,
                    hh: A,
                    d: A,
                    dd: A,
                    M: A,
                    MM: A,
                    y: A,
                    yy: A
                },
                dayOfMonthOrdinalParse: /\d{1,2}\./,
                ordinal: "%d.",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("tzm-latn", {
                months: "innayr_brˤayrˤ_marˤsˤ_ibrir_mayyw_ywnyw_ywlywz_ɣwšt_šwtanbir_ktˤwbrˤ_nwwanbir_dwjnbir".split("_"),
                monthsShort: "innayr_brˤayrˤ_marˤsˤ_ibrir_mayyw_ywnyw_ywlywz_ɣwšt_šwtanbir_ktˤwbrˤ_nwwanbir_dwjnbir".split("_"),
                weekdays: "asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),
                weekdaysShort: "asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),
                weekdaysMin: "asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[asdkh g] LT",
                    nextDay: "[aska g] LT",
                    nextWeek: "dddd [g] LT",
                    lastDay: "[assant g] LT",
                    lastWeek: "dddd [g] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "dadkh s yan %s",
                    past: "yan %s",
                    s: "imik",
                    ss: "%d imik",
                    m: "minuḍ",
                    mm: "%d minuḍ",
                    h: "saɛa",
                    hh: "%d tassaɛin",
                    d: "ass",
                    dd: "%d ossan",
                    M: "ayowr",
                    MM: "%d iyyirn",
                    y: "asgas",
                    yy: "%d isgasn"
                },
                week: {
                    dow: 6,
                    doy: 12
                }
            }), e.defineLocale("tzm", {
                months: "ⵉⵏⵏⴰⵢⵔ_ⴱⵕⴰⵢⵕ_ⵎⴰⵕⵚ_ⵉⴱⵔⵉⵔ_ⵎⴰⵢⵢⵓ_ⵢⵓⵏⵢⵓ_ⵢⵓⵍⵢⵓⵣ_ⵖⵓⵛⵜ_ⵛⵓⵜⴰⵏⴱⵉⵔ_ⴽⵟⵓⴱⵕ_ⵏⵓⵡⴰⵏⴱⵉⵔ_ⴷⵓⵊⵏⴱⵉⵔ".split("_"),
                monthsShort: "ⵉⵏⵏⴰⵢⵔ_ⴱⵕⴰⵢⵕ_ⵎⴰⵕⵚ_ⵉⴱⵔⵉⵔ_ⵎⴰⵢⵢⵓ_ⵢⵓⵏⵢⵓ_ⵢⵓⵍⵢⵓⵣ_ⵖⵓⵛⵜ_ⵛⵓⵜⴰⵏⴱⵉⵔ_ⴽⵟⵓⴱⵕ_ⵏⵓⵡⴰⵏⴱⵉⵔ_ⴷⵓⵊⵏⴱⵉⵔ".split("_"),
                weekdays: "ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),
                weekdaysShort: "ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),
                weekdaysMin: "ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[ⴰⵙⴷⵅ ⴴ] LT",
                    nextDay: "[ⴰⵙⴽⴰ ⴴ] LT",
                    nextWeek: "dddd [ⴴ] LT",
                    lastDay: "[ⴰⵚⴰⵏⵜ ⴴ] LT",
                    lastWeek: "dddd [ⴴ] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "ⴷⴰⴷⵅ ⵙ ⵢⴰⵏ %s",
                    past: "ⵢⴰⵏ %s",
                    s: "ⵉⵎⵉⴽ",
                    ss: "%d ⵉⵎⵉⴽ",
                    m: "ⵎⵉⵏⵓⴺ",
                    mm: "%d ⵎⵉⵏⵓⴺ",
                    h: "ⵙⴰⵄⴰ",
                    hh: "%d ⵜⴰⵙⵙⴰⵄⵉⵏ",
                    d: "ⴰⵙⵙ",
                    dd: "%d oⵙⵙⴰⵏ",
                    M: "ⴰⵢoⵓⵔ",
                    MM: "%d ⵉⵢⵢⵉⵔⵏ",
                    y: "ⴰⵙⴳⴰⵙ",
                    yy: "%d ⵉⵙⴳⴰⵙⵏ"
                },
                week: {
                    dow: 6,
                    doy: 12
                }
            }), e.defineLocale("ug-cn", {
                months: "يانۋار_فېۋرال_مارت_ئاپرېل_ماي_ئىيۇن_ئىيۇل_ئاۋغۇست_سېنتەبىر_ئۆكتەبىر_نويابىر_دېكابىر".split("_"),
                monthsShort: "يانۋار_فېۋرال_مارت_ئاپرېل_ماي_ئىيۇن_ئىيۇل_ئاۋغۇست_سېنتەبىر_ئۆكتەبىر_نويابىر_دېكابىر".split("_"),
                weekdays: "يەكشەنبە_دۈشەنبە_سەيشەنبە_چارشەنبە_پەيشەنبە_جۈمە_شەنبە".split("_"),
                weekdaysShort: "يە_دۈ_سە_چا_پە_جۈ_شە".split("_"),
                weekdaysMin: "يە_دۈ_سە_چا_پە_جۈ_شە".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY-MM-DD",
                    LL: "YYYY-يىلىM-ئاينىڭD-كۈنى",
                    LLL: "YYYY-يىلىM-ئاينىڭD-كۈنى، HH:mm",
                    LLLL: "dddd، YYYY-يىلىM-ئاينىڭD-كۈنى، HH:mm"
                },
                meridiemParse: /يېرىم كېچە|سەھەر|چۈشتىن بۇرۇن|چۈش|چۈشتىن كېيىن|كەچ/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "يېرىم كېچە" === t || "سەھەر" === t || "چۈشتىن بۇرۇن" === t ? e : "چۈشتىن كېيىن" === t || "كەچ" === t ? e + 12 : e >= 11 ? e : e + 12
                },
                meridiem: function(e, t, a) {
                    var n = 100 * e + t;
                    return n < 600 ? "يېرىم كېچە" : n < 900 ? "سەھەر" : n < 1130 ? "چۈشتىن بۇرۇن" : n < 1230 ? "چۈش" : n < 1800 ? "چۈشتىن كېيىن" : "كەچ"
                },
                calendar: {
                    sameDay: "[بۈگۈن سائەت] LT",
                    nextDay: "[ئەتە سائەت] LT",
                    nextWeek: "[كېلەركى] dddd [سائەت] LT",
                    lastDay: "[تۆنۈگۈن] LT",
                    lastWeek: "[ئالدىنقى] dddd [سائەت] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s كېيىن",
                    past: "%s بۇرۇن",
                    s: "نەچچە سېكونت",
                    ss: "%d سېكونت",
                    m: "بىر مىنۇت",
                    mm: "%d مىنۇت",
                    h: "بىر سائەت",
                    hh: "%d سائەت",
                    d: "بىر كۈن",
                    dd: "%d كۈن",
                    M: "بىر ئاي",
                    MM: "%d ئاي",
                    y: "بىر يىل",
                    yy: "%d يىل"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(-كۈنى|-ئاي|-ھەپتە)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "d":
                        case "D":
                        case "DDD":
                            return e + "-كۈنى";
                        case "w":
                        case "W":
                            return e + "-ھەپتە";
                        default:
                            return e
                    }
                },
                preparse: function(e) {
                    return e.replace(/،/g, ",")
                },
                postformat: function(e) {
                    return e.replace(/,/g, "،")
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("uk", {
                months: {
                    format: "січня_лютого_березня_квітня_травня_червня_липня_серпня_вересня_жовтня_листопада_грудня".split("_"),
                    standalone: "січень_лютий_березень_квітень_травень_червень_липень_серпень_вересень_жовтень_листопад_грудень".split("_")
                },
                monthsShort: "січ_лют_бер_квіт_трав_черв_лип_серп_вер_жовт_лист_груд".split("_"),
                weekdays: function(e, t) {
                    var a = {
                        nominative: "неділя_понеділок_вівторок_середа_четвер_п’ятниця_субота".split("_"),
                        accusative: "неділю_понеділок_вівторок_середу_четвер_п’ятницю_суботу".split("_"),
                        genitive: "неділі_понеділка_вівторка_середи_четверга_п’ятниці_суботи".split("_")
                    };
                    return e ? a[/(\[[ВвУу]\]) ?dddd/.test(t) ? "accusative" : /\[?(?:минулої|наступної)? ?\] ?dddd/.test(t) ? "genitive" : "nominative"][e.day()] : a.nominative
                },
                weekdaysShort: "нд_пн_вт_ср_чт_пт_сб".split("_"),
                weekdaysMin: "нд_пн_вт_ср_чт_пт_сб".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD.MM.YYYY",
                    LL: "D MMMM YYYY р.",
                    LLL: "D MMMM YYYY р., HH:mm",
                    LLLL: "dddd, D MMMM YYYY р., HH:mm"
                },
                calendar: {
                    sameDay: F("[Сьогодні "),
                    nextDay: F("[Завтра "),
                    lastDay: F("[Вчора "),
                    nextWeek: F("[У] dddd ["),
                    lastWeek: function() {
                        switch (this.day()) {
                            case 0:
                            case 3:
                            case 5:
                            case 6:
                                return F("[Минулої] dddd [").call(this);
                            case 1:
                            case 2:
                            case 4:
                                return F("[Минулого] dddd [").call(this)
                        }
                    },
                    sameElse: "L"
                },
                relativeTime: {
                    future: "за %s",
                    past: "%s тому",
                    s: "декілька секунд",
                    ss: z,
                    m: z,
                    mm: z,
                    h: "годину",
                    hh: z,
                    d: "день",
                    dd: z,
                    M: "місяць",
                    MM: z,
                    y: "рік",
                    yy: z
                },
                meridiemParse: /ночі|ранку|дня|вечора/,
                isPM: function(e) {
                    return /^(дня|вечора)$/.test(e)
                },
                meridiem: function(e, t, a) {
                    return e < 4 ? "ночі" : e < 12 ? "ранку" : e < 17 ? "дня" : "вечора"
                },
                dayOfMonthOrdinalParse: /\d{1,2}-(й|го)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "M":
                        case "d":
                        case "DDD":
                        case "w":
                        case "W":
                            return e + "-й";
                        case "D":
                            return e + "-го";
                        default:
                            return e
                    }
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            });
            var ht = ["جنوری", "فروری", "مارچ", "اپریل", "مئی", "جون", "جولائی", "اگست", "ستمبر", "اکتوبر", "نومبر", "دسمبر"],
                pt = ["اتوار", "پیر", "منگل", "بدھ", "جمعرات", "جمعہ", "ہفتہ"];
            return e.defineLocale("ur", {
                months: ht,
                monthsShort: ht,
                weekdays: pt,
                weekdaysShort: pt,
                weekdaysMin: pt,
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd، D MMMM YYYY HH:mm"
                },
                meridiemParse: /صبح|شام/,
                isPM: function(e) {
                    return "شام" === e
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? "صبح" : "شام"
                },
                calendar: {
                    sameDay: "[آج بوقت] LT",
                    nextDay: "[کل بوقت] LT",
                    nextWeek: "dddd [بوقت] LT",
                    lastDay: "[گذشتہ روز بوقت] LT",
                    lastWeek: "[گذشتہ] dddd [بوقت] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s بعد",
                    past: "%s قبل",
                    s: "چند سیکنڈ",
                    ss: "%d سیکنڈ",
                    m: "ایک منٹ",
                    mm: "%d منٹ",
                    h: "ایک گھنٹہ",
                    hh: "%d گھنٹے",
                    d: "ایک دن",
                    dd: "%d دن",
                    M: "ایک ماہ",
                    MM: "%d ماہ",
                    y: "ایک سال",
                    yy: "%d سال"
                },
                preparse: function(e) {
                    return e.replace(/،/g, ",")
                },
                postformat: function(e) {
                    return e.replace(/,/g, "،")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("uz-latn", {
                months: "Yanvar_Fevral_Mart_Aprel_May_Iyun_Iyul_Avgust_Sentabr_Oktabr_Noyabr_Dekabr".split("_"),
                monthsShort: "Yan_Fev_Mar_Apr_May_Iyun_Iyul_Avg_Sen_Okt_Noy_Dek".split("_"),
                weekdays: "Yakshanba_Dushanba_Seshanba_Chorshanba_Payshanba_Juma_Shanba".split("_"),
                weekdaysShort: "Yak_Dush_Sesh_Chor_Pay_Jum_Shan".split("_"),
                weekdaysMin: "Ya_Du_Se_Cho_Pa_Ju_Sha".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "D MMMM YYYY, dddd HH:mm"
                },
                calendar: {
                    sameDay: "[Bugun soat] LT [da]",
                    nextDay: "[Ertaga] LT [da]",
                    nextWeek: "dddd [kuni soat] LT [da]",
                    lastDay: "[Kecha soat] LT [da]",
                    lastWeek: "[O'tgan] dddd [kuni soat] LT [da]",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "Yaqin %s ichida",
                    past: "Bir necha %s oldin",
                    s: "soniya",
                    ss: "%d soniya",
                    m: "bir daqiqa",
                    mm: "%d daqiqa",
                    h: "bir soat",
                    hh: "%d soat",
                    d: "bir kun",
                    dd: "%d kun",
                    M: "bir oy",
                    MM: "%d oy",
                    y: "bir yil",
                    yy: "%d yil"
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("uz", {
                months: "январ_феврал_март_апрел_май_июн_июл_август_сентябр_октябр_ноябр_декабр".split("_"),
                monthsShort: "янв_фев_мар_апр_май_июн_июл_авг_сен_окт_ноя_дек".split("_"),
                weekdays: "Якшанба_Душанба_Сешанба_Чоршанба_Пайшанба_Жума_Шанба".split("_"),
                weekdaysShort: "Якш_Душ_Сеш_Чор_Пай_Жум_Шан".split("_"),
                weekdaysMin: "Як_Ду_Се_Чо_Па_Жу_Ша".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "D MMMM YYYY, dddd HH:mm"
                },
                calendar: {
                    sameDay: "[Бугун соат] LT [да]",
                    nextDay: "[Эртага] LT [да]",
                    nextWeek: "dddd [куни соат] LT [да]",
                    lastDay: "[Кеча соат] LT [да]",
                    lastWeek: "[Утган] dddd [куни соат] LT [да]",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "Якин %s ичида",
                    past: "Бир неча %s олдин",
                    s: "фурсат",
                    ss: "%d фурсат",
                    m: "бир дакика",
                    mm: "%d дакика",
                    h: "бир соат",
                    hh: "%d соат",
                    d: "бир кун",
                    dd: "%d кун",
                    M: "бир ой",
                    MM: "%d ой",
                    y: "бир йил",
                    yy: "%d йил"
                },
                week: {
                    dow: 1,
                    doy: 7
                }
            }), e.defineLocale("vi", {
                months: "tháng 1_tháng 2_tháng 3_tháng 4_tháng 5_tháng 6_tháng 7_tháng 8_tháng 9_tháng 10_tháng 11_tháng 12".split("_"),
                monthsShort: "Th01_Th02_Th03_Th04_Th05_Th06_Th07_Th08_Th09_Th10_Th11_Th12".split("_"),
                monthsParseExact: !0,
                weekdays: "chủ nhật_thứ hai_thứ ba_thứ tư_thứ năm_thứ sáu_thứ bảy".split("_"),
                weekdaysShort: "CN_T2_T3_T4_T5_T6_T7".split("_"),
                weekdaysMin: "CN_T2_T3_T4_T5_T6_T7".split("_"),
                weekdaysParseExact: !0,
                meridiemParse: /sa|ch/i,
                isPM: function(e) {
                    return /^ch$/i.test(e)
                },
                meridiem: function(e, t, a) {
                    return e < 12 ? a ? "sa" : "SA" : a ? "ch" : "CH"
                },
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM [năm] YYYY",
                    LLL: "D MMMM [năm] YYYY HH:mm",
                    LLLL: "dddd, D MMMM [năm] YYYY HH:mm",
                    l: "DD/M/YYYY",
                    ll: "D MMM YYYY",
                    lll: "D MMM YYYY HH:mm",
                    llll: "ddd, D MMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[Hôm nay lúc] LT",
                    nextDay: "[Ngày mai lúc] LT",
                    nextWeek: "dddd [tuần tới lúc] LT",
                    lastDay: "[Hôm qua lúc] LT",
                    lastWeek: "dddd [tuần rồi lúc] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "%s tới",
                    past: "%s trước",
                    s: "vài giây",
                    ss: "%d giây",
                    m: "một phút",
                    mm: "%d phút",
                    h: "một giờ",
                    hh: "%d giờ",
                    d: "một ngày",
                    dd: "%d ngày",
                    M: "một tháng",
                    MM: "%d tháng",
                    y: "một năm",
                    yy: "%d năm"
                },
                dayOfMonthOrdinalParse: /\d{1,2}/,
                ordinal: function(e) {
                    return e
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("x-pseudo", {
                months: "J~áñúá~rý_F~ébrú~árý_~Márc~h_Áp~ríl_~Máý_~Júñé~_Júl~ý_Áú~gúst~_Sép~témb~ér_Ó~ctób~ér_Ñ~óvém~bér_~Décé~mbér".split("_"),
                monthsShort: "J~áñ_~Féb_~Már_~Ápr_~Máý_~Júñ_~Júl_~Áúg_~Sép_~Óct_~Ñóv_~Déc".split("_"),
                monthsParseExact: !0,
                weekdays: "S~úñdá~ý_Mó~ñdáý~_Túé~sdáý~_Wéd~ñésd~áý_T~húrs~dáý_~Fríd~áý_S~átúr~dáý".split("_"),
                weekdaysShort: "S~úñ_~Móñ_~Túé_~Wéd_~Thú_~Frí_~Sát".split("_"),
                weekdaysMin: "S~ú_Mó~_Tú_~Wé_T~h_Fr~_Sá".split("_"),
                weekdaysParseExact: !0,
                longDateFormat: {
                    LT: "HH:mm",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY HH:mm",
                    LLLL: "dddd, D MMMM YYYY HH:mm"
                },
                calendar: {
                    sameDay: "[T~ódá~ý át] LT",
                    nextDay: "[T~ómó~rró~w át] LT",
                    nextWeek: "dddd [át] LT",
                    lastDay: "[Ý~ést~érdá~ý át] LT",
                    lastWeek: "[L~ást] dddd [át] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "í~ñ %s",
                    past: "%s á~gó",
                    s: "á ~féw ~sécó~ñds",
                    ss: "%d s~écóñ~ds",
                    m: "á ~míñ~úté",
                    mm: "%d m~íñú~tés",
                    h: "á~ñ hó~úr",
                    hh: "%d h~óúrs",
                    d: "á ~dáý",
                    dd: "%d d~áýs",
                    M: "á ~móñ~th",
                    MM: "%d m~óñt~hs",
                    y: "á ~ýéár",
                    yy: "%d ý~éárs"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
                ordinal: function(e) {
                    var t = e % 10;
                    return e + (1 == ~~(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th")
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("yo", {
                months: "Sẹ́rẹ́_Èrèlè_Ẹrẹ̀nà_Ìgbé_Èbibi_Òkùdu_Agẹmo_Ògún_Owewe_Ọ̀wàrà_Bélú_Ọ̀pẹ̀̀".split("_"),
                monthsShort: "Sẹ́r_Èrl_Ẹrn_Ìgb_Èbi_Òkù_Agẹ_Ògú_Owe_Ọ̀wà_Bél_Ọ̀pẹ̀̀".split("_"),
                weekdays: "Àìkú_Ajé_Ìsẹ́gun_Ọjọ́rú_Ọjọ́bọ_Ẹtì_Àbámẹ́ta".split("_"),
                weekdaysShort: "Àìk_Ajé_Ìsẹ́_Ọjr_Ọjb_Ẹtì_Àbá".split("_"),
                weekdaysMin: "Àì_Aj_Ìs_Ọr_Ọb_Ẹt_Àb".split("_"),
                longDateFormat: {
                    LT: "h:mm A",
                    LTS: "h:mm:ss A",
                    L: "DD/MM/YYYY",
                    LL: "D MMMM YYYY",
                    LLL: "D MMMM YYYY h:mm A",
                    LLLL: "dddd, D MMMM YYYY h:mm A"
                },
                calendar: {
                    sameDay: "[Ònì ni] LT",
                    nextDay: "[Ọ̀la ni] LT",
                    nextWeek: "dddd [Ọsẹ̀ tón'bọ] [ni] LT",
                    lastDay: "[Àna ni] LT",
                    lastWeek: "dddd [Ọsẹ̀ tólọ́] [ni] LT",
                    sameElse: "L"
                },
                relativeTime: {
                    future: "ní %s",
                    past: "%s kọjá",
                    s: "ìsẹjú aayá die",
                    ss: "aayá %d",
                    m: "ìsẹjú kan",
                    mm: "ìsẹjú %d",
                    h: "wákati kan",
                    hh: "wákati %d",
                    d: "ọjọ́ kan",
                    dd: "ọjọ́ %d",
                    M: "osù kan",
                    MM: "osù %d",
                    y: "ọdún kan",
                    yy: "ọdún %d"
                },
                dayOfMonthOrdinalParse: /ọjọ́\s\d{1,2}/,
                ordinal: "ọjọ́ %d",
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("zh-cn", {
                months: "一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月".split("_"),
                monthsShort: "1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),
                weekdays: "星期日_星期一_星期二_星期三_星期四_星期五_星期六".split("_"),
                weekdaysShort: "周日_周一_周二_周三_周四_周五_周六".split("_"),
                weekdaysMin: "日_一_二_三_四_五_六".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY/MM/DD",
                    LL: "YYYY年M月D日",
                    LLL: "YYYY年M月D日Ah点mm分",
                    LLLL: "YYYY年M月D日ddddAh点mm分",
                    l: "YYYY/M/D",
                    ll: "YYYY年M月D日",
                    lll: "YYYY年M月D日 HH:mm",
                    llll: "YYYY年M月D日dddd HH:mm"
                },
                meridiemParse: /凌晨|早上|上午|中午|下午|晚上/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "凌晨" === t || "早上" === t || "上午" === t ? e : "下午" === t || "晚上" === t ? e + 12 : e >= 11 ? e : e + 12
                },
                meridiem: function(e, t, a) {
                    var n = 100 * e + t;
                    return n < 600 ? "凌晨" : n < 900 ? "早上" : n < 1130 ? "上午" : n < 1230 ? "中午" : n < 1800 ? "下午" : "晚上"
                },
                calendar: {
                    sameDay: "[今天]LT",
                    nextDay: "[明天]LT",
                    nextWeek: "[下]ddddLT",
                    lastDay: "[昨天]LT",
                    lastWeek: "[上]ddddLT",
                    sameElse: "L"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(日|月|周)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "d":
                        case "D":
                        case "DDD":
                            return e + "日";
                        case "M":
                            return e + "月";
                        case "w":
                        case "W":
                            return e + "周";
                        default:
                            return e
                    }
                },
                relativeTime: {
                    future: "%s内",
                    past: "%s前",
                    s: "几秒",
                    ss: "%d 秒",
                    m: "1 分钟",
                    mm: "%d 分钟",
                    h: "1 小时",
                    hh: "%d 小时",
                    d: "1 天",
                    dd: "%d 天",
                    M: "1 个月",
                    MM: "%d 个月",
                    y: "1 年",
                    yy: "%d 年"
                },
                week: {
                    dow: 1,
                    doy: 4
                }
            }), e.defineLocale("zh-hk", {
                months: "一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月".split("_"),
                monthsShort: "1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),
                weekdays: "星期日_星期一_星期二_星期三_星期四_星期五_星期六".split("_"),
                weekdaysShort: "週日_週一_週二_週三_週四_週五_週六".split("_"),
                weekdaysMin: "日_一_二_三_四_五_六".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY/MM/DD",
                    LL: "YYYY年M月D日",
                    LLL: "YYYY年M月D日 HH:mm",
                    LLLL: "YYYY年M月D日dddd HH:mm",
                    l: "YYYY/M/D",
                    ll: "YYYY年M月D日",
                    lll: "YYYY年M月D日 HH:mm",
                    llll: "YYYY年M月D日dddd HH:mm"
                },
                meridiemParse: /凌晨|早上|上午|中午|下午|晚上/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "凌晨" === t || "早上" === t || "上午" === t ? e : "中午" === t ? e >= 11 ? e : e + 12 : "下午" === t || "晚上" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    var n = 100 * e + t;
                    return n < 600 ? "凌晨" : n < 900 ? "早上" : n < 1130 ? "上午" : n < 1230 ? "中午" : n < 1800 ? "下午" : "晚上"
                },
                calendar: {
                    sameDay: "[今天]LT",
                    nextDay: "[明天]LT",
                    nextWeek: "[下]ddddLT",
                    lastDay: "[昨天]LT",
                    lastWeek: "[上]ddddLT",
                    sameElse: "L"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(日|月|週)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "d":
                        case "D":
                        case "DDD":
                            return e + "日";
                        case "M":
                            return e + "月";
                        case "w":
                        case "W":
                            return e + "週";
                        default:
                            return e
                    }
                },
                relativeTime: {
                    future: "%s內",
                    past: "%s前",
                    s: "幾秒",
                    ss: "%d 秒",
                    m: "1 分鐘",
                    mm: "%d 分鐘",
                    h: "1 小時",
                    hh: "%d 小時",
                    d: "1 天",
                    dd: "%d 天",
                    M: "1 個月",
                    MM: "%d 個月",
                    y: "1 年",
                    yy: "%d 年"
                }
            }), e.defineLocale("zh-tw", {
                months: "一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月".split("_"),
                monthsShort: "1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),
                weekdays: "星期日_星期一_星期二_星期三_星期四_星期五_星期六".split("_"),
                weekdaysShort: "週日_週一_週二_週三_週四_週五_週六".split("_"),
                weekdaysMin: "日_一_二_三_四_五_六".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    LTS: "HH:mm:ss",
                    L: "YYYY/MM/DD",
                    LL: "YYYY年M月D日",
                    LLL: "YYYY年M月D日 HH:mm",
                    LLLL: "YYYY年M月D日dddd HH:mm",
                    l: "YYYY/M/D",
                    ll: "YYYY年M月D日",
                    lll: "YYYY年M月D日 HH:mm",
                    llll: "YYYY年M月D日dddd HH:mm"
                },
                meridiemParse: /凌晨|早上|上午|中午|下午|晚上/,
                meridiemHour: function(e, t) {
                    return 12 === e && (e = 0), "凌晨" === t || "早上" === t || "上午" === t ? e : "中午" === t ? e >= 11 ? e : e + 12 : "下午" === t || "晚上" === t ? e + 12 : void 0
                },
                meridiem: function(e, t, a) {
                    var n = 100 * e + t;
                    return n < 600 ? "凌晨" : n < 900 ? "早上" : n < 1130 ? "上午" : n < 1230 ? "中午" : n < 1800 ? "下午" : "晚上"
                },
                calendar: {
                    sameDay: "[今天] LT",
                    nextDay: "[明天] LT",
                    nextWeek: "[下]dddd LT",
                    lastDay: "[昨天] LT",
                    lastWeek: "[上]dddd LT",
                    sameElse: "L"
                },
                dayOfMonthOrdinalParse: /\d{1,2}(日|月|週)/,
                ordinal: function(e, t) {
                    switch (t) {
                        case "d":
                        case "D":
                        case "DDD":
                            return e + "日";
                        case "M":
                            return e + "月";
                        case "w":
                        case "W":
                            return e + "週";
                        default:
                            return e
                    }
                },
                relativeTime: {
                    future: "%s內",
                    past: "%s前",
                    s: "幾秒",
                    ss: "%d 秒",
                    m: "1 分鐘",
                    mm: "%d 分鐘",
                    h: "1 小時",
                    hh: "%d 小時",
                    d: "1 天",
                    dd: "%d 天",
                    M: "1 個月",
                    MM: "%d 個月",
                    y: "1 年",
                    yy: "%d 年"
                }
            }), e.locale("en"), e
        })
    }, {
        "../moment": 5
    }],
    5: [function(e, t, a) {
        (function(e) {
            (function(e, t, a, n, s) {
                ! function(a, s) {
                    "object" == typeof t && void 0 !== e ? e.exports = s() : "function" == typeof n && n.amd ? n(s) : a.moment = s()
                }(this, function() {
                    "use strict";

                    function t() {
                        return Xe.apply(null, arguments)
                    }

                    function n(e) {
                        return e instanceof Array || "[object Array]" === Object.prototype.toString.call(e)
                    }

                    function s(e) {
                        return null != e && "[object Object]" === Object.prototype.toString.call(e)
                    }

                    function i(e) {
                        return void 0 === e
                    }

                    function r(e) {
                        return "number" == typeof e || "[object Number]" === Object.prototype.toString.call(e)
                    }

                    function o(e) {
                        return e instanceof Date || "[object Date]" === Object.prototype.toString.call(e)
                    }

                    function d(e, t) {
                        var a, n = [];
                        for (a = 0; a < e.length; ++a) n.push(t(e[a], a));
                        return n
                    }

                    function l(e, t) {
                        return Object.prototype.hasOwnProperty.call(e, t)
                    }

                    function u(e, t) {
                        for (var a in t) l(t, a) && (e[a] = t[a]);
                        return l(t, "toString") && (e.toString = t.toString), l(t, "valueOf") && (e.valueOf = t.valueOf), e
                    }

                    function _(e, t, a, n) {
                        return ge(e, t, a, n, !0).utc()
                    }

                    function m(e) {
                        return null == e._pf && (e._pf = {
                            empty: !1,
                            unusedTokens: [],
                            unusedInput: [],
                            overflow: -2,
                            charsLeftOver: 0,
                            nullInput: !1,
                            invalidMonth: null,
                            invalidFormat: !1,
                            userInvalidated: !1,
                            iso: !1,
                            parsedDateParts: [],
                            meridiem: null,
                            rfc2822: !1,
                            weekdayMismatch: !1
                        }), e._pf
                    }

                    function c(e) {
                        if (null == e._isValid) {
                            var t = m(e),
                                a = et.call(t.parsedDateParts, function(e) {
                                    return null != e
                                }),
                                n = !isNaN(e._d.getTime()) && t.overflow < 0 && !t.empty && !t.invalidMonth && !t.invalidWeekday && !t.weekdayMismatch && !t.nullInput && !t.invalidFormat && !t.userInvalidated && (!t.meridiem || t.meridiem && a);
                            if (e._strict && (n = n && 0 === t.charsLeftOver && 0 === t.unusedTokens.length && void 0 === t.bigHour), null != Object.isFrozen && Object.isFrozen(e)) return n;
                            e._isValid = n
                        }
                        return e._isValid
                    }

                    function h(e) {
                        var t = _(NaN);
                        return null != e ? u(m(t), e) : m(t).userInvalidated = !0, t
                    }

                    function p(e, t) {
                        var a, n, s;
                        if (i(t._isAMomentObject) || (e._isAMomentObject = t._isAMomentObject), i(t._i) || (e._i = t._i), i(t._f) || (e._f = t._f), i(t._l) || (e._l = t._l), i(t._strict) || (e._strict = t._strict), i(t._tzm) || (e._tzm = t._tzm), i(t._isUTC) || (e._isUTC = t._isUTC), i(t._offset) || (e._offset = t._offset), i(t._pf) || (e._pf = m(t)), i(t._locale) || (e._locale = t._locale), tt.length > 0)
                            for (a = 0; a < tt.length; a++) i(s = t[n = tt[a]]) || (e[n] = s);
                        return e
                    }

                    function f(e) {
                        p(this, e), this._d = new Date(null != e._d ? e._d.getTime() : NaN), this.isValid() || (this._d = new Date(NaN)), !1 === at && (at = !0, t.updateOffset(this), at = !1)
                    }

                    function M(e) {
                        return e instanceof f || null != e && null != e._isAMomentObject
                    }

                    function y(e) {
                        return e < 0 ? Math.ceil(e) || 0 : Math.floor(e)
                    }

                    function g(e) {
                        var t = +e,
                            a = 0;
                        return 0 !== t && isFinite(t) && (a = y(t)), a
                    }

                    function L(e, t, a) {
                        var n, s = Math.min(e.length, t.length),
                            i = Math.abs(e.length - t.length),
                            r = 0;
                        for (n = 0; n < s; n++)(a && e[n] !== t[n] || !a && g(e[n]) !== g(t[n])) && r++;
                        return r + i
                    }

                    function Y(e) {
                        !1 === t.suppressDeprecationWarnings && "undefined" != typeof console && console.warn && console.warn("Deprecation warning: " + e)
                    }

                    function v(e, a) {
                        var n = !0;
                        return u(function() {
                            if (null != t.deprecationHandler && t.deprecationHandler(null, e), n) {
                                for (var s, i = [], r = 0; r < arguments.length; r++) {
                                    if (s = "", "object" == typeof arguments[r]) {
                                        s += "\n[" + r + "] ";
                                        for (var o in arguments[0]) s += o + ": " + arguments[0][o] + ", ";
                                        s = s.slice(0, -2)
                                    } else s = arguments[r];
                                    i.push(s)
                                }
                                Y(e + "\nArguments: " + Array.prototype.slice.call(i).join("") + "\n" + (new Error).stack), n = !1
                            }
                            return a.apply(this, arguments)
                        }, a)
                    }

                    function w(e, a) {
                        null != t.deprecationHandler && t.deprecationHandler(e, a), nt[e] || (Y(a), nt[e] = !0)
                    }

                    function k(e) {
                        return e instanceof Function || "[object Function]" === Object.prototype.toString.call(e)
                    }

                    function D(e, t) {
                        var a, n = u({}, e);
                        for (a in t) l(t, a) && (s(e[a]) && s(t[a]) ? (n[a] = {}, u(n[a], e[a]), u(n[a], t[a])) : null != t[a] ? n[a] = t[a] : delete n[a]);
                        for (a in e) l(e, a) && !l(t, a) && s(e[a]) && (n[a] = u({}, n[a]));
                        return n
                    }

                    function b(e) {
                        null != e && this.set(e)
                    }

                    function T(e, t) {
                        var a = e.toLowerCase();
                        it[a] = it[a + "s"] = it[t] = e
                    }

                    function S(e) {
                        return "string" == typeof e ? it[e] || it[e.toLowerCase()] : void 0
                    }

                    function x(e) {
                        var t, a, n = {};
                        for (a in e) l(e, a) && (t = S(a)) && (n[t] = e[a]);
                        return n
                    }

                    function H(e, t) {
                        rt[e] = t
                    }

                    function j(e, t, a) {
                        var n = "" + Math.abs(e),
                            s = t - n.length;
                        return (e >= 0 ? a ? "+" : "" : "-") + Math.pow(10, Math.max(0, s)).toString().substr(1) + n
                    }

                    function C(e, t, a, n) {
                        var s = n;
                        "string" == typeof n && (s = function() {
                            return this[n]()
                        }), e && (ut[e] = s), t && (ut[t[0]] = function() {
                            return j(s.apply(this, arguments), t[1], t[2])
                        }), a && (ut[a] = function() {
                            return this.localeData().ordinal(s.apply(this, arguments), e)
                        })
                    }

                    function P(e) {
                        return e.match(/\[[\s\S]/) ? e.replace(/^\[|\]$/g, "") : e.replace(/\\/g, "")
                    }

                    function O(e, t) {
                        return e.isValid() ? (t = E(t, e.localeData()), lt[t] = lt[t] || function(e) {
                            var t, a, n = e.match(ot);
                            for (t = 0, a = n.length; t < a; t++) ut[n[t]] ? n[t] = ut[n[t]] : n[t] = P(n[t]);
                            return function(t) {
                                var s, i = "";
                                for (s = 0; s < a; s++) i += k(n[s]) ? n[s].call(t, e) : n[s];
                                return i
                            }
                        }(t), lt[t](e)) : e.localeData().invalidDate()
                    }

                    function E(e, t) {
                        function a(e) {
                            return t.longDateFormat(e) || e
                        }
                        var n = 5;
                        for (dt.lastIndex = 0; n >= 0 && dt.test(e);) e = e.replace(dt, a), dt.lastIndex = 0, n -= 1;
                        return e
                    }

                    function W(e, t, a) {
                        Tt[e] = k(t) ? t : function(e, n) {
                            return e && a ? a : t
                        }
                    }

                    function A(e, t) {
                        return l(Tt, e) ? Tt[e](t._strict, t._locale) : new RegExp(function(e) {
                            return z(e.replace("\\", "").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function(e, t, a, n, s) {
                                return t || a || n || s
                            }))
                        }(e))
                    }

                    function z(e) {
                        return e.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
                    }

                    function F(e, t) {
                        var a, n = t;
                        for ("string" == typeof e && (e = [e]), r(t) && (n = function(e, a) {
                                a[t] = g(e)
                            }), a = 0; a < e.length; a++) St[e[a]] = n
                    }

                    function I(e, t) {
                        F(e, function(e, a, n, s) {
                            n._w = n._w || {}, t(e, n._w, n, s)
                        })
                    }

                    function $(e, t, a) {
                        null != t && l(St, e) && St[e](t, a._a, a, e)
                    }

                    function N(e) {
                        return R(e) ? 366 : 365
                    }

                    function R(e) {
                        return e % 4 == 0 && e % 100 != 0 || e % 400 == 0
                    }

                    function J(e, a) {
                        return function(n) {
                            return null != n ? (U(this, e, n), t.updateOffset(this, a), this) : B(this, e)
                        }
                    }

                    function B(e, t) {
                        return e.isValid() ? e._d["get" + (e._isUTC ? "UTC" : "") + t]() : NaN
                    }

                    function U(e, t, a) {
                        e.isValid() && !isNaN(a) && ("FullYear" === t && R(e.year()) && 1 === e.month() && 29 === e.date() ? e._d["set" + (e._isUTC ? "UTC" : "") + t](a, e.month(), V(a, e.month())) : e._d["set" + (e._isUTC ? "UTC" : "") + t](a))
                    }

                    function V(e, t) {
                        if (isNaN(e) || isNaN(t)) return NaN;
                        var a = function(e, t) {
                            return (e % t + t) % t
                        }(t, 12);
                        return e += (t - a) / 12, 1 === a ? R(e) ? 29 : 28 : 31 - a % 7 % 2
                    }

                    function q(e, t) {
                        var a;
                        if (!e.isValid()) return e;
                        if ("string" == typeof t)
                            if (/^\d+$/.test(t)) t = g(t);
                            else if (t = e.localeData().monthsParse(t), !r(t)) return e;
                        return a = Math.min(e.date(), V(e.year(), t)), e._d["set" + (e._isUTC ? "UTC" : "") + "Month"](t, a), e
                    }

                    function G(e) {
                        return null != e ? (q(this, e), t.updateOffset(this, !0), this) : B(this, "Month")
                    }

                    function Z() {
                        function e(e, t) {
                            return t.length - e.length
                        }
                        var t, a, n = [],
                            s = [],
                            i = [];
                        for (t = 0; t < 12; t++) a = _([2e3, t]), n.push(this.monthsShort(a, "")), s.push(this.months(a, "")), i.push(this.months(a, "")), i.push(this.monthsShort(a, ""));
                        for (n.sort(e), s.sort(e), i.sort(e), t = 0; t < 12; t++) n[t] = z(n[t]), s[t] = z(s[t]);
                        for (t = 0; t < 24; t++) i[t] = z(i[t]);
                        this._monthsRegex = new RegExp("^(" + i.join("|") + ")", "i"), this._monthsShortRegex = this._monthsRegex, this._monthsStrictRegex = new RegExp("^(" + s.join("|") + ")", "i"), this._monthsShortStrictRegex = new RegExp("^(" + n.join("|") + ")", "i")
                    }

                    function K(e) {
                        var t = new Date(Date.UTC.apply(null, arguments));
                        return e < 100 && e >= 0 && isFinite(t.getUTCFullYear()) && t.setUTCFullYear(e), t
                    }

                    function Q(e, t, a) {
                        var n = 7 + t - a;
                        return -((7 + K(e, 0, n).getUTCDay() - t) % 7) + n - 1
                    }

                    function X(e, t, a, n, s) {
                        var i, r, o = 1 + 7 * (t - 1) + (7 + a - n) % 7 + Q(e, n, s);
                        return o <= 0 ? r = N(i = e - 1) + o : o > N(e) ? (i = e + 1, r = o - N(e)) : (i = e, r = o), {
                            year: i,
                            dayOfYear: r
                        }
                    }

                    function ee(e, t, a) {
                        var n, s, i = Q(e.year(), t, a),
                            r = Math.floor((e.dayOfYear() - i - 1) / 7) + 1;
                        return r < 1 ? n = r + te(s = e.year() - 1, t, a) : r > te(e.year(), t, a) ? (n = r - te(e.year(), t, a), s = e.year() + 1) : (s = e.year(), n = r), {
                            week: n,
                            year: s
                        }
                    }

                    function te(e, t, a) {
                        var n = Q(e, t, a),
                            s = Q(e + 1, t, a);
                        return (N(e) - n + s) / 7
                    }

                    function ae() {
                        function e(e, t) {
                            return t.length - e.length
                        }
                        var t, a, n, s, i, r = [],
                            o = [],
                            d = [],
                            l = [];
                        for (t = 0; t < 7; t++) a = _([2e3, 1]).day(t), n = this.weekdaysMin(a, ""), s = this.weekdaysShort(a, ""), i = this.weekdays(a, ""), r.push(n), o.push(s), d.push(i), l.push(n), l.push(s), l.push(i);
                        for (r.sort(e), o.sort(e), d.sort(e), l.sort(e), t = 0; t < 7; t++) o[t] = z(o[t]), d[t] = z(d[t]), l[t] = z(l[t]);
                        this._weekdaysRegex = new RegExp("^(" + l.join("|") + ")", "i"), this._weekdaysShortRegex = this._weekdaysRegex, this._weekdaysMinRegex = this._weekdaysRegex, this._weekdaysStrictRegex = new RegExp("^(" + d.join("|") + ")", "i"), this._weekdaysShortStrictRegex = new RegExp("^(" + o.join("|") + ")", "i"), this._weekdaysMinStrictRegex = new RegExp("^(" + r.join("|") + ")", "i")
                    }

                    function ne() {
                        return this.hours() % 12 || 12
                    }

                    function se(e, t) {
                        C(e, 0, 0, function() {
                            return this.localeData().meridiem(this.hours(), this.minutes(), t)
                        })
                    }

                    function ie(e, t) {
                        return t._meridiemParse
                    }

                    function re(e) {
                        return e ? e.toLowerCase().replace("_", "-") : e
                    }

                    function oe(t) {
                        var n = null;
                        if (!ea[t] && void 0 !== e && e && e.exports) try {
                            n = Kt._abbr;
                            a("./locale/" + t), de(n)
                        } catch (e) {}
                        return ea[t]
                    }

                    function de(e, t) {
                        var a;
                        return e && ((a = i(t) ? ue(e) : le(e, t)) ? Kt = a : "undefined" != typeof console && console.warn && console.warn("Locale " + e + " not found. Did you forget to load it?")), Kt._abbr
                    }

                    function le(e, t) {
                        if (null !== t) {
                            var a, n = Xt;
                            if (t.abbr = e, null != ea[e]) w("defineLocaleOverride", "use moment.updateLocale(localeName, config) to change an existing locale. moment.defineLocale(localeName, config) should only be used for creating a new locale See http://momentjs.com/guides/#/warnings/define-locale/ for more info."), n = ea[e]._config;
                            else if (null != t.parentLocale)
                                if (null != ea[t.parentLocale]) n = ea[t.parentLocale]._config;
                                else {
                                    if (null == (a = oe(t.parentLocale))) return ta[t.parentLocale] || (ta[t.parentLocale] = []), ta[t.parentLocale].push({
                                        name: e,
                                        config: t
                                    }), null;
                                    n = a._config
                                }
                            return ea[e] = new b(D(n, t)), ta[e] && ta[e].forEach(function(e) {
                                le(e.name, e.config)
                            }), de(e), ea[e]
                        }
                        return delete ea[e], null
                    }

                    function ue(e) {
                        var t;
                        if (e && e._locale && e._locale._abbr && (e = e._locale._abbr), !e) return Kt;
                        if (!n(e)) {
                            if (t = oe(e)) return t;
                            e = [e]
                        }
                        return function(e) {
                            for (var t, a, n, s, i = 0; i < e.length;) {
                                for (t = (s = re(e[i]).split("-")).length, a = (a = re(e[i + 1])) ? a.split("-") : null; t > 0;) {
                                    if (n = oe(s.slice(0, t).join("-"))) return n;
                                    if (a && a.length >= t && L(s, a, !0) >= t - 1) break;
                                    t--
                                }
                                i++
                            }
                            return Kt
                        }(e)
                    }

                    function _e(e) {
                        var t, a = e._a;
                        return a && -2 === m(e).overflow && (t = a[Ht] < 0 || a[Ht] > 11 ? Ht : a[jt] < 1 || a[jt] > V(a[xt], a[Ht]) ? jt : a[Ct] < 0 || a[Ct] > 24 || 24 === a[Ct] && (0 !== a[Pt] || 0 !== a[Ot] || 0 !== a[Et]) ? Ct : a[Pt] < 0 || a[Pt] > 59 ? Pt : a[Ot] < 0 || a[Ot] > 59 ? Ot : a[Et] < 0 || a[Et] > 999 ? Et : -1, m(e)._overflowDayOfYear && (t < xt || t > jt) && (t = jt), m(e)._overflowWeeks && -1 === t && (t = Wt), m(e)._overflowWeekday && -1 === t && (t = At), m(e).overflow = t), e
                    }

                    function me(e, t, a) {
                        return null != e ? e : null != t ? t : a
                    }

                    function ce(e) {
                        var a, n, s, i, r, o = [];
                        if (!e._d) {
                            for (s = function(e) {
                                    var a = new Date(t.now());
                                    return e._useUTC ? [a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate()] : [a.getFullYear(), a.getMonth(), a.getDate()]
                                }(e), e._w && null == e._a[jt] && null == e._a[Ht] && function(e) {
                                    var t, a, n, s, i, r, o, d;
                                    if (null != (t = e._w).GG || null != t.W || null != t.E) i = 1, r = 4, a = me(t.GG, e._a[xt], ee(Le(), 1, 4).year), n = me(t.W, 1), ((s = me(t.E, 1)) < 1 || s > 7) && (d = !0);
                                    else {
                                        i = e._locale._week.dow, r = e._locale._week.doy;
                                        var l = ee(Le(), i, r);
                                        a = me(t.gg, e._a[xt], l.year), n = me(t.w, l.week), null != t.d ? ((s = t.d) < 0 || s > 6) && (d = !0) : null != t.e ? (s = t.e + i, (t.e < 0 || t.e > 6) && (d = !0)) : s = i
                                    }
                                    n < 1 || n > te(a, i, r) ? m(e)._overflowWeeks = !0 : null != d ? m(e)._overflowWeekday = !0 : (o = X(a, n, s, i, r), e._a[xt] = o.year, e._dayOfYear = o.dayOfYear)
                                }(e), null != e._dayOfYear && (r = me(e._a[xt], s[xt]), (e._dayOfYear > N(r) || 0 === e._dayOfYear) && (m(e)._overflowDayOfYear = !0), n = K(r, 0, e._dayOfYear), e._a[Ht] = n.getUTCMonth(), e._a[jt] = n.getUTCDate()), a = 0; a < 3 && null == e._a[a]; ++a) e._a[a] = o[a] = s[a];
                            for (; a < 7; a++) e._a[a] = o[a] = null == e._a[a] ? 2 === a ? 1 : 0 : e._a[a];
                            24 === e._a[Ct] && 0 === e._a[Pt] && 0 === e._a[Ot] && 0 === e._a[Et] && (e._nextDay = !0, e._a[Ct] = 0), e._d = (e._useUTC ? K : function(e, t, a, n, s, i, r) {
                                var o = new Date(e, t, a, n, s, i, r);
                                return e < 100 && e >= 0 && isFinite(o.getFullYear()) && o.setFullYear(e), o
                            }).apply(null, o), i = e._useUTC ? e._d.getUTCDay() : e._d.getDay(), null != e._tzm && e._d.setUTCMinutes(e._d.getUTCMinutes() - e._tzm), e._nextDay && (e._a[Ct] = 24), e._w && void 0 !== e._w.d && e._w.d !== i && (m(e).weekdayMismatch = !0)
                        }
                    }

                    function he(e) {
                        var t, a, n, s, i, r, o = e._i,
                            d = aa.exec(o) || na.exec(o);
                        if (d) {
                            for (m(e).iso = !0, t = 0, a = ia.length; t < a; t++)
                                if (ia[t][1].exec(d[1])) {
                                    s = ia[t][0], n = !1 !== ia[t][2];
                                    break
                                }
                            if (null == s) return void(e._isValid = !1);
                            if (d[3]) {
                                for (t = 0, a = ra.length; t < a; t++)
                                    if (ra[t][1].exec(d[3])) {
                                        i = (d[2] || " ") + ra[t][0];
                                        break
                                    }
                                if (null == i) return void(e._isValid = !1)
                            }
                            if (!n && null != i) return void(e._isValid = !1);
                            if (d[4]) {
                                if (!sa.exec(d[4])) return void(e._isValid = !1);
                                r = "Z"
                            }
                            e._f = s + (i || "") + (r || ""), Me(e)
                        } else e._isValid = !1
                    }

                    function pe(e, t, a, n, s, i) {
                        var r = [function(e) {
                            var t = parseInt(e, 10); {
                                if (t <= 49) return 2e3 + t;
                                if (t <= 999) return 1900 + t
                            }
                            return t
                        }(e), Nt.indexOf(t), parseInt(a, 10), parseInt(n, 10), parseInt(s, 10)];
                        return i && r.push(parseInt(i, 10)), r
                    }

                    function fe(e) {
                        var t = da.exec(function(e) {
                            return e.replace(/\([^)]*\)|[\n\t]/g, " ").replace(/(\s\s+)/g, " ").replace(/^\s\s*/, "").replace(/\s\s*$/, "")
                        }(e._i));
                        if (t) {
                            var a = pe(t[4], t[3], t[2], t[5], t[6], t[7]);
                            if (! function(e, t, a) {
                                    if (e && Ut.indexOf(e) !== new Date(t[0], t[1], t[2]).getDay()) return m(a).weekdayMismatch = !0, a._isValid = !1, !1;
                                    return !0
                                }(t[1], a, e)) return;
                            e._a = a, e._tzm = function(e, t, a) {
                                if (e) return la[e];
                                if (t) return 0;
                                var n = parseInt(a, 10),
                                    s = n % 100;
                                return (n - s) / 100 * 60 + s
                            }(t[8], t[9], t[10]), e._d = K.apply(null, e._a), e._d.setUTCMinutes(e._d.getUTCMinutes() - e._tzm), m(e).rfc2822 = !0
                        } else e._isValid = !1
                    }

                    function Me(e) {
                        if (e._f !== t.ISO_8601)
                            if (e._f !== t.RFC_2822) {
                                e._a = [], m(e).empty = !0;
                                var a, n, s, i, r, o = "" + e._i,
                                    d = o.length,
                                    l = 0;
                                for (s = E(e._f, e._locale).match(ot) || [], a = 0; a < s.length; a++) i = s[a], (n = (o.match(A(i, e)) || [])[0]) && ((r = o.substr(0, o.indexOf(n))).length > 0 && m(e).unusedInput.push(r), o = o.slice(o.indexOf(n) + n.length), l += n.length), ut[i] ? (n ? m(e).empty = !1 : m(e).unusedTokens.push(i), $(i, n, e)) : e._strict && !n && m(e).unusedTokens.push(i);
                                m(e).charsLeftOver = d - l, o.length > 0 && m(e).unusedInput.push(o), e._a[Ct] <= 12 && !0 === m(e).bigHour && e._a[Ct] > 0 && (m(e).bigHour = void 0), m(e).parsedDateParts = e._a.slice(0), m(e).meridiem = e._meridiem, e._a[Ct] = function(e, t, a) {
                                    var n;
                                    if (null == a) return t;
                                    return null != e.meridiemHour ? e.meridiemHour(t, a) : null != e.isPM ? ((n = e.isPM(a)) && t < 12 && (t += 12), n || 12 !== t || (t = 0), t) : t
                                }(e._locale, e._a[Ct], e._meridiem), ce(e), _e(e)
                            } else fe(e);
                        else he(e)
                    }

                    function ye(e) {
                        var a = e._i,
                            l = e._f;
                        return e._locale = e._locale || ue(e._l), null === a || void 0 === l && "" === a ? h({
                            nullInput: !0
                        }) : ("string" == typeof a && (e._i = a = e._locale.preparse(a)), M(a) ? new f(_e(a)) : (o(a) ? e._d = a : n(l) ? function(e) {
                            var t, a, n, s, i;
                            if (0 === e._f.length) return m(e).invalidFormat = !0, void(e._d = new Date(NaN));
                            for (s = 0; s < e._f.length; s++) i = 0, t = p({}, e), null != e._useUTC && (t._useUTC = e._useUTC), t._f = e._f[s], Me(t), c(t) && (i += m(t).charsLeftOver, i += 10 * m(t).unusedTokens.length, m(t).score = i, (null == n || i < n) && (n = i, a = t));
                            u(e, a || t)
                        }(e) : l ? Me(e) : function(e) {
                            var a = e._i;
                            i(a) ? e._d = new Date(t.now()) : o(a) ? e._d = new Date(a.valueOf()) : "string" == typeof a ? function(e) {
                                var a = oa.exec(e._i);
                                null === a ? (he(e), !1 === e._isValid && (delete e._isValid, fe(e), !1 === e._isValid && (delete e._isValid, t.createFromInputFallback(e)))) : e._d = new Date(+a[1])
                            }(e) : n(a) ? (e._a = d(a.slice(0), function(e) {
                                return parseInt(e, 10)
                            }), ce(e)) : s(a) ? function(e) {
                                if (!e._d) {
                                    var t = x(e._i);
                                    e._a = d([t.year, t.month, t.day || t.date, t.hour, t.minute, t.second, t.millisecond], function(e) {
                                        return e && parseInt(e, 10)
                                    }), ce(e)
                                }
                            }(e) : r(a) ? e._d = new Date(a) : t.createFromInputFallback(e)
                        }(e), c(e) || (e._d = null), e))
                    }

                    function ge(e, t, a, i, r) {
                        var o = {};
                        return !0 !== a && !1 !== a || (i = a, a = void 0), (s(e) && function(e) {
                                if (Object.getOwnPropertyNames) return 0 === Object.getOwnPropertyNames(e).length;
                                var t;
                                for (t in e)
                                    if (e.hasOwnProperty(t)) return !1;
                                return !0
                            }(e) || n(e) && 0 === e.length) && (e = void 0), o._isAMomentObject = !0, o._useUTC = o._isUTC = r, o._l = a, o._i = e, o._f = t, o._strict = i,
                            function(e) {
                                var t = new f(_e(ye(e)));
                                return t._nextDay && (t.add(1, "d"), t._nextDay = void 0), t
                            }(o)
                    }

                    function Le(e, t, a, n) {
                        return ge(e, t, a, n, !1)
                    }

                    function Ye(e, t) {
                        var a, s;
                        if (1 === t.length && n(t[0]) && (t = t[0]), !t.length) return Le();
                        for (a = t[0], s = 1; s < t.length; ++s) t[s].isValid() && !t[s][e](a) || (a = t[s]);
                        return a
                    }

                    function ve(e) {
                        var t = x(e),
                            a = t.year || 0,
                            n = t.quarter || 0,
                            s = t.month || 0,
                            i = t.week || 0,
                            r = t.day || 0,
                            o = t.hour || 0,
                            d = t.minute || 0,
                            l = t.second || 0,
                            u = t.millisecond || 0;
                        this._isValid = function(e) {
                            for (var t in e)
                                if (-1 === zt.call(ma, t) || null != e[t] && isNaN(e[t])) return !1;
                            for (var a = !1, n = 0; n < ma.length; ++n)
                                if (e[ma[n]]) {
                                    if (a) return !1;
                                    parseFloat(e[ma[n]]) !== g(e[ma[n]]) && (a = !0)
                                }
                            return !0
                        }(t), this._milliseconds = +u + 1e3 * l + 6e4 * d + 1e3 * o * 60 * 60, this._days = +r + 7 * i, this._months = +s + 3 * n + 12 * a, this._data = {}, this._locale = ue(), this._bubble()
                    }

                    function we(e) {
                        return e instanceof ve
                    }

                    function ke(e) {
                        return e < 0 ? -1 * Math.round(-1 * e) : Math.round(e)
                    }

                    function De(e, t) {
                        C(e, 0, 0, function() {
                            var e = this.utcOffset(),
                                a = "+";
                            return e < 0 && (e = -e, a = "-"), a + j(~~(e / 60), 2) + t + j(~~e % 60, 2)
                        })
                    }

                    function be(e, t) {
                        var a = (t || "").match(e);
                        if (null === a) return null;
                        var n = ((a[a.length - 1] || []) + "").match(ca) || ["-", 0, 0],
                            s = 60 * n[1] + g(n[2]);
                        return 0 === s ? 0 : "+" === n[0] ? s : -s
                    }

                    function Te(e, a) {
                        var n, s;
                        return a._isUTC ? (n = a.clone(), s = (M(e) || o(e) ? e.valueOf() : Le(e).valueOf()) - n.valueOf(), n._d.setTime(n._d.valueOf() + s), t.updateOffset(n, !1), n) : Le(e).local()
                    }

                    function Se(e) {
                        return 15 * -Math.round(e._d.getTimezoneOffset() / 15)
                    }

                    function xe() {
                        return !!this.isValid() && (this._isUTC && 0 === this._offset)
                    }

                    function He(e, t) {
                        var a, n, s, i = e,
                            o = null;
                        return we(e) ? i = {
                            ms: e._milliseconds,
                            d: e._days,
                            M: e._months
                        } : r(e) ? (i = {}, t ? i[t] = e : i.milliseconds = e) : (o = ha.exec(e)) ? (a = "-" === o[1] ? -1 : 1, i = {
                            y: 0,
                            d: g(o[jt]) * a,
                            h: g(o[Ct]) * a,
                            m: g(o[Pt]) * a,
                            s: g(o[Ot]) * a,
                            ms: g(ke(1e3 * o[Et])) * a
                        }) : (o = pa.exec(e)) ? (a = "-" === o[1] ? -1 : (o[1], 1), i = {
                            y: je(o[2], a),
                            M: je(o[3], a),
                            w: je(o[4], a),
                            d: je(o[5], a),
                            h: je(o[6], a),
                            m: je(o[7], a),
                            s: je(o[8], a)
                        }) : null == i ? i = {} : "object" == typeof i && ("from" in i || "to" in i) && (s = function(e, t) {
                            var a;
                            if (!e.isValid() || !t.isValid()) return {
                                milliseconds: 0,
                                months: 0
                            };
                            t = Te(t, e), e.isBefore(t) ? a = Ce(e, t) : ((a = Ce(t, e)).milliseconds = -a.milliseconds, a.months = -a.months);
                            return a
                        }(Le(i.from), Le(i.to)), (i = {}).ms = s.milliseconds, i.M = s.months), n = new ve(i), we(e) && l(e, "_locale") && (n._locale = e._locale), n
                    }

                    function je(e, t) {
                        var a = e && parseFloat(e.replace(",", "."));
                        return (isNaN(a) ? 0 : a) * t
                    }

                    function Ce(e, t) {
                        var a = {
                            milliseconds: 0,
                            months: 0
                        };
                        return a.months = t.month() - e.month() + 12 * (t.year() - e.year()), e.clone().add(a.months, "M").isAfter(t) && --a.months, a.milliseconds = +t - +e.clone().add(a.months, "M"), a
                    }

                    function Pe(e, t) {
                        return function(a, n) {
                            var s, i;
                            return null === n || isNaN(+n) || (w(t, "moment()." + t + "(period, number) is deprecated. Please use moment()." + t + "(number, period). See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info."), i = a, a = n, n = i), a = "string" == typeof a ? +a : a, s = He(a, n), Oe(this, s, e), this
                        }
                    }

                    function Oe(e, a, n, s) {
                        var i = a._milliseconds,
                            r = ke(a._days),
                            o = ke(a._months);
                        e.isValid() && (s = null == s || s, o && q(e, B(e, "Month") + o * n), r && U(e, "Date", B(e, "Date") + r * n), i && e._d.setTime(e._d.valueOf() + i * n), s && t.updateOffset(e, r || o))
                    }

                    function Ee(e, t) {
                        var a, n = 12 * (t.year() - e.year()) + (t.month() - e.month()),
                            s = e.clone().add(n, "months");
                        return a = t - s < 0 ? (t - s) / (s - e.clone().add(n - 1, "months")) : (t - s) / (e.clone().add(n + 1, "months") - s), -(n + a) || 0
                    }

                    function We(e) {
                        var t;
                        return void 0 === e ? this._locale._abbr : (null != (t = ue(e)) && (this._locale = t), this)
                    }

                    function Ae() {
                        return this._locale
                    }

                    function ze(e, t) {
                        C(0, [e, e.length], 0, t)
                    }

                    function Fe(e, t, a, n, s) {
                        var i;
                        return null == e ? ee(this, n, s).year : (i = te(e, n, s), t > i && (t = i), function(e, t, a, n, s) {
                            var i = X(e, t, a, n, s),
                                r = K(i.year, 0, i.dayOfYear);
                            return this.year(r.getUTCFullYear()), this.month(r.getUTCMonth()), this.date(r.getUTCDate()), this
                        }.call(this, e, t, a, n, s))
                    }

                    function Ie(e, t) {
                        t[Et] = g(1e3 * ("0." + e))
                    }

                    function $e(e) {
                        return e
                    }

                    function Ne(e, t, a, n) {
                        var s = ue(),
                            i = _().set(n, t);
                        return s[a](i, e)
                    }

                    function Re(e, t, a) {
                        if (r(e) && (t = e, e = void 0), e = e || "", null != t) return Ne(e, t, a, "month");
                        var n, s = [];
                        for (n = 0; n < 12; n++) s[n] = Ne(e, n, a, "month");
                        return s
                    }

                    function Je(e, t, a, n) {
                        "boolean" == typeof e ? (r(t) && (a = t, t = void 0), t = t || "") : (a = t = e, e = !1, r(t) && (a = t, t = void 0), t = t || "");
                        var s = ue(),
                            i = e ? s._week.dow : 0;
                        if (null != a) return Ne(t, (a + i) % 7, n, "day");
                        var o, d = [];
                        for (o = 0; o < 7; o++) d[o] = Ne(t, (o + i) % 7, n, "day");
                        return d
                    }

                    function Be(e, t, a, n) {
                        var s = He(t, a);
                        return e._milliseconds += n * s._milliseconds, e._days += n * s._days, e._months += n * s._months, e._bubble()
                    }

                    function Ue(e) {
                        return e < 0 ? Math.floor(e) : Math.ceil(e)
                    }

                    function Ve(e) {
                        return 4800 * e / 146097
                    }

                    function qe(e) {
                        return 146097 * e / 4800
                    }

                    function Ge(e) {
                        return function() {
                            return this.as(e)
                        }
                    }

                    function Ze(e) {
                        return function() {
                            return this.isValid() ? this._data[e] : NaN
                        }
                    }

                    function Ke(e) {
                        return (e > 0) - (e < 0) || +e
                    }

                    function Qe() {
                        if (!this.isValid()) return this.localeData().invalidDate();
                        var e, t, a = Ja(this._milliseconds) / 1e3,
                            n = Ja(this._days),
                            s = Ja(this._months);
                        t = y((e = y(a / 60)) / 60), a %= 60, e %= 60;
                        var i = y(s / 12),
                            r = s %= 12,
                            o = n,
                            d = t,
                            l = e,
                            u = a ? a.toFixed(3).replace(/\.?0+$/, "") : "",
                            _ = this.asSeconds();
                        if (!_) return "P0D";
                        var m = _ < 0 ? "-" : "",
                            c = Ke(this._months) !== Ke(_) ? "-" : "",
                            h = Ke(this._days) !== Ke(_) ? "-" : "",
                            p = Ke(this._milliseconds) !== Ke(_) ? "-" : "";
                        return m + "P" + (i ? c + i + "Y" : "") + (r ? c + r + "M" : "") + (o ? h + o + "D" : "") + (d || l || u ? "T" : "") + (d ? p + d + "H" : "") + (l ? p + l + "M" : "") + (u ? p + u + "S" : "")
                    }
                    var Xe, et;
                    et = Array.prototype.some ? Array.prototype.some : function(e) {
                        for (var t = Object(this), a = t.length >>> 0, n = 0; n < a; n++)
                            if (n in t && e.call(this, t[n], n, t)) return !0;
                        return !1
                    };
                    var tt = t.momentProperties = [],
                        at = !1,
                        nt = {};
                    t.suppressDeprecationWarnings = !1, t.deprecationHandler = null;
                    var st;
                    st = Object.keys ? Object.keys : function(e) {
                        var t, a = [];
                        for (t in e) l(e, t) && a.push(t);
                        return a
                    };
                    var it = {},
                        rt = {},
                        ot = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,
                        dt = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,
                        lt = {},
                        ut = {},
                        _t = /\d/,
                        mt = /\d\d/,
                        ct = /\d{3}/,
                        ht = /\d{4}/,
                        pt = /[+-]?\d{6}/,
                        ft = /\d\d?/,
                        Mt = /\d\d\d\d?/,
                        yt = /\d\d\d\d\d\d?/,
                        gt = /\d{1,3}/,
                        Lt = /\d{1,4}/,
                        Yt = /[+-]?\d{1,6}/,
                        vt = /\d+/,
                        wt = /[+-]?\d+/,
                        kt = /Z|[+-]\d\d:?\d\d/gi,
                        Dt = /Z|[+-]\d\d(?::?\d\d)?/gi,
                        bt = /[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFF07\uFF10-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i,
                        Tt = {},
                        St = {},
                        xt = 0,
                        Ht = 1,
                        jt = 2,
                        Ct = 3,
                        Pt = 4,
                        Ot = 5,
                        Et = 6,
                        Wt = 7,
                        At = 8;
                    C("Y", 0, 0, function() {
                        var e = this.year();
                        return e <= 9999 ? "" + e : "+" + e
                    }), C(0, ["YY", 2], 0, function() {
                        return this.year() % 100
                    }), C(0, ["YYYY", 4], 0, "year"), C(0, ["YYYYY", 5], 0, "year"), C(0, ["YYYYYY", 6, !0], 0, "year"), T("year", "y"), H("year", 1), W("Y", wt), W("YY", ft, mt), W("YYYY", Lt, ht), W("YYYYY", Yt, pt), W("YYYYYY", Yt, pt), F(["YYYYY", "YYYYYY"], xt), F("YYYY", function(e, a) {
                        a[xt] = 2 === e.length ? t.parseTwoDigitYear(e) : g(e)
                    }), F("YY", function(e, a) {
                        a[xt] = t.parseTwoDigitYear(e)
                    }), F("Y", function(e, t) {
                        t[xt] = parseInt(e, 10)
                    }), t.parseTwoDigitYear = function(e) {
                        return g(e) + (g(e) > 68 ? 1900 : 2e3)
                    };
                    var zt, Ft = J("FullYear", !0);
                    zt = Array.prototype.indexOf ? Array.prototype.indexOf : function(e) {
                        var t;
                        for (t = 0; t < this.length; ++t)
                            if (this[t] === e) return t;
                        return -1
                    }, C("M", ["MM", 2], "Mo", function() {
                        return this.month() + 1
                    }), C("MMM", 0, 0, function(e) {
                        return this.localeData().monthsShort(this, e)
                    }), C("MMMM", 0, 0, function(e) {
                        return this.localeData().months(this, e)
                    }), T("month", "M"), H("month", 8), W("M", ft), W("MM", ft, mt), W("MMM", function(e, t) {
                        return t.monthsShortRegex(e)
                    }), W("MMMM", function(e, t) {
                        return t.monthsRegex(e)
                    }), F(["M", "MM"], function(e, t) {
                        t[Ht] = g(e) - 1
                    }), F(["MMM", "MMMM"], function(e, t, a, n) {
                        var s = a._locale.monthsParse(e, n, a._strict);
                        null != s ? t[Ht] = s : m(a).invalidMonth = e
                    });
                    var It = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/,
                        $t = "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
                        Nt = "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
                        Rt = bt,
                        Jt = bt;
                    C("w", ["ww", 2], "wo", "week"), C("W", ["WW", 2], "Wo", "isoWeek"), T("week", "w"), T("isoWeek", "W"), H("week", 5), H("isoWeek", 5), W("w", ft), W("ww", ft, mt), W("W", ft), W("WW", ft, mt), I(["w", "ww", "W", "WW"], function(e, t, a, n) {
                        t[n.substr(0, 1)] = g(e)
                    });
                    C("d", 0, "do", "day"), C("dd", 0, 0, function(e) {
                        return this.localeData().weekdaysMin(this, e)
                    }), C("ddd", 0, 0, function(e) {
                        return this.localeData().weekdaysShort(this, e)
                    }), C("dddd", 0, 0, function(e) {
                        return this.localeData().weekdays(this, e)
                    }), C("e", 0, 0, "weekday"), C("E", 0, 0, "isoWeekday"), T("day", "d"), T("weekday", "e"), T("isoWeekday", "E"), H("day", 11), H("weekday", 11), H("isoWeekday", 11), W("d", ft), W("e", ft), W("E", ft), W("dd", function(e, t) {
                        return t.weekdaysMinRegex(e)
                    }), W("ddd", function(e, t) {
                        return t.weekdaysShortRegex(e)
                    }), W("dddd", function(e, t) {
                        return t.weekdaysRegex(e)
                    }), I(["dd", "ddd", "dddd"], function(e, t, a, n) {
                        var s = a._locale.weekdaysParse(e, n, a._strict);
                        null != s ? t.d = s : m(a).invalidWeekday = e
                    }), I(["d", "e", "E"], function(e, t, a, n) {
                        t[n] = g(e)
                    });
                    var Bt = "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
                        Ut = "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
                        Vt = "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
                        qt = bt,
                        Gt = bt,
                        Zt = bt;
                    C("H", ["HH", 2], 0, "hour"), C("h", ["hh", 2], 0, ne), C("k", ["kk", 2], 0, function() {
                        return this.hours() || 24
                    }), C("hmm", 0, 0, function() {
                        return "" + ne.apply(this) + j(this.minutes(), 2)
                    }), C("hmmss", 0, 0, function() {
                        return "" + ne.apply(this) + j(this.minutes(), 2) + j(this.seconds(), 2)
                    }), C("Hmm", 0, 0, function() {
                        return "" + this.hours() + j(this.minutes(), 2)
                    }), C("Hmmss", 0, 0, function() {
                        return "" + this.hours() + j(this.minutes(), 2) + j(this.seconds(), 2)
                    }), se("a", !0), se("A", !1), T("hour", "h"), H("hour", 13), W("a", ie), W("A", ie), W("H", ft), W("h", ft), W("k", ft), W("HH", ft, mt), W("hh", ft, mt), W("kk", ft, mt), W("hmm", Mt), W("hmmss", yt), W("Hmm", Mt), W("Hmmss", yt), F(["H", "HH"], Ct), F(["k", "kk"], function(e, t, a) {
                        var n = g(e);
                        t[Ct] = 24 === n ? 0 : n
                    }), F(["a", "A"], function(e, t, a) {
                        a._isPm = a._locale.isPM(e), a._meridiem = e
                    }), F(["h", "hh"], function(e, t, a) {
                        t[Ct] = g(e), m(a).bigHour = !0
                    }), F("hmm", function(e, t, a) {
                        var n = e.length - 2;
                        t[Ct] = g(e.substr(0, n)), t[Pt] = g(e.substr(n)), m(a).bigHour = !0
                    }), F("hmmss", function(e, t, a) {
                        var n = e.length - 4,
                            s = e.length - 2;
                        t[Ct] = g(e.substr(0, n)), t[Pt] = g(e.substr(n, 2)), t[Ot] = g(e.substr(s)), m(a).bigHour = !0
                    }), F("Hmm", function(e, t, a) {
                        var n = e.length - 2;
                        t[Ct] = g(e.substr(0, n)), t[Pt] = g(e.substr(n))
                    }), F("Hmmss", function(e, t, a) {
                        var n = e.length - 4,
                            s = e.length - 2;
                        t[Ct] = g(e.substr(0, n)), t[Pt] = g(e.substr(n, 2)), t[Ot] = g(e.substr(s))
                    });
                    var Kt, Qt = J("Hours", !0),
                        Xt = {
                            calendar: {
                                sameDay: "[Today at] LT",
                                nextDay: "[Tomorrow at] LT",
                                nextWeek: "dddd [at] LT",
                                lastDay: "[Yesterday at] LT",
                                lastWeek: "[Last] dddd [at] LT",
                                sameElse: "L"
                            },
                            longDateFormat: {
                                LTS: "h:mm:ss A",
                                LT: "h:mm A",
                                L: "MM/DD/YYYY",
                                LL: "MMMM D, YYYY",
                                LLL: "MMMM D, YYYY h:mm A",
                                LLLL: "dddd, MMMM D, YYYY h:mm A"
                            },
                            invalidDate: "Invalid date",
                            ordinal: "%d",
                            dayOfMonthOrdinalParse: /\d{1,2}/,
                            relativeTime: {
                                future: "in %s",
                                past: "%s ago",
                                s: "a few seconds",
                                ss: "%d seconds",
                                m: "a minute",
                                mm: "%d minutes",
                                h: "an hour",
                                hh: "%d hours",
                                d: "a day",
                                dd: "%d days",
                                M: "a month",
                                MM: "%d months",
                                y: "a year",
                                yy: "%d years"
                            },
                            months: $t,
                            monthsShort: Nt,
                            week: {
                                dow: 0,
                                doy: 6
                            },
                            weekdays: Bt,
                            weekdaysMin: Vt,
                            weekdaysShort: Ut,
                            meridiemParse: /[ap]\.?m?\.?/i
                        },
                        ea = {},
                        ta = {},
                        aa = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
                        na = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
                        sa = /Z|[+-]\d\d(?::?\d\d)?/,
                        ia = [
                            ["YYYYYY-MM-DD", /[+-]\d{6}-\d\d-\d\d/],
                            ["YYYY-MM-DD", /\d{4}-\d\d-\d\d/],
                            ["GGGG-[W]WW-E", /\d{4}-W\d\d-\d/],
                            ["GGGG-[W]WW", /\d{4}-W\d\d/, !1],
                            ["YYYY-DDD", /\d{4}-\d{3}/],
                            ["YYYY-MM", /\d{4}-\d\d/, !1],
                            ["YYYYYYMMDD", /[+-]\d{10}/],
                            ["YYYYMMDD", /\d{8}/],
                            ["GGGG[W]WWE", /\d{4}W\d{3}/],
                            ["GGGG[W]WW", /\d{4}W\d{2}/, !1],
                            ["YYYYDDD", /\d{7}/]
                        ],
                        ra = [
                            ["HH:mm:ss.SSSS", /\d\d:\d\d:\d\d\.\d+/],
                            ["HH:mm:ss,SSSS", /\d\d:\d\d:\d\d,\d+/],
                            ["HH:mm:ss", /\d\d:\d\d:\d\d/],
                            ["HH:mm", /\d\d:\d\d/],
                            ["HHmmss.SSSS", /\d\d\d\d\d\d\.\d+/],
                            ["HHmmss,SSSS", /\d\d\d\d\d\d,\d+/],
                            ["HHmmss", /\d\d\d\d\d\d/],
                            ["HHmm", /\d\d\d\d/],
                            ["HH", /\d\d/]
                        ],
                        oa = /^\/?Date\((\-?\d+)/i,
                        da = /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/,
                        la = {
                            UT: 0,
                            GMT: 0,
                            EDT: -240,
                            EST: -300,
                            CDT: -300,
                            CST: -360,
                            MDT: -360,
                            MST: -420,
                            PDT: -420,
                            PST: -480
                        };
                    t.createFromInputFallback = v("value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are discouraged and will be removed in an upcoming major release. Please refer to http://momentjs.com/guides/#/warnings/js-date/ for more info.", function(e) {
                        e._d = new Date(e._i + (e._useUTC ? " UTC" : ""))
                    }), t.ISO_8601 = function() {}, t.RFC_2822 = function() {};
                    var ua = v("moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/", function() {
                            var e = Le.apply(null, arguments);
                            return this.isValid() && e.isValid() ? e < this ? this : e : h()
                        }),
                        _a = v("moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/", function() {
                            var e = Le.apply(null, arguments);
                            return this.isValid() && e.isValid() ? e > this ? this : e : h()
                        }),
                        ma = ["year", "quarter", "month", "week", "day", "hour", "minute", "second", "millisecond"];
                    De("Z", ":"), De("ZZ", ""), W("Z", Dt), W("ZZ", Dt), F(["Z", "ZZ"], function(e, t, a) {
                        a._useUTC = !0, a._tzm = be(Dt, e)
                    });
                    var ca = /([\+\-]|\d\d)/gi;
                    t.updateOffset = function() {};
                    var ha = /^(\-|\+)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/,
                        pa = /^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;
                    He.fn = ve.prototype, He.invalid = function() {
                        return He(NaN)
                    };
                    var fa = Pe(1, "add"),
                        Ma = Pe(-1, "subtract");
                    t.defaultFormat = "YYYY-MM-DDTHH:mm:ssZ", t.defaultFormatUtc = "YYYY-MM-DDTHH:mm:ss[Z]";
                    var ya = v("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.", function(e) {
                        return void 0 === e ? this.localeData() : this.locale(e)
                    });
                    C(0, ["gg", 2], 0, function() {
                        return this.weekYear() % 100
                    }), C(0, ["GG", 2], 0, function() {
                        return this.isoWeekYear() % 100
                    }), ze("gggg", "weekYear"), ze("ggggg", "weekYear"), ze("GGGG", "isoWeekYear"), ze("GGGGG", "isoWeekYear"), T("weekYear", "gg"), T("isoWeekYear", "GG"), H("weekYear", 1), H("isoWeekYear", 1), W("G", wt), W("g", wt), W("GG", ft, mt), W("gg", ft, mt), W("GGGG", Lt, ht), W("gggg", Lt, ht), W("GGGGG", Yt, pt), W("ggggg", Yt, pt), I(["gggg", "ggggg", "GGGG", "GGGGG"], function(e, t, a, n) {
                        t[n.substr(0, 2)] = g(e)
                    }), I(["gg", "GG"], function(e, a, n, s) {
                        a[s] = t.parseTwoDigitYear(e)
                    }), C("Q", 0, "Qo", "quarter"), T("quarter", "Q"), H("quarter", 7), W("Q", _t), F("Q", function(e, t) {
                        t[Ht] = 3 * (g(e) - 1)
                    }), C("D", ["DD", 2], "Do", "date"), T("date", "D"), H("date", 9), W("D", ft), W("DD", ft, mt), W("Do", function(e, t) {
                        return e ? t._dayOfMonthOrdinalParse || t._ordinalParse : t._dayOfMonthOrdinalParseLenient
                    }), F(["D", "DD"], jt), F("Do", function(e, t) {
                        t[jt] = g(e.match(ft)[0])
                    });
                    var ga = J("Date", !0);
                    C("DDD", ["DDDD", 3], "DDDo", "dayOfYear"), T("dayOfYear", "DDD"), H("dayOfYear", 4), W("DDD", gt), W("DDDD", ct), F(["DDD", "DDDD"], function(e, t, a) {
                        a._dayOfYear = g(e)
                    }), C("m", ["mm", 2], 0, "minute"), T("minute", "m"), H("minute", 14), W("m", ft), W("mm", ft, mt), F(["m", "mm"], Pt);
                    var La = J("Minutes", !1);
                    C("s", ["ss", 2], 0, "second"), T("second", "s"), H("second", 15), W("s", ft), W("ss", ft, mt), F(["s", "ss"], Ot);
                    var Ya = J("Seconds", !1);
                    C("S", 0, 0, function() {
                        return ~~(this.millisecond() / 100)
                    }), C(0, ["SS", 2], 0, function() {
                        return ~~(this.millisecond() / 10)
                    }), C(0, ["SSS", 3], 0, "millisecond"), C(0, ["SSSS", 4], 0, function() {
                        return 10 * this.millisecond()
                    }), C(0, ["SSSSS", 5], 0, function() {
                        return 100 * this.millisecond()
                    }), C(0, ["SSSSSS", 6], 0, function() {
                        return 1e3 * this.millisecond()
                    }), C(0, ["SSSSSSS", 7], 0, function() {
                        return 1e4 * this.millisecond()
                    }), C(0, ["SSSSSSSS", 8], 0, function() {
                        return 1e5 * this.millisecond()
                    }), C(0, ["SSSSSSSSS", 9], 0, function() {
                        return 1e6 * this.millisecond()
                    }), T("millisecond", "ms"), H("millisecond", 16), W("S", gt, _t), W("SS", gt, mt), W("SSS", gt, ct);
                    var va;
                    for (va = "SSSS"; va.length <= 9; va += "S") W(va, vt);
                    for (va = "S"; va.length <= 9; va += "S") F(va, Ie);
                    var wa = J("Milliseconds", !1);
                    C("z", 0, 0, "zoneAbbr"), C("zz", 0, 0, "zoneName");
                    var ka = f.prototype;
                    ka.add = fa, ka.calendar = function(e, a) {
                        var n = e || Le(),
                            s = Te(n, this).startOf("day"),
                            i = t.calendarFormat(this, s) || "sameElse",
                            r = a && (k(a[i]) ? a[i].call(this, n) : a[i]);
                        return this.format(r || this.localeData().calendar(i, this, Le(n)))
                    }, ka.clone = function() {
                        return new f(this)
                    }, ka.diff = function(e, t, a) {
                        var n, s, i;
                        if (!this.isValid()) return NaN;
                        if (!(n = Te(e, this)).isValid()) return NaN;
                        switch (s = 6e4 * (n.utcOffset() - this.utcOffset()), t = S(t)) {
                            case "year":
                                i = Ee(this, n) / 12;
                                break;
                            case "month":
                                i = Ee(this, n);
                                break;
                            case "quarter":
                                i = Ee(this, n) / 3;
                                break;
                            case "second":
                                i = (this - n) / 1e3;
                                break;
                            case "minute":
                                i = (this - n) / 6e4;
                                break;
                            case "hour":
                                i = (this - n) / 36e5;
                                break;
                            case "day":
                                i = (this - n - s) / 864e5;
                                break;
                            case "week":
                                i = (this - n - s) / 6048e5;
                                break;
                            default:
                                i = this - n
                        }
                        return a ? i : y(i)
                    }, ka.endOf = function(e) {
                        return void 0 === (e = S(e)) || "millisecond" === e ? this : ("date" === e && (e = "day"), this.startOf(e).add(1, "isoWeek" === e ? "week" : e).subtract(1, "ms"))
                    }, ka.format = function(e) {
                        e || (e = this.isUtc() ? t.defaultFormatUtc : t.defaultFormat);
                        var a = O(this, e);
                        return this.localeData().postformat(a)
                    }, ka.from = function(e, t) {
                        return this.isValid() && (M(e) && e.isValid() || Le(e).isValid()) ? He({
                            to: this,
                            from: e
                        }).locale(this.locale()).humanize(!t) : this.localeData().invalidDate()
                    }, ka.fromNow = function(e) {
                        return this.from(Le(), e)
                    }, ka.to = function(e, t) {
                        return this.isValid() && (M(e) && e.isValid() || Le(e).isValid()) ? He({
                            from: this,
                            to: e
                        }).locale(this.locale()).humanize(!t) : this.localeData().invalidDate()
                    }, ka.toNow = function(e) {
                        return this.to(Le(), e)
                    }, ka.get = function(e) {
                        return e = S(e), k(this[e]) ? this[e]() : this
                    }, ka.invalidAt = function() {
                        return m(this).overflow
                    }, ka.isAfter = function(e, t) {
                        var a = M(e) ? e : Le(e);
                        return !(!this.isValid() || !a.isValid()) && ("millisecond" === (t = S(i(t) ? "millisecond" : t)) ? this.valueOf() > a.valueOf() : a.valueOf() < this.clone().startOf(t).valueOf())
                    }, ka.isBefore = function(e, t) {
                        var a = M(e) ? e : Le(e);
                        return !(!this.isValid() || !a.isValid()) && ("millisecond" === (t = S(i(t) ? "millisecond" : t)) ? this.valueOf() < a.valueOf() : this.clone().endOf(t).valueOf() < a.valueOf())
                    }, ka.isBetween = function(e, t, a, n) {
                        return ("(" === (n = n || "()")[0] ? this.isAfter(e, a) : !this.isBefore(e, a)) && (")" === n[1] ? this.isBefore(t, a) : !this.isAfter(t, a))
                    }, ka.isSame = function(e, t) {
                        var a, n = M(e) ? e : Le(e);
                        return !(!this.isValid() || !n.isValid()) && ("millisecond" === (t = S(t || "millisecond")) ? this.valueOf() === n.valueOf() : (a = n.valueOf(), this.clone().startOf(t).valueOf() <= a && a <= this.clone().endOf(t).valueOf()))
                    }, ka.isSameOrAfter = function(e, t) {
                        return this.isSame(e, t) || this.isAfter(e, t)
                    }, ka.isSameOrBefore = function(e, t) {
                        return this.isSame(e, t) || this.isBefore(e, t)
                    }, ka.isValid = function() {
                        return c(this)
                    }, ka.lang = ya, ka.locale = We, ka.localeData = Ae, ka.max = _a, ka.min = ua, ka.parsingFlags = function() {
                        return u({}, m(this))
                    }, ka.set = function(e, t) {
                        if ("object" == typeof e)
                            for (var a = function(e) {
                                    var t = [];
                                    for (var a in e) t.push({
                                        unit: a,
                                        priority: rt[a]
                                    });
                                    return t.sort(function(e, t) {
                                        return e.priority - t.priority
                                    }), t
                                }(e = x(e)), n = 0; n < a.length; n++) this[a[n].unit](e[a[n].unit]);
                        else if (e = S(e), k(this[e])) return this[e](t);
                        return this
                    }, ka.startOf = function(e) {
                        switch (e = S(e)) {
                            case "year":
                                this.month(0);
                            case "quarter":
                            case "month":
                                this.date(1);
                            case "week":
                            case "isoWeek":
                            case "day":
                            case "date":
                                this.hours(0);
                            case "hour":
                                this.minutes(0);
                            case "minute":
                                this.seconds(0);
                            case "second":
                                this.milliseconds(0)
                        }
                        return "week" === e && this.weekday(0), "isoWeek" === e && this.isoWeekday(1), "quarter" === e && this.month(3 * Math.floor(this.month() / 3)), this
                    }, ka.subtract = Ma, ka.toArray = function() {
                        return [this.year(), this.month(), this.date(), this.hour(), this.minute(), this.second(), this.millisecond()]
                    }, ka.toObject = function() {
                        return {
                            years: this.year(),
                            months: this.month(),
                            date: this.date(),
                            hours: this.hours(),
                            minutes: this.minutes(),
                            seconds: this.seconds(),
                            milliseconds: this.milliseconds()
                        }
                    }, ka.toDate = function() {
                        return new Date(this.valueOf())
                    }, ka.toISOString = function(e) {
                        if (!this.isValid()) return null;
                        var t = !0 !== e,
                            a = t ? this.clone().utc() : this;
                        return a.year() < 0 || a.year() > 9999 ? O(a, t ? "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]" : "YYYYYY-MM-DD[T]HH:mm:ss.SSSZ") : k(Date.prototype.toISOString) ? t ? this.toDate().toISOString() : new Date(this.valueOf() + 60 * this.utcOffset() * 1e3).toISOString().replace("Z", O(a, "Z")) : O(a, t ? "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]" : "YYYY-MM-DD[T]HH:mm:ss.SSSZ")
                    }, ka.inspect = function() {
                        if (!this.isValid()) return "moment.invalid(/* " + this._i + " */)";
                        var e = "moment",
                            t = "";
                        this.isLocal() || (e = 0 === this.utcOffset() ? "moment.utc" : "moment.parseZone", t = "Z");
                        var a = "[" + e + '("]',
                            n = 0 <= this.year() && this.year() <= 9999 ? "YYYY" : "YYYYYY",
                            s = t + '[")]';
                        return this.format(a + n + "-MM-DD[T]HH:mm:ss.SSS" + s)
                    }, ka.toJSON = function() {
                        return this.isValid() ? this.toISOString() : null
                    }, ka.toString = function() {
                        return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")
                    }, ka.unix = function() {
                        return Math.floor(this.valueOf() / 1e3)
                    }, ka.valueOf = function() {
                        return this._d.valueOf() - 6e4 * (this._offset || 0)
                    }, ka.creationData = function() {
                        return {
                            input: this._i,
                            format: this._f,
                            locale: this._locale,
                            isUTC: this._isUTC,
                            strict: this._strict
                        }
                    }, ka.year = Ft, ka.isLeapYear = function() {
                        return R(this.year())
                    }, ka.weekYear = function(e) {
                        return Fe.call(this, e, this.week(), this.weekday(), this.localeData()._week.dow, this.localeData()._week.doy)
                    }, ka.isoWeekYear = function(e) {
                        return Fe.call(this, e, this.isoWeek(), this.isoWeekday(), 1, 4)
                    }, ka.quarter = ka.quarters = function(e) {
                        return null == e ? Math.ceil((this.month() + 1) / 3) : this.month(3 * (e - 1) + this.month() % 3)
                    }, ka.month = G, ka.daysInMonth = function() {
                        return V(this.year(), this.month())
                    }, ka.week = ka.weeks = function(e) {
                        var t = this.localeData().week(this);
                        return null == e ? t : this.add(7 * (e - t), "d")
                    }, ka.isoWeek = ka.isoWeeks = function(e) {
                        var t = ee(this, 1, 4).week;
                        return null == e ? t : this.add(7 * (e - t), "d")
                    }, ka.weeksInYear = function() {
                        var e = this.localeData()._week;
                        return te(this.year(), e.dow, e.doy)
                    }, ka.isoWeeksInYear = function() {
                        return te(this.year(), 1, 4)
                    }, ka.date = ga, ka.day = ka.days = function(e) {
                        if (!this.isValid()) return null != e ? this : NaN;
                        var t = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
                        return null != e ? (e = function(e, t) {
                            return "string" != typeof e ? e : isNaN(e) ? "number" == typeof(e = t.weekdaysParse(e)) ? e : null : parseInt(e, 10)
                        }(e, this.localeData()), this.add(e - t, "d")) : t
                    }, ka.weekday = function(e) {
                        if (!this.isValid()) return null != e ? this : NaN;
                        var t = (this.day() + 7 - this.localeData()._week.dow) % 7;
                        return null == e ? t : this.add(e - t, "d")
                    }, ka.isoWeekday = function(e) {
                        if (!this.isValid()) return null != e ? this : NaN;
                        if (null != e) {
                            var t = function(e, t) {
                                return "string" == typeof e ? t.weekdaysParse(e) % 7 || 7 : isNaN(e) ? null : e
                            }(e, this.localeData());
                            return this.day(this.day() % 7 ? t : t - 7)
                        }
                        return this.day() || 7
                    }, ka.dayOfYear = function(e) {
                        var t = Math.round((this.clone().startOf("day") - this.clone().startOf("year")) / 864e5) + 1;
                        return null == e ? t : this.add(e - t, "d")
                    }, ka.hour = ka.hours = Qt, ka.minute = ka.minutes = La, ka.second = ka.seconds = Ya, ka.millisecond = ka.milliseconds = wa, ka.utcOffset = function(e, a, n) {
                        var s, i = this._offset || 0;
                        if (!this.isValid()) return null != e ? this : NaN;
                        if (null != e) {
                            if ("string" == typeof e) {
                                if (null === (e = be(Dt, e))) return this
                            } else Math.abs(e) < 16 && !n && (e *= 60);
                            return !this._isUTC && a && (s = Se(this)), this._offset = e, this._isUTC = !0, null != s && this.add(s, "m"), i !== e && (!a || this._changeInProgress ? Oe(this, He(e - i, "m"), 1, !1) : this._changeInProgress || (this._changeInProgress = !0, t.updateOffset(this, !0), this._changeInProgress = null)), this
                        }
                        return this._isUTC ? i : Se(this)
                    }, ka.utc = function(e) {
                        return this.utcOffset(0, e)
                    }, ka.local = function(e) {
                        return this._isUTC && (this.utcOffset(0, e), this._isUTC = !1, e && this.subtract(Se(this), "m")), this
                    }, ka.parseZone = function() {
                        if (null != this._tzm) this.utcOffset(this._tzm, !1, !0);
                        else if ("string" == typeof this._i) {
                            var e = be(kt, this._i);
                            null != e ? this.utcOffset(e) : this.utcOffset(0, !0)
                        }
                        return this
                    }, ka.hasAlignedHourOffset = function(e) {
                        return !!this.isValid() && (e = e ? Le(e).utcOffset() : 0, (this.utcOffset() - e) % 60 == 0)
                    }, ka.isDST = function() {
                        return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset()
                    }, ka.isLocal = function() {
                        return !!this.isValid() && !this._isUTC
                    }, ka.isUtcOffset = function() {
                        return !!this.isValid() && this._isUTC
                    }, ka.isUtc = xe, ka.isUTC = xe, ka.zoneAbbr = function() {
                        return this._isUTC ? "UTC" : ""
                    }, ka.zoneName = function() {
                        return this._isUTC ? "Coordinated Universal Time" : ""
                    }, ka.dates = v("dates accessor is deprecated. Use date instead.", ga), ka.months = v("months accessor is deprecated. Use month instead", G), ka.years = v("years accessor is deprecated. Use year instead", Ft), ka.zone = v("moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/", function(e, t) {
                        return null != e ? ("string" != typeof e && (e = -e), this.utcOffset(e, t), this) : -this.utcOffset()
                    }), ka.isDSTShifted = v("isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information", function() {
                        if (!i(this._isDSTShifted)) return this._isDSTShifted;
                        var e = {};
                        if (p(e, this), (e = ye(e))._a) {
                            var t = e._isUTC ? _(e._a) : Le(e._a);
                            this._isDSTShifted = this.isValid() && L(e._a, t.toArray()) > 0
                        } else this._isDSTShifted = !1;
                        return this._isDSTShifted
                    });
                    var Da = b.prototype;
                    Da.calendar = function(e, t, a) {
                        var n = this._calendar[e] || this._calendar.sameElse;
                        return k(n) ? n.call(t, a) : n
                    }, Da.longDateFormat = function(e) {
                        var t = this._longDateFormat[e],
                            a = this._longDateFormat[e.toUpperCase()];
                        return t || !a ? t : (this._longDateFormat[e] = a.replace(/MMMM|MM|DD|dddd/g, function(e) {
                            return e.slice(1)
                        }), this._longDateFormat[e])
                    }, Da.invalidDate = function() {
                        return this._invalidDate
                    }, Da.ordinal = function(e) {
                        return this._ordinal.replace("%d", e)
                    }, Da.preparse = $e, Da.postformat = $e, Da.relativeTime = function(e, t, a, n) {
                        var s = this._relativeTime[a];
                        return k(s) ? s(e, t, a, n) : s.replace(/%d/i, e)
                    }, Da.pastFuture = function(e, t) {
                        var a = this._relativeTime[e > 0 ? "future" : "past"];
                        return k(a) ? a(t) : a.replace(/%s/i, t)
                    }, Da.set = function(e) {
                        var t, a;
                        for (a in e) k(t = e[a]) ? this[a] = t : this["_" + a] = t;
                        this._config = e, this._dayOfMonthOrdinalParseLenient = new RegExp((this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) + "|" + /\d{1,2}/.source)
                    }, Da.months = function(e, t) {
                        return e ? n(this._months) ? this._months[e.month()] : this._months[(this._months.isFormat || It).test(t) ? "format" : "standalone"][e.month()] : n(this._months) ? this._months : this._months.standalone
                    }, Da.monthsShort = function(e, t) {
                        return e ? n(this._monthsShort) ? this._monthsShort[e.month()] : this._monthsShort[It.test(t) ? "format" : "standalone"][e.month()] : n(this._monthsShort) ? this._monthsShort : this._monthsShort.standalone
                    }, Da.monthsParse = function(e, t, a) {
                        var n, s, i;
                        if (this._monthsParseExact) return function(e, t, a) {
                            var n, s, i, r = e.toLocaleLowerCase();
                            if (!this._monthsParse)
                                for (this._monthsParse = [], this._longMonthsParse = [], this._shortMonthsParse = [], n = 0; n < 12; ++n) i = _([2e3, n]), this._shortMonthsParse[n] = this.monthsShort(i, "").toLocaleLowerCase(), this._longMonthsParse[n] = this.months(i, "").toLocaleLowerCase();
                            return a ? "MMM" === t ? -1 !== (s = zt.call(this._shortMonthsParse, r)) ? s : null : -1 !== (s = zt.call(this._longMonthsParse, r)) ? s : null : "MMM" === t ? -1 !== (s = zt.call(this._shortMonthsParse, r)) ? s : -1 !== (s = zt.call(this._longMonthsParse, r)) ? s : null : -1 !== (s = zt.call(this._longMonthsParse, r)) ? s : -1 !== (s = zt.call(this._shortMonthsParse, r)) ? s : null
                        }.call(this, e, t, a);
                        for (this._monthsParse || (this._monthsParse = [], this._longMonthsParse = [], this._shortMonthsParse = []), n = 0; n < 12; n++) {
                            if (s = _([2e3, n]), a && !this._longMonthsParse[n] && (this._longMonthsParse[n] = new RegExp("^" + this.months(s, "").replace(".", "") + "$", "i"), this._shortMonthsParse[n] = new RegExp("^" + this.monthsShort(s, "").replace(".", "") + "$", "i")), a || this._monthsParse[n] || (i = "^" + this.months(s, "") + "|^" + this.monthsShort(s, ""), this._monthsParse[n] = new RegExp(i.replace(".", ""), "i")), a && "MMMM" === t && this._longMonthsParse[n].test(e)) return n;
                            if (a && "MMM" === t && this._shortMonthsParse[n].test(e)) return n;
                            if (!a && this._monthsParse[n].test(e)) return n
                        }
                    }, Da.monthsRegex = function(e) {
                        return this._monthsParseExact ? (l(this, "_monthsRegex") || Z.call(this), e ? this._monthsStrictRegex : this._monthsRegex) : (l(this, "_monthsRegex") || (this._monthsRegex = Jt), this._monthsStrictRegex && e ? this._monthsStrictRegex : this._monthsRegex)
                    }, Da.monthsShortRegex = function(e) {
                        return this._monthsParseExact ? (l(this, "_monthsRegex") || Z.call(this), e ? this._monthsShortStrictRegex : this._monthsShortRegex) : (l(this, "_monthsShortRegex") || (this._monthsShortRegex = Rt), this._monthsShortStrictRegex && e ? this._monthsShortStrictRegex : this._monthsShortRegex)
                    }, Da.week = function(e) {
                        return ee(e, this._week.dow, this._week.doy).week
                    }, Da.firstDayOfYear = function() {
                        return this._week.doy
                    }, Da.firstDayOfWeek = function() {
                        return this._week.dow
                    }, Da.weekdays = function(e, t) {
                        return e ? n(this._weekdays) ? this._weekdays[e.day()] : this._weekdays[this._weekdays.isFormat.test(t) ? "format" : "standalone"][e.day()] : n(this._weekdays) ? this._weekdays : this._weekdays.standalone
                    }, Da.weekdaysMin = function(e) {
                        return e ? this._weekdaysMin[e.day()] : this._weekdaysMin
                    }, Da.weekdaysShort = function(e) {
                        return e ? this._weekdaysShort[e.day()] : this._weekdaysShort
                    }, Da.weekdaysParse = function(e, t, a) {
                        var n, s, i;
                        if (this._weekdaysParseExact) return function(e, t, a) {
                            var n, s, i, r = e.toLocaleLowerCase();
                            if (!this._weekdaysParse)
                                for (this._weekdaysParse = [], this._shortWeekdaysParse = [], this._minWeekdaysParse = [], n = 0; n < 7; ++n) i = _([2e3, 1]).day(n), this._minWeekdaysParse[n] = this.weekdaysMin(i, "").toLocaleLowerCase(), this._shortWeekdaysParse[n] = this.weekdaysShort(i, "").toLocaleLowerCase(), this._weekdaysParse[n] = this.weekdays(i, "").toLocaleLowerCase();
                            return a ? "dddd" === t ? -1 !== (s = zt.call(this._weekdaysParse, r)) ? s : null : "ddd" === t ? -1 !== (s = zt.call(this._shortWeekdaysParse, r)) ? s : null : -1 !== (s = zt.call(this._minWeekdaysParse, r)) ? s : null : "dddd" === t ? -1 !== (s = zt.call(this._weekdaysParse, r)) ? s : -1 !== (s = zt.call(this._shortWeekdaysParse, r)) ? s : -1 !== (s = zt.call(this._minWeekdaysParse, r)) ? s : null : "ddd" === t ? -1 !== (s = zt.call(this._shortWeekdaysParse, r)) ? s : -1 !== (s = zt.call(this._weekdaysParse, r)) ? s : -1 !== (s = zt.call(this._minWeekdaysParse, r)) ? s : null : -1 !== (s = zt.call(this._minWeekdaysParse, r)) ? s : -1 !== (s = zt.call(this._weekdaysParse, r)) ? s : -1 !== (s = zt.call(this._shortWeekdaysParse, r)) ? s : null
                        }.call(this, e, t, a);
                        for (this._weekdaysParse || (this._weekdaysParse = [], this._minWeekdaysParse = [], this._shortWeekdaysParse = [], this._fullWeekdaysParse = []), n = 0; n < 7; n++) {
                            if (s = _([2e3, 1]).day(n), a && !this._fullWeekdaysParse[n] && (this._fullWeekdaysParse[n] = new RegExp("^" + this.weekdays(s, "").replace(".", "\\.?") + "$", "i"), this._shortWeekdaysParse[n] = new RegExp("^" + this.weekdaysShort(s, "").replace(".", "\\.?") + "$", "i"), this._minWeekdaysParse[n] = new RegExp("^" + this.weekdaysMin(s, "").replace(".", "\\.?") + "$", "i")), this._weekdaysParse[n] || (i = "^" + this.weekdays(s, "") + "|^" + this.weekdaysShort(s, "") + "|^" + this.weekdaysMin(s, ""), this._weekdaysParse[n] = new RegExp(i.replace(".", ""), "i")), a && "dddd" === t && this._fullWeekdaysParse[n].test(e)) return n;
                            if (a && "ddd" === t && this._shortWeekdaysParse[n].test(e)) return n;
                            if (a && "dd" === t && this._minWeekdaysParse[n].test(e)) return n;
                            if (!a && this._weekdaysParse[n].test(e)) return n
                        }
                    }, Da.weekdaysRegex = function(e) {
                        return this._weekdaysParseExact ? (l(this, "_weekdaysRegex") || ae.call(this), e ? this._weekdaysStrictRegex : this._weekdaysRegex) : (l(this, "_weekdaysRegex") || (this._weekdaysRegex = qt), this._weekdaysStrictRegex && e ? this._weekdaysStrictRegex : this._weekdaysRegex)
                    }, Da.weekdaysShortRegex = function(e) {
                        return this._weekdaysParseExact ? (l(this, "_weekdaysRegex") || ae.call(this), e ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex) : (l(this, "_weekdaysShortRegex") || (this._weekdaysShortRegex = Gt), this._weekdaysShortStrictRegex && e ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex)
                    }, Da.weekdaysMinRegex = function(e) {
                        return this._weekdaysParseExact ? (l(this, "_weekdaysRegex") || ae.call(this), e ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex) : (l(this, "_weekdaysMinRegex") || (this._weekdaysMinRegex = Zt), this._weekdaysMinStrictRegex && e ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex)
                    }, Da.isPM = function(e) {
                        return "p" === (e + "").toLowerCase().charAt(0)
                    }, Da.meridiem = function(e, t, a) {
                        return e > 11 ? a ? "pm" : "PM" : a ? "am" : "AM"
                    }, de("en", {
                        dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
                        ordinal: function(e) {
                            var t = e % 10;
                            return e + (1 === g(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th")
                        }
                    }), t.lang = v("moment.lang is deprecated. Use moment.locale instead.", de), t.langData = v("moment.langData is deprecated. Use moment.localeData instead.", ue);
                    var ba = Math.abs,
                        Ta = Ge("ms"),
                        Sa = Ge("s"),
                        xa = Ge("m"),
                        Ha = Ge("h"),
                        ja = Ge("d"),
                        Ca = Ge("w"),
                        Pa = Ge("M"),
                        Oa = Ge("y"),
                        Ea = Ze("milliseconds"),
                        Wa = Ze("seconds"),
                        Aa = Ze("minutes"),
                        za = Ze("hours"),
                        Fa = Ze("days"),
                        Ia = Ze("months"),
                        $a = Ze("years"),
                        Na = Math.round,
                        Ra = {
                            ss: 44,
                            s: 45,
                            m: 45,
                            h: 22,
                            d: 26,
                            M: 11
                        },
                        Ja = Math.abs,
                        Ba = ve.prototype;
                    return Ba.isValid = function() {
                            return this._isValid
                        }, Ba.abs = function() {
                            var e = this._data;
                            return this._milliseconds = ba(this._milliseconds), this._days = ba(this._days), this._months = ba(this._months), e.milliseconds = ba(e.milliseconds), e.seconds = ba(e.seconds), e.minutes = ba(e.minutes), e.hours = ba(e.hours), e.months = ba(e.months), e.years = ba(e.years), this
                        }, Ba.add = function(e, t) {
                            return Be(this, e, t, 1)
                        }, Ba.subtract = function(e, t) {
                            return Be(this, e, t, -1)
                        }, Ba.as = function(e) {
                            if (!this.isValid()) return NaN;
                            var t, a, n = this._milliseconds;
                            if ("month" === (e = S(e)) || "year" === e) return t = this._days + n / 864e5, a = this._months + Ve(t), "month" === e ? a : a / 12;
                            switch (t = this._days + Math.round(qe(this._months)), e) {
                                case "week":
                                    return t / 7 + n / 6048e5;
                                case "day":
                                    return t + n / 864e5;
                                case "hour":
                                    return 24 * t + n / 36e5;
                                case "minute":
                                    return 1440 * t + n / 6e4;
                                case "second":
                                    return 86400 * t + n / 1e3;
                                case "millisecond":
                                    return Math.floor(864e5 * t) + n;
                                default:
                                    throw new Error("Unknown unit " + e)
                            }
                        }, Ba.asMilliseconds = Ta, Ba.asSeconds = Sa, Ba.asMinutes = xa, Ba.asHours = Ha, Ba.asDays = ja, Ba.asWeeks = Ca, Ba.asMonths = Pa, Ba.asYears = Oa, Ba.valueOf = function() {
                            return this.isValid() ? this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * g(this._months / 12) : NaN
                        }, Ba._bubble = function() {
                            var e, t, a, n, s, i = this._milliseconds,
                                r = this._days,
                                o = this._months,
                                d = this._data;
                            return i >= 0 && r >= 0 && o >= 0 || i <= 0 && r <= 0 && o <= 0 || (i += 864e5 * Ue(qe(o) + r), r = 0, o = 0), d.milliseconds = i % 1e3, e = y(i / 1e3), d.seconds = e % 60, t = y(e / 60), d.minutes = t % 60, a = y(t / 60), d.hours = a % 24, r += y(a / 24), s = y(Ve(r)), o += s, r -= Ue(qe(s)), n = y(o / 12), o %= 12, d.days = r, d.months = o, d.years = n, this
                        }, Ba.clone = function() {
                            return He(this)
                        }, Ba.get = function(e) {
                            return e = S(e), this.isValid() ? this[e + "s"]() : NaN
                        }, Ba.milliseconds = Ea, Ba.seconds = Wa, Ba.minutes = Aa, Ba.hours = za, Ba.days = Fa, Ba.weeks = function() {
                            return y(this.days() / 7)
                        }, Ba.months = Ia, Ba.years = $a, Ba.humanize = function(e) {
                            if (!this.isValid()) return this.localeData().invalidDate();
                            var t = this.localeData(),
                                a = function(e, t, a) {
                                    var n = He(e).abs(),
                                        s = Na(n.as("s")),
                                        i = Na(n.as("m")),
                                        r = Na(n.as("h")),
                                        o = Na(n.as("d")),
                                        d = Na(n.as("M")),
                                        l = Na(n.as("y")),
                                        u = s <= Ra.ss && ["s", s] || s < Ra.s && ["ss", s] || i <= 1 && ["m"] || i < Ra.m && ["mm", i] || r <= 1 && ["h"] || r < Ra.h && ["hh", r] || o <= 1 && ["d"] || o < Ra.d && ["dd", o] || d <= 1 && ["M"] || d < Ra.M && ["MM", d] || l <= 1 && ["y"] || ["yy", l];
                                    return u[2] = t, u[3] = +e > 0, u[4] = a,
                                        function(e, t, a, n, s) {
                                            return s.relativeTime(t || 1, !!a, e, n)
                                        }.apply(null, u)
                                }(this, !e, t);
                            return e && (a = t.pastFuture(+this, a)), t.postformat(a)
                        }, Ba.toISOString = Qe, Ba.toString = Qe, Ba.toJSON = Qe, Ba.locale = We, Ba.localeData = Ae, Ba.toIsoString = v("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)", Qe), Ba.lang = ya, C("X", 0, 0, "unix"), C("x", 0, 0, "valueOf"), W("x", wt), W("X", /[+-]?\d+(\.\d{1,3})?/), F("X", function(e, t, a) {
                            a._d = new Date(1e3 * parseFloat(e, 10))
                        }), F("x", function(e, t, a) {
                            a._d = new Date(g(e))
                        }), t.version = "2.22.2",
                        function(e) {
                            Xe = e
                        }(Le), t.fn = ka, t.min = function() {
                            return Ye("isBefore", [].slice.call(arguments, 0))
                        }, t.max = function() {
                            return Ye("isAfter", [].slice.call(arguments, 0))
                        }, t.now = function() {
                            return Date.now ? Date.now() : +new Date
                        }, t.utc = _, t.unix = function(e) {
                            return Le(1e3 * e)
                        }, t.months = function(e, t) {
                            return Re(e, t, "months")
                        }, t.isDate = o, t.locale = de, t.invalid = h, t.duration = He, t.isMoment = M, t.weekdays = function(e, t, a) {
                            return Je(e, t, a, "weekdays")
                        }, t.parseZone = function() {
                            return Le.apply(null, arguments).parseZone()
                        }, t.localeData = ue, t.isDuration = we, t.monthsShort = function(e, t) {
                            return Re(e, t, "monthsShort")
                        }, t.weekdaysMin = function(e, t, a) {
                            return Je(e, t, a, "weekdaysMin")
                        }, t.defineLocale = le, t.updateLocale = function(e, t) {
                            if (null != t) {
                                var a, n, s = Xt;
                                null != (n = oe(e)) && (s = n._config), (a = new b(t = D(s, t))).parentLocale = ea[e], ea[e] = a, de(e)
                            } else null != ea[e] && (null != ea[e].parentLocale ? ea[e] = ea[e].parentLocale : null != ea[e] && delete ea[e]);
                            return ea[e]
                        }, t.locales = function() {
                            return st(ea)
                        }, t.weekdaysShort = function(e, t, a) {
                            return Je(e, t, a, "weekdaysShort")
                        }, t.normalizeUnits = S, t.relativeTimeRounding = function(e) {
                            return void 0 === e ? Na : "function" == typeof e && (Na = e, !0)
                        }, t.relativeTimeThreshold = function(e, t) {
                            return void 0 !== Ra[e] && (void 0 === t ? Ra[e] : (Ra[e] = t, "s" === e && (Ra.ss = t - 1), !0))
                        }, t.calendarFormat = function(e, t) {
                            var a = e.diff(t, "days", !0);
                            return a < -6 ? "sameElse" : a < -1 ? "lastWeek" : a < 0 ? "lastDay" : a < 1 ? "sameDay" : a < 2 ? "nextDay" : a < 7 ? "nextWeek" : "sameElse"
                        }, t.prototype = ka, t.HTML5_FMT = {
                            DATETIME_LOCAL: "YYYY-MM-DDTHH:mm",
                            DATETIME_LOCAL_SECONDS: "YYYY-MM-DDTHH:mm:ss",
                            DATETIME_LOCAL_MS: "YYYY-MM-DDTHH:mm:ss.SSS",
                            DATE: "YYYY-MM-DD",
                            TIME: "HH:mm",
                            TIME_SECONDS: "HH:mm:ss",
                            TIME_MS: "HH:mm:ss.SSS",
                            WEEK: "YYYY-[W]WW",
                            MONTH: "YYYY-MM"
                        }, t
                }), s("undefined" != typeof moment ? moment : window.moment)
            }).call(e, void 0, void 0, void 0, void 0, function(e) {
                t.exports = e
            })
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {}],
    6: [function(e, t, a) {
        ! function(e) {
            "function" == typeof define && define.amd ? define([], e) : "object" == typeof a ? t.exports = e() : window.noUiSlider = e()
        }(function() {
            "use strict";

            function e(e) {
                return null !== e && void 0 !== e
            }

            function t(e) {
                e.preventDefault()
            }

            function a(e) {
                return "number" == typeof e && !isNaN(e) && isFinite(e)
            }

            function n(e, t, a) {
                a > 0 && (o(e, t), setTimeout(function() {
                    d(e, t)
                }, a))
            }

            function s(e) {
                return Math.max(Math.min(e, 100), 0)
            }

            function i(e) {
                return Array.isArray(e) ? e : [e]
            }

            function r(e) {
                var t = (e = String(e)).split(".");
                return t.length > 1 ? t[1].length : 0
            }

            function o(e, t) {
                e.classList ? e.classList.add(t) : e.className += " " + t
            }

            function d(e, t) {
                e.classList ? e.classList.remove(t) : e.className = e.className.replace(new RegExp("(^|\\b)" + t.split(" ").join("|") + "(\\b|$)", "gi"), " ")
            }

            function l(e) {
                var t = void 0 !== window.pageXOffset,
                    a = "CSS1Compat" === (e.compatMode || "");
                return {
                    x: t ? window.pageXOffset : a ? e.documentElement.scrollLeft : e.body.scrollLeft,
                    y: t ? window.pageYOffset : a ? e.documentElement.scrollTop : e.body.scrollTop
                }
            }

            function u(e, t) {
                return 100 / (t - e)
            }

            function _(e, t) {
                return 100 * t / (e[1] - e[0])
            }

            function m(e, t) {
                for (var a = 1; e >= t[a];) a += 1;
                return a
            }

            function c(e, t, a) {
                if (a >= e.slice(-1)[0]) return 100;
                var n = m(a, e),
                    s = e[n - 1],
                    i = e[n],
                    r = t[n - 1],
                    o = t[n];
                return r + function(e, t) {
                    return _(e, e[0] < 0 ? t + Math.abs(e[0]) : t - e[0])
                }([s, i], a) / u(r, o)
            }

            function h(e, t, a, n) {
                if (100 === n) return n;
                var s = m(n, e),
                    i = e[s - 1],
                    r = e[s];
                return a ? n - i > (r - i) / 2 ? r : i : t[s - 1] ? e[s - 1] + function(e, t) {
                    return Math.round(e / t) * t
                }(n - e[s - 1], t[s - 1]) : n
            }

            function p(e, t, n) {
                var s;
                if ("number" == typeof t && (t = [t]), !Array.isArray(t)) throw new Error("noUiSlider (" + F + "): 'range' contains invalid value.");
                if (s = "min" === e ? 0 : "max" === e ? 100 : parseFloat(e), !a(s) || !a(t[0])) throw new Error("noUiSlider (" + F + "): 'range' value isn't numeric.");
                n.xPct.push(s), n.xVal.push(t[0]), s ? n.xSteps.push(!isNaN(t[1]) && t[1]) : isNaN(t[1]) || (n.xSteps[0] = t[1]), n.xHighestCompleteStep.push(0)
            }

            function f(e, t, a) {
                if (!t) return !0;
                a.xSteps[e] = _([a.xVal[e], a.xVal[e + 1]], t) / u(a.xPct[e], a.xPct[e + 1]);
                var n = (a.xVal[e + 1] - a.xVal[e]) / a.xNumSteps[e],
                    s = Math.ceil(Number(n.toFixed(3)) - 1),
                    i = a.xVal[e] + a.xNumSteps[e] * s;
                a.xHighestCompleteStep[e] = i
            }

            function M(e, t, a) {
                this.xPct = [], this.xVal = [], this.xSteps = [a || !1], this.xNumSteps = [!1], this.xHighestCompleteStep = [], this.snap = t;
                var n, s = [];
                for (n in e) e.hasOwnProperty(n) && s.push([e[n], n]);
                for (s.length && "object" == typeof s[0][0] ? s.sort(function(e, t) {
                        return e[0][0] - t[0][0]
                    }) : s.sort(function(e, t) {
                        return e[0] - t[0]
                    }), n = 0; n < s.length; n++) p(s[n][1], s[n][0], this);
                for (this.xNumSteps = this.xSteps.slice(0), n = 0; n < this.xNumSteps.length; n++) f(n, this.xNumSteps[n], this)
            }

            function y(e) {
                if (function(e) {
                        return "object" == typeof e && "function" == typeof e.to && "function" == typeof e.from
                    }(e)) return !0;
                throw new Error("noUiSlider (" + F + "): 'format' requires 'to' and 'from' methods.")
            }

            function g(e, t) {
                if (!a(t)) throw new Error("noUiSlider (" + F + "): 'step' is not numeric.");
                e.singleStep = t
            }

            function L(e, t) {
                if ("object" != typeof t || Array.isArray(t)) throw new Error("noUiSlider (" + F + "): 'range' is not an object.");
                if (void 0 === t.min || void 0 === t.max) throw new Error("noUiSlider (" + F + "): Missing 'min' or 'max' in 'range'.");
                if (t.min === t.max) throw new Error("noUiSlider (" + F + "): 'range' 'min' and 'max' cannot be equal.");
                e.spectrum = new M(t, e.snap, e.singleStep)
            }

            function Y(e, t) {
                if (t = i(t), !Array.isArray(t) || !t.length) throw new Error("noUiSlider (" + F + "): 'start' option is incorrect.");
                e.handles = t.length, e.start = t
            }

            function v(e, t) {
                if (e.snap = t, "boolean" != typeof t) throw new Error("noUiSlider (" + F + "): 'snap' option must be a boolean.")
            }

            function w(e, t) {
                if (e.animate = t, "boolean" != typeof t) throw new Error("noUiSlider (" + F + "): 'animate' option must be a boolean.")
            }

            function k(e, t) {
                if (e.animationDuration = t, "number" != typeof t) throw new Error("noUiSlider (" + F + "): 'animationDuration' option must be a number.")
            }

            function D(e, t) {
                var a, n = [!1];
                if ("lower" === t ? t = [!0, !1] : "upper" === t && (t = [!1, !0]), !0 === t || !1 === t) {
                    for (a = 1; a < e.handles; a++) n.push(t);
                    n.push(!1)
                } else {
                    if (!Array.isArray(t) || !t.length || t.length !== e.handles + 1) throw new Error("noUiSlider (" + F + "): 'connect' option doesn't match handle count.");
                    n = t
                }
                e.connect = n
            }

            function b(e, t) {
                switch (t) {
                    case "horizontal":
                        e.ort = 0;
                        break;
                    case "vertical":
                        e.ort = 1;
                        break;
                    default:
                        throw new Error("noUiSlider (" + F + "): 'orientation' option is invalid.")
                }
            }

            function T(e, t) {
                if (!a(t)) throw new Error("noUiSlider (" + F + "): 'margin' option must be numeric.");
                if (0 !== t && (e.margin = e.spectrum.getMargin(t), !e.margin)) throw new Error("noUiSlider (" + F + "): 'margin' option is only supported on linear sliders.")
            }

            function S(e, t) {
                if (!a(t)) throw new Error("noUiSlider (" + F + "): 'limit' option must be numeric.");
                if (e.limit = e.spectrum.getMargin(t), !e.limit || e.handles < 2) throw new Error("noUiSlider (" + F + "): 'limit' option is only supported on linear sliders with 2 or more handles.")
            }

            function x(e, t) {
                if (!a(t) && !Array.isArray(t)) throw new Error("noUiSlider (" + F + "): 'padding' option must be numeric or array of exactly 2 numbers.");
                if (Array.isArray(t) && 2 !== t.length && !a(t[0]) && !a(t[1])) throw new Error("noUiSlider (" + F + "): 'padding' option must be numeric or array of exactly 2 numbers.");
                if (0 !== t) {
                    if (Array.isArray(t) || (t = [t, t]), e.padding = [e.spectrum.getMargin(t[0]), e.spectrum.getMargin(t[1])], !1 === e.padding[0] || !1 === e.padding[1]) throw new Error("noUiSlider (" + F + "): 'padding' option is only supported on linear sliders.");
                    if (e.padding[0] < 0 || e.padding[1] < 0) throw new Error("noUiSlider (" + F + "): 'padding' option must be a positive number(s).");
                    if (e.padding[0] + e.padding[1] >= 100) throw new Error("noUiSlider (" + F + "): 'padding' option must not exceed 100% of the range.")
                }
            }

            function H(e, t) {
                switch (t) {
                    case "ltr":
                        e.dir = 0;
                        break;
                    case "rtl":
                        e.dir = 1;
                        break;
                    default:
                        throw new Error("noUiSlider (" + F + "): 'direction' option was not recognized.")
                }
            }

            function j(e, t) {
                if ("string" != typeof t) throw new Error("noUiSlider (" + F + "): 'behaviour' must be a string containing options.");
                var a = t.indexOf("tap") >= 0,
                    n = t.indexOf("drag") >= 0,
                    s = t.indexOf("fixed") >= 0,
                    i = t.indexOf("snap") >= 0,
                    r = t.indexOf("hover") >= 0;
                if (s) {
                    if (2 !== e.handles) throw new Error("noUiSlider (" + F + "): 'fixed' behaviour must be used with 2 handles");
                    T(e, e.start[1] - e.start[0])
                }
                e.events = {
                    tap: a || i,
                    drag: n,
                    fixed: s,
                    snap: i,
                    hover: r
                }
            }

            function C(e, t) {
                if (!1 !== t)
                    if (!0 === t) {
                        e.tooltips = [];
                        for (var a = 0; a < e.handles; a++) e.tooltips.push(!0)
                    } else {
                        if (e.tooltips = i(t), e.tooltips.length !== e.handles) throw new Error("noUiSlider (" + F + "): must pass a formatter for all handles.");
                        e.tooltips.forEach(function(e) {
                            if ("boolean" != typeof e && ("object" != typeof e || "function" != typeof e.to)) throw new Error("noUiSlider (" + F + "): 'tooltips' must be passed a formatter or 'false'.")
                        })
                    }
            }

            function P(e, t) {
                e.ariaFormat = t, y(t)
            }

            function O(e, t) {
                e.format = t, y(t)
            }

            function E(e, t) {
                if ("string" != typeof t && !1 !== t) throw new Error("noUiSlider (" + F + "): 'cssPrefix' must be a string or `false`.");
                e.cssPrefix = t
            }

            function W(e, t) {
                if ("object" != typeof t) throw new Error("noUiSlider (" + F + "): 'cssClasses' must be an object.");
                if ("string" == typeof e.cssPrefix) {
                    e.cssClasses = {};
                    for (var a in t) t.hasOwnProperty(a) && (e.cssClasses[a] = e.cssPrefix + t[a])
                } else e.cssClasses = t
            }

            function A(t) {
                var a = {
                        margin: 0,
                        limit: 0,
                        padding: 0,
                        animate: !0,
                        animationDuration: 300,
                        ariaFormat: I,
                        format: I
                    },
                    n = {
                        step: {
                            r: !1,
                            t: g
                        },
                        start: {
                            r: !0,
                            t: Y
                        },
                        connect: {
                            r: !0,
                            t: D
                        },
                        direction: {
                            r: !0,
                            t: H
                        },
                        snap: {
                            r: !1,
                            t: v
                        },
                        animate: {
                            r: !1,
                            t: w
                        },
                        animationDuration: {
                            r: !1,
                            t: k
                        },
                        range: {
                            r: !0,
                            t: L
                        },
                        orientation: {
                            r: !1,
                            t: b
                        },
                        margin: {
                            r: !1,
                            t: T
                        },
                        limit: {
                            r: !1,
                            t: S
                        },
                        padding: {
                            r: !1,
                            t: x
                        },
                        behaviour: {
                            r: !0,
                            t: j
                        },
                        ariaFormat: {
                            r: !1,
                            t: P
                        },
                        format: {
                            r: !1,
                            t: O
                        },
                        tooltips: {
                            r: !1,
                            t: C
                        },
                        cssPrefix: {
                            r: !0,
                            t: E
                        },
                        cssClasses: {
                            r: !0,
                            t: W
                        }
                    },
                    s = {
                        connect: !1,
                        direction: "ltr",
                        behaviour: "tap",
                        orientation: "horizontal",
                        cssPrefix: "noUi-",
                        cssClasses: {
                            target: "target",
                            base: "base",
                            origin: "origin",
                            handle: "handle",
                            handleLower: "handle-lower",
                            handleUpper: "handle-upper",
                            horizontal: "horizontal",
                            vertical: "vertical",
                            background: "background",
                            connect: "connect",
                            connects: "connects",
                            ltr: "ltr",
                            rtl: "rtl",
                            draggable: "draggable",
                            drag: "state-drag",
                            tap: "state-tap",
                            active: "active",
                            tooltip: "tooltip",
                            pips: "pips",
                            pipsHorizontal: "pips-horizontal",
                            pipsVertical: "pips-vertical",
                            marker: "marker",
                            markerHorizontal: "marker-horizontal",
                            markerVertical: "marker-vertical",
                            markerNormal: "marker-normal",
                            markerLarge: "marker-large",
                            markerSub: "marker-sub",
                            value: "value",
                            valueHorizontal: "value-horizontal",
                            valueVertical: "value-vertical",
                            valueNormal: "value-normal",
                            valueLarge: "value-large",
                            valueSub: "value-sub"
                        }
                    };
                t.format && !t.ariaFormat && (t.ariaFormat = t.format), Object.keys(n).forEach(function(i) {
                    if (!e(t[i]) && void 0 === s[i]) {
                        if (n[i].r) throw new Error("noUiSlider (" + F + "): '" + i + "' is required.");
                        return !0
                    }
                    n[i].t(a, e(t[i]) ? t[i] : s[i])
                }), a.pips = t.pips;
                var i = document.createElement("div"),
                    r = void 0 !== i.style.msTransform,
                    o = void 0 !== i.style.transform;
                a.transformRule = o ? "transform" : r ? "msTransform" : "webkitTransform";
                return a.style = [
                    ["left", "top"],
                    ["right", "bottom"]
                ][a.dir][a.ort], a
            }

            function z(e, a, r) {
                function u(e, t) {
                    var a = ee.createElement("div");
                    return t && o(a, t), e.appendChild(a), a
                }

                function _(e, t) {
                    var n = u(e, a.cssClasses.origin),
                        s = u(n, a.cssClasses.handle);
                    return s.setAttribute("data-handle", t), s.setAttribute("tabindex", "0"), s.setAttribute("role", "slider"), s.setAttribute("aria-orientation", a.ort ? "vertical" : "horizontal"), 0 === t ? o(s, a.cssClasses.handleLower) : t === a.handles - 1 && o(s, a.cssClasses.handleUpper), n
                }

                function m(e, t) {
                    return !!t && u(e, a.cssClasses.connect)
                }

                function c(e, t) {
                    return !!a.tooltips[t] && u(e.firstChild, a.cssClasses.tooltip)
                }

                function h(e, t, n) {
                    function s(e, t) {
                        var n = t === a.cssClasses.value,
                            s = n ? r : d;
                        return t + " " + (n ? l : _)[a.ort] + " " + s[e]
                    }
                    var i = ee.createElement("div"),
                        r = [a.cssClasses.valueNormal, a.cssClasses.valueLarge, a.cssClasses.valueSub],
                        d = [a.cssClasses.markerNormal, a.cssClasses.markerLarge, a.cssClasses.markerSub],
                        l = [a.cssClasses.valueHorizontal, a.cssClasses.valueVertical],
                        _ = [a.cssClasses.markerHorizontal, a.cssClasses.markerVertical];
                    return o(i, a.cssClasses.pips), o(i, 0 === a.ort ? a.cssClasses.pipsHorizontal : a.cssClasses.pipsVertical), Object.keys(e).forEach(function(r) {
                        ! function(e, r) {
                            r[1] = r[1] && t ? t(r[0], r[1]) : r[1];
                            var o = u(i, !1);
                            o.className = s(r[1], a.cssClasses.marker), o.style[a.style] = e + "%", r[1] && ((o = u(i, !1)).className = s(r[1], a.cssClasses.value), o.setAttribute("data-value", r[0]), o.style[a.style] = e + "%", o.innerText = n.to(r[0]))
                        }(r, e[r])
                    }), i
                }

                function p() {
                    J && (! function(e) {
                        e.parentElement.removeChild(e)
                    }(J), J = null)
                }

                function f(e) {
                    p();
                    var t = e.mode,
                        a = e.density || 1,
                        n = e.filter || !1,
                        s = function(e, t, a) {
                            function n(e, t) {
                                return (e + t).toFixed(7) / 1
                            }
                            var s = {},
                                i = K.xVal[0],
                                r = K.xVal[K.xVal.length - 1],
                                o = !1,
                                d = !1,
                                l = 0;
                            return (a = function(e) {
                                return e.filter(function(e) {
                                    return !this[e] && (this[e] = !0)
                                }, {})
                            }(a.slice().sort(function(e, t) {
                                return e - t
                            })))[0] !== i && (a.unshift(i), o = !0), a[a.length - 1] !== r && (a.push(r), d = !0), a.forEach(function(i, r) {
                                var u, _, m, c, h, p, f, M, y, g = i,
                                    L = a[r + 1];
                                if ("steps" === t && (u = K.xNumSteps[r]), u || (u = L - g), !1 !== g && void 0 !== L)
                                    for (u = Math.max(u, 1e-7), _ = g; _ <= L; _ = n(_, u)) {
                                        for (f = (h = (c = K.toStepping(_)) - l) / e, y = h / (M = Math.round(f)), m = 1; m <= M; m += 1) s[(l + m * y).toFixed(5)] = ["x", 0];
                                        p = a.indexOf(_) > -1 ? 1 : "steps" === t ? 2 : 0, !r && o && (p = 0), _ === L && d || (s[c.toFixed(5)] = [_, p]), l = c
                                    }
                            }), s
                        }(a, t, function(e, t, a) {
                            if ("range" === e || "steps" === e) return K.xVal;
                            if ("count" === e) {
                                if (t < 2) throw new Error("noUiSlider (" + F + "): 'values' (>= 2) required for mode 'count'.");
                                var n = t - 1,
                                    s = 100 / n;
                                for (t = []; n--;) t[n] = n * s;
                                t.push(100), e = "positions"
                            }
                            return "positions" === e ? t.map(function(e) {
                                return K.fromStepping(a ? K.getStep(e) : e)
                            }) : "values" === e ? a ? t.map(function(e) {
                                return K.fromStepping(K.getStep(K.toStepping(e)))
                            }) : t : void 0
                        }(t, e.values || !1, e.stepped || !1)),
                        i = e.format || {
                            to: Math.round
                        };
                    return J = V.appendChild(h(s, n, i))
                }

                function M() {
                    var e = I.getBoundingClientRect(),
                        t = "offset" + ["Width", "Height"][a.ort];
                    return 0 === a.ort ? e.width || I[t] : e.height || I[t]
                }

                function y(e, t, n, s) {
                    var i = function(i) {
                            return !!(i = function(e, t, a) {
                                var n, s, i = 0 === e.type.indexOf("touch"),
                                    r = 0 === e.type.indexOf("mouse"),
                                    o = 0 === e.type.indexOf("pointer");
                                0 === e.type.indexOf("MSPointer") && (o = !0);
                                if (i) {
                                    var d = function(e) {
                                        return e.target === a || a.contains(e.target)
                                    };
                                    if ("touchstart" === e.type) {
                                        var u = Array.prototype.filter.call(e.touches, d);
                                        if (u.length > 1) return !1;
                                        n = u[0].pageX, s = u[0].pageY
                                    } else {
                                        var _ = Array.prototype.find.call(e.changedTouches, d);
                                        if (!_) return !1;
                                        n = _.pageX, s = _.pageY
                                    }
                                }
                                t = t || l(ee), (r || o) && (n = e.clientX + t.x, s = e.clientY + t.y);
                                return e.pageOffset = t, e.points = [n, s], e.cursor = r || o, e
                            }(i, s.pageOffset, s.target || t)) && (!(V.hasAttribute("disabled") && !s.doNotReject) && (!(function(e, t) {
                                return e.classList ? e.classList.contains(t) : new RegExp("\\b" + t + "\\b").test(e.className)
                            }(V, a.cssClasses.tap) && !s.doNotReject) && (!(e === B.start && void 0 !== i.buttons && i.buttons > 1) && ((!s.hover || !i.buttons) && (U || i.preventDefault(), i.calcPoint = i.points[a.ort], void n(i, s))))))
                        },
                        r = [];
                    return e.split(" ").forEach(function(e) {
                        t.addEventListener(e, i, !!U && {
                            passive: !0
                        }), r.push([e, i])
                    }), r
                }

                function g(e) {
                    var t = 100 * (e - function(e, t) {
                        var a = e.getBoundingClientRect(),
                            n = e.ownerDocument,
                            s = n.documentElement,
                            i = l(n);
                        return /webkit.*Chrome.*Mobile/i.test(navigator.userAgent) && (i.x = 0), t ? a.top + i.y - s.clientTop : a.left + i.x - s.clientLeft
                    }(I, a.ort)) / M();
                    return t = s(t), a.dir ? 100 - t : t
                }

                function L(e, t) {
                    "mouseout" === e.type && "HTML" === e.target.nodeName && null === e.relatedTarget && v(e, t)
                }

                function Y(e, t) {
                    if (-1 === navigator.appVersion.indexOf("MSIE 9") && 0 === e.buttons && 0 !== t.buttonsProperty) return v(e, t);
                    var n = (a.dir ? -1 : 1) * (e.calcPoint - t.startCalcPoint);
                    j(n > 0, 100 * n / t.baseSize, t.locations, t.handleNumbers)
                }

                function v(e, n) {
                    n.handle && (d(n.handle, a.cssClasses.active), Z -= 1), n.listeners.forEach(function(e) {
                        te.removeEventListener(e[0], e[1])
                    }), 0 === Z && (d(V, a.cssClasses.drag), P(), e.cursor && (ae.style.cursor = "", ae.removeEventListener("selectstart", t))), n.handleNumbers.forEach(function(e) {
                        T("change", e), T("set", e), T("end", e)
                    })
                }

                function w(e, n) {
                    var s;
                    if (1 === n.handleNumbers.length) {
                        var i = $[n.handleNumbers[0]];
                        if (i.hasAttribute("disabled")) return !1;
                        s = i.children[0], Z += 1, o(s, a.cssClasses.active)
                    }
                    e.stopPropagation();
                    var r = [],
                        d = y(B.move, te, Y, {
                            target: e.target,
                            handle: s,
                            listeners: r,
                            startCalcPoint: e.calcPoint,
                            baseSize: M(),
                            pageOffset: e.pageOffset,
                            handleNumbers: n.handleNumbers,
                            buttonsProperty: e.buttons,
                            locations: q.slice()
                        }),
                        l = y(B.end, te, v, {
                            target: e.target,
                            handle: s,
                            listeners: r,
                            doNotReject: !0,
                            handleNumbers: n.handleNumbers
                        }),
                        u = y("mouseout", te, L, {
                            target: e.target,
                            handle: s,
                            listeners: r,
                            doNotReject: !0,
                            handleNumbers: n.handleNumbers
                        });
                    r.push.apply(r, d.concat(l, u)), e.cursor && (ae.style.cursor = getComputedStyle(e.target).cursor, $.length > 1 && o(V, a.cssClasses.drag), ae.addEventListener("selectstart", t, !1)), n.handleNumbers.forEach(function(e) {
                        T("start", e)
                    })
                }

                function k(e) {
                    e.stopPropagation();
                    var t = g(e.calcPoint),
                        s = function(e) {
                            var t = 100,
                                a = !1;
                            return $.forEach(function(n, s) {
                                if (!n.hasAttribute("disabled")) {
                                    var i = Math.abs(q[s] - e);
                                    (i < t || 100 === i && 100 === t) && (a = s, t = i)
                                }
                            }), a
                        }(t);
                    if (!1 === s) return !1;
                    a.events.snap || n(V, a.cssClasses.tap, a.animationDuration), O(s, t, !0, !0), P(), T("slide", s, !0), T("update", s, !0), T("change", s, !0), T("set", s, !0), a.events.snap && w(e, {
                        handleNumbers: [s]
                    })
                }

                function D(e) {
                    var t = g(e.calcPoint),
                        a = K.getStep(t),
                        n = K.fromStepping(a);
                    Object.keys(X).forEach(function(e) {
                        "hover" === e.split(".")[0] && X[e].forEach(function(e) {
                            e.call(R, n)
                        })
                    })
                }

                function b(e, t) {
                    X[e] = X[e] || [], X[e].push(t), "update" === e.split(".")[0] && $.forEach(function(e, t) {
                        T("update", t)
                    })
                }

                function T(e, t, n) {
                    Object.keys(X).forEach(function(s) {
                        var i = s.split(".")[0];
                        e === i && X[s].forEach(function(e) {
                            e.call(R, Q.map(a.format.to), t, Q.slice(), n || !1, q.slice())
                        })
                    })
                }

                function S(e) {
                    return e + "%"
                }

                function x(e, t, n, i, r, o) {
                    return $.length > 1 && (i && t > 0 && (n = Math.max(n, e[t - 1] + a.margin)), r && t < $.length - 1 && (n = Math.min(n, e[t + 1] - a.margin))), $.length > 1 && a.limit && (i && t > 0 && (n = Math.min(n, e[t - 1] + a.limit)), r && t < $.length - 1 && (n = Math.max(n, e[t + 1] - a.limit))), a.padding && (0 === t && (n = Math.max(n, a.padding[0])), t === $.length - 1 && (n = Math.min(n, 100 - a.padding[1]))), n = K.getStep(n), !((n = s(n)) === e[t] && !o) && n
                }

                function H(e, t) {
                    var n = a.ort;
                    return (n ? t : e) + ", " + (n ? e : t)
                }

                function j(e, t, a, n) {
                    var s = a.slice(),
                        i = [!e, e],
                        r = [e, !e];
                    n = n.slice(), e && n.reverse(), n.length > 1 ? n.forEach(function(e, a) {
                        var n = x(s, e, s[e] + t, i[a], r[a], !1);
                        !1 === n ? t = 0 : (t = n - s[e], s[e] = n)
                    }) : i = r = [!0];
                    var o = !1;
                    n.forEach(function(e, n) {
                        o = O(e, a[e] + t, i[n], r[n]) || o
                    }), o && n.forEach(function(e) {
                        T("update", e), T("slide", e)
                    })
                }

                function C(e, t) {
                    return a.dir ? 100 - e - t : e
                }

                function P() {
                    G.forEach(function(e) {
                        var t = q[e] > 50 ? -1 : 1,
                            a = 3 + ($.length + t * e);
                        $[e].style.zIndex = a
                    })
                }

                function O(e, t, n, s) {
                    return !1 !== (t = x(q, e, t, n, s, !1)) && (function(e, t) {
                        q[e] = t, Q[e] = K.fromStepping(t);
                        var n = "translate(" + H(S(C(t, 0) - ne), "0") + ")";
                        $[e].style[a.transformRule] = n, E(e), E(e + 1)
                    }(e, t), !0)
                }

                function E(e) {
                    if (N[e]) {
                        var t = 0,
                            n = 100;
                        0 !== e && (t = q[e - 1]), e !== N.length - 1 && (n = q[e]);
                        var s = n - t,
                            i = "translate(" + H(S(C(t, s)), "0") + ")",
                            r = "scale(" + H(s / 100, "1") + ")";
                        N[e].style[a.transformRule] = i + " " + r
                    }
                }

                function W(e, t) {
                    var s = i(e),
                        r = void 0 === q[0];
                    t = void 0 === t || !!t, a.animate && !r && n(V, a.cssClasses.tap, a.animationDuration), G.forEach(function(e) {
                        O(e, function(e, t) {
                            return null === e || !1 === e || void 0 === e ? q[t] : ("number" == typeof e && (e = String(e)), e = a.format.from(e), !1 === (e = K.toStepping(e)) || isNaN(e) ? q[t] : e)
                        }(s[e], e), !0, !1)
                    }), G.forEach(function(e) {
                        O(e, q[e], !0, !0)
                    }), P(), G.forEach(function(e) {
                        T("update", e), null !== s[e] && t && T("set", e)
                    })
                }

                function z() {
                    var e = Q.map(a.format.to);
                    return 1 === e.length ? e[0] : e
                }
                var I, $, N, R, J, B = window.navigator.pointerEnabled ? {
                        start: "pointerdown",
                        move: "pointermove",
                        end: "pointerup"
                    } : window.navigator.msPointerEnabled ? {
                        start: "MSPointerDown",
                        move: "MSPointerMove",
                        end: "MSPointerUp"
                    } : {
                        start: "mousedown touchstart",
                        move: "mousemove touchmove",
                        end: "mouseup touchend"
                    },
                    U = window.CSS && CSS.supports && CSS.supports("touch-action", "none") && function() {
                        var e = !1;
                        try {
                            var t = Object.defineProperty({}, "passive", {
                                get: function() {
                                    e = !0
                                }
                            });
                            window.addEventListener("test", null, t)
                        } catch (e) {}
                        return e
                    }(),
                    V = e,
                    q = [],
                    G = [],
                    Z = 0,
                    K = a.spectrum,
                    Q = [],
                    X = {},
                    ee = e.ownerDocument,
                    te = ee.documentElement,
                    ae = ee.body,
                    ne = "rtl" === ee.dir || 1 === a.ort ? 0 : 100;
                return function(e) {
                        o(e, a.cssClasses.target), 0 === a.dir ? o(e, a.cssClasses.ltr) : o(e, a.cssClasses.rtl), 0 === a.ort ? o(e, a.cssClasses.horizontal) : o(e, a.cssClasses.vertical), I = u(e, a.cssClasses.base)
                    }(V),
                    function(e, t) {
                        var n = u(t, a.cssClasses.connects);
                        $ = [], (N = []).push(m(n, e[0]));
                        for (var s = 0; s < a.handles; s++) $.push(_(t, s)), G[s] = s, N.push(m(n, e[s + 1]))
                    }(a.connect, I),
                    function(e) {
                        e.fixed || $.forEach(function(e, t) {
                            y(B.start, e.children[0], w, {
                                handleNumbers: [t]
                            })
                        }), e.tap && y(B.start, I, k, {}), e.hover && y(B.move, I, D, {
                            hover: !0
                        }), e.drag && N.forEach(function(t, n) {
                            if (!1 !== t && 0 !== n && n !== N.length - 1) {
                                var s = $[n - 1],
                                    i = $[n],
                                    r = [t];
                                o(t, a.cssClasses.draggable), e.fixed && (r.push(s.children[0]), r.push(i.children[0])), r.forEach(function(e) {
                                    y(B.start, e, w, {
                                        handles: [s, i],
                                        handleNumbers: [n - 1, n]
                                    })
                                })
                            }
                        })
                    }(a.events), W(a.start), R = {
                        destroy: function() {
                            for (var e in a.cssClasses) a.cssClasses.hasOwnProperty(e) && d(V, a.cssClasses[e]);
                            for (; V.firstChild;) V.removeChild(V.firstChild);
                            delete V.noUiSlider
                        },
                        steps: function() {
                            return q.map(function(e, t) {
                                var a = K.getNearbySteps(e),
                                    n = Q[t],
                                    s = a.thisStep.step,
                                    i = null;
                                !1 !== s && n + s > a.stepAfter.startValue && (s = a.stepAfter.startValue - n), i = n > a.thisStep.startValue ? a.thisStep.step : !1 !== a.stepBefore.step && n - a.stepBefore.highestStep, 100 === e ? s = null : 0 === e && (i = null);
                                var r = K.countStepDecimals();
                                return null !== s && !1 !== s && (s = Number(s.toFixed(r))), null !== i && !1 !== i && (i = Number(i.toFixed(r))), [i, s]
                            })
                        },
                        on: b,
                        off: function(e) {
                            var t = e && e.split(".")[0],
                                a = t && e.substring(t.length);
                            Object.keys(X).forEach(function(e) {
                                var n = e.split(".")[0],
                                    s = e.substring(n.length);
                                t && t !== n || a && a !== s || delete X[e]
                            })
                        },
                        get: z,
                        set: W,
                        reset: function(e) {
                            W(a.start, e)
                        },
                        __moveHandles: function(e, t, a) {
                            j(e, t, q, a)
                        },
                        options: r,
                        updateOptions: function(e, t) {
                            var n = z(),
                                s = ["margin", "limit", "padding", "range", "animate", "snap", "step", "format"];
                            s.forEach(function(t) {
                                void 0 !== e[t] && (r[t] = e[t])
                            });
                            var i = A(r);
                            s.forEach(function(t) {
                                void 0 !== e[t] && (a[t] = i[t])
                            }), K = i.spectrum, a.margin = i.margin, a.limit = i.limit, a.padding = i.padding, a.pips && f(a.pips), q = [], W(e.start || n, t)
                        },
                        target: V,
                        removePips: p,
                        pips: f
                    }, a.pips && f(a.pips), a.tooltips && function() {
                        var e = $.map(c);
                        b("update", function(t, n, s) {
                            if (e[n]) {
                                var i = t[n];
                                !0 !== a.tooltips[n] && (i = a.tooltips[n].to(s[n])), e[n].innerHTML = i
                            }
                        })
                    }(), b("update", function(e, t, n, s, i) {
                        G.forEach(function(e) {
                            var t = $[e],
                                s = x(q, e, 0, !0, !0, !0),
                                r = x(q, e, 100, !0, !0, !0),
                                o = i[e],
                                d = a.ariaFormat.to(n[e]);
                            t.children[0].setAttribute("aria-valuemin", s.toFixed(1)), t.children[0].setAttribute("aria-valuemax", r.toFixed(1)), t.children[0].setAttribute("aria-valuenow", o.toFixed(1)), t.children[0].setAttribute("aria-valuetext", d)
                        })
                    }), R
            }
            var F = "11.1.0";
            M.prototype.getMargin = function(e) {
                var t = this.xNumSteps[0];
                if (t && e / t % 1 != 0) throw new Error("noUiSlider (" + F + "): 'limit', 'margin' and 'padding' must be divisible by step.");
                return 2 === this.xPct.length && _(this.xVal, e)
            }, M.prototype.toStepping = function(e) {
                return e = c(this.xVal, this.xPct, e)
            }, M.prototype.fromStepping = function(e) {
                return function(e, t, a) {
                    if (a >= 100) return e.slice(-1)[0];
                    var n = m(a, t),
                        s = e[n - 1],
                        i = e[n],
                        r = t[n - 1];
                    return function(e, t) {
                        return t * (e[1] - e[0]) / 100 + e[0]
                    }([s, i], (a - r) * u(r, t[n]))
                }(this.xVal, this.xPct, e)
            }, M.prototype.getStep = function(e) {
                return e = h(this.xPct, this.xSteps, this.snap, e)
            }, M.prototype.getNearbySteps = function(e) {
                var t = m(e, this.xPct);
                return {
                    stepBefore: {
                        startValue: this.xVal[t - 2],
                        step: this.xNumSteps[t - 2],
                        highestStep: this.xHighestCompleteStep[t - 2]
                    },
                    thisStep: {
                        startValue: this.xVal[t - 1],
                        step: this.xNumSteps[t - 1],
                        highestStep: this.xHighestCompleteStep[t - 1]
                    },
                    stepAfter: {
                        startValue: this.xVal[t - 0],
                        step: this.xNumSteps[t - 0],
                        highestStep: this.xHighestCompleteStep[t - 0]
                    }
                }
            }, M.prototype.countStepDecimals = function() {
                var e = this.xNumSteps.map(r);
                return Math.max.apply(null, e)
            }, M.prototype.convert = function(e) {
                return this.getStep(this.toStepping(e))
            };
            var I = {
                to: function(e) {
                    return void 0 !== e && e.toFixed(2)
                },
                from: Number
            };
            return {
                version: F,
                create: function(e, t) {
                    if (!e || !e.nodeName) throw new Error("noUiSlider (" + F + "): create requires a single element, got: " + e);
                    if (e.noUiSlider) throw new Error("noUiSlider (" + F + "): Slider was already initialized.");
                    var a = z(e, A(t), t);
                    return e.noUiSlider = a, a
                }
            }
        })
    }, {}],
    7: [function(e, t, a) {
        ! function(e, t, a, n) {
            function s(t, a) {
                this.settings = null, this.options = e.extend({}, s.Defaults, a), this.$element = e(t), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
                    time: null,
                    target: null,
                    pointer: null,
                    stage: {
                        start: null,
                        current: null
                    },
                    direction: null
                }, this._states = {
                    current: {},
                    tags: {
                        initializing: ["busy"],
                        animating: ["busy"],
                        dragging: ["interacting"]
                    }
                }, e.each(["onResize", "onThrottledResize"], e.proxy(function(t, a) {
                    this._handlers[a] = e.proxy(this[a], this)
                }, this)), e.each(s.Plugins, e.proxy(function(e, t) {
                    this._plugins[e.charAt(0).toLowerCase() + e.slice(1)] = new t(this)
                }, this)), e.each(s.Workers, e.proxy(function(t, a) {
                    this._pipe.push({
                        filter: a.filter,
                        run: e.proxy(a.run, this)
                    })
                }, this)), this.setup(), this.initialize()
            }
            s.Defaults = {
                items: 3,
                loop: !1,
                center: !1,
                rewind: !1,
                checkVisibility: !0,
                mouseDrag: !0,
                touchDrag: !0,
                pullDrag: !0,
                freeDrag: !1,
                margin: 0,
                stagePadding: 0,
                merge: !1,
                mergeFit: !0,
                autoWidth: !1,
                startPosition: 0,
                rtl: !1,
                smartSpeed: 250,
                fluidSpeed: !1,
                dragEndSpeed: !1,
                responsive: {},
                responsiveRefreshRate: 200,
                responsiveBaseElement: t,
                fallbackEasing: "swing",
                slideTransition: "",
                info: !1,
                nestedItemSelector: !1,
                itemElement: "div",
                stageElement: "div",
                refreshClass: "owl-refresh",
                loadedClass: "owl-loaded",
                loadingClass: "owl-loading",
                rtlClass: "owl-rtl",
                responsiveClass: "owl-responsive",
                dragClass: "owl-drag",
                itemClass: "owl-item",
                stageClass: "owl-stage",
                stageOuterClass: "owl-stage-outer",
                grabClass: "owl-grab"
            }, s.Width = {
                Default: "default",
                Inner: "inner",
                Outer: "outer"
            }, s.Type = {
                Event: "event",
                State: "state"
            }, s.Plugins = {}, s.Workers = [{
                filter: ["width", "settings"],
                run: function() {
                    this._width = this.$element.width()
                }
            }, {
                filter: ["width", "items", "settings"],
                run: function(e) {
                    e.current = this._items && this._items[this.relative(this._current)]
                }
            }, {
                filter: ["items", "settings"],
                run: function() {
                    this.$stage.children(".cloned").remove()
                }
            }, {
                filter: ["width", "items", "settings"],
                run: function(e) {
                    var t = this.settings.margin || "",
                        a = !this.settings.autoWidth,
                        n = this.settings.rtl,
                        s = {
                            width: "auto",
                            "margin-left": n ? t : "",
                            "margin-right": n ? "" : t
                        };
                    !a && this.$stage.children().css(s), e.css = s
                }
            }, {
                filter: ["width", "items", "settings"],
                run: function(e) {
                    var t = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
                        a = null,
                        n = this._items.length,
                        s = !this.settings.autoWidth,
                        i = [];
                    for (e.items = {
                            merge: !1,
                            width: t
                        }; n--;) a = this._mergers[n], a = this.settings.mergeFit && Math.min(a, this.settings.items) || a, e.items.merge = a > 1 || e.items.merge, i[n] = s ? t * a : this._items[n].width();
                    this._widths = i
                }
            }, {
                filter: ["items", "settings"],
                run: function() {
                    var t = [],
                        a = this._items,
                        n = this.settings,
                        s = Math.max(2 * n.items, 4),
                        i = 2 * Math.ceil(a.length / 2),
                        r = n.loop && a.length ? n.rewind ? s : Math.max(s, i) : 0,
                        o = "",
                        d = "";
                    for (r /= 2; r > 0;) t.push(this.normalize(t.length / 2, !0)), o += a[t[t.length - 1]][0].outerHTML, t.push(this.normalize(a.length - 1 - (t.length - 1) / 2, !0)), d = a[t[t.length - 1]][0].outerHTML + d, r -= 1;
                    this._clones = t, e(o).addClass("cloned").appendTo(this.$stage), e(d).addClass("cloned").prependTo(this.$stage)
                }
            }, {
                filter: ["width", "items", "settings"],
                run: function() {
                    for (var e = this.settings.rtl ? 1 : -1, t = this._clones.length + this._items.length, a = -1, n = 0, s = 0, i = []; ++a < t;) n = i[a - 1] || 0, s = this._widths[this.relative(a)] + this.settings.margin, i.push(n + s * e);
                    this._coordinates = i
                }
            }, {
                filter: ["width", "items", "settings"],
                run: function() {
                    var e = this.settings.stagePadding,
                        t = this._coordinates,
                        a = {
                            width: Math.ceil(Math.abs(t[t.length - 1])) + 2 * e,
                            "padding-left": e || "",
                            "padding-right": e || ""
                        };
                    this.$stage.css(a)
                }
            }, {
                filter: ["width", "items", "settings"],
                run: function(e) {
                    var t = this._coordinates.length,
                        a = !this.settings.autoWidth,
                        n = this.$stage.children();
                    if (a && e.items.merge)
                        for (; t--;) e.css.width = this._widths[this.relative(t)], n.eq(t).css(e.css);
                    else a && (e.css.width = e.items.width, n.css(e.css))
                }
            }, {
                filter: ["items"],
                run: function() {
                    this._coordinates.length < 1 && this.$stage.removeAttr("style")
                }
            }, {
                filter: ["width", "items", "settings"],
                run: function(e) {
                    e.current = e.current ? this.$stage.children().index(e.current) : 0, e.current = Math.max(this.minimum(), Math.min(this.maximum(), e.current)), this.reset(e.current)
                }
            }, {
                filter: ["position"],
                run: function() {
                    this.animate(this.coordinates(this._current))
                }
            }, {
                filter: ["width", "position", "items", "settings"],
                run: function() {
                    var e, t, a, n, s = this.settings.rtl ? 1 : -1,
                        i = 2 * this.settings.stagePadding,
                        r = this.coordinates(this.current()) + i,
                        o = r + this.width() * s,
                        d = [];
                    for (a = 0, n = this._coordinates.length; a < n; a++) e = this._coordinates[a - 1] || 0, t = Math.abs(this._coordinates[a]) + i * s, (this.op(e, "<=", r) && this.op(e, ">", o) || this.op(t, "<", r) && this.op(t, ">", o)) && d.push(a);
                    this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + d.join("), :eq(") + ")").addClass("active"), this.$stage.children(".center").removeClass("center"), this.settings.center && this.$stage.children().eq(this.current()).addClass("center")
                }
            }], s.prototype.initializeStage = function() {
                this.$stage = this.$element.find("." + this.settings.stageClass), this.$stage.length || (this.$element.addClass(this.options.loadingClass), this.$stage = e("<" + this.settings.stageElement + ">", {
                    class: this.settings.stageClass
                }).wrap(e("<div/>", {
                    class: this.settings.stageOuterClass
                })), this.$element.append(this.$stage.parent()))
            }, s.prototype.initializeItems = function() {
                var t = this.$element.find(".owl-item");
                if (t.length) return this._items = t.get().map(function(t) {
                    return e(t)
                }), this._mergers = this._items.map(function() {
                    return 1
                }), void this.refresh();
                this.replace(this.$element.children().not(this.$stage.parent())), this.isVisible() ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass)
            }, s.prototype.initialize = function() {
                if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) {
                    var e, t, a;
                    e = this.$element.find("img"), t = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : void 0, a = this.$element.children(t).width(), e.length && a <= 0 && this.preloadAutoWidthImages(e)
                }
                this.initializeStage(), this.initializeItems(), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized")
            }, s.prototype.isVisible = function() {
                return !this.settings.checkVisibility || this.$element.is(":visible")
            }, s.prototype.setup = function() {
                var t = this.viewport(),
                    a = this.options.responsive,
                    n = -1,
                    s = null;
                a ? (e.each(a, function(e) {
                    e <= t && e > n && (n = Number(e))
                }), "function" == typeof(s = e.extend({}, this.options, a[n])).stagePadding && (s.stagePadding = s.stagePadding()), delete s.responsive, s.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + n))) : s = e.extend({}, this.options), this.trigger("change", {
                    property: {
                        name: "settings",
                        value: s
                    }
                }), this._breakpoint = n, this.settings = s, this.invalidate("settings"), this.trigger("changed", {
                    property: {
                        name: "settings",
                        value: this.settings
                    }
                })
            }, s.prototype.optionsLogic = function() {
                this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
            }, s.prototype.prepare = function(t) {
                var a = this.trigger("prepare", {
                    content: t
                });
                return a.data || (a.data = e("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(t)), this.trigger("prepared", {
                    content: a.data
                }), a.data
            }, s.prototype.update = function() {
                for (var t = 0, a = this._pipe.length, n = e.proxy(function(e) {
                        return this[e]
                    }, this._invalidated), s = {}; t < a;)(this._invalidated.all || e.grep(this._pipe[t].filter, n).length > 0) && this._pipe[t].run(s), t++;
                this._invalidated = {}, !this.is("valid") && this.enter("valid")
            }, s.prototype.width = function(e) {
                switch (e = e || s.Width.Default) {
                    case s.Width.Inner:
                    case s.Width.Outer:
                        return this._width;
                    default:
                        return this._width - 2 * this.settings.stagePadding + this.settings.margin
                }
            }, s.prototype.refresh = function() {
                this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed")
            }, s.prototype.onThrottledResize = function() {
                t.clearTimeout(this.resizeTimer), this.resizeTimer = t.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
            }, s.prototype.onResize = function() {
                return !!this._items.length && (this._width !== this.$element.width() && (!!this.isVisible() && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))))
            }, s.prototype.registerEventHandlers = function() {
                e.support.transition && this.$stage.on(e.support.transition.end + ".owl.core", e.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(t, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", e.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function() {
                    return !1
                })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", e.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", e.proxy(this.onDragEnd, this)))
            }, s.prototype.onDragStart = function(t) {
                var n = null;
                3 !== t.which && (e.support.transform ? n = {
                    x: (n = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","))[16 === n.length ? 12 : 4],
                    y: n[16 === n.length ? 13 : 5]
                } : (n = this.$stage.position(), n = {
                    x: this.settings.rtl ? n.left + this.$stage.width() - this.width() + this.settings.margin : n.left,
                    y: n.top
                }), this.is("animating") && (e.support.transform ? this.animate(n.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === t.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = e(t.target), this._drag.stage.start = n, this._drag.stage.current = n, this._drag.pointer = this.pointer(t), e(a).on("mouseup.owl.core touchend.owl.core", e.proxy(this.onDragEnd, this)), e(a).one("mousemove.owl.core touchmove.owl.core", e.proxy(function(t) {
                    var n = this.difference(this._drag.pointer, this.pointer(t));
                    e(a).on("mousemove.owl.core touchmove.owl.core", e.proxy(this.onDragMove, this)), Math.abs(n.x) < Math.abs(n.y) && this.is("valid") || (t.preventDefault(), this.enter("dragging"), this.trigger("drag"))
                }, this)))
            }, s.prototype.onDragMove = function(e) {
                var t = null,
                    a = null,
                    n = null,
                    s = this.difference(this._drag.pointer, this.pointer(e)),
                    i = this.difference(this._drag.stage.start, s);
                this.is("dragging") && (e.preventDefault(), this.settings.loop ? (t = this.coordinates(this.minimum()), a = this.coordinates(this.maximum() + 1) - t, i.x = ((i.x - t) % a + a) % a + t) : (t = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), a = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), n = this.settings.pullDrag ? -1 * s.x / 5 : 0, i.x = Math.max(Math.min(i.x, t + n), a + n)), this._drag.stage.current = i, this.animate(i.x))
            }, s.prototype.onDragEnd = function(t) {
                var n = this.difference(this._drag.pointer, this.pointer(t)),
                    s = this._drag.stage.current,
                    i = n.x > 0 ^ this.settings.rtl ? "left" : "right";
                e(a).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== n.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(s.x, 0 !== n.x ? i : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = i, (Math.abs(n.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function() {
                    return !1
                })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"))
            }, s.prototype.closest = function(t, a) {
                var n = -1,
                    s = this.width(),
                    i = this.coordinates();
                return this.settings.freeDrag || e.each(i, e.proxy(function(e, r) {
                    return "left" === a && t > r - 30 && t < r + 30 ? n = e : "right" === a && t > r - s - 30 && t < r - s + 30 ? n = e + 1 : this.op(t, "<", r) && this.op(t, ">", void 0 !== i[e + 1] ? i[e + 1] : r - s) && (n = "left" === a ? e + 1 : e), -1 === n
                }, this)), this.settings.loop || (this.op(t, ">", i[this.minimum()]) ? n = t = this.minimum() : this.op(t, "<", i[this.maximum()]) && (n = t = this.maximum())), n
            }, s.prototype.animate = function(t) {
                var a = this.speed() > 0;
                this.is("animating") && this.onTransitionEnd(), a && (this.enter("animating"), this.trigger("translate")), e.support.transform3d && e.support.transition ? this.$stage.css({
                    transform: "translate3d(" + t + "px,0px,0px)",
                    transition: this.speed() / 1e3 + "s" + (this.settings.slideTransition ? " " + this.settings.slideTransition : "")
                }) : a ? this.$stage.animate({
                    left: t + "px"
                }, this.speed(), this.settings.fallbackEasing, e.proxy(this.onTransitionEnd, this)) : this.$stage.css({
                    left: t + "px"
                })
            }, s.prototype.is = function(e) {
                return this._states.current[e] && this._states.current[e] > 0
            }, s.prototype.current = function(e) {
                if (void 0 === e) return this._current;
                if (0 !== this._items.length) {
                    if (e = this.normalize(e), this._current !== e) {
                        var t = this.trigger("change", {
                            property: {
                                name: "position",
                                value: e
                            }
                        });
                        void 0 !== t.data && (e = this.normalize(t.data)), this._current = e, this.invalidate("position"), this.trigger("changed", {
                            property: {
                                name: "position",
                                value: this._current
                            }
                        })
                    }
                    return this._current
                }
            }, s.prototype.invalidate = function(t) {
                return "string" === e.type(t) && (this._invalidated[t] = !0, this.is("valid") && this.leave("valid")), e.map(this._invalidated, function(e, t) {
                    return t
                })
            }, s.prototype.reset = function(e) {
                void 0 !== (e = this.normalize(e)) && (this._speed = 0, this._current = e, this.suppress(["translate", "translated"]), this.animate(this.coordinates(e)), this.release(["translate", "translated"]))
            }, s.prototype.normalize = function(e, t) {
                var a = this._items.length,
                    n = t ? 0 : this._clones.length;
                return !this.isNumeric(e) || a < 1 ? e = void 0 : (e < 0 || e >= a + n) && (e = ((e - n / 2) % a + a) % a + n / 2), e
            }, s.prototype.relative = function(e) {
                return e -= this._clones.length / 2, this.normalize(e, !0)
            }, s.prototype.maximum = function(e) {
                var t, a, n, s = this.settings,
                    i = this._coordinates.length;
                if (s.loop) i = this._clones.length / 2 + this._items.length - 1;
                else if (s.autoWidth || s.merge) {
                    if (t = this._items.length)
                        for (a = this._items[--t].width(), n = this.$element.width(); t-- && !((a += this._items[t].width() + this.settings.margin) > n););
                    i = t + 1
                } else i = s.center ? this._items.length - 1 : this._items.length - s.items;
                return e && (i -= this._clones.length / 2), Math.max(i, 0)
            }, s.prototype.minimum = function(e) {
                return e ? 0 : this._clones.length / 2
            }, s.prototype.items = function(e) {
                return void 0 === e ? this._items.slice() : (e = this.normalize(e, !0), this._items[e])
            }, s.prototype.mergers = function(e) {
                return void 0 === e ? this._mergers.slice() : (e = this.normalize(e, !0), this._mergers[e])
            }, s.prototype.clones = function(t) {
                var a = this._clones.length / 2,
                    n = a + this._items.length,
                    s = function(e) {
                        return e % 2 == 0 ? n + e / 2 : a - (e + 1) / 2
                    };
                return void 0 === t ? e.map(this._clones, function(e, t) {
                    return s(t)
                }) : e.map(this._clones, function(e, a) {
                    return e === t ? s(a) : null
                })
            }, s.prototype.speed = function(e) {
                return void 0 !== e && (this._speed = e), this._speed
            }, s.prototype.coordinates = function(t) {
                var a, n = 1,
                    s = t - 1;
                return void 0 === t ? e.map(this._coordinates, e.proxy(function(e, t) {
                    return this.coordinates(t)
                }, this)) : (this.settings.center ? (this.settings.rtl && (n = -1, s = t + 1), a = this._coordinates[t], a += (this.width() - a + (this._coordinates[s] || 0)) / 2 * n) : a = this._coordinates[s] || 0, a = Math.ceil(a))
            }, s.prototype.duration = function(e, t, a) {
                return 0 === a ? 0 : Math.min(Math.max(Math.abs(t - e), 1), 6) * Math.abs(a || this.settings.smartSpeed)
            }, s.prototype.to = function(e, t) {
                var a = this.current(),
                    n = null,
                    s = e - this.relative(a),
                    i = (s > 0) - (s < 0),
                    r = this._items.length,
                    o = this.minimum(),
                    d = this.maximum();
                this.settings.loop ? (!this.settings.rewind && Math.abs(s) > r / 2 && (s += -1 * i * r), (n = (((e = a + s) - o) % r + r) % r + o) !== e && n - s <= d && n - s > 0 && (a = n - s, e = n, this.reset(a))) : e = this.settings.rewind ? (e % (d += 1) + d) % d : Math.max(o, Math.min(d, e)), this.speed(this.duration(a, e, t)), this.current(e), this.isVisible() && this.update()
            }, s.prototype.next = function(e) {
                e = e || !1, this.to(this.relative(this.current()) + 1, e)
            }, s.prototype.prev = function(e) {
                e = e || !1, this.to(this.relative(this.current()) - 1, e)
            }, s.prototype.onTransitionEnd = function(e) {
                if (void 0 !== e && (e.stopPropagation(), (e.target || e.srcElement || e.originalTarget) !== this.$stage.get(0))) return !1;
                this.leave("animating"), this.trigger("translated")
            }, s.prototype.viewport = function() {
                var n;
                return this.options.responsiveBaseElement !== t ? n = e(this.options.responsiveBaseElement).width() : t.innerWidth ? n = t.innerWidth : a.documentElement && a.documentElement.clientWidth ? n = a.documentElement.clientWidth : console.warn("Can not detect viewport width."), n
            }, s.prototype.replace = function(t) {
                this.$stage.empty(), this._items = [], t && (t = t instanceof jQuery ? t : e(t)), this.settings.nestedItemSelector && (t = t.find("." + this.settings.nestedItemSelector)), t.filter(function() {
                    return 1 === this.nodeType
                }).each(e.proxy(function(e, t) {
                    t = this.prepare(t), this.$stage.append(t), this._items.push(t), this._mergers.push(1 * t.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)
                }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
            }, s.prototype.add = function(t, a) {
                var n = this.relative(this._current);
                a = void 0 === a ? this._items.length : this.normalize(a, !0), t = t instanceof jQuery ? t : e(t), this.trigger("add", {
                    content: t,
                    position: a
                }), t = this.prepare(t), 0 === this._items.length || a === this._items.length ? (0 === this._items.length && this.$stage.append(t), 0 !== this._items.length && this._items[a - 1].after(t), this._items.push(t), this._mergers.push(1 * t.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[a].before(t), this._items.splice(a, 0, t), this._mergers.splice(a, 0, 1 * t.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[n] && this.reset(this._items[n].index()), this.invalidate("items"), this.trigger("added", {
                    content: t,
                    position: a
                })
            }, s.prototype.remove = function(e) {
                void 0 !== (e = this.normalize(e, !0)) && (this.trigger("remove", {
                    content: this._items[e],
                    position: e
                }), this._items[e].remove(), this._items.splice(e, 1), this._mergers.splice(e, 1), this.invalidate("items"), this.trigger("removed", {
                    content: null,
                    position: e
                }))
            }, s.prototype.preloadAutoWidthImages = function(t) {
                t.each(e.proxy(function(t, a) {
                    this.enter("pre-loading"), a = e(a), e(new Image).one("load", e.proxy(function(e) {
                        a.attr("src", e.target.src), a.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh()
                    }, this)).attr("src", a.attr("src") || a.attr("data-src") || a.attr("data-src-retina"))
                }, this))
            }, s.prototype.destroy = function() {
                this.$element.off(".owl.core"), this.$stage.off(".owl.core"), e(a).off(".owl.core"), !1 !== this.settings.responsive && (t.clearTimeout(this.resizeTimer), this.off(t, "resize", this._handlers.onThrottledResize));
                for (var n in this._plugins) this._plugins[n].destroy();
                this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$stage.remove(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
            }, s.prototype.op = function(e, t, a) {
                var n = this.settings.rtl;
                switch (t) {
                    case "<":
                        return n ? e > a : e < a;
                    case ">":
                        return n ? e < a : e > a;
                    case ">=":
                        return n ? e <= a : e >= a;
                    case "<=":
                        return n ? e >= a : e <= a
                }
            }, s.prototype.on = function(e, t, a, n) {
                e.addEventListener ? e.addEventListener(t, a, n) : e.attachEvent && e.attachEvent("on" + t, a)
            }, s.prototype.off = function(e, t, a, n) {
                e.removeEventListener ? e.removeEventListener(t, a, n) : e.detachEvent && e.detachEvent("on" + t, a)
            }, s.prototype.trigger = function(t, a, n, i, r) {
                var o = {
                        item: {
                            count: this._items.length,
                            index: this.current()
                        }
                    },
                    d = e.camelCase(e.grep(["on", t, n], function(e) {
                        return e
                    }).join("-").toLowerCase()),
                    l = e.Event([t, "owl", n || "carousel"].join(".").toLowerCase(), e.extend({
                        relatedTarget: this
                    }, o, a));
                return this._supress[t] || (e.each(this._plugins, function(e, t) {
                    t.onTrigger && t.onTrigger(l)
                }), this.register({
                    type: s.Type.Event,
                    name: t
                }), this.$element.trigger(l), this.settings && "function" == typeof this.settings[d] && this.settings[d].call(this, l)), l
            }, s.prototype.enter = function(t) {
                e.each([t].concat(this._states.tags[t] || []), e.proxy(function(e, t) {
                    void 0 === this._states.current[t] && (this._states.current[t] = 0), this._states.current[t]++
                }, this))
            }, s.prototype.leave = function(t) {
                e.each([t].concat(this._states.tags[t] || []), e.proxy(function(e, t) {
                    this._states.current[t]--
                }, this))
            }, s.prototype.register = function(t) {
                if (t.type === s.Type.Event) {
                    if (e.event.special[t.name] || (e.event.special[t.name] = {}), !e.event.special[t.name].owl) {
                        var a = e.event.special[t.name]._default;
                        e.event.special[t.name]._default = function(e) {
                            return !a || !a.apply || e.namespace && -1 !== e.namespace.indexOf("owl") ? e.namespace && e.namespace.indexOf("owl") > -1 : a.apply(this, arguments)
                        }, e.event.special[t.name].owl = !0
                    }
                } else t.type === s.Type.State && (this._states.tags[t.name] ? this._states.tags[t.name] = this._states.tags[t.name].concat(t.tags) : this._states.tags[t.name] = t.tags, this._states.tags[t.name] = e.grep(this._states.tags[t.name], e.proxy(function(a, n) {
                    return e.inArray(a, this._states.tags[t.name]) === n
                }, this)))
            }, s.prototype.suppress = function(t) {
                e.each(t, e.proxy(function(e, t) {
                    this._supress[t] = !0
                }, this))
            }, s.prototype.release = function(t) {
                e.each(t, e.proxy(function(e, t) {
                    delete this._supress[t]
                }, this))
            }, s.prototype.pointer = function(e) {
                var a = {
                    x: null,
                    y: null
                };
                return e = e.originalEvent || e || t.event, (e = e.touches && e.touches.length ? e.touches[0] : e.changedTouches && e.changedTouches.length ? e.changedTouches[0] : e).pageX ? (a.x = e.pageX, a.y = e.pageY) : (a.x = e.clientX, a.y = e.clientY), a
            }, s.prototype.isNumeric = function(e) {
                return !isNaN(parseFloat(e))
            }, s.prototype.difference = function(e, t) {
                return {
                    x: e.x - t.x,
                    y: e.y - t.y
                }
            }, e.fn.owlCarousel = function(t) {
                var a = Array.prototype.slice.call(arguments, 1);
                return this.each(function() {
                    var n = e(this),
                        i = n.data("owl.carousel");
                    i || (i = new s(this, "object" == typeof t && t), n.data("owl.carousel", i), e.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function(t, a) {
                        i.register({
                            type: s.Type.Event,
                            name: a
                        }), i.$element.on(a + ".owl.carousel.core", e.proxy(function(e) {
                            e.namespace && e.relatedTarget !== this && (this.suppress([a]), i[a].apply(this, [].slice.call(arguments, 1)), this.release([a]))
                        }, i))
                    })), "string" == typeof t && "_" !== t.charAt(0) && i[t].apply(i, a)
                })
            }, e.fn.owlCarousel.Constructor = s
        }(window.Zepto || window.jQuery, window, document),
        function(e, t, a, n) {
            var s = function(t) {
                this._core = t, this._interval = null, this._visible = null, this._handlers = {
                    "initialized.owl.carousel": e.proxy(function(e) {
                        e.namespace && this._core.settings.autoRefresh && this.watch()
                    }, this)
                }, this._core.options = e.extend({}, s.Defaults, this._core.options), this._core.$element.on(this._handlers)
            };
            s.Defaults = {
                autoRefresh: !0,
                autoRefreshInterval: 500
            }, s.prototype.watch = function() {
                this._interval || (this._visible = this._core.isVisible(), this._interval = t.setInterval(e.proxy(this.refresh, this), this._core.settings.autoRefreshInterval))
            }, s.prototype.refresh = function() {
                this._core.isVisible() !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh())
            }, s.prototype.destroy = function() {
                var e, a;
                t.clearInterval(this._interval);
                for (e in this._handlers) this._core.$element.off(e, this._handlers[e]);
                for (a in Object.getOwnPropertyNames(this)) "function" != typeof this[a] && (this[a] = null)
            }, e.fn.owlCarousel.Constructor.Plugins.AutoRefresh = s
        }(window.Zepto || window.jQuery, window, document),
        function(e, t, a, n) {
            var s = function(t) {
                this._core = t, this._loaded = [], this._handlers = {
                    "initialized.owl.carousel change.owl.carousel resized.owl.carousel": e.proxy(function(t) {
                        if (t.namespace && this._core.settings && this._core.settings.lazyLoad && (t.property && "position" == t.property.name || "initialized" == t.type)) {
                            var a = this._core.settings,
                                n = a.center && Math.ceil(a.items / 2) || a.items,
                                s = a.center && -1 * n || 0,
                                i = (t.property && void 0 !== t.property.value ? t.property.value : this._core.current()) + s,
                                r = this._core.clones().length,
                                o = e.proxy(function(e, t) {
                                    this.load(t)
                                }, this);
                            for (a.lazyLoadEager > 0 && (n += a.lazyLoadEager, a.loop && (i -= a.lazyLoadEager, n++)); s++ < n;) this.load(r / 2 + this._core.relative(i)), r && e.each(this._core.clones(this._core.relative(i)), o), i++
                        }
                    }, this)
                }, this._core.options = e.extend({}, s.Defaults, this._core.options), this._core.$element.on(this._handlers)
            };
            s.Defaults = {
                lazyLoad: !1,
                lazyLoadEager: 0
            }, s.prototype.load = function(a) {
                var n = this._core.$stage.children().eq(a),
                    s = n && n.find(".owl-lazy");
                !s || e.inArray(n.get(0), this._loaded) > -1 || (s.each(e.proxy(function(a, n) {
                    var s, i = e(n),
                        r = t.devicePixelRatio > 1 && i.attr("data-src-retina") || i.attr("data-src") || i.attr("data-srcset");
                    this._core.trigger("load", {
                        element: i,
                        url: r
                    }, "lazy"), i.is("img") ? i.one("load.owl.lazy", e.proxy(function() {
                        i.css("opacity", 1), this._core.trigger("loaded", {
                            element: i,
                            url: r
                        }, "lazy")
                    }, this)).attr("src", r) : i.is("source") ? i.one("load.owl.lazy", e.proxy(function() {
                        this._core.trigger("loaded", {
                            element: i,
                            url: r
                        }, "lazy")
                    }, this)).attr("srcset", r) : ((s = new Image).onload = e.proxy(function() {
                        i.css({
                            "background-image": 'url("' + r + '")',
                            opacity: "1"
                        }), this._core.trigger("loaded", {
                            element: i,
                            url: r
                        }, "lazy")
                    }, this), s.src = r)
                }, this)), this._loaded.push(n.get(0)))
            }, s.prototype.destroy = function() {
                var e, t;
                for (e in this.handlers) this._core.$element.off(e, this.handlers[e]);
                for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
            }, e.fn.owlCarousel.Constructor.Plugins.Lazy = s
        }(window.Zepto || window.jQuery, window, document),
        function(e, t, a, n) {
            var s = function(a) {
                this._core = a, this._previousHeight = null, this._handlers = {
                    "initialized.owl.carousel refreshed.owl.carousel": e.proxy(function(e) {
                        e.namespace && this._core.settings.autoHeight && this.update()
                    }, this),
                    "changed.owl.carousel": e.proxy(function(e) {
                        e.namespace && this._core.settings.autoHeight && "position" === e.property.name && this.update()
                    }, this),
                    "loaded.owl.lazy": e.proxy(function(e) {
                        e.namespace && this._core.settings.autoHeight && e.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update()
                    }, this)
                }, this._core.options = e.extend({}, s.Defaults, this._core.options), this._core.$element.on(this._handlers), this._intervalId = null;
                var n = this;
                e(t).on("load", function() {
                    n._core.settings.autoHeight && n.update()
                }), e(t).resize(function() {
                    n._core.settings.autoHeight && (null != n._intervalId && clearTimeout(n._intervalId), n._intervalId = setTimeout(function() {
                        n.update()
                    }, 250))
                })
            };
            s.Defaults = {
                autoHeight: !1,
                autoHeightClass: "owl-height"
            }, s.prototype.update = function() {
                var t = this._core._current,
                    a = t + this._core.settings.items,
                    n = this._core.settings.lazyLoad,
                    s = this._core.$stage.children().toArray().slice(t, a),
                    i = [],
                    r = 0;
                e.each(s, function(t, a) {
                    i.push(e(a).height())
                }), (r = Math.max.apply(null, i)) <= 1 && n && this._previousHeight && (r = this._previousHeight), this._previousHeight = r, this._core.$stage.parent().height(r).addClass(this._core.settings.autoHeightClass)
            }, s.prototype.destroy = function() {
                var e, t;
                for (e in this._handlers) this._core.$element.off(e, this._handlers[e]);
                for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
            }, e.fn.owlCarousel.Constructor.Plugins.AutoHeight = s
        }(window.Zepto || window.jQuery, window, document),
        function(e, t, a, n) {
            var s = function(t) {
                this._core = t, this._videos = {}, this._playing = null, this._handlers = {
                    "initialized.owl.carousel": e.proxy(function(e) {
                        e.namespace && this._core.register({
                            type: "state",
                            name: "playing",
                            tags: ["interacting"]
                        })
                    }, this),
                    "resize.owl.carousel": e.proxy(function(e) {
                        e.namespace && this._core.settings.video && this.isInFullScreen() && e.preventDefault()
                    }, this),
                    "refreshed.owl.carousel": e.proxy(function(e) {
                        e.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove()
                    }, this),
                    "changed.owl.carousel": e.proxy(function(e) {
                        e.namespace && "position" === e.property.name && this._playing && this.stop()
                    }, this),
                    "prepared.owl.carousel": e.proxy(function(t) {
                        if (t.namespace) {
                            var a = e(t.content).find(".owl-video");
                            a.length && (a.css("display", "none"), this.fetch(a, e(t.content)))
                        }
                    }, this)
                }, this._core.options = e.extend({}, s.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", e.proxy(function(e) {
                    this.play(e)
                }, this))
            };
            s.Defaults = {
                video: !1,
                videoHeight: !1,
                videoWidth: !1
            }, s.prototype.fetch = function(e, t) {
                var a = e.attr("data-vimeo-id") ? "vimeo" : e.attr("data-vzaar-id") ? "vzaar" : "youtube",
                    n = e.attr("data-vimeo-id") || e.attr("data-youtube-id") || e.attr("data-vzaar-id"),
                    s = e.attr("data-width") || this._core.settings.videoWidth,
                    i = e.attr("data-height") || this._core.settings.videoHeight,
                    r = e.attr("href");
                if (!r) throw new Error("Missing video URL.");
                if ((n = r.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com|be\-nocookie\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/))[3].indexOf("youtu") > -1) a = "youtube";
                else if (n[3].indexOf("vimeo") > -1) a = "vimeo";
                else {
                    if (!(n[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");
                    a = "vzaar"
                }
                n = n[6], this._videos[r] = {
                    type: a,
                    id: n,
                    width: s,
                    height: i
                }, t.attr("data-video", r), this.thumbnail(e, this._videos[r])
            }, s.prototype.thumbnail = function(t, a) {
                var n, s, i, r = a.width && a.height ? "width:" + a.width + "px;height:" + a.height + "px;" : "",
                    o = t.find("img"),
                    d = "src",
                    l = "",
                    u = this._core.settings,
                    _ = function(a) {
                        s = '<div class="owl-video-play-icon"></div>', n = u.lazyLoad ? e("<div/>", {
                            class: "owl-video-tn " + l,
                            srcType: a
                        }) : e("<div/>", {
                            class: "owl-video-tn",
                            style: "opacity:1;background-image:url(" + a + ")"
                        }), t.after(n), t.after(s)
                    };
                if (t.wrap(e("<div/>", {
                        class: "owl-video-wrapper",
                        style: r
                    })), this._core.settings.lazyLoad && (d = "data-src", l = "owl-lazy"), o.length) return _(o.attr(d)), o.remove(), !1;
                "youtube" === a.type ? (i = "//img.youtube.com/vi/" + a.id + "/hqdefault.jpg", _(i)) : "vimeo" === a.type ? e.ajax({
                    type: "GET",
                    url: "//vimeo.com/api/v2/video/" + a.id + ".json",
                    jsonp: "callback",
                    dataType: "jsonp",
                    success: function(e) {
                        i = e[0].thumbnail_large, _(i)
                    }
                }) : "vzaar" === a.type && e.ajax({
                    type: "GET",
                    url: "//vzaar.com/api/videos/" + a.id + ".json",
                    jsonp: "callback",
                    dataType: "jsonp",
                    success: function(e) {
                        i = e.framegrab_url, _(i)
                    }
                })
            }, s.prototype.stop = function() {
                this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video")
            }, s.prototype.play = function(t) {
                var a, n = e(t.target).closest("." + this._core.settings.itemClass),
                    s = this._videos[n.attr("data-video")],
                    i = s.width || "100%",
                    r = s.height || this._core.$stage.height();
                this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), n = this._core.items(this._core.relative(n.index())), this._core.reset(n.index()), (a = e('<iframe frameborder="0" allowfullscreen mozallowfullscreen webkitAllowFullScreen ></iframe>')).attr("height", r), a.attr("width", i), "youtube" === s.type ? a.attr("src", "//www.youtube.com/embed/" + s.id + "?autoplay=1&rel=0&v=" + s.id) : "vimeo" === s.type ? a.attr("src", "//player.vimeo.com/video/" + s.id + "?autoplay=1") : "vzaar" === s.type && a.attr("src", "//view.vzaar.com/" + s.id + "/player?autoplay=true"), e(a).wrap('<div class="owl-video-frame" />').insertAfter(n.find(".owl-video")), this._playing = n.addClass("owl-video-playing"))
            }, s.prototype.isInFullScreen = function() {
                var t = a.fullscreenElement || a.mozFullScreenElement || a.webkitFullscreenElement;
                return t && e(t).parent().hasClass("owl-video-frame")
            }, s.prototype.destroy = function() {
                var e, t;
                this._core.$element.off("click.owl.video");
                for (e in this._handlers) this._core.$element.off(e, this._handlers[e]);
                for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
            }, e.fn.owlCarousel.Constructor.Plugins.Video = s
        }(window.Zepto || window.jQuery, window, document),
        function(e, t, a, n) {
            var s = function(t) {
                this.core = t, this.core.options = e.extend({}, s.Defaults, this.core.options), this.swapping = !0, this.previous = void 0, this.next = void 0, this.handlers = {
                    "change.owl.carousel": e.proxy(function(e) {
                        e.namespace && "position" == e.property.name && (this.previous = this.core.current(), this.next = e.property.value)
                    }, this),
                    "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": e.proxy(function(e) {
                        e.namespace && (this.swapping = "translated" == e.type)
                    }, this),
                    "translate.owl.carousel": e.proxy(function(e) {
                        e.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap()
                    }, this)
                }, this.core.$element.on(this.handlers)
            };
            s.Defaults = {
                animateOut: !1,
                animateIn: !1
            }, s.prototype.swap = function() {
                if (1 === this.core.settings.items && e.support.animation && e.support.transition) {
                    this.core.speed(0);
                    var t, a = e.proxy(this.clear, this),
                        n = this.core.$stage.children().eq(this.previous),
                        s = this.core.$stage.children().eq(this.next),
                        i = this.core.settings.animateIn,
                        r = this.core.settings.animateOut;
                    this.core.current() !== this.previous && (r && (t = this.core.coordinates(this.previous) - this.core.coordinates(this.next), n.one(e.support.animation.end, a).css({
                        left: t + "px"
                    }).addClass("animated owl-animated-out").addClass(r)), i && s.one(e.support.animation.end, a).addClass("animated owl-animated-in").addClass(i))
                }
            }, s.prototype.clear = function(t) {
                e(t.target).css({
                    left: ""
                }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd()
            }, s.prototype.destroy = function() {
                var e, t;
                for (e in this.handlers) this.core.$element.off(e, this.handlers[e]);
                for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
            }, e.fn.owlCarousel.Constructor.Plugins.Animate = s
        }(window.Zepto || window.jQuery, window, document),
        function(e, t, a, n) {
            var s = function(t) {
                this._core = t, this._call = null, this._time = 0, this._timeout = 0, this._paused = !0, this._handlers = {
                    "changed.owl.carousel": e.proxy(function(e) {
                        e.namespace && "settings" === e.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : e.namespace && "position" === e.property.name && this._paused && (this._time = 0)
                    }, this),
                    "initialized.owl.carousel": e.proxy(function(e) {
                        e.namespace && this._core.settings.autoplay && this.play()
                    }, this),
                    "play.owl.autoplay": e.proxy(function(e, t, a) {
                        e.namespace && this.play(t, a)
                    }, this),
                    "stop.owl.autoplay": e.proxy(function(e) {
                        e.namespace && this.stop()
                    }, this),
                    "mouseover.owl.autoplay": e.proxy(function() {
                        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
                    }, this),
                    "mouseleave.owl.autoplay": e.proxy(function() {
                        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play()
                    }, this),
                    "touchstart.owl.core": e.proxy(function() {
                        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
                    }, this),
                    "touchend.owl.core": e.proxy(function() {
                        this._core.settings.autoplayHoverPause && this.play()
                    }, this)
                }, this._core.$element.on(this._handlers), this._core.options = e.extend({}, s.Defaults, this._core.options)
            };
            s.Defaults = {
                autoplay: !1,
                autoplayTimeout: 5e3,
                autoplayHoverPause: !1,
                autoplaySpeed: !1
            }, s.prototype._next = function(n) {
                this._call = t.setTimeout(e.proxy(this._next, this, n), this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read()), this._core.is("interacting") || a.hidden || this._core.next(n || this._core.settings.autoplaySpeed)
            }, s.prototype.read = function() {
                return (new Date).getTime() - this._time
            }, s.prototype.play = function(a, n) {
                var s;
                this._core.is("rotating") || this._core.enter("rotating"), a = a || this._core.settings.autoplayTimeout, s = Math.min(this._time % (this._timeout || a), a), this._paused ? (this._time = this.read(), this._paused = !1) : t.clearTimeout(this._call), this._time += this.read() % a - s, this._timeout = a, this._call = t.setTimeout(e.proxy(this._next, this, n), a - s)
            }, s.prototype.stop = function() {
                this._core.is("rotating") && (this._time = 0, this._paused = !0, t.clearTimeout(this._call), this._core.leave("rotating"))
            }, s.prototype.pause = function() {
                this._core.is("rotating") && !this._paused && (this._time = this.read(), this._paused = !0, t.clearTimeout(this._call))
            }, s.prototype.destroy = function() {
                var e, t;
                this.stop();
                for (e in this._handlers) this._core.$element.off(e, this._handlers[e]);
                for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
            }, e.fn.owlCarousel.Constructor.Plugins.autoplay = s
        }(window.Zepto || window.jQuery, window, document),
        function(e, t, a, n) {
            "use strict";
            var s = function(t) {
                this._core = t, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
                    next: this._core.next,
                    prev: this._core.prev,
                    to: this._core.to
                }, this._handlers = {
                    "prepared.owl.carousel": e.proxy(function(t) {
                        t.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + e(t.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>")
                    }, this),
                    "added.owl.carousel": e.proxy(function(e) {
                        e.namespace && this._core.settings.dotsData && this._templates.splice(e.position, 0, this._templates.pop())
                    }, this),
                    "remove.owl.carousel": e.proxy(function(e) {
                        e.namespace && this._core.settings.dotsData && this._templates.splice(e.position, 1)
                    }, this),
                    "changed.owl.carousel": e.proxy(function(e) {
                        e.namespace && "position" == e.property.name && this.draw()
                    }, this),
                    "initialized.owl.carousel": e.proxy(function(e) {
                        e.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"))
                    }, this),
                    "refreshed.owl.carousel": e.proxy(function(e) {
                        e.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"))
                    }, this)
                }, this._core.options = e.extend({}, s.Defaults, this._core.options), this.$element.on(this._handlers)
            };
            s.Defaults = {
                nav: !1,
                navText: ['<span aria-label="Previous">&#x2039;</span>', '<span aria-label="Next">&#x203a;</span>'],
                navSpeed: !1,
                navElement: 'button type="button" role="presentation"',
                navContainer: !1,
                navContainerClass: "owl-nav",
                navClass: ["owl-prev", "owl-next"],
                slideBy: 1,
                dotClass: "owl-dot",
                dotsClass: "owl-dots",
                dots: !0,
                dotsEach: !1,
                dotsData: !1,
                dotsSpeed: !1,
                dotsContainer: !1
            }, s.prototype.initialize = function() {
                var t, a = this._core.settings;
                this._controls.$relative = (a.navContainer ? e(a.navContainer) : e("<div>").addClass(a.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = e("<" + a.navElement + ">").addClass(a.navClass[0]).html(a.navText[0]).prependTo(this._controls.$relative).on("click", e.proxy(function(e) {
                    this.prev(a.navSpeed)
                }, this)), this._controls.$next = e("<" + a.navElement + ">").addClass(a.navClass[1]).html(a.navText[1]).appendTo(this._controls.$relative).on("click", e.proxy(function(e) {
                    this.next(a.navSpeed)
                }, this)), a.dotsData || (this._templates = [e('<button role="button">').addClass(a.dotClass).append(e("<span>")).prop("outerHTML")]), this._controls.$absolute = (a.dotsContainer ? e(a.dotsContainer) : e("<div>").addClass(a.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "button", e.proxy(function(t) {
                    var n = e(t.target).parent().is(this._controls.$absolute) ? e(t.target).index() : e(t.target).parent().index();
                    t.preventDefault(), this.to(n, a.dotsSpeed)
                }, this));
                for (t in this._overrides) this._core[t] = e.proxy(this[t], this)
            }, s.prototype.destroy = function() {
                var e, t, a, n, s;
                s = this._core.settings;
                for (e in this._handlers) this.$element.off(e, this._handlers[e]);
                for (t in this._controls) "$relative" === t && s.navContainer ? this._controls[t].html("") : this._controls[t].remove();
                for (n in this.overides) this._core[n] = this._overrides[n];
                for (a in Object.getOwnPropertyNames(this)) "function" != typeof this[a] && (this[a] = null)
            }, s.prototype.update = function() {
                var e, t, a = this._core.clones().length / 2,
                    n = a + this._core.items().length,
                    s = this._core.maximum(!0),
                    i = this._core.settings,
                    r = i.center || i.autoWidth || i.dotsData ? 1 : i.dotsEach || i.items;
                if ("page" !== i.slideBy && (i.slideBy = Math.min(i.slideBy, i.items)), i.dots || "page" == i.slideBy)
                    for (this._pages = [], e = a, t = 0, 0; e < n; e++) {
                        if (t >= r || 0 === t) {
                            if (this._pages.push({
                                    start: Math.min(s, e - a),
                                    end: e - a + r - 1
                                }), Math.min(s, e - a) === s) break;
                            t = 0, 0
                        }
                        t += this._core.mergers(this._core.relative(e))
                    }
            }, s.prototype.draw = function() {
                var t, a = this._core.settings,
                    n = this._core.items().length <= a.items,
                    s = this._core.relative(this._core.current()),
                    i = a.loop || a.rewind;
                this._controls.$relative.toggleClass("disabled", !a.nav || n), a.nav && (this._controls.$previous.toggleClass("disabled", !i && s <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !i && s >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !a.dots || n), a.dots && (t = this._pages.length - this._controls.$absolute.children().length, a.dotsData && 0 !== t ? this._controls.$absolute.html(this._templates.join("")) : t > 0 ? this._controls.$absolute.append(new Array(t + 1).join(this._templates[0])) : t < 0 && this._controls.$absolute.children().slice(t).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(e.inArray(this.current(), this._pages)).addClass("active"))
            }, s.prototype.onTrigger = function(t) {
                var a = this._core.settings;
                t.page = {
                    index: e.inArray(this.current(), this._pages),
                    count: this._pages.length,
                    size: a && (a.center || a.autoWidth || a.dotsData ? 1 : a.dotsEach || a.items)
                }
            }, s.prototype.current = function() {
                var t = this._core.relative(this._core.current());
                return e.grep(this._pages, e.proxy(function(e, a) {
                    return e.start <= t && e.end >= t
                }, this)).pop()
            }, s.prototype.getPosition = function(t) {
                var a, n, s = this._core.settings;
                return "page" == s.slideBy ? (a = e.inArray(this.current(), this._pages), n = this._pages.length, t ? ++a : --a, a = this._pages[(a % n + n) % n].start) : (a = this._core.relative(this._core.current()), n = this._core.items().length, t ? a += s.slideBy : a -= s.slideBy), a
            }, s.prototype.next = function(t) {
                e.proxy(this._overrides.to, this._core)(this.getPosition(!0), t)
            }, s.prototype.prev = function(t) {
                e.proxy(this._overrides.to, this._core)(this.getPosition(!1), t)
            }, s.prototype.to = function(t, a, n) {
                var s;
                !n && this._pages.length ? (s = this._pages.length, e.proxy(this._overrides.to, this._core)(this._pages[(t % s + s) % s].start, a)) : e.proxy(this._overrides.to, this._core)(t, a)
            }, e.fn.owlCarousel.Constructor.Plugins.Navigation = s
        }(window.Zepto || window.jQuery, window, document),
        function(e, t, a, n) {
            "use strict";
            var s = function(a) {
                this._core = a, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
                    "initialized.owl.carousel": e.proxy(function(a) {
                        a.namespace && "URLHash" === this._core.settings.startPosition && e(t).trigger("hashchange.owl.navigation")
                    }, this),
                    "prepared.owl.carousel": e.proxy(function(t) {
                        if (t.namespace) {
                            var a = e(t.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
                            if (!a) return;
                            this._hashes[a] = t.content
                        }
                    }, this),
                    "changed.owl.carousel": e.proxy(function(a) {
                        if (a.namespace && "position" === a.property.name) {
                            var n = this._core.items(this._core.relative(this._core.current())),
                                s = e.map(this._hashes, function(e, t) {
                                    return e === n ? t : null
                                }).join();
                            if (!s || t.location.hash.slice(1) === s) return;
                            t.location.hash = s
                        }
                    }, this)
                }, this._core.options = e.extend({}, s.Defaults, this._core.options), this.$element.on(this._handlers), e(t).on("hashchange.owl.navigation", e.proxy(function(e) {
                    var a = t.location.hash.substring(1),
                        n = this._core.$stage.children(),
                        s = this._hashes[a] && n.index(this._hashes[a]);
                    void 0 !== s && s !== this._core.current() && this._core.to(this._core.relative(s), !1, !0)
                }, this))
            };
            s.Defaults = {
                URLhashListener: !1
            }, s.prototype.destroy = function() {
                var a, n;
                e(t).off("hashchange.owl.navigation");
                for (a in this._handlers) this._core.$element.off(a, this._handlers[a]);
                for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null)
            }, e.fn.owlCarousel.Constructor.Plugins.Hash = s
        }(window.Zepto || window.jQuery, window, document),
        function(e, t, a, n) {
            function s(t, a) {
                var s = !1,
                    i = t.charAt(0).toUpperCase() + t.slice(1);
                return e.each((t + " " + o.join(i + " ") + i).split(" "), function(e, t) {
                    if (r[t] !== n) return s = !a || t, !1
                }), s
            }

            function i(e) {
                return s(e, !0)
            }
            var r = e("<support>").get(0).style,
                o = "Webkit Moz O ms".split(" "),
                d = {
                    end: {
                        WebkitTransition: "webkitTransitionEnd",
                        MozTransition: "transitionend",
                        OTransition: "oTransitionEnd",
                        transition: "transitionend"
                    }
                },
                l = {
                    end: {
                        WebkitAnimation: "webkitAnimationEnd",
                        MozAnimation: "animationend",
                        OAnimation: "oAnimationEnd",
                        animation: "animationend"
                    }
                },
                u = function() {
                    return !!s("transform")
                },
                _ = function() {
                    return !!s("perspective")
                },
                m = function() {
                    return !!s("animation")
                };
            (function() {
                return !!s("transition")
            })() && (e.support.transition = new String(i("transition")), e.support.transition.end = d.end[e.support.transition]), m() && (e.support.animation = new String(i("animation")), e.support.animation.end = l.end[e.support.animation]), u() && (e.support.transform = new String(i("transform")), e.support.transform3d = _())
        }(window.Zepto || window.jQuery, window, document)
    }, {}],
    8: [function(e, t, a) {
        "use strict";

        function n(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }
        e("owl.carousel"), e("magnific-popup");
        var s = n(e("./modules/accordion")),
            i = n(e("./modules/delivery")),
            r = n(e("./modules/maintenance")),
            o = n(e("./modules/localte-point")),
            d = n(e("./modules/profile"));
        ! function(e) {
            s.default.init(), i.default.init(), new r.default, new o.default, new d.default;
            var t = e(".js-tabs");
            e.each(t, function(a, n) {
                var s = e(n),
                    i = s.next(),
                    r = "click";
                s.find("li").eq(0).addClass("is-active"), i.children().hide(), i.children().eq(0).show(), t.hasClass("hover") && (r += " mouseover"), console.log(r), s.find("li").on(r, function(t) {
                    var a = e(t.currentTarget),
                        n = a.data("tab");
                    s.find("li").removeClass("is-active"), a.addClass("is-active"), i.children().hide(), i.children("#" + n).show()
                })
            })
        }(jQuery)
    }, {
        "./modules/accordion": 9,
        "./modules/delivery": 11,
        "./modules/localte-point": 13,
        "./modules/maintenance": 14,
        "./modules/profile": 15,
        "magnific-popup": 3,
        "owl.carousel": 7
    }],
    9: [function(e, t, a) {
        "use strict";
        Object.defineProperty(a, "__esModule", {
            value: !0
        });
        var n, s = {
            mediaQueries: {
                isMobile: window.matchMedia("(max-width: 767px)")
            },
            settings: {
                $body: "body",
                arrowLeft: '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 284.935 284.936"><path d="M110.488 142.468L222.694 30.264c1.902-1.903 2.854-4.093 2.854-6.567s-.951-4.664-2.854-6.563L208.417 2.857C206.513.955 204.324 0 201.856 0c-2.475 0-4.664.955-6.567 2.857L62.24 135.9c-1.903 1.903-2.852 4.093-2.852 6.567 0 2.475.949 4.664 2.852 6.567l133.042 133.043c1.906 1.906 4.097 2.857 6.571 2.857 2.471 0 4.66-.951 6.563-2.857l14.277-14.267c1.902-1.903 2.851-4.094 2.851-6.57 0-2.472-.948-4.661-2.851-6.564L110.488 142.468z"/></svg>',
                arrowRight: '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 284.935 284.936"><path d="M222.701 135.9L89.652 2.857C87.748.955 85.557 0 83.084 0c-2.474 0-4.664.955-6.567 2.857L62.244 17.133c-1.906 1.903-2.855 4.089-2.855 6.567 0 2.478.949 4.664 2.855 6.567l112.204 112.204L62.244 254.677c-1.906 1.903-2.855 4.093-2.855 6.564 0 2.477.949 4.667 2.855 6.57l14.274 14.271c1.903 1.905 4.093 2.854 6.567 2.854 2.473 0 4.663-.951 6.567-2.854l133.042-133.044c1.902-1.902 2.854-4.093 2.854-6.567s-.945-4.664-2.847-6.571z"/></svg>'
            },
            init: function() {
                n = this.settings, this.bindEvents(), this.accordion(), this.stickyHeader(), this.initBannerHomeCarousel(), this.initTestimonyCarousel(), this.initProductsCarousel(), this.initBlogCarousel(), this.initCategoriesBlogCarousel(), this.initFunctionCarousel(), this.initBenefitsCarousel(), this.showNavMobile(), this.goToDown(), this.changeTypeFormAccount(), this.initHamburguerPopup(), this.animateScrolling(), this.dropdown()
            },
            bindEvents: function() {},
            accordion: function(e) {
                var t = $(".js-accordion");
                $.each(t, function(e, t) {
                    var a = $(t);
                    $("> .panel:eq(0) .panel__header", a).addClass("is-active").next().slideDown(), $(".panel__header", a).on("click", function(e) {
                        var t = $(e.currentTarget),
                            n = t.closest(".panel").find("> .panel__content");
                        t.closest(a).find(".panel > .panel__content").not(n).slideUp(), t.hasClass("is-active") ? t.removeClass("is-active") : (t.closest(a).find(".panel__header.is-active").removeClass("is-active"), t.addClass("is-active")), n.stop(!1, !0).slideToggle(), e.preventDefault()
                    })
                })
            },
            stickyHeader: function() {
                var e = $(".site-nav");
                $(window).scroll(function() {
                    $(window).scrollTop() >= 200 ? e.addClass("is-sticky") : e.removeClass("is-sticky")
                })
            },
            initBannerHomeCarousel: function() {
                var e = $(".js-banner-home-carousel");
                e.on("initialize.owl.carousel", function(t) {
                    s.changeImageBanner(e)
                }), e.owlCarousel({
                    items: 1,
                    loop: !0,
                    autoplay: !0,
                    autoplayTimeout: 4e3,
                    animateOut: "fadeOut",
                    dots: !1
                }), s.mediaQueries.isMobile.addListener(s.changeImageBanner)
            },
            changeImageBanner: function(e) {
                var t = $(e).find(".item");
                s.mediaQueries.isMobile.matches ? $.each(t, function(e, t) {
                    var a = $(t).attr("style");
                    if (-1 != a.indexOf("desktop")) {
                        var n = a.replace("desktop", "mobile");
                        $(t).attr("style", n)
                    }
                }) : $.each(t, function(e, t) {
                    var a = $(t).attr("style");
                    if (-1 != a.indexOf("mobile")) {
                        var n = a.replace("mobile", "desktop");
                        $(t).attr("style", n)
                    }
                })
            },
            initTestimonyCarousel: function() {
                $(".js-testimony-carousel").owlCarousel({
                    items: 1,
                    autoHeight: !0,
                    loop: !0,
                    autoplay: !0,
                    autoplayTimeout: 6e3
                })
            },
            initProductsCarousel: function() {
                $(".js-products-carousel").owlCarousel({
                    autoHeight: !0,
                    responsive: {
                        0: {
                            items: 1,
                            dots: !0
                        },
                        768: {
                            items: 2,
                            margin: 30,
                            dots: !0
                        },
                        992: {
                            items: 3,
                            margin: 25,
                            dots: !1
                        }
                    }
                })
            },
            initBlogCarousel: function() {
                $(".js-blog-carousel").owlCarousel({
                    items: 1,
                    responsive: {
                        992: {}
                    }
                })
            },
            initCategoriesBlogCarousel: function() {
                var e = function() {
                    var e = $(".owl-stage"),
                        t = e.width(),
                        a = 0;
                    $(".owl-item").each(function() {
                        a += $(this).width() + +$(this).css("margin-right").slice(0, -2)
                    }), a > t && e.width(a)
                };
                $(".js-categories-blog-carousel").owlCarousel({
                    margin: 25,
                    autoWidth: !0,
                    dots: !1,
                    navText: [n.arrowLeft, n.arrowRight],
                    onInitialized: e,
                    onRefreshed: e,
                    responsive: {
                        0: {
                            nav: !1,
                            margin: 10
                        },
                        768: {
                            items: 6,
                            nav: !0
                        }
                    }
                })
            },
            initFunctionCarousel: function() {
                var e = $(".js-function-carousel"),
                    t = $(".js-function-items");
                e.owlCarousel({
                    responsive: {
                        0: {
                            items: 1,
                            dots: !1
                        },
                        768: {
                            items: 3,
                            margin: 15
                        },
                        992: {
                            items: 3,
                            margin: 60
                        }
                    }
                }), e.on("changed.owl.carousel", function(e) {
                    var a = $(e.target).parent().prev(t),
                        n = e.item.index + 1 - e.relatedTarget._clones.length / 2;
                    a.children().removeClass("is-active"), a.children().eq(n - 1).addClass("is-active")
                }), t.children().on("click", function(a) {
                    var n = $(a.currentTarget),
                        s = n.data("tab");
                    e.trigger("to.owl.carousel", s - 1), t.children().removeClass("is-active"), n.addClass("is-active")
                })
            },
            initBenefitsCarousel: function() {
                var e = $(".js-benefit-carousel"),
                    t = $(".js-benefit-items");
                e.owlCarousel({
                    items: 1,
                    dots: !1,
                    loop: !0,
                    autoHeight: !0
                }), e.on("changed.owl.carousel", function(e) {
                    var a = $(e.target).parent().prev(t),
                        n = e.item.index + 1 - e.relatedTarget._clones.length / 2;
                    a.children().removeClass("is-active"), a.children().eq(n - 1).addClass("is-active")
                }), t.children().on("click", function(a) {
                    var n = $(a.currentTarget),
                        s = n.data("tab");
                    e.trigger("to.owl.carousel", s - 1), t.children().removeClass("is-active"), n.addClass("is-active")
                })
            },
            showNavMobile: function() {
                var e = $(".js-btn-menu"),
                    t = $(".js-btn-barker-nav"),
                    a = window.matchMedia("(max-width: 767px)");
                e.on("click", function(e) {
                    e.preventDefault(), $(n.$body).hasClass("is-show-menu") ? $(n.$body).removeClass("is-show-menu is-show-phases-menu") : $(n.$body).addClass("is-show-menu")
                }), t.on("click", function(e) {
                    a.matches && (e.preventDefault(), $(n.$body).hasClass("is-show-barker-menu") ? $(n.$body).removeClass("is-show-barker-menu") : $(n.$body).addClass("is-show-barker-menu"))
                })
            },
            goToDown: function() {
                var e = $(".js-btn-godown"),
                    t = $(".js-site-nav").outerHeight();
                e.on("click", function(e) {
                    var a = $(e.currentTarget).closest("section");
                    $("html, body").animate({
                        scrollTop: a.next().offset().top - t
                    }, 800)
                })
            },
            changeTypeFormAccount: function() {
                var e = $(".js-btn-form-type"),
                    t = $(".js-account-form-type");
                e.on("click", function(a) {
                    a.preventDefault();
                    var n = $(a.currentTarget),
                        s = n.data("form");
                    switch (console.log(s), s) {
                        case "form-register":
                            $("main").removeClass("is-register is-login is-reset"), $("main").addClass("is-register");
                            break;
                        case "form-login":
                            $("main").removeClass("is-register is-login is-reset"), $("main").addClass("is-login");
                            break;
                        case "form-reset":
                            $("main").removeClass("is-register is-login is-reset"), $("main").addClass("is-reset")
                    }
                    e.removeClass("is-active"), n.addClass("is-active"), t.children().hide(), $("#" + s).show()
                })
            },
            initHamburguerPopup: function() {
                $(".js-hamburguer-popup").on("click", function(e) {
                    e.preventDefault(), $.magnificPopup.open({
                        closeBtnInside: !0,
                        closeOnBgClick: !1,
                        items: {
                            src: "#barker-hamburguer",
                            type: "inline"
                        },
                        callbacks: {
                            beforeOpen: function() {
                                $("body").css("overflow", "hidden")
                            },
                            beforeClose: function() {
                                $("body").css("overflow", "auto")
                            }
                        }
                    })
                })
            },
            animateScrolling: function() {
                var e = new ScrollMagic,
                    t = TweenMax.staggerFromTo(".js-slide-toggle", .5, {
                        width: "0%"
                    }, {
                        width: "100%"
                    }, .4);
                new ScrollScene({
                    triggerElement: "#scene",
                    duration: 500
                }).setTween(t).addTo(e).addIndicators()
            },
            dropdown: function() {
                $('[data-effect="dropdown"]').on("click", function(e) {
                    e.preventDefault(), e.stopPropagation();
                    $(e.currentTarget).next(".dropdown-menu").toggleClass("is-open")
                }), $("html").on("click", function() {
                    $(".dropdown-menu").removeClass("is-open")
                })
            }
        };
        a.default = s
    }, {}],
    10: [function(e, t, a) {
        "use strict";
        Object.defineProperty(a, "__esModule", {
            value: !0
        }), a.default = [{
            name: "pollo",
            image: "pollo",
            text: "<p>Piezas de pollo molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>"
        }, {
            name: "pavo",
            image: "pavo",
            text: "<p>Piezas de pavo molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>"
        }, {
            name: "carne de res",
            image: "res",
            text: "<p>Piezas de carne de res molida, hígado y tejido blando (fuente de colágeno), arroz, legumbres frescas, vinagre de manzana, huevo pasteurizado, probióticos (levadura de cerveza), Vitamina E, Vitaminas A, B1, B2, B6, B12, D3, K3, Niacina, Ácido fólico, Biotina, Óxido de Zinc, Sulfato Ferroso, Sulfato de Cobre, Óxido de Manganeso, Iodato de Calcio, Selenito de Sodio, sal.</p>"
        }]
    }, {}],
    11: [function(e, t, a) {
        "use strict";

        function n(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }
        Object.defineProperty(a, "__esModule", {
            value: !0
        }), e("jquery-steps");
        var s = n(e("../vendor/w-numb")),
            i = n(e("nouislider"));
        n(e("moment"));
        e("bootstrap4-datetimepicker"), e("moment-locales");
        var r, o = n(e("./data-flavors")),
            d = n(e("./districts")),
            l = {
                $flavorsCarousel: "",
                dogName: "",
                flavors: "",
                orderList: [],
                arrowLeft: '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 284.935 284.936"><path d="M110.488 142.468L222.694 30.264c1.902-1.903 2.854-4.093 2.854-6.567s-.951-4.664-2.854-6.563L208.417 2.857C206.513.955 204.324 0 201.856 0c-2.475 0-4.664.955-6.567 2.857L62.24 135.9c-1.903 1.903-2.852 4.093-2.852 6.567 0 2.475.949 4.664 2.852 6.567l133.042 133.043c1.906 1.906 4.097 2.857 6.571 2.857 2.471 0 4.66-.951 6.563-2.857l14.277-14.267c1.902-1.903 2.851-4.094 2.851-6.57 0-2.472-.948-4.661-2.851-6.564L110.488 142.468z"/></svg>',
                arrowRight: '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 284.935 284.936"><path d="M222.701 135.9L89.652 2.857C87.748.955 85.557 0 83.084 0c-2.474 0-4.664.955-6.567 2.857L62.244 17.133c-1.906 1.903-2.855 4.089-2.855 6.567 0 2.478.949 4.664 2.855 6.567l112.204 112.204L62.244 254.677c-1.906 1.903-2.855 4.093-2.855 6.564 0 2.477.949 4.667 2.855 6.57l14.274 14.271c1.903 1.905 4.093 2.854 6.567 2.854 2.473 0 4.663-.951 6.567-2.854l133.042-133.044c1.902-1.902 2.854-4.093 2.854-6.567s-.945-4.664-2.847-6.571z"/></svg>',
                settings: {},
                init: function() {
                    r = this, this.settings, this.initWizard(), this.bindEvents(), this.previewImage(), this.calculateSizePet(), this.chooseFlavor(), this.orderForAWeek(), this.initSuscriptionCarousel(), this.filterBySex(), this.filterByStatusFemale(), this.getDogName(), this.selectDistrict(), this.addDog(), this.addDistrict(), this.showModalNutricional()
                },
                bindEvents: function() {
                    $('input[name="f-suscription-time"]').on("change", r.orderForAWeek)
                },
                initWizard: function() {
                    $("#delivery-wizard").steps({
                        headerTag: "h3",
                        bodyTag: "section",
                        autoFocus: !0,
                        labels: {
                            finish: "Realizar pedido",
                            next: "Continuar",
                            previous: "Regresar"
                        },
                        onStepChanging: function(e, t, a) {
                            return !0
                        },
                        onStepChanged: function(e, t, a) {},
                        onFinishing: function(e, t) {},
                        onFinished: function(e, t) {}
                    })
                },
                sendFormQuote: function() {},
                addDistrict: function() {
                    $(".js-add-district-popup").on("click", function(e) {

                        
                         $.magnificPopup.instance._onFocusIn = function(e) {
                            if ($(e.target).hasClass("select2-search__field")) return !0;
                            $.magnificPopup.proto._onFocusIn.call(this, e)
                        }, $.magnificPopup.open({
                            items: {
                                src: ".js-add-district",
                                type: "inline"
                            }
                        }), $(".js-add-district-select").select2({
                            placeholder: "Busca tu distrito",
                            width: "100%"
                        })
                        console.log('click para abrir popup');

                    })
                },
                addDistrictSearch: function() {
                    console.log("open now"), $(".js-add-district-select").select2({
                        placeholder: "Busca tu distrito",
                        width: "100%"
                    })
                },
                previewImage: function() {
                    $(".file-upload .js-upload-hidden").on("change", function(e) {
                        var t = $(e.currentTarget),
                            a = t.parent().prev();
                        if (window.FileReader) {
                            if (!t[0].files[0].type.match(/image\//)) return;
                            var n = new FileReader;
                            n.onload = function(e) {
                                var t = e.target.result;
                                console.log(t), a.find("img").attr("src", t)
                            }, n.readAsDataURL(t[0].files[0])
                        } else t[0].select(), t[0].blur()
                    })
                },
                filterBySex: function() {
                    $('input[name="f-dog-sex"]').on("change", function(e) {
                        "f-female" == $(e.currentTarget).attr("id") ? $(".filter-female").fadeIn("fast") : $(".filter-female").fadeOut("fast")
                    })
                },
                filterByStatusFemale: function() {},
                calculateSizePet: function() {
                    var e = $("#slider-size-dog")[0],
                        t = $(".js-dog-weight"),
                        a = $(".js-dog-race"),
                        n = $(".js-dog-image"),
                        r = $(".js-dog-less"),
                        o = $(".js-dog-higher"),
                        d = $("#dog-kg"),
                        l = $("#dog-race"),
                        u = {};
                    u[0] = "Menor", u[dogs.length - 1] = "Mayor", e && (i.default.create(e, {
                        start: Math.floor(dogs.length - dogs.length),
                        step: 1,
                        format: (0, s.default)({
                            decimals: 0
                        }),
                        range: {
                            min: 0,
                            max: dogs.length - 1
                        },
                        pips: {
                            mode: "range",
                            density: -1,
                            format: {
                                to: function(e) {
                                    return u[e]
                                }
                            }
                        }
                    }), r.find("img").attr("src", "" + dogs[0].src), o.find("img").attr("src", "" + dogs[dogs.length - 1].src), e.noUiSlider.on("update", function(e, s) {
                        var i = dogs[e[s]];
                        d.val(i.weight), l.val(i.race), t.html("" + i.category), a.html(i.race), n.find("img").attr("src", "" + i.src)
                    }))
                },
                getObjectInArray: function(e, t) {
                    var a = null;
                    return e.map(function(e) {
                        e.name == t && (a = e)
                    }), a
                },
                chooseFlavor: function() {
                    r.$flavorsCarousel = $(".js-delivery-flavors-carousel"), $('input[name="f-flavors"]').on("change", function(e) {
                        var t = $(e.currentTarget),
                            a = t.data("flavor"),
                            n = r.getObjectInArray(o.default, a);
                        r.addFlavorItem(t, a, n)
                    })
                },
                initFlavorsCarousel: function() {
                    r.$flavorsCarousel.owlCarousel({
                        items: 1,
                        nav: !0,
                        navText: [r.arrowLeft, r.arrowRight]
                    })
                },
                initSuscriptionCarousel: function() {
                    window.matchMedia("(max-width: 991px)").matches && $(".js-suscription-type-carousel").owlCarousel({
                        items: 1,
                        stagePadding: 25
                    })
                },
                orderForAWeek: function() {
                    var e = $("#f-one-week"),
                        t = $("#f-mixed-flavor");
                    e.length && e.is(":checked") ? (t.is(":checked") && (r.destroyFlavorsCarousel(), r.flavors = ""), t.prop("disabled", !0).prop("checked", !1)) : t.prop("disabled", !1)
                },
                addFlavorItem: function(e, t, a) {
                    r.$flavorsCarousel.children().length || (r.$flavorsCarousel.closest(".delivery-flavors").removeClass("is-empty"), r.initFlavorsCarousel()), r.flavors = t, r.$flavorsCarousel.trigger("remove.owl.carousel", 0).trigger("add.owl.carousel", [r.tplFlavor(a), 0]).trigger("refresh.owl.carousel")
                },
                destroyFlavorsCarousel: function() {
                    r.$flavorsCarousel.closest(".delivery-flavors").addClass("is-empty");
                    for (var e = 0; e < $(".owl-item").length; e++) r.$flavorsCarousel.trigger("destroy.owl.carousel").children().remove()
                },
                tplFlavor: function(e) {
                    return '<div class="delivery-flavor">\n\t\t\t<figure class="delivery-flavor__image"><img src="' + baseAssets + "images/productos/" + e.image + '.png" alt=""></figure>\n\t\t\t<div class="delivery-flavor__title subtitle-sm">\n\t\t\t\t<h3>' + e.name + '</h3>\n\t\t\t</div>\n\t\t\t<div class="delivery-flavor__content">\n\t\t\t\t' + e.text + '\n\t\t\t</div>\n\t\t\t<div class="delivery-flavor__cta"><a class="btn btn--primary btn--block js-flavor-box-nutritional" href="">Ver cuadro nutricional</a></div>\n\t\t</div>'
                },
                getDogName: function() {
                    var e = $(".js-dog-name");
                    $("#f-dog-name").keyup(function(t) {
                        var a = $(t.currentTarget).val();
                        r.dogName = a, console.log(r.dogName), "" != a ? e.html(a) : e.html("tu mascota")
                    })
                },
                selectDistrict: function() {
                    var e = $("#f-shipping-address"),
                        t = $("#f-info-date");
                    t.hide(), e.on("change", function(e) {
                        var a = $(e.currentTarget).val();
                        if ("" != a) {
                            var n = r.getObjectInArray(d.default, a);
                            r.updatePicker(n.availableDays)
                        } else t.fadeOut("fast")
                    })
                },
                updatePicker: function(e) {
                    var t = $("#f-info-date"),
                        a = [];
                    e.map(function(e) {
                        a.push(e)
                    }), t.val("Días de entrega: " + a.join(" y ")), t.fadeIn("fast")
                },
                addDog: function() {
                    var e = $(".js-add-dog");
                    $(".js-confirm-similar-info").find(".btn");
                    e.on("click", function(e) {
                        r.saveOrder(), console.log(r.orderList)
                    })
                },
                saveOrder: function() {
                    var e = {
                        indexAvatar: 1,
                        dogName: $("#f-dog-name").val(),
                        dogSex: $('input[name="f-dog-sex"]:checked').val(),
                        dogStatusFelamel: $('input[name="f-status-female"]:checked').val(),
                        dogAge: $('input[name="f-dog-age"]:checked').val(),
                        dogActivity: $('input[name="f-level-activity"]:checked').val(),
                        flavor: $('input[name="f-flavors"]:checked').val(),
                        suscriptionType: $('input[name="f-suscription-time"]:checked').val()
                    };
                    r.orderList.push(e)
                },
                showModalNutricional: function() {
                    var e = $(".delivery-flavor__cta");
                    e.length && e.on("click", "a", function(e) {
                        e.preventDefault(), $.magnificPopup.open({
                            mainClass: "box-nutritional",
                            items: {
                                src: $('<div class="product__box popup-inner">\n\t\t\t\t\t\t\t\t<div class="subtitle-xs u-text-center">\n\t\t\t\t\t\t\t\t\t<h3>Cuadro nutricional</h3>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<table class="product__table">\n\t\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t<th>Energía (Kcal, kg)</th>\n\t\t\t\t\t\t\t\t\t\t\t<th>250</th>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t<td>% Proteína, min</td>\n\t\t\t\t\t\t\t\t\t\t\t<td>14.50</td>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t<td>% Grasa, min</td>\n\t\t\t\t\t\t\t\t\t\t\t<td>9.00</td>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t<td>% Fibra, max</td>\n\t\t\t\t\t\t\t\t\t\t\t<td>0.30</td>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t<td>% Carbohidratos, min</td>\n\t\t\t\t\t\t\t\t\t\t\t<td>6.00</td>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t<td>% Calcio, min</td>\n\t\t\t\t\t\t\t\t\t\t\t<td>0.90</td>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t<td>% Fósforo, min</td>\n\t\t\t\t\t\t\t\t\t\t\t<td>0.50</td>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t\t</div>'),
                                type: "inline"
                            }
                        })
                    })
                }
            };
        a.default = l
    }, {
        "../vendor/w-numb": 16,
        "./data-flavors": 10,
        "./districts": 12,
        "bootstrap4-datetimepicker": 1,
        "jquery-steps": 2,
        moment: 5,
        "moment-locales": 4,
        nouislider: 6
    }],
    12: [function(e, t, a) {
        "use strict";
        Object.defineProperty(a, "__esModule", {
            value: !0
        }), a.default = [{
            name: "Barranco",
            availableDays: ["Martes", "Sábado"]
        }, {
            name: "Chorrillos",
            availableDays: ["Martes", "Sábado"]
        }, {
            name: "Jesús María",
            availableDays: ["Miércoles", "Viernes"]
        }, {
            name: "La Molina",
            availableDays: ["Lunes", "Jueves"]
        }, {
            name: "Lince",
            availableDays: ["Miércoles", "Viernes"]
        }, {
            name: "Miraflores",
            availableDays: ["Martes", "Sábado"]
        }, {
            name: "San Borja",
            availableDays: ["Miércoles", "Viernes"]
        }, {
            name: "San Isidro",
            availableDays: ["Miércoles", "Viernes"]
        }, {
            name: "San Miguel",
            availableDays: ["Miércoles", "Viernes"]
        }, {
            name: "Surco",
            availableDays: ["Lunes", "Jueves"]
        }, {
            name: "Surquillo",
            availableDays: ["Martes", "Sábado"]
        }]
    }, {}],
    13: [function(e, t, a) {
        "use strict";
        Object.defineProperty(a, "__esModule", {
            value: !0
        });
        var n = function() {
                function e(e, t) {
                    for (var a = 0; a < t.length; a++) {
                        var n = t[a];
                        n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
                    }
                }
                return function(t, a, n) {
                    return a && e(t.prototype, a), n && e(t, n), t
                }
            }(),
            s = function() {
                function e() {
                    ! function(e, t) {
                        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
                    }(this, e), this.userEvents(), this.renderPoints(jsonPuntosVenta), this.getLocationUser()
                }
                return n(e, [{
                    key: "userEvents",
                    value: function() {
                        var e = this;
                        $(".js-btn-search").on("click", this.getValInput.bind(this)), $(".js-search-field").on("keydown", function(t) {
                            13 != t.which && 13 != t.keyCode || (t.preventDefault(), e.getValInput())
                        })
                    }
                }, {
                    key: "renderPoints",
                    value: function(e) {
                        var t = this,
                            a = [];
                        e.map(function(e, n) {
                            var s = t.tplPoint();
                            s = (s = (s = (s = s.replace(":district:", e.district)).replace(":name:", e.local)).replace(":address:", e.address)).replace(":phone:", e.phone), a.push(s)
                        }), $(".js-point-container").append(a.join("")), setTimeout(function() {
                            t.googleMapInitialize(e)
                        }, 250)
                    }
                }, {
                    key: "googleMapInitialize",
                    value: function(e) {
                        document.getElementById("map") && google.maps.event.addDomListener(window, "load", this.googleMapConfig(e))
                    }
                }, {
                    key: "googleMapConfig",
                    value: function(e) {
                        var t, a, n = {
                                url: baseAssets + "images/icons/pin.png",
                                scaledSize: new google.maps.Size(59, 67)
                            },
                            s = new google.maps.Map(document.getElementById("map"), {
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            }),
                            i = new google.maps.LatLngBounds,
                            r = new google.maps.InfoWindow;
                        for (a = 0; a < e.length; a++) t = new google.maps.Marker({
                            position: new google.maps.LatLng(Number(e[a].coords[0]), Number(e[a].coords[1])),
                            map: s,
                            icon: n,
                            title: e[a].local
                        }), i.extend(t.position), google.maps.event.addListener(t, "click", function(t, a) {
                            return function() {
                                r.setContent(e[a].local), r.open(s, t)
                            }
                        }(t, a));
                        s.fitBounds(i)
                    }
                }, {
                    key: "getLocationUser",
                    value: function() {
                        "geolocation" in navigator ? navigator.geolocation.getCurrentPosition(function(e) {
                            console.log(e.coords.latitude, e.coords.longitude)
                        }, function(e) {
                            console.error("A ocurrido un error al recuperar la ubicación", e)
                        }) : console.log("geolocation is not enabled on this browser")
                    }
                }, {
                    key: "matchLocations",
                    value: function(e, t) {}
                }, {
                    key: "getValInput",
                    value: function(e) {
                        var t = $(".js-search-field"),
                            a = t.val();
                        "" != a ? (this.removeCardPoints(), this.searchWord(a)) : (t.focus(), this.renderPoints(jsonPuntosVenta))
                    }
                }, {
                    key: "removeCardPoints",
                    value: function(e) {
                        $(".js-point-container").children().remove()
                    }
                }, {
                    key: "searchWord",
                    value: function(e) {
                        var t = [],
                            a = RegExp(e.toLowerCase());
                        jsonPuntosVenta.map(function(n) {
                            a.test(n.district.toLowerCase()) && "" != e && t.push(n)
                        }), t.length ? this.renderPoints(t) : $(".js-point-container").append("<p>No se encontraron puntos de venta.</p>")
                    }
                }, {
                    key: "tplPoint",
                    value: function() {
                        return '<div class="point-sale col-lg-4">\n              <div class="point-sale__box">\n                <h2 class="point-sale__title js-district-name">:district:</h2>\n                <ul class="point-sale__data">\n                  <li>\n                    <span><svg><use xlink:href="' + baseAssets + 'images/sprite.svg#local"></use></svg></span>\n                    :name:\n                  </li>\n                  <li>\n                    <span><svg><use xlink:href="' + baseAssets + 'images/sprite.svg#location"></use></svg></span>\n                    :address:\n                  </li>\n                  <li>\n                    <span><svg><use xlink:href="' + baseAssets + 'images/sprite.svg#phone"></use></svg></span>\n                    :phone:\n                  </li>\n                </ul>\n              </div>\n            </div>'
                    }
                }]), e
            }();
        a.default = s
    }, {}],
    14: [function(e, t, a) {
        "use strict";
        Object.defineProperty(a, "__esModule", {
            value: !0
        });
        var n = function() {
                function e(e, t) {
                    for (var a = 0; a < t.length; a++) {
                        var n = t[a];
                        n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
                    }
                }
                return function(t, a, n) {
                    return a && e(t.prototype, a), n && e(t, n), t
                }
            }(),
            s = function() {
                function e() {
                    ! function(e, t) {
                        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
                    }(this, e), this.userEvents(), this.changeStatus()
                }
                return n(e, [{
                    key: "userEvents",
                    value: function() {
                        $('input[name^="f-suscription-status"]').on("change", this.changeStatus), $(".js-btn-remove-order").on("click", this.removeOrder.bind(this)), $(".js-btn-enable-order").on("click", this.enableOrder.bind(this)), $(".js-order-short > div").on("click", this.showDetail), $(".js-show-order-all").on("click", this.showAll), $(".js-btn-info-order").on("click", this.showInfo)
                    }
                }, {
                    key: "showDetail",
                    value: function(e) {
                        var t = $(e.currentTarget).parent();
                        t.children().eq(1).slideDown(), t.siblings().fadeOut("fast"), $(".orders-current__head").addClass("is-show-action")
                    }
                }, {
                    key: "showAll",
                    value: function(e) {
                        var t = $(".js-order-short");
                        $.each(t, function(e, t) {
                            $(t).children().eq(1).slideUp(), $(t).fadeIn("fast")
                        }), $(".orders-current__head").removeClass("is-show-action")
                    }
                }, {
                    key: "showInfo",
                    value: function(e) {
                        var t = $(e.currentTarget).closest(".order-short__box").siblings(".js-info-order").html();
                        $.magnificPopup.open({
                            items: {
                                src: t,
                                type: "inline"
                            }
                        })
                    }
                }, {
                    key: "changeStatus",
                    value: function(e) {
                        var t = e ? $(e.currentTarget) : $('input[name^="f-suscription-status"]');
                        $.each(t, function(e, t) {
                            var a = $(t).parent().next();
                            $(t).is(":checked") ? a.text("Renovación automática.") : a.text("Renovación automática cancelada.")
                        })
                    }
                }, {
                    key: "removeOrder",
                    value: function(e) {
                        e.stopPropagation();
                        var t = $(e.currentTarget).closest(".js-order-short").data("order-id");
                        this.confirmDialog(t, "eliminar")
                    }
                }, {
                    key: "enableOrder",
                    value: function(e) {
                        e.stopPropagation();
                        var t = $(e.currentTarget).closest(".js-order-short").data("order-id");
                        this.confirmDialog(t, "habilitar")
                    }
                }, {
                    key: "tplDialogConfirm",
                    value: function(e) {
                        return '<div class="js-confirm modal-confirm">\n      <div class="modal-confirm__body">\n        <h4>' + e + '</h4>\n      </div>\n      <div class="modal-confirm__footer">\n        <button class="btn btn--secondary" type="button" data-response="not">No</button>\n        <button class="btn btn--primary" type="button" data-response="yes">Sí</button>\n      </div>\n    </div>'
                    }
                }, {
                    key: "confirmDialog",
                    value: function(e, t) {
                        var a = this,
                            n = this.tplDialogConfirm("¿Deseas " + t + " el pedido?");
                        $.magnificPopup.open({
                            closeOnBgClick: !1,
                            items: {
                                src: n,
                                type: "inline"
                            },
                            callbacks: {
                                open: function() {
                                    $(".js-confirm").on("click", "button.btn", function(n) {
                                        "yes" == $(n.currentTarget).data("response") ? "eliminar" == t ? a.sendRemove(e) : a.sendEnable(e) : $.magnificPopup.close()
                                    })
                                }
                            }
                        })
                    }
                }, {
                    key: "sendRemove",
                    value: function(e) {
                        console.log("eliminar"), $.ajax({
                            url: "",
                            type: "post",
                            data: e
                        })
                    }
                }, {
                    key: "sendEnable",
                    value: function(e) {
                        console.log("hailitar")
                    }
                }]), e
            }();
        a.default = s
    }, {}],
    15: [function(e, t, a) {
        "use strict";
        Object.defineProperty(a, "__esModule", {
            value: !0
        });
        var n = function() {
                function e(e, t) {
                    for (var a = 0; a < t.length; a++) {
                        var n = t[a];
                        n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
                    }
                }
                return function(t, a, n) {
                    return a && e(t.prototype, a), n && e(t, n), t
                }
            }(),
            s = function() {
                function e() {
                    ! function(e, t) {
                        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
                    }(this, e), this.showModalEdit()
                }
                return n(e, [{
                    key: "showModalEdit",
                    value: function() {
                        $(".js-btn-edit-data-profile").on("click", function(e) {
                            console.log($(e.currentTarget));
                            var t = $(e.currentTarget).closest(".profile-data__box").next().html();
                            $.magnificPopup.open({
                                items: {
                                    src: $(t),
                                    type: "inline"
                                }
                            })
                        })
                    }
                }]), e
            }();
        a.default = s
    }, {}],
    16: [function(e, t, a) {
        "use strict";
        var n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
            return typeof e
        } : function(e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        };
        ! function(e) {
            "function" == typeof define && define.amd ? define([], e) : "object" === (void 0 === a ? "undefined" : n(a)) ? t.exports = e() : window.wNumb = e()
        }(function() {
            function e(e) {
                return e.split("").reverse().join("")
            }

            function t(e, t) {
                return e.substring(0, t.length) === t
            }

            function a(e, t, a) {
                if ((e[t] || e[a]) && e[t] === e[a]) throw new Error(t)
            }

            function s(e) {
                return "number" == typeof e && isFinite(e)
            }

            function i(t, a, n, i, r, o, d, l, u, _, m, c) {
                var h, p, f, M = c,
                    y = "",
                    g = "";
                return o && (c = o(c)), !!s(c) && (!1 !== t && 0 === parseFloat(c.toFixed(t)) && (c = 0), c < 0 && (h = !0, c = Math.abs(c)), !1 !== t && (c = function(e, t) {
                    return e = e.toString().split("e"), e = Math.round(+(e[0] + "e" + (e[1] ? +e[1] + t : t))), (+((e = e.toString().split("e"))[0] + "e" + (e[1] ? +e[1] - t : -t))).toFixed(t)
                }(c, t)), -1 !== (c = c.toString()).indexOf(".") ? (f = (p = c.split("."))[0], n && (y = n + p[1])) : f = c, a && (f = e((f = e(f).match(/.{1,3}/g)).join(e(a)))), h && l && (g += l), i && (g += i), h && u && (g += u), g += f, g += y, r && (g += r), _ && (g = _(g, M)), g)
            }

            function r(e, a, n, i, r, o, d, l, u, _, m, c) {
                var h, p = "";
                return m && (c = m(c)), !(!c || "string" != typeof c) && (l && t(c, l) && (c = c.replace(l, ""), h = !0), i && t(c, i) && (c = c.replace(i, "")), u && t(c, u) && (c = c.replace(u, ""), h = !0), r && function(e, t) {
                    return e.slice(-1 * t.length) === t
                }(c, r) && (c = c.slice(0, -1 * r.length)), a && (c = c.split(a).join("")), n && (c = c.replace(n, ".")), h && (p += "-"), p += c, "" !== (p = p.replace(/[^0-9\.\-.]/g, "")) && (p = Number(p), d && (p = d(p)), !!s(p) && p))
            }

            function o(e, t, a) {
                var n, s = [];
                for (n = 0; n < l.length; n += 1) s.push(e[l[n]]);
                return s.push(a), t.apply("", s)
            }

            function d(e) {
                if (!(this instanceof d)) return new d(e);
                "object" === (void 0 === e ? "undefined" : n(e)) && (e = function(e) {
                    var t, n, s, i = {};
                    for (void 0 === e.suffix && (e.suffix = e.postfix), t = 0; t < l.length; t += 1)
                        if (n = l[t], void 0 === (s = e[n])) "negative" !== n || i.negativeBefore ? "mark" === n && "." !== i.thousand ? i[n] = "." : i[n] = !1 : i[n] = "-";
                        else if ("decimals" === n) {
                        if (!(s >= 0 && s < 8)) throw new Error(n);
                        i[n] = s
                    } else if ("encoder" === n || "decoder" === n || "edit" === n || "undo" === n) {
                        if ("function" != typeof s) throw new Error(n);
                        i[n] = s
                    } else {
                        if ("string" != typeof s) throw new Error(n);
                        i[n] = s
                    }
                    return a(i, "mark", "thousand"), a(i, "prefix", "negative"), a(i, "prefix", "negativeBefore"), i
                }(e), this.to = function(t) {
                    return o(e, i, t)
                }, this.from = function(t) {
                    return o(e, r, t)
                })
            }
            var l = ["decimals", "thousand", "mark", "prefix", "suffix", "encoder", "decoder", "negativeBefore", "negative", "edit", "undo"];
            return d
        })
    }, {}]
}, {}, [8]);
//# sourceMappingURL=main.min.js.map