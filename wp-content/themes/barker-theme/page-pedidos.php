<?php
get_header();
if ( !is_user_logged_in() ) {
    auth_redirect();
}
?>
<div class="page-wrap">
      <main class="main">
        <div class="page-name">
          <h2>Pedidos</h2>
        </div>
        <section class="section faq">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-10">
                <div class="section-header">
                  <div class="title u-text-center">
                    <h2>Revisa tus pedidos</h2>
                  </div>
                  <p class="u-text-center">Type something Type something texto falso, es la muestra del texto falso, la del texto falso es un lorem ipsum la del texto falso es un lorem ipsum</p>
                </div>
              </div>
            </div>
            <div class="tab row">
              <div class="faq__categories tabs-nav col-lg-2 js-tabs">
                <div class="subtitle-xs">
                  <h5>Pedidos</h5>
                </div>
                <ul>
                  <li data-tab="1"><span>Suscritos</span></li>
                  <li data-tab="2"><span>Finalizados</span></li>
                </ul>
              </div>
              <div class="tab__content col-lg-10">
                <!-- Pedidos realizados-->
                <div class="tab__content-item" id="1">
                  <div class="orders-current__head">
                    <h5>Tus pedidos</h5>
                    <button class="js-show-order-all" type="button">Mostrar todos
                      <svg>
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#show"></use>
                      </svg>
                    </button>
                  </div>
                  <div class="row">
                    <div class="order-short js-order-short col-md-6" data-order-id="o1">
                      <div class="order-short__box">
                        <div class="order-short__data">
                          <figure class="order-short__data-img"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/avatar-dog.png" alt=""></figure>
                          <div class="order-short__data-text">
                            <h4>Caja de <span>Messi</span></h4><span>Creada el 24/10/17</span>
                          </div>
                        </div>
                        <div class="order-short__action">
                          <button class="js-btn-remove-order" title="Eliminar pedido">
                            <svg>
                              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#trash"></use>
                            </svg>
                          </button>
                        </div>
                      </div>
                      <div class="order-short__detail">
                        <p><strong>Luli</strong> necesitaba 150 calorías diarias.</p>
                        <form class="form" action="" method="post">
                          <div class="field-wrapper field-wrapper--simple">
                            <label>Estado de suscripción</label>
                            <div class="switch">
                              <label class="switch__element">
                                <input type="checkbox" name="f-suscription-status1" id="f-suscription-status1" checked><span class="switch__slider"></span>
                              </label>
                              <p class="switch__result">Renovación automática.</p>
                            </div>
                          </div>
                          <div class="field-wrapper field-wrapper--simple">
                            <label>Horario de entrega</label>
                            <div class="radio-group radio-group--full row">
                              <div class="radio radio--small col-md-6">
                                <input type="radio" name="f-delivery-time1" id="f-moorning1" value="cachorro" checked>
                                <label for="f-moorning1">
                                  <div class="radio__content">
                                    <div class="radio__text">
                                      <h6>9am a 12pm</h6>
                                      <p>Turno día</p>
                                    </div>
                                  </div>
                                  <label class="radio__icon" for="f-moorning1"></label>
                                </label>
                              </div>
                              <div class="radio radio--small col-md-6">
                                <input type="radio" name="f-delivery-time1" id="f-afternoon1" value="adulto">
                                <label for="f-afternoon1">
                                  <div class="radio__content">
                                    <div class="radio__text">
                                      <h6>1pm a 6pm</h6>
                                      <p>Turno tarde</p>
                                    </div>
                                  </div>
                                  <label class="radio__icon" for="f-afternoon1"></label>
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="field-wrapper field-wrapper--simple">
                            <label for="cost">Costo de la suscripción</label>
                            <div class="input">
                              <input class="fill" type="text" name="f-cost1" id="f-cost1" value="S/ 150.00 Mensual" disabled>
                            </div>
                          </div>
                          <div class="field-action">
                            <button class="btn btn--primary" type="submit">Guardar cambios</button>
                          </div>
                        </form>
                      </div>
                    </div>
                    <div class="order-short js-order-short col-md-6" data-order-id="o2">
                      <div class="order-short__box">
                        <div class="order-short__data">
                          <figure class="order-short__data-img"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/avatar-dog.png" alt=""></figure>
                          <div class="order-short__data-text">
                            <h4>Caja de <span>Messi</span></h4><span>Creada el 24/10/17</span>
                          </div>
                        </div>
                        <div class="order-short__action">
                          <button class="js-btn-remove-order" title="Eliminar pedido">
                            <svg>
                              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#trash"></use>
                            </svg>
                          </button>
                        </div>
                      </div>
                      <div class="order-short__detail">
                        <p><strong>Luli</strong> necesitaba 150 calorías diarias.</p>
                        <form class="form" action="" method="post">
                          <div class="field-wrapper field-wrapper--simple">
                            <label>Estado de suscripción</label>
                            <div class="switch">
                              <label class="switch__element">
                                <input type="checkbox" name="f-suscription-status2" id="f-suscription-status2" checked><span class="switch__slider"></span>
                              </label>
                              <p class="switch__result">Renovación automática.</p>
                            </div>
                          </div>
                          <div class="field-wrapper field-wrapper--simple">
                            <label>Horario de entrega</label>
                            <div class="radio-group radio-group--full row">
                              <div class="radio radio--small col-md-6">
                                <input type="radio" name="f-delivery-time2" id="f-moorning2" value="cachorro" checked>
                                <label for="f-moorning2">
                                  <div class="radio__content">
                                    <div class="radio__text">
                                      <h6>9am a 12pm</h6>
                                      <p>Turno día</p>
                                    </div>
                                  </div>
                                  <label class="radio__icon" for="f-moorning2"></label>
                                </label>
                              </div>
                              <div class="radio radio--small col-md-6">
                                <input type="radio" name="f-delivery-time2" id="f-afternoon2" value="adulto">
                                <label for="f-afternoon2">
                                  <div class="radio__content">
                                    <div class="radio__text">
                                      <h6>1pm a 6pm</h6>
                                      <p>Turno tarde</p>
                                    </div>
                                  </div>
                                  <label class="radio__icon" for="f-afternoon2"></label>
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="field-wrapper field-wrapper--simple">
                            <label for="cost">Costo de la suscripción</label>
                            <div class="input">
                              <input class="fill" type="text" name="f-cost2" id="f-cost2" value="S/ 150.00 Mensual" disabled>
                            </div>
                          </div>
                          <div class="field-action">
                            <button class="btn btn--primary" type="submit">Guardar cambios</button>
                          </div>
                        </form>
                      </div>
                    </div>
                    <div class="order-short js-order-short col-md-6" data-order-id="o3">
                      <div class="order-short__box">
                        <div class="order-short__data">
                          <figure class="order-short__data-img"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/avatar-dog.png" alt=""></figure>
                          <div class="order-short__data-text">
                            <h4>Caja de <span>Messi</span></h4><span>Creada el 24/10/17</span>
                          </div>
                        </div>
                        <div class="order-short__action">
                          <button class="js-btn-remove-order" title="Eliminar pedido">
                            <svg>
                              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#trash"></use>
                            </svg>
                          </button>
                        </div>
                      </div>
                      <div class="order-short__detail">
                        <p><strong>Luli</strong> necesitaba 150 calorías diarias.</p>
                        <form class="form" action="" method="post">
                          <div class="field-wrapper field-wrapper--simple">
                            <label>Estado de suscripción</label>
                            <div class="switch">
                              <label class="switch__element">
                                <input type="checkbox" name="f-suscription-status3" id="f-suscription-status3" checked><span class="switch__slider"></span>
                              </label>
                              <p class="switch__result">Renovación automática.</p>
                            </div>
                          </div>
                          <div class="field-wrapper field-wrapper--simple">
                            <label>Horario de entrega</label>
                            <div class="radio-group radio-group--full row">
                              <div class="radio radio--small col-md-6">
                                <input type="radio" name="f-delivery-time3" id="f-moorning3" value="cachorro" checked>
                                <label for="f-moorning3">
                                  <div class="radio__content">
                                    <div class="radio__text">
                                      <h6>9am a 12pm</h6>
                                      <p>Turno día</p>
                                    </div>
                                  </div>
                                  <label class="radio__icon" for="f-moorning3"></label>
                                </label>
                              </div>
                              <div class="radio radio--small col-md-6">
                                <input type="radio" name="f-delivery-time3" id="f-afternoon3" value="adulto">
                                <label for="f-afternoon3">
                                  <div class="radio__content">
                                    <div class="radio__text">
                                      <h6>1pm a 6pm</h6>
                                      <p>Turno tarde</p>
                                    </div>
                                  </div>
                                  <label class="radio__icon" for="f-afternoon3"></label>
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="field-wrapper field-wrapper--simple">
                            <label for="cost">Costo de la suscripción</label>
                            <div class="input">
                              <input class="fill" type="text" name="f-cost3" id="f-cost3" value="S/ 150.00 Mensual" disabled>
                            </div>
                          </div>
                          <div class="field-action">
                            <button class="btn btn--primary" type="submit">Guardar cambios</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Pedidos finalizados-->
                <div class="tab__content-item" id="2">
                  <div class="orders-current">
                    <div class="row">
                      <div class="order-short col-md-6" data-order-id="01">
                        <div class="order-short__box">
                          <div class="order-short__data">
                            <figure class="order-short__data-img"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/avatar-dog.png" alt=""></figure>
                            <div class="order-short__data-text">
                              <h4>Caja de <span>Messi</span></h4><span>Creada el 24/10/17</span>
                            </div>
                          </div>
                          <div class="order-short__action">
                            <button class="js-btn-enable-order" title="Habilitar pedido">
                              <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#enable"></use>
                              </svg>
                            </button>
                            <button class="js-btn-info-order" title="Ver información">
                              <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#info"></use>
                              </svg>
                            </button>
                          </div>
                        </div>
                        <div class="js-info-order">
                          <div class="order-info popup-inner">
                            <div class="order-info__content">
                              <div class="order-info__head">
                                <div class="subtitle-xs">
                                  <h3>Caja de <span>Messi</span> - Detalles</h3>
                                </div>
                              </div>
                              <div class="order-info__body">
                                <div class="order-info__row">
                                  <p>Barker x 1 semana</p>
                                  <p>Todas en presentación de 1KG, contenidas en 20 hamburguesas de 50 gramos cada una.</p>
                                </div>
                                <div class="order-info__row">
                                  <dl>
                                    <dt>Horario de entrega</dt>
                                    <dd>De 9am a 12pm<br><span>Día</span></dd>
                                  </dl>
                                </div>
                                <div class="order-info__row">
                                  <dl>
                                    <dt>Envío</dt>
                                    <dd>¡GRATIS!</dd>
                                    <dt><strong>Total</strong></dt>
                                    <dd><strong>S/. 112.00</strong></dd>
                                  </dl>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="order-short col-md-6" data-order-id="02">
                        <div class="order-short__box">
                          <div class="order-short__data">
                            <figure class="order-short__data-img"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/avatar-dog.png" alt=""></figure>
                            <div class="order-short__data-text">
                              <h4>Caja de <span>Messi</span></h4><span>Creada el 24/10/17</span>
                            </div>
                          </div>
                          <div class="order-short__action">
                            <button class="js-btn-enable-order" title="Habilitar pedido">
                              <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#enable"></use>
                              </svg>
                            </button>
                            <button class="js-btn-info-order" title="Ver información">
                              <svg>
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#info"></use>
                              </svg>
                            </button>
                          </div>
                        </div>
                        <div class="js-info-order">
                          <div class="order-info popup-inner">
                            <div class="order-info__content">
                              <div class="order-info__head">
                                <div class="subtitle-xs">
                                  <h3>Caja de <span>Messi</span> - Detalles</h3>
                                </div>
                              </div>
                              <div class="order-info__body">
                                <div class="order-info__row">
                                  <p>Barker x 1 semana</p>
                                  <p>Todas en presentación de 1KG, contenidas en 20 hamburguesas de 50 gramos cada una.</p>
                                </div>
                                <div class="order-info__row">
                                  <dl>
                                    <dt>Horario de entrega</dt>
                                    <dd>De 9am a 12pm<br><span>Día</span></dd>
                                  </dl>
                                </div>
                                <div class="order-info__row">
                                  <dl>
                                    <dt>Envío</dt>
                                    <dd>¡GRATIS!</dd>
                                    <dt><strong>Total</strong></dt>
                                    <dd><strong>S/. 112.00</strong></dd>
                                  </dl>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
    <?php
get_footer();
