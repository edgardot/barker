<?php
get_header();

while ( have_posts() ) : the_post();
$terms = get_the_terms( get_the_ID(),'category');
?>
<div class="page-wrap">
      <main class="main blog">
        <div class="blog-banner">
          <div class="blog-banner__item" style="background-image: url('<?php echo get_field('imagen_header')['url']; ?>');">
            <div class="container u-full-height u-flex align-items-end">
              <div class="news__head u-hidden-tablet">
                <div class="breadcrumbs">
                  <ul>
                    <li><a href="<?php echo home_url().'/blog' ?>">Blog</a></li>
                    <li><a href="<?php echo get_category_link($terms[0]->term_id); ?>"><?php echo $terms[0]->name;?></a></li>
                    <!--<li><a href="">Post</a></li>-->
                  </ul>
                </div>
                <h1 class="news__title"><?php the_title();?></h1><span class="news__date">Publicado: <?php echo apply_filters( 'the_date', get_the_date(__('d \d\e F \d\e\l Y')), get_option( 'date_format' ), '', '' ); ?></span>
                <div class="social">
                    <ul>
                      <li><a href="https://twitter.com/share?url=<?php echo get_permalink()?>">
                          <svg>
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#twitter"></use>
                          </svg></a></li>
                      <li><a href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink()?>">
                          <svg>
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#facebook"></use>
                          </svg></a></li>
                      <!--<li><a href="">
                          <svg>
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#instagram"></use>
                          </svg></a></li>-->
                    </ul>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <section class="section news">
          <div class="container">
            <div class="row">
              <article class="col-lg-8">
                <div class="news__head u-visible-tablet">
                  <div class="breadcrumbs">
                    <ul>
                      <li><a href="<?php echo home_url().'/blog' ?>">Blog</a></li>
                      <li><a href="<?php echo get_category_link($terms[0]->term_id); ?>"><?php echo $terms[0]->name;?></a></li>
                      <!--<li><a href="">Post</a></li>-->
                    </ul>
                  </div>
                  <h1 class="news__title"><?php the_title();?></h1><span class="news__date">Publicado: <?php echo apply_filters( 'the_date', get_the_date(__('d \d\e F \d\e\l Y')), get_option( 'date_format' ), '', '' ); ?></span>
                  <div class="social">
                    <ul>
                      <li><a href="https://twitter.com/share?url=<?php echo get_permalink()?>">
                          <svg>
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#twitter"></use>
                          </svg></a></li>
                      <li><a href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink()?>">
                          <svg>
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#facebook"></use>
                          </svg></a></li>
                      <!--<li><a href="">
                          <svg>
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#instagram"></use>
                          </svg></a></li>-->
                    </ul>
                  </div>
                </div>
                <div class="news__content">
                  <?php the_content(); ?>
                </div>
              </article>
        
              <aside class="sidebar col-lg-4 u-hidden-tablet-wide">
                <div class="search-inner">
                  <form action="">
                    <div class="field-wrapper">
                      <input type="text" placeholder="">
                      <button type="submit">
                        <svg>
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#search"></use>
                        </svg>
                      </button>
                    </div>
                  </form>
                </div>
                <div class="sidebar__item news__related">
                  <h4 class="sidebar__title">Noticias relacionadas</h4>
                  <ul class="sidebar__list">
                    <?php
                    $args=array(
                    'cat' => $terms[0]->term_id,
                    'post__not_in' => array(get_the_ID()),
                    'posts_per_page'=>3
                    );
                    
                    $the_query = new wp_query( $args );
                    
                    if ($the_query->have_posts()) : while ($the_query->have_posts()) :$the_query->the_post();
                      echo '<li>
                      <div class="news-thumb"><a class="news-thumb__wrapper" href="'.get_permalink(get_the_ID()).'">
                          <figure class="news-thumb__image"><img src="" alt="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'"></figure>
                          <div class="news-thumb__content">
                            <h2 class="news-thumb__title">'.get_the_title().'</h2>
                              <span class="news-thumb__date">'.apply_filters( 'the_date', get_the_date(__('d \d\e F \d\e\l Y')), get_option( 'date_format' ), '', '' ).'</span>
                          </div></a></div>
                    </li>';
                     endwhile; endif;
                      
                      wp_reset_query();
                    ?>
                  </ul>
                </div>
                <div class="sidebar__item news__categories">
                  <h4 class="sidebar__title">Categorías</h4>
                  <ul class="sidebar__list">
                    <?php
                      $cat = get_terms('category',
                        array('hide_empty' => false,'parent'=>0,'order'=> 'ASC'));
                    foreach($cat as $row){
                      echo '<li><a href="'.get_category_link($row->term_id).'"><span>
                          <svg>
                            <use xlink:href="'.get_template_directory_uri().'/assets/images/sprite.svg#share"></use>
                          </svg></span>'.$row->name.'</a></li>';
                 }
                  ?>
                  </ul>
                </div>
              </aside>
            </div>
          </div>
        </section>
      </main>
    </div>
<?php
endwhile; 
get_footer();
