 <?php
get_header();

$res=query_posts(array('post_type' => 'punto-de-ventas') );
  if(count($res)>0){
    $b=1;
    $jsonPuntosVenta="[";

    foreach ($res as $resPost) { 
    $jsonPuntosVenta.="{
    district: '".get_field( 'distrito', $resPost->ID )."',
    local: '".$resPost->post_title."',
    address: '".get_field( 'direccion', $resPost->ID )."',
    phone: '".get_field( 'telefono', $resPost->ID )."',
    coords: [".get_field( 'ubicacion_longitud', $resPost->ID ).", ".get_field( 'ubicacion_latidud', $resPost->ID )."]
  },";
    }
    $jsonPuntosVenta.="]";
}
?>
<script type="text/javascript">
    jsonPuntosVenta=<?php echo $jsonPuntosVenta; ?>;
    </script>
<div class="page-wrap">
      <main class="main location">
        <div class="page-name">
          <h2>Puntos de venta</h2>
        </div>
        <section class="section">
          <div class="container">
            <div class="title">
              <h2><span>Encuentra tu producto</span>Ubica nuestros puntos de venta</h2>
            </div>
            <div class="search-inner">
              <form action="">
                <div class="field-wrapper">
                  <input class="js-search-field" type="text" placeholder="Busca tu punto de venta">
                  <button type="submit">
                    <svg>
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/sprite.svg#search"></use>
                    </svg>
                  </button>
                </div>
              </form>
            </div>
            <div class="map-location" id="map"></div>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQayMO5EYL8z2MjXUvW2y7FFlykh_Wc1I" async defer></script>
            <div class="points-sale">
              <div class="subtitle-common">
                <h5>Direcciones de las veterinarias</h5>
              </div>
              <div class="row justify-content-center js-point-container"></div>
            </div>
          </div>
        </section>
      </main>
    </div>
    <?php
get_footer();