<?php
get_header();
?> 

     <main class="main not-found">
      <section class="section">
        <div class="container">
          <div class="row">
            <div class="col-lg-12"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/404.png" alt="">
              <div class="subtitle u-text-center">
                <h4>oh no! La página que solicitó no se<br>pudo encontrar.</h4>
              </div>
              <p class="u-text-center"><a class="btn btn--primary" href="<?php echo home_url().'/Nosotros' ?>">Volver al inicio</a></p>
            </div>
          </div>
        </div>
      </section>
    </main>
    <div class="hamburguer popup-inner" id="barker-hamburguer">
      <div class="hamburguer__content">
        <div class="subtitle-xs">
          <h3>¿Cuántas hamburguesas barker debes darle a tu perro?</h3>
        </div>
        <p>*Cada hamburguesa contiene 50 gramos de barker</p>
        <div class="hamburguer__table">
          <table>
            <thead>
              <tr>
                <th>PESO DEL PERRO</th>
                <th>PESO</th>
                <th>CACHORRO</th>
                <th>ADULTO</th>
                <th>ADULTO MAYOR</th>
                <th>GESTANTE/LACTANTE</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td>1.5 a 11 m</td>
                <td>12m a 8 años</td>
                <td>8 años a más</td>
                <td>1/3 gestación y lactancia</td>
              </tr>
              <tr>
                <td>MINIATURA</td>
                <td>1 - 5 kg</td>
                <td>1</td>
                <td>2</td>
                <td>2</td>
                <td>3</td>
              </tr>
              <tr>
                <td>PEQUEÑO</td>
                <td>5 - 10 kg</td>
                <td>3</td>
                <td>4</td>
                <td>3</td>
                <td>6</td>
              </tr>
              <tr>
                <td>PEQUEÑO</td>
                <td>10 - 22 kg</td>
                <td>5</td>
                <td>8</td>
                <td>6</td>
                <td>12</td>
              </tr>
              <tr>
                <td>GRANDE</td>
                <td>22 - 45 kg</td>
                <td>11</td>
                <td>16</td>
                <td>12</td>
                <td>24</td>
              </tr>
            </tbody>
          </table>
        </div>
        <p class="hamburguer__feedback">*Actividad Física intermedia. De ser el caso de perros de alta actividad, considerar duplicar o triplicar la ración de acuerdo a la exigencia física.</p>
      </div>
    </div>

