<?php
get_header();
?>
<nav class="nav-secondary">
      <div class="nav-secondary__content">
         <div class="container u-flex"><a href="<?php echo home_url().'/nosotros' ?>">Nosotros</a><a href="<?php echo home_url().'/ingredientes' ?>">Ingredientes</a><a class="is-active" href="<?php echo home_url().'/como-funciona' ?>">Cómo funciona</a><a href="<?php echo home_url().'/faq' ?>">Preguntas frecuentes</a></div>
      </div>
    </nav>
    <div class="page-wrap">
      <main class="main">
        <div class="page-name">
          <h2>Ingredientes</h2>
        </div>
        <section class="section section--big ingredients">
          <div class="container">
            <div class="title u-text-center">
              <h2>Ingredientes</h2>
            </div>
            <article class="ingredients__featured">
              <div class="row align-items-center u-text-justify">
                <div class="col-lg-6 order-lg-2"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/nosotros/comida/comida-insumos.png" alt=""></div>
                <div class="col-lg-6 order-lg-1">
                  <div class="subtitle">
                    <h4>Insumos frescos de<br>Grado Humano:</h4>
                  </div>
                  <p>Los insumos utilizados para la formulación de Barker® son de grado de consumo humano, seleccionados con altos estándares de calidad y cada lote es aprobado con rigurosos análisis.</p>
                </div>
              </div>
            </article>
            <article class="ingredients__featured">
              <div class="row align-items-center u-text-justify">
                <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ingredientes/balance-nutricional.png" alt=""></div>
                <div class="col-lg-6">
                  <div class="subtitle">
                    <h4>Balance Nutricional</h4>
                  </div>
                  <p>Barker® está diseñado para cubrir responsablemente, todos los requerimientos nutricionales y el importante balance entre ellos:</p>
                  <p>Desde el importante nivel de Energía, los específicos niveles de aminoácidos y calidad de proteína, pasando por los niveles de fibras solubles, importantes para las movilizaciones peristálticas y el buen funcionamiento gastrointestinal así como las importantes vitaminas y minerales.</p>
                </div>
              </div>
            </article>
          </div>
        </section>
        <section class="section benefits">
          <div class="container">
            <div class="row justify-content-center u-text-center">
              <div class="col-lg-8">
                <div class="subtitle u-text-center">
                  <h2>Beneficios de los ingredientes</h2>
                </div>
                <div class="benefits__main">
                  <?php
                $resPosts=query_posts(array('post_type' => 'Beneficios','posts_per_page' => 0) );
                  if(count($resPosts)>0){
                      $i=1;
                      foreach ($resPosts as $resPost) { 
                          $imagenesBeneficios.=' <li class="benefits__item '.($i==1?"is-active":"").'" data-tab="'.$i.'">
                       <figure>
                        <img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'" alt="">
                       <img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $resPost->ID),'categoria-default')[0].'" alt=""></figure>
                    </li>';
                    $contenidoBeneficios.='<div class="item">
                     <div class="subtitle-sm">
                        <h4>'.$resPost->post_title.'</h4>
                      </div>
                      <p>'.$resPost->post_excerpt.'</p>
                      </div>';
                          $i++;
                      }
                  }
                  wp_reset_query();
              ?>
                  <ul class="benefits__list js-benefit-items">
                     <?php echo $imagenesBeneficios; ?>
                  </ul>
                  <div class="benefits__text">
                    <div class="owl-carousel js-benefit-carousel">
                      <?php echo $contenidoBeneficios; ?>
                      <div class="item">
                        <div class="subtitle-sm">
                          <h4>Proteína fresca</h4>
                        </div>
                        <p>Porciones de Pollo, carne y pavo fresco, de grado de consumo humano que con el tamaño de molienda que permiten aprovechar y asimilar sus nutrientes.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <p class="u-text-center"><a class="btn btn--primary" href="<?php echo home_url().'/suscripcion' ?>">Diseña tu pedido</a></p>
          </div>
        </section>
      </main>
    </div>
<?php
get_footer();
