<div class="barker_page_pedidos_sidebar faq__categories tabs-nav col-lg-2 js-tabs">
    <div class="subtitle-xs">
      <h5>Pedidos</h5>
    </div>
    <ul>
      <li data-tab="1" class="barker_pedidos_sidebar_button bkp_suscritos is-active"><span>Suscritos</span></li>
      <li data-tab="2" class="barker_pedidos_sidebar_button bkp_finalizados"><span>Finalizados</span></li>
    </ul>
  </div>   