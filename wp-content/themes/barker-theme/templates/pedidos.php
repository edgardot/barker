<div class="page-wrap">
      <main class="main">
        <div class="page-name">
          <h2>Mis Pedidos</h2>
        </div>
        <section class="section faq">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-10">
                <div class="section-header">
                  <div class="title u-text-center">
                    <h2>Revisa tus pedidos</h2>
                  </div>
                  <p class="u-text-center" style="font-weight: bold;">En esta sección podrás revisar los pedidos actuales, como los finalizados</p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
<!-- MODAL -->
  <div id="barker-open-modal" class="barker-modal-window">
    <div>
      <a href="#barker-modal-close" title="Close" class="barker-modal-close">X</a>
      <div class="u-text-center">
        <div class="subtitle-xs">
          <h3>Necesitas cambiar de Horario</h3>
        </div>
        <p class="text-black">Elije uno y envia la solicitud</p>
      </div>
      <?php echo do_shortcode('[contact-form-7 id="318" title="Solicitud de Cambio de Horario"]') ?>
    </div>
  </div>
 
    </div>
