<?php
date_default_timezone_set('America/Lima');

// include_once('includes/acf.php');
$user_id = get_current_user_id();

function incluir_scripts() {
    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css');
    wp_enqueue_style('main', get_stylesheet_directory_uri() . '/assets/css/main.min.css');
    wp_enqueue_style('barker-css', get_stylesheet_directory_uri() . '/assets/css/barker-css.css');
    wp_enqueue_script("jquery");
    wp_enqueue_script('pagina', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.14.0/TweenMax.min.js', '', '', true);
    wp_enqueue_script('web', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/1.3.0/jquery.scrollmagic.min.js', '', '', true);
    wp_enqueue_script('enlace', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/1.3.0/jquery.scrollmagic.debug.js', '', '', true);

    // Add to select2
    wp_enqueue_script('select2-js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js', '', '', true);
    wp_enqueue_script('main', get_stylesheet_directory_uri() . '/assets/js/main-full.js', '', '', true);
    wp_enqueue_script('barker-js', get_stylesheet_directory_uri() . '/assets/js/barker-js.js', '', '', true);

    // wp_enqueue_script( 'brk_script', get_stylesheet_directory_uri() .'/assets/js/brk_ajax.js', array('jquery'), '1.0', true );
    wp_localize_script('barker-js', 'brk_ajax_vars', array(
        'ajaxurl' => admin_url('admin-ajax.php')
    ));
}

add_action('wp_enqueue_scripts', 'incluir_scripts', 1);
add_theme_support('post-thumbnails');

if (function_exists('add_theme_support')) {
    add_image_size('categoria-default', 800, 450, array(
        'left',
        'top'
    ));
}

function modify_jquery() {
    if (!is_admin()) {

        // comment out the next two lines to load the local copy of jQuery
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js', '', '', true);
        wp_enqueue_script('jquery');
    }
}

add_action('init', 'modify_jquery');
wp_register_theme_activation_hook('barker', 'wpse_25885_theme_activate');
wp_register_theme_deactivation_hook('barker', 'wpse_25885_theme_deactivate');
/**
 *
 * @desc registers a theme activation hook
 * @param string $code : Code of the theme. This can be the base folder of your theme. Eg if your theme is in folder 'mytheme' then code will be 'mytheme'
 * @param callback $function : Function to call when theme gets activated.
 */

function wp_register_theme_activation_hook($code, $function) {
    $optionKey = "theme_is_activated_" . $code;
    if (!get_option($optionKey)) {
        call_user_func($function);
        update_option($optionKey, 1);
    }
}

/**
 * @desc registers deactivation hook
 * @param string $code : Code of the theme. This must match the value you provided in wp_register_theme_activation_hook function as $code
 * @param callback $function : Function to call when theme gets deactivated.
 */

function wp_register_theme_deactivation_hook($code, $function) {

    // store function in code specific global
    $GLOBALS["wp_register_theme_deactivation_hook_function" . $code] = $function;

    // create a runtime function which will delete the option set while activation of this theme and will call deactivation function provided in $function
    $fn = create_function('$theme', ' call_user_func($GLOBALS["wp_register_theme_deactivation_hook_function' . $code . '"]); delete_option("theme_is_activated_' . $code . '");');

    // add above created function to switch_theme action hook. This hook gets called when admin changes the theme.
    // Due to wordpress core implementation this hook can only be received by currently active theme (which is going to be deactivated as admin has chosen another one.
    // Your theme can perceive this hook as a deactivation hook.)
    add_action("switch_theme", $fn);
}

function wpse_25885_theme_activate() {
    $default_pages = array(
        array(
            'title' => 'Nosotros',
            'content' => ''
        ) ,
        array(
            'title' => 'Punto de venta',
            'content' => ''
        ) ,
        array(
            'title' => 'Sabores',
            'content' => ''
        ) ,
        array(
            'title' => 'Blog',
            'content' => ''
        ) ,
        array(
            'title' => 'Cuenta',
            'content' => ''
        ) ,
        array(
            'title' => 'Faq',
            'content' => ''
        ) ,
        array(
            'title' => 'Suscripción',
            'content' => ''
        ) ,
        array(
            'title' => 'Perfil',
            'content' => ''
        ) ,
        array(
            'title' => 'Pedidos',
            'content' => ''
        )
    );
    $existing_pages = get_pages();
    $existing_titles = array();
    foreach ($existing_pages as $page) {
        $existing_titles[] = $page->post_title;
    }

    foreach ($default_pages as $new_page) {
        if (!in_array($new_page['title'], $existing_titles)) {

            // create post object
            $add_default_pages = array(
                'post_title' => $new_page['title'],
                'post_content' => $new_page['content'],
                'post_status' => 'publish',
                'post_type' => 'page'
            );

            // insert the post into the database
            $result = wp_insert_post($add_default_pages);
        }
    }
}

function wpse_25885_theme_deactivate() {

    // code to execute on theme deactivation
    
}

function mytheme_customize_register($wp_customize) {

    // lo que va incluido en el panel
    $wp_customize->add_panel('bk_panel', array(
        'title' => __('Personalizar Barker') ,
        'description' => $description, // Include html tags such as <p>.
        'priority' => 160, // Mixed with top-level-section hierarchy.
        
    ));
    $wp_customize->add_section("bk_administrar_inicio", array(
        'title' => "Administrar Inicio",
        'panel' => 'bk_panel',
    ));
    $wp_customize->add_setting('bk_titulo', array(
        'default' => '',

        // LO QUE TU MASCOTA ELEGIRÍA
        
    ));
    $wp_customize->add_control('bk_titulo', array(
        'label' => 'Titulo',
        'section' => 'bk_administrar_inicio',
        'type' => 'text',
        'settings' => 'bk_titulo',
    ));
    $wp_customize->add_setting('bk_subtitulo', array(
        'default' => '',

        // Barker significa disfrutar de la mejor versión de nuestras mascotas.
        
    ));
    $wp_customize->add_control('bk_subtitulo', array(
        'label' => 'Subtitulo',
        'section' => 'bk_administrar_inicio',
        'type' => 'text',
        'settings' => 'bk_subtitulo',
    ));
    $wp_customize->add_setting('bk_botonsemana', array(
        'default' => '',

        // Barker significa disfrutar de la mejor versión de nuestras mascotas.
        
    ));
    $wp_customize->add_control('bk_botonsemana', array(
        'label' => 'Boton Izquierda',
        'section' => 'bk_administrar_inicio',
        'type' => 'text',
        'settings' => 'bk_botonsemana',
    ));
    $wp_customize->add_setting('bk_botondisena', array(
        'default' => '',

        // Barker significa disfrutar de la mejor versión de nuestras mascotas.
        
    ));
    $wp_customize->add_control('bk_botondisena', array(
        'label' => 'Boton Derecha',
        'section' => 'bk_administrar_inicio',
        'type' => 'text',
        'settings' => 'bk_botondisena',
    ));
    $wp_customize->add_setting('bk_botonsemanaI', array(
        'default' => '',

        // Barker significa disfrutar de la mejor versión de nuestras mascotas.
        
    ));
    $wp_customize->add_control('bk_botonsemanaI', array(
        'label' => 'Boton Izquierda URL',
        'section' => 'bk_administrar_inicio',
        'type' => 'text',
        'settings' => 'bk_botonsemanaI',
    ));
    $wp_customize->add_setting('bk_botondisenaI', array(
        'default' => '',

        // Barker significa disfrutar de la mejor versión de nuestras mascotas.
        
    ));
    $wp_customize->add_control('bk_botondisenaI', array(
        'label' => 'Boton Derecha URL',
        'section' => 'bk_administrar_inicio',
        'type' => 'text',
        'settings' => 'bk_botondisenaI',
    ));
    /* $wp_customize->add_setting('bk_imagen', array(
        'default'        => '',
        ));
        $wp_customize->add_control('bk_imagen', array(
        'label'   => 'Imagen de Fondo',
        'section' => 'bk_administrar_inicio',
        'type'    => 'text',
        'settings'=>'bk_imagen',
        ));*/
    $wp_customize->add_section("bk_administrar_contenido", array(
        'title' => "Administrar Contenido",
        'panel' => 'bk_panel',
    ));
    $wp_customize->add_setting('bk_barker', array(
        'default' => '',

        // ¿Que es Barker?
        
    ));
    $wp_customize->add_control('bk_barker', array(
        'label' => 'Que es Barker',
        'section' => 'bk_administrar_contenido',
        'type' => 'text',
        'settings' => 'bk_barker',
    ));
    $wp_customize->add_setting('bk_contenido', array(
        'default' => '',

        // Barker significa disfrutar de la mejor versión de nuestras mascotas. Hoy, somos conscientes que lo que comemos se relaciona directamente con nuestra calidad de vida. Lo mismo pasa con nuestras mascotas. Barker nace de la tendencia BARF (Biologically Appropiate Raw Food) proponiendo una formula balanceada y responsable que se adapta al estilo de vida de un perro hoy en día. Nuestra comida está basada en proteína cruda, vegetales y vitaminas.
        
    ));
    $wp_customize->add_control('bk_contenido', array(
        'label' => 'Contenido del Tituto',
        'section' => 'bk_administrar_contenido',
        'type' => 'text',
        'settings' => 'bk_contenido',
    ));
    $wp_customize->add_section("bk_redes", array(
        'title' => "Personalizar Redes",
        'panel' => 'bk_panel',
    ));
    $wp_customize->add_setting('bk_facebook', array(
        'default' => '',
    ));
    $wp_customize->add_control('bk_facebook', array(
        'label' => 'Facebook',
        'section' => 'bk_redes',
        'type' => 'text',
        'settings' => 'bk_facebook',
    ));
    $wp_customize->add_setting('bk_mail', array(
        'default' => '',
    ));
    $wp_customize->add_control('bk_mail', array(
        'label' => 'Mail',
        'section' => 'bk_redes',
        'type' => 'text',
        'settings' => 'bk_mail',
    ));
    $wp_customize->add_setting('bk_whatsapp', array(
        'default' => '',
    ));
    $wp_customize->add_control('bk_whatsapp', array(
        'label' => 'Whatsapp',
        'section' => 'bk_redes',
        'type' => 'text',
        'settings' => 'bk_whatsapp',
    ));
    $wp_customize->add_setting('bk_instagram', array(
        'default' => '',
    ));
    $wp_customize->add_control('bk_instagram', array(
        'label' => 'Instagram',
        'section' => 'bk_redes',
        'type' => 'text',
        'settings' => 'bk_instagram',
    ));
}

add_action('customize_register', 'mytheme_customize_register');

function create_posttype() {
    register_post_type('nosotros-barker', array(
        'labels' => array(
            'name' => __('Nosotros') ,
            'singular_name' => __('Nosotros') ,
            'add_new' => __('Agregar Nosotros') ,
            'add_new_item' => __('Nosotros') ,
            'edit_item' => __('Nosotros')
        ) ,
        'public' => true,
        'has_archive' => true,

        // 'rewrite' => array('slug' => '/'),
        // 'taxonomies'          => array( 'post_tag','category' ),
        'supports' => array(
            'title',
            'excerpt',
            'thumbnail'
        ) ,
    ));
    register_post_type('beneficios', array(
        'labels' => array(
            'name' => __('Beneficios') ,
            'singular_name' => __('Beneficios') ,
            'add_new' => __('Agregar Beneficios') ,
            'add_new_item' => __('Beneficios') ,
            'edit_item' => __('Beneficios')
        ) ,
        'public' => true,
        'has_archive' => true,

        // 'rewrite' => array('slug' => '/'),
        // 'taxonomies'          => array( 'post_tag','category' ),
        'supports' => array(
            'title',
            'excerpt',
            'thumbnail'
        ) ,
    ));
    register_post_type('pasos', array(
        'labels' => array(
            'name' => __('Pasos') ,
            'singular_name' => __('Pasos') ,
            'add_new' => __('Agregar Pasos') ,
            'add_new_item' => __('Pasos') ,
            'edit_item' => __('Pasos')
        ) ,
        'public' => true,
        'has_archive' => true,

        // 'rewrite' => array('slug' => '/'),
        // 'taxonomies'          => array( 'post_tag','category' ),
        'supports' => array(
            'title',
            'excerpt',
            'thumbnail'
        ) ,
    ));
    register_post_type('testimonios', array(
        'labels' => array(
            'name' => __('Testimonios') ,
            'singular_name' => __('Testimonios') ,
            'add_new' => __('Agregar Testimonios ') ,
            'add_new_item' => __('Testimonios') ,
            'edit_item' => __('Testimonios')
        ) ,
        'public' => true,
        'has_archive' => true,

        // 'rewrite' => array('slug' => '/'),
        // 'taxonomies'          => array( 'post_tag','category' ),
        'supports' => array(
            'title',
            'excerpt',
            'thumbnail'
        ) ,
    ));
    register_post_type('preguntas-frecuentes', array(
        'labels' => array(
            'name' => __('Preguntas Frecuentes') ,
            'singular_name' => __('Preguntas Frecuentes') ,
            'add_new' => __('Agregar Preguntas ') ,
            'add_new_item' => __('Preguntas Frecuentes') ,
            'edit_item' => __('Preguntas Frecuentes')
        ) ,
        'public' => true,
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'preguntas-frecuentes/%categorias-preguntas%'
        ) ,
        'taxonomies' => array(
            'categorias-preguntas'
        ) ,
        'supports' => array(
            'title',
            'excerpt',
            'editor'
        ) ,
    ));
    register_post_type('punto-de-ventas', array(
        'labels' => array(
            'name' => __('Puntos de Ventas') ,
            'singular_name' => __('Puntos de ventas') ,
            'add_new' => __('Agregar Puntos de Ventas ') ,
            'add_new_item' => __('Puntos de Ventas') ,
            'edit_item' => __('Puntos de Ventas')
        ) ,
        'public' => true,
        'has_archive' => true,

        // 'rewrite' => array('slug' => '/'),
        // 'taxonomies'          => array( 'post_tag','category' ),
        'supports' => array(
            'title'
        ) ,
    ));
    register_post_type('slider-blog', array(
        'labels' => array(
            'name' => __('Slider Blog') ,
            'singular_name' => __('Slider Bog') ,
            'add_new' => __('Agregar Slider BLog ') ,
            'add_new_item' => __('Slider Blog') ,
            'edit_item' => __('Slider Blog')
        ) ,
        'public' => true,
        'has_archive' => true,

        // 'rewrite' => array('slug' => '/'),
        // 'taxonomies'          => array( 'post_tag','category' ),
        'supports' => array(
            'title',
            'excerpt',
            'thumbnail'
        ) ,
    ));
    register_post_type('slider-home', array(
        'labels' => array(
            'name' => __('Slider Home') ,
            'singular_name' => __('Slider Home') ,
            'add_new' => __('Agregar Slider Home ') ,
            'add_new_item' => __('Slider Home') ,
            'edit_item' => __('Slider Home')
        ) ,
        'public' => true,
        'has_archive' => true,

        // 'rewrite' => array('slug' => '/'),
        // 'taxonomies'          => array( 'post_tag','category' ),
        'supports' => array(
            'title',
            'thumbnail'
        ) ,
    ));
    register_post_type('raza-categoria-peso', array(
        'labels' => array(
            'name' => __('Raza categoría peso') ,
            'singular_name' => __('Raza') ,
            'add_new' => __('Agregar Raza') ,
            'add_new_item' => __('Nueva Raza') ,
            'edit_item' => __('Editar Raza')
        ) ,
        'public' => true,
        'has_archive' => true,
        'taxonomies' => array(
            'categorias-peso'
        ) ,
        'supports' => array(
            'title',
            'thumbnail'
        ) ,
    ));
}

// Hooking up our function to theme setup
add_action('init', 'create_posttype');

// Lo enganchamos en la acción init y llamamos a la función create_book_taxonomies() cuando arranque
add_action('init', 'create_book_taxonomies', 0);

// Creamos dos taxonomías, género y autor para el custom post type "libro"
function create_book_taxonomies() {

    // Añadimos nueva taxonomía y la hacemos jerárquica (como las categorías por defecto)
    $labels = array(
        'name' => _x('Categorías', 'taxonomy general name') ,
    );
    register_taxonomy('categorias-preguntas', array(
        'categorias-preguntas'
    ) , array(
        'hierarchical' => true,
        'labels' => $labels, // ADVERTENCIA: Aquí es donde se utiliza la variable $labels
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array(
            'slug' => 'productos'
        ) ,
    ));
    register_taxonomy('categorias-peso', array(
        'categorias-peso'
    ) , array(
        'hierarchical' => true,
        'labels' => $labels, // ADVERTENCIA: Aquí es donde se utiliza la variable $labels
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array(
            'slug' => 'categorias-peso'
        ) ,
    ));
}

add_action('user_register', 'crf_user_register');

function crf_user_register($user_id) {
    if (!empty($_POST['rg-phone'])) {
        update_user_meta($user_id, 'rg-phone', esc_sql($_POST['rg-phone']));
    }
}

function redirect_login_page() {
    $register_page = home_url('/cuenta');
    $login_page = home_url('/cuenta?action=login');
    $page_viewed = basename($_SERVER['REQUEST_URI']);
    if ($page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
        wp_redirect($login_page);
        exit;
    }

    if ($page_viewed == "wp-login.php?action=register" && $_SERVER['REQUEST_METHOD'] == 'GET') {
        wp_redirect($register_page);
        exit;
    }
}

add_action('init', 'redirect_login_page');

// Redirect For Login Failed
function login_failed() {
    wp_redirect(home_url('/cuenta?action=login&login=failed'));
    exit;
}

add_action('wp_login_failed', 'login_failed');

// Redirect For Empty Username Or Password
function verify_username_password($user, $username, $password) {
    if ($username == "" || $password == "") {
        wp_redirect(home_url('/cuenta?action=login&login=empty'));
        exit;
    }
}

add_filter('authenticate', 'verify_username_password', 1, 3);
add_filter('show_admin_bar', '__return_false');

function suscription_custom() {
    add_menu_page('custom menu title', 'Suscripción', 'add_users', 'custompage', '_custom_suscription', null, 6);
}

add_action('admin_menu', 'suscription_custom');

function _custom_suscription() {
    include ("includes/suscription-admin.php");

}

add_action('wp_ajax_nopriv_brk_suscription_ajax', 'brk_suscription_ajax');
add_action('wp_ajax_brk_suscription_ajax', 'brk_suscription_ajax');

function brk_suscription_ajax() {
    header('Content-Type: application/json');
    if (isset($_POST['submit-suscription-form']) && md5("Subs" . date("Ymd")) == $_POST['submit-suscription-form']) {
        global $wpdb;
        $suscription = $wpdb->prefix . "suscription";
        if ($wpdb->get_var("SHOW TABLES LIKE '$suscription'") != $suscription) {
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE $suscription (
                      id int(11) NOT NULL AUTO_INCREMENT,
                      email varchar(1000) DEFAULT NULL,
                      fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (`id`)
                    ) $charset_collate;";
            require_once (ABSPATH . 'wp-admin/includes/upgrade.php');

            dbDelta($sql);
        }

        $previous = $wpdb->get_var("SELECT email FROM $suscription WHERE email = '" . $_POST["email-subscribe"] . "' limit 1;");
        if (count($previous) < 1) {
            $wpdb->insert($suscription, array(
                'email' => $_POST["email-subscribe"]
            ) , array(
                '%s'
            ));
            $wpdb->flush();
            echo json_encode(array(
                "msg" => "ok"
            ));
        }
        else {
            echo json_encode(array(
                "msg" => "no"
            ));
        }
    }
    else {
        echo json_encode(array(
            "msg" => "no"
        ));
    }

    wp_die();
}

add_action('wp_ajax_brk_dog_ajax', 'brk_dog_ajax');

function brk_dog_ajax() {
    $newName = preg_replace("/[^A-Za-z0-9]/", "", $_POST["name"]);
    if (isset($_SESSION["dogsUser"]) && count($_SESSION["dogsUser"]) > 0) {
        $dogs = $_SESSION["dogsUser"];
        if ($dogs[$newName]) {
            echo "ya tiene registrado una mascota con el mismo nombre";
            wp_die();
        }
        else {
            $mascotas = add_mascota($dogs);
            update_user_meta($user_id->ID, 'dog_value', $mascotas);
        }
    }
    else {
        $mascotas = add_mascota();
        add_user_meta($user_id->ID, 'dog_value', $mascotas);
    }

    wp_die();
}

function add_mascota($VALS = array()) {
    $pictureName = trim(str_replace("  ", " ", $newName)) . date("YmdHm") . "." . $_POST["formato"];
    $user_id = wp_get_current_user();

    // print_r($user_id);
    // $havemeta = get_user_meta($user_id->ID, 'dog_value', false);
    $upload = wp_upload_bits($pictureName, null, file_get_contents($_POST["picture"]));
    $wp_filetype = wp_check_filetype(basename($upload['file']) , null);
    $wp_upload_dir = wp_upload_dir();
    $attachment = array(
        'guid' => $wp_upload_dir['baseurl'] . _wp_relative_upload_path($upload['file']) ,
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => preg_replace('/\.[^.]+$/', '', basename($upload['file'])) ,
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment($attachment, $upload['file'], $post_id);
    unset($_POST["picture"]);
    $VALS[$newName] = $_POST;
    $VALS[$newName]["picture"] = wp_get_attachment_url($attach_id);
    $VALS[$newName]["payEfective"] = "NO";
    $_SESSION["dogsUser"] = $VALS;
    return json_encode($VALS);
}

// ===============================================
// ANDINA SOFTWARE-> INIT BARKER CUSTOM CHECKOUT
// ===============================================
// CUSTOM FIELD
// ============
if (function_exists("register_field_group")) {
    register_field_group(array(
        'id' => 'acf_suscripcion',
        'title' => 'Suscripción',
        'fields' => array(
            array(
                'key' => 'field_5b69fa5f153b8',
                'label' => 'Productos',
                'name' => 'barker_suscripcion_productos',
                'type' => 'relationship',
                'post_type' => array(
                    0 => 'product',
                ) ,
                'taxonomy' => array(
                    0 => 'all',
                ) ,
                'filters' => array(
                    0 => 'search',
                    1 => 'post_type',
                ) ,
                'result_elements' => array(
                    0 => 'featured_image',
                    1 => 'post_title',
                    2 => 'post_type',
                ) ,
                'max' => '',
                'return_format' => 'object',
            ) ,
        ) ,
        'location' => array(
            array(
                array(
                    'param' => 'page',
                    'operator' => '==',
                    'value' => '125',
                    'order_no' => 0,
                    'group_no' => 0,
                ) ,
            ) ,
        ) ,
        'options' => array(
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array() ,
        ) ,
        'menu_order' => 2,
    ));
}

function andina_software_redirect_checkout_add_cart($url) {
    $url = get_permalink(get_option('woocommerce_checkout_page_id'));
    return $url;
}

add_filter('woocommerce_add_to_cart_redirect', 'andina_software_redirect_checkout_add_cart');

// REMOVE GENERALES FIELDS WOOCOMMERCE CHECKOUT
// -----------------------------------------------
add_filter('woocommerce_checkout_fields', 'andina_software_remover_campos_checkout');

function andina_software_remover_campos_checkout($fields) {
    unset($fields['order']['order_comments']); // Comentarios
    unset($fields['billing']['billing_company']); // Compañia
    unset($fields['billing']['billing_postcode']); // CEP
    $fields['billing']['billing_city']['required'] = false;; // Compañia
    return $fields;
}

// DEAFAULT VALUES BILING ORDERS
// LMA
add_filter('default_checkout_billing_state', 'bbloomer_change_default_checkout_state');

function bbloomer_change_default_checkout_state() {
    return 'LMA'; // state code
    
}

// // Example 2: default country to United States
// add_filter( 'default_checkout_billing_country', 'bbloomer_change_default_checkout_country' );
// function bbloomer_change_default_checkout_country() {
//   return 'US';
// }
// ORDER GENERALES FIELDS WOOCOMMERCE BILLING
// -----------------------------------------------
add_filter('woocommerce_checkout_fields', 'woocommerce_billing_fields_custom', 9999);

function woocommerce_billing_fields_custom($fields) {
    $fields['billing']['billing_first_name']['priority'] = 10;
    $fields['billing']['billing_first_name']['label'] = '';
    $fields['billing']['billing_first_name']['placeholder'] = 'Nombre';
    $fields['billing']['billing_first_name']['class'] = array(
        'form-row-wide'
    );
    $fields['billing']['billing_last_name']['priority'] = 20;
    $fields['billing']['billing_last_name']['label'] = '';
    $fields['billing']['billing_last_name']['placeholder'] = 'Apellido';
    $fields['billing']['billing_last_name']['class'] = array(
        'form-row-wide '
    );
    $fields['billing']['billing_phone']['priority'] = 30;
    $fields['billing']['billing_phone']['label'] = '';
    $fields['billing']['billing_phone']['placeholder'] = 'Número de teléfono';
    $fields['billing']['billing_phone']['class'] = array(
        'form-row-wide'
    );
    $fields['billing']['billing_country']['class'] = array(
        'barker-checkout_hidenField'
    );
    $fields['billing']['billing_country']['priority'] = 80;
    $fields['billing']['billing_email']['class'] = array(
        'barker-checkout_hidenField'
    );
    $fields['billing']['billing_email']['priority'] = 90;
    return $fields;
}

add_filter('woocommerce_default_address_fields', 'custom_override_default_locale_fields');

function custom_override_default_locale_fields($fields) {
    $fields['state']['priority'] = 40;
    $fields['state']['label'] = 'Dirección de envío';
    $fields['state']['class'] = array(
        'form-row-wide'
    );
    $fields['state']['class'] = array(
        'barker-checkout_hidenField'
    );
    $fields['address_1']['priority'] = 50;
    $fields['address_1']['label'] = '';
    $fields['address_1']['placeholder'] = 'Av. Jr.';
    $fields['address_1']['class'] = array(
        'form-row-wide'
    );
    $fields['address_2']['priority'] = 60;
    $fields['address_2']['placeholder'] = 'Urbanizado';
    $fields['address_2']['class'] = array(
        'form-row-wide'
    );
    $fields['city']['priority'] = 70;
    $fields['city']['label'] = '';
    $fields['city']['placeholder'] = 'Dpto';
    $fields['city']['class'] = array(
        'form-row-wide'
    );
    return $fields;
}

// ADD & CONVERT CUSTOM INPUT RADIO BILLING FIELD --> DELIVERY TIME
// -----------------------------------------------------------------
function woocommerce_form_field_radio($key, $args, $value = '') {
    global $woocommerce;
    $defaults = array(
        'type' => 'radio',
        'label' => '',
        'placeholder' => '',
        'required' => false,
        'class' => array() ,
        'label_class' => array() ,
        'return' => false,
        'options' => array()
    );
    $args = wp_parse_args($args, $defaults);
    if ((isset($args['clear']) && $args['clear'])) $after = '
                <div class="clear"></div>
                ';
    else $after = '';
    $required = ($args['required']) ? ' <abbr class="required" title="' . esc_attr__('required', 'woocommerce') . '">*</abbr>' : '';
    switch ($args['type']) {
        case "select":
            $options = '';
            if (!empty($args['options'])) foreach ($args['options'] as $option_key => $option_text) $options .= '<div class="barker-checkout-radio-container"><label for="' . $key . '" class="' . implode(' ', $args['label_class']) . '">' . $args['label'] . $required . $option_text . '</label><input type="radio" name="' . $key . '" value="' . $option_key . '" ' . selected($value, $option_key, false) . 'class="select"></div>' . "\r\n";
            $field = '
                    <p class="form-row ' . implode(' ', $args['class']) . '" id="' . $key . '_field">
                        
                        ' . $options . '
                    </p>
                        
                        ' . $after;
            break;
        } //$args[ 'type' ]
        if ($args['return']) return $field;
        else echo $field;
    }

    // ADD & CONVERT CUSTOM INPUT RADIO BILLING FIELD --> KIND HOUSE
    // -----------------------------------------------------------------
    function woocommerce_form_field_radio_house($key, $args, $value = '') {
        global $woocommerce;
        $defaults = array(
            'type' => 'radio',
            'label' => '',
            'placeholder' => '',
            'required' => false,
            'class' => array() ,
            'label_class' => array() ,
            'return' => false,
            'options' => array()
        );
        $args = wp_parse_args($args, $defaults);
        if ((isset($args['clear']) && $args['clear'])) $after = '
                <div class="clear"></div>
                ';
        else $after = '';
        $required = ($args['required']) ? ' <abbr class="required" title="' . esc_attr__('required', 'woocommerce') . '">*</abbr>' : '';
        switch ($args['type']) {
            case "select":
                $options = '';
                if (!empty($args['options'])) foreach ($args['options'] as $option_key => $option_text) $options .= '<div class="barker-checkout-radio-house-container"><label for="' . $key . '" class="' . implode(' ', $args['label_class']) . '">' . $args['label'] . $required . $option_text . '</label><input type="radio" name="' . $key . '" value="' . $option_key . '" ' . selected($value, $option_key, false) . 'class="select"></div>' . "\r\n";
                $field = '
                    <p class="form-row ' . implode(' ', $args['class']) . '" id="' . $key . '_field">
                        
                        ' . $options . '
                    </p>
                        
                        ' . $after;
                break;
            } //$args[ 'type' ]
            if ($args['return']) return $field;
            else echo $field;
        }

        // ADD CUSTOM INPUT RADIO BILLING FIELD ON CHECKOUT --> DELIVERY TIME
        // -----------------------------------------------------------------
        add_action('woocommerce_after_checkout_billing_form', 'barker_horario_reparto', 10);

        function barker_horario_reparto($checkout) {

            // get_template_part( 'templates/select-distric' );
            woocommerce_form_field('f_shipping_address', array(
                'type' => 'select',
                'class' => array(
                    'lms-drop form-row-wide'
                ) ,
                'label' => __('Dirección de envío') ,
                'required' => true,
                'options' => array(
                    'blank' => __('Distrito', 'barker_distrito') ,
                    'Barranco' => __('Barranco', 'barker_distrito') ,
                    'Chorrillos' => __('Chorrillos', 'barker_distrito') ,
                    'Jesús María' => __('Jesús María', 'barker_distrito') ,
                    'La Molina' => __('La Molina', 'barker_distrito') ,
                    'Lince' => __('Lince', 'barker_distrito') ,
                    'Miraflores' => __('Miraflores', 'barker_distrito') ,
                    'San Borja' => __('San Borja', 'barker_distrito') ,
                    'San Isidro' => __('San Isidro', 'barker_distrito') ,
                    'Surco' => __('Surco', 'barker_distrito') ,
                    'Surquillo' => __('Surquillo', 'barker_distrito')
                )
            ) , $checkout->get_value('f_shipping_address'));
            echo '
            <div id="barker_horario_reparto" data-priority="45">
            
                ';
            woocommerce_form_field_radio('barker_horario_reparto', array(
                'type' => 'select',
                'priority' => 45,
                'class' => array(
                    'here-about-us form-row-wide'
                ) ,
                'label' => __('') ,
                'placeholder' => __('') ,
                'required' => true,
                'options' => array(
                    '9am a 12pm' => '9am a 12pm<br/><span class="barker_item_horario">Turno día</span>',
                    '1pm a 6pm' => '1pm a 6pm<br/><span class="barker_item_horario">Turno tarde</span>'
                )
            ) , $checkout->get_value('barker_horario_reparto'));
            echo '
            </div>
            ';
        }

        // ADD CUSTOM INPUT RADIO BILLING FIELD ON CHECKOUT --> DELIVERY TIME
        // -----------------------------------------------------------------
        add_action('woocommerce_after_checkout_billing_form', 'barker_checkout_kind_house', 10);

        function barker_checkout_kind_house($checkout) {

            // ADD CUSTOM FIELD TO CHECKOUT --> ORDER ID PAYMENT
            // --------------------------------------------------
            // ADD CUSTOM FIELD TO CHECKOUT --> KIND HOUSE
            // --------------------------------------------
            echo '
            <div id="barker_checkout_kind_house">
                
                ';
            woocommerce_form_field_radio_house('barker_checkout_kind_house', array(
                'type' => 'select',
                'priority' => 45,
                'id' => '',
                'class' => array(
                    'here-about-us form-row-wide'
                ) ,
                'label' => __('') ,
                'placeholder' => __('') ,
                'required' => true,
                'options' => array(
                    'Casa' => 'Casa<br/>',
                    'Quinta' => 'Quinta<br/>',
                    'Edificio' => 'Edificio<br/>',
                    'Condominio' => 'Condominio<br/>'
                )
            ) , $checkout->get_value('barker_checkout_kind_house'));
            echo '
            </div>
            ';
        }

        // PROCESSS VALIDATION TO CHECKOUT --> DELIVERY TIME
        // --------------------------------------------------
        add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

        function my_custom_checkout_field_process() {
            global $woocommerce;

            // Check if set, if its not set add an error.
            if (!$_POST['barker_horario_reparto']) $woocommerce->add_error(__('Por favor selecciona un horario de reparto.'));
        }

        // REGISTER CUSTOM FIELD TO ORDER WOOCOMMERCE --> DELIVERY TIME
        // -------------------------------------------------------------
        add_action('woocommerce_checkout_update_order_meta', 'barker_horario_reparto_field_update_order_meta');

        function barker_horario_reparto_field_update_order_meta($order_id) {
            if ($_POST['barker_horario_reparto']) update_post_meta($order_id, 'Horario de reparto', esc_attr($_POST['barker_horario_reparto']));
        }

        // PROCESSS VALIDATION TO CHECKOUT --> DELIVERY TIME
        // --------------------------------------------------
        add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process_casa');

        function my_custom_checkout_field_process_casa() {
            global $woocommerce;

            // Check if set, if its not set add an error.
            if (!$_POST['barker_checkout_kind_house']) $woocommerce->add_error(__('Por favor selecciona un tipo de casa.'));
        }

        // REGISTER CUSTOM FIELD TO ORDER WOOCOMMERCE --> KIND HOUSE
        // --------------------------------------------------
        add_action('woocommerce_checkout_update_order_meta', 'barker_checkout_kind_house_field_update_order_meta_casa');

        function barker_checkout_kind_house_field_update_order_meta_casa($order_id) {
            if ($_POST['barker_checkout_kind_house']) update_post_meta($order_id, 'Tipo de Casa', esc_attr($_POST['barker_checkout_kind_house']));
        }

        // REGISTER CUSTOM FIELD TO ORDER WOOCOMMERCE --> ORDER PAYMENT ID
        // --------------------------------------------------
        add_action('woocommerce_checkout_update_order_meta', 'barker_order_payment_id');

        function barker_order_payment_id($order_id) {
            if ($_POST['barker_order_payment_id']) update_post_meta($order_id, 'barker_order_payment_id', esc_attr($_POST['barker_order_payment_id']));
        }

        // PROCESSS VALIDATION TO CHECKOUT --> DISTRICT
        // --------------------------------------------------
        add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process_address');

        function my_custom_checkout_field_process_address() {
            global $woocommerce;

            // Check if set, if its not set add an error.
            if (!$_POST['f_shipping_address']) $woocommerce->add_error(__('Por favor selecciona un distrito.'));
        }

        // REGISTER CUSTOM FIELD TO ORDER WOOCOMMERCE --> DISTRICT
        // -------------------------------------------------------
        add_action('woocommerce_checkout_update_order_meta', 'f_shipping_address_field_update_order_meta_address');

        function f_shipping_address_field_update_order_meta_address($order_id) {
            if ($_POST['f_shipping_address']) update_post_meta($order_id, 'Distrito', esc_attr($_POST['f_shipping_address']));
        }

        // ADITINAL FIELD TO WOOCOMMERCE CHECKOUT --> WOOCOMMERCE COUPON
        // --------------------------------------------------------------
        add_action('woocommerce_checkout_after_terms_and_conditions', 'barker_checkout_coupon');

        function barker_checkout_coupon() {
            echo '<form id="wooCouponCode" class="barker-checkout-customCoupon-box checkout_coupon woocommerce-form-coupon" method="post" style="display:none">

        <p>If you have a coupon code, please apply it below.</p>

        <p class="form-row form-row-first">
            <input type="text" name="coupon_code" class="input-text" placeholder="Coupon code" id="coupon_code" value="">
        </p>

        <p class="form-row form-row-last">
            <button type="submit" class="button" name="apply_coupon" value="Apply coupon">Apply coupon</button>
        </p>

        <div class="clear"></div>
    </form>';
        }

        add_action('woocommerce_checkout_order_review', 'barker_checkout_header_title');

        function barker_checkout_header_title() {
            echo '
    <div class="order-detail__head u-text-center">
        <div class="subtitle-xs">
            <h4>Resumen del pedido</h4>
        </div>
        <div class="order-item__head u-text-center">
            <h4>Caja de <span class="js-dog-name">tu mascota</span></h4>
        </div>
    </div>
    
    ';
            echo '
<div class="order-detail__row barker-checkout-date-box">
    <dl>
        <dt>Fecha de entrega estimada</dt>
        <dd>Mié, 04/25</dd>
    </dl>
</div>';
            echo '<div class="order-detail__row_notice u-not-border u-text-center barker-checkout-notice-cancel">
        <p class="feedback">Puedes cancelar la suscripción en<br />cualquier momento.</p>
    </div>';
        }

        // CHANGUE BUTTON PLACE ORDER WOOCOMMERCE CHECKOUT
        // -----------------------------------------------
        add_filter('woocommerce_order_button_text', 'woo_custom_order_button_text');

        function woo_custom_order_button_text() {
            return __('Realizar Pedido', 'woocommerce');
        }

        // CHANGUE TITLE BILLING FIELDS WOOCOMMERCE CHECKOUT
        // -------------------------------------------------
        function wc_billing_field_strings($translated_text, $text, $domain) {
            switch ($translated_text) {
                case 'Billing details':
                    $translated_text = __('Complete sus datos para comenzar su prueba', 'woocommerce');
                break;
            }

            return $translated_text;
        }

        add_filter('gettext', 'wc_billing_field_strings', 20, 3);
        add_action('woocommerce_before_checkout_billing_form', 'barker_checkout_header_top');

        // ADD SUBTITLE BILLING FIELDS WOOCOMMERCE CHECKOUT
        // -------------------------------------------------
        function barker_checkout_header_top() {
            echo '<p>Estás tan cerca de la comida real, ¡Tu perro puede olerla!</p>';
        }

        add_action('woocommerce_before_checkout_form', 'andina_software_barker_checkout_steps_header');

        // ADD PAYFORM FOR CULQI
        // ---------------------
        function andina_software_barker_checkout_steps_header() {
            get_template_part('/templates/steps-header');
        }

        // ADD PAYFORM FOR CULQI
        // ---------------------
        // add_action('woocommerce_checkout_after_customer_details', 'andina_software_barker_culqipayform');
        // function andina_software_barker_culqipayform()
        // {
        //     get_template_part('/templates/culqi-payform');
        // }
        // ADD STANDAR PAYFORM LAYOUT
        // --------------------------
        add_action('woocommerce_checkout_before_customer_details', 'andina_software_mirror');

        function andina_software_mirror() {
            htmlspecialchars($_GET["nombre"]);
            get_template_part('/templates/subscription');
        }

        // THANKS PAGE
        // Here add a custom template page
        // ================================
        add_filter('the_title', 'andina_software_woo_title_order_received', 10, 2);

        function andina_software_woo_title_order_received($title, $id) {
            if (function_exists('is_order_received_page') && is_order_received_page() && get_the_ID() === $id) {
                $title = "BARKER agradece tu orden! :)";

                // here add get_template_page function
                
            }

            return $title;
        }

        // REMOVE SPAN CF7
        // ====================
        add_filter('wpcf7_form_elements', function ($content) {
            $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
            return $content;
        });

        // SAVE FIELD FOR STEP 1 SUSCRIPTION
        // ================================
        // Saneando y salvando el campo
        // =============================
        // add_action('update_user_meta', 'barker_save_field_suscription_step_one');
        function barker_save_field_suscription_step_one($user_id) {
            if (isset($_POST['barker_dog_name']) && isset($_POST['f-dog-sex']) || isset($_POST['f_status_female'])) {
                update_user_meta($user_id, 'barker_dog_name', $_POST['barker_dog_name']);
                update_user_meta($user_id, 'f-dog-sex', $_POST['f-dog-sex']);
                update_user_meta($user_id, 'f_status_female', $_POST['f_status_female']);
                echo '<META HTTP-EQUIV="Refresh" Content="0; URL=' . $location . '">';
                exit;
            }
            else {

                // echo '<script> alert("Completa todos los campos porfavor"); </script>';
                
            }

            return;
        }

        // Mostrando Campos
        // =============================
        add_action('show_user_profile', 'extra_user_profile_fields');
        add_action('edit_user_profile', 'extra_user_profile_fields');

        function extra_user_profile_fields($user) {
            echo '<h2>Mis Mascotas</h2><table id="list-coti" class="wp-list-table widefat">
            <thead>
                <tr><th>Nombre Mascota</th><th>Foto</th><th>Sexo</th><th>Edad</th><th>Categoría</th><th>Actividad</th></tr>
            </thead>
            <tbody>
                <tr><td>' . get_the_author_meta('barker_dog_name', $user->ID) . '</td><td>imagen</td><td>Macho</td><td>1.5m a 11m</td><td>5 - 10 kg</td><td>Medio</td></tr>
                <tr><td>Mascota 2</td><td>imagen</td><td>Hembra</td><td>1.5m a 11m</td><td>5 - 10 kg</td><td>Medio</td></tr>
            </tbody>
            </table>';
        }

        // MEMBERSHIPS CONTENT
        // =======================
        add_action('wc_memberships_before_my_memberships', 'content_demo_membership');

        function content_demo_membership() {
            get_template_part('templates/pedidos');
        }

        add_action('wc_memberships_after_my_memberships', 'content_demo_membership_sidebar');

        function content_demo_membership_sidebar() {
            get_template_part('templates/pedidos-sidebar');
        }

        add_action('wc_memberships_after_my_memberships', 'content_demo_membership_clearfix');

        function content_demo_membership_clearfix() {
            get_template_part('templates/pedidos-clearfix');
        }

        add_action('woocommerce_view_order', 'before_woocommerce_order_details', 5);

        function before_woocommerce_order_details($order_id) {
            $order = new WC_Order($order_id);

            // echo "<h1>Order Status : ".$order->get_status() . "</h1>";
            // $order_data = $order;
            // echo wc_get_order_item_meta($order_id,"Horario de reparto");
            $order_meta2 = get_post_meta($order_id) ["Horario de reparto"][0];
            echo $order_meta2;

            // echo '<pre>';
            // var_dump($order_meta2["Horario de reparto"]);
            // // echo $order->meta_data;
            // echo '</pre>';
            echo '<div class="orders-current__head is-show-action">
                    <a href="#">
                    <button class="js-show-order-all" type="button">Regresar
                      <svg>
                        <use xlink:href="'.get_stylesheet_directory_uri() . '/assets/images/sprite.svg#show"></use>
                      </svg>
                    </button>
                    </a>
                  </div>';
            echo '<div class="order-short__box">
        <div class="order-short__data">
          <figure class="order-short__data-img"><img src="'.get_stylesheet_directory_uri() . '/assets/images/avatar-dog.png" alt=""></figure>
          <div class="order-short__data-text">
            <h4>Caja de <span>tu mascota</span></h4><span>Creada el ' . date('Y/m/d', strtotime($order->order_date)) . '</span>
          </div>
        </div>
        <div class="order-short__action">
          <button class="js-btn-remove-order" title="Eliminar pedido">
            <svg>
              <use xlink:href="'.get_stylesheet_directory_uri() . '/assets/images/sprite.svg#trash"></use>
            </svg>
          </button>
        </div>
      </div>';

            // horario
            echo '<div class="field-wrapper field-wrapper--simple">
                            <label>Horario de entrega</label>
                            <div class="radio-group radio-group--full row">
                              <div class="radio radio--small col-md-6">';
            echo '        <label for="f-moorning1">
                                  <div class="radio__content">
                                    <div class="radio__text">
                                      <h6>9am a 12pm</h6>
                                      <p>Turno día</p>
                                    </div>
                                  </div>
                                  <label class="radio__icon" for="f-moorning1"></label>
                                </label>
                              </div>
                              <div class="radio radio--small col-md-6">';

            // <input type="radio" name="f-delivery-time1" id="f-afternoon1" value="adulto">
            echo '                     <label for="f-afternoon1">
                                  <div class="radio__content">
                                    <div class="radio__text">
                                      <h6>1pm a 6pm</h6>
                                      <p>Turno tarde</p>
                                    </div>
                                  </div>
                                  <label class="radio__icon" for="f-afternoon1"></label>
                                </label>
                              </div>
                            </div>
                          </div>';
        }

        // demo
        add_action('wc_memberships_before_my_memberships', 'barker_view_order');

        function barker_view_order() {
            $barker_memberships_id = wc_memberships_get_user_memberships(get_current_user_id());

            // echo "<pre>";
            //     var_dump(wc_memberships_get_user_memberships(get_current_user_id()));
            // echo "</pre>";
            // echo "<hr/>";
            $customer_orders = get_posts(array(
                'numberposts' => - 1,
                'meta_key' => '_customer_user',
                'meta_value' => get_current_user_id() ,
                'post_type' => wc_get_order_types() ,
                'post_status' => array_keys(wc_get_order_statuses()) ,
            ));
        }

        add_filter('woocommerce_cart_item_permalink', '__return_false');

        // ADD CONTENT THANKS PAGE
        add_filter('the_title', 'woo_title_order_received', 10, 2);

        function woo_title_order_received($title, $id) {
            if (function_exists('is_order_received_page') && is_order_received_page() && get_the_ID() === $id) {
                $title = "¡Gracias!";
            }

            return $title;
        }

        add_filter('woocommerce_thankyou_order_received_text', 'woo_change_order_received_text', 10, 2);

        function woo_change_order_received_text($str, $order) {
            $new_str = 'Gracias por tu pedido, puedes verlo en la sección de tus pedidos o puedes agregar otra mascota.';
            return $new_str;
        }

        // REDIRECT AFTER REGISTRATION
        // add_filter( 'registration_redirect', 'my_redirect_home' );
        // function my_redirect_home( $registration_redirect ) {
        //     return home_url();
        // }


        //Redirect After Login all users
        function my_login_redirect( $redirect_to, $request, $user ) {
            //is there a user to check?
            if (isset($user->roles) && is_array($user->roles)) {
                //check for subscribers
                if (in_array('subscriber', $user->roles)) {
                    // redirect them to another URL, in this case, the homepage 
                    $redirect_to =  home_url();
                }
            }

            return $redirect_to;
        }

        add_filter( 'login_redirect', function( $url, $query, $user ) {
            return home_url();
        }, 10, 3 );



        function auto_login_new_user($user_id) {
            wp_set_current_user($user_id);
            wp_set_auth_cookie($user_id);
            wp_redirect(home_url()); //
            exit;
        }

        add_action('user_register', 'auto_login_new_user');

        // CUSTOM MY ACCOUNT PAGE
        function custom_my_account_menu_items($items) {
            unset($items['dashboard']);
            unset($items['downloads']);
            unset($items['orders']);
            unset($items['customer-logout']);
            return $items;
        }

        add_filter('woocommerce_account_menu_items', 'custom_my_account_menu_items');

        //ADD CUSTOM TEXT FOR MY ACCOUNT PAGE 

        // function barker_custom_text_account_dashboard () {
        //     echo 'Texto a medida';
        // }

        // add_filter( 'woocommerce_account_dashboard', 'barker_custom_text_account_dashboard' );

        // edit address
        function storefront_child_remove_unwanted_form_fields($fields) {
            unset($fields['company']);
            unset($fields['postcode']);
            return $fields;
        }

        add_filter('woocommerce_default_address_fields', 'storefront_child_remove_unwanted_form_fields');

        function wpb_woo_my_account_order() {
            $myorder = array(
                'members-area' => __('Suscripciones', 'woocommerce-memberships') ,
                'edit-account' => __('Editar cuenta', 'woocommerce') ,
                'edit-address' => __('Direcciones de entrega', 'woocommerce') ,
            );
            return $myorder;
        }

        add_filter('woocommerce_account_menu_items', 'wpb_woo_my_account_order');

        // button cart
        add_filter('gettext', 'change_woocommerce_return_to_shop_text', 20, 3);

        function change_woocommerce_return_to_shop_text($translated_text, $text, $domain) {
            switch ($translated_text) {
                case 'Return to shop':
                    $translated_text = __('Compra una subscripción', 'woocommerce');
                break;
            }

            return $translated_text;
        }

        function wc_empty_cart_redirect_url() {
            $buton_cart_barker = get_home_url();
            return get_home_url() . '/suscripcion/';
        }

        add_filter('woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url');
        


//

        // Allow only one(or pre-defined) product per category in the cart
add_filter( 'woocommerce_add_to_cart_validation', 'andina_software_allowed_quantity_per_category_in_the_cart', 10, 2 );
function andina_software_allowed_quantity_per_category_in_the_cart( $passed, $product_id) {

    $max_num_products = 1;// change the maximum allowed in the cart
    $running_qty = 0;

    $restricted_product_cats = array();

    //Restrict particular category/categories by category slug
    $restricted_product_cats[] = 'res';
    $restricted_product_cats[] = 'pavo';
    $restricted_product_cats[] = 'mixto';
    $restricted_product_cats[] = 'pollo';
    // $restricted_product_cats[] = 'suscripciones';

    // Getting the current product category slugs in an array
    $product_cats_object = get_the_terms( $product_id, 'product_cat' );
    foreach($product_cats_object as $obj_prod_cat) $current_product_cats[]=$obj_prod_cat->slug;


    // Iterating through each cart item
    foreach (WC()->cart->get_cart() as $cart_item_key=>$cart_item ){

        // Restrict $max_num_products from each category
        if( has_term( $current_product_cats, 'product_cat', $cart_item['product_id'] )) {

        // Restrict $max_num_products from restricted product categories
        //if( array_intersect($restricted_product_cats, $current_product_cats) && has_term( $restricted_product_cats, 'product_cat', $cart_item['product_id'] )) {

            // count(selected category) quantity
            $running_qty += (int) $cart_item['quantity'];

            // More than allowed products in the cart is not allowed
            if( $running_qty >= $max_num_products ) {
                wc_add_notice( sprintf( 'Only %s '.($max_num_products>1?'products from this category are':'product from this category is').' allowed in the cart.',  $max_num_products ), 'error' );
                $passed = false; // don't add the new product to the cart
                // We stop the loop
                break;
            }

        }
    }
    return $passed;
}

// VACIAR CARRITO


add_action ('init', function () {
     global $woocommerce;
     if (isset( $_GET['vaciar-carrito']))
          $woocommerce->cart->empty_cart(); 
});

