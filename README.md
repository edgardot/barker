# Barker.pe 

## Introducción

> Se ha desarrollado las funcionalidades de 
> 1) "formula de pedido segun barker",  
> 2) Pagos recurrentes con culqi 
> 3) Subscripciones de pagos recurrentes en Woocommerce 
> 4) adecuación de formulario de checkout    

## Instalación y consideraciones

### Base de datos
La base de datos esta adjunta y contiene información basica sobre la configuración del sitio que no es necesaria para el deploy del sitio en producción, pero si es recomendable exportarla. Revisar y usar '/ database'.

### Implementación de formularios de CF7
> Los formularios incluidos para el funcionamiento de pedidos y de cambio de horario estan incluidos en el archivo sql de la caprta '/ database', alli se podra exportar el contenido e incluirlo en producción o donde se requiera.

### Instalación de Culqi
> La funcionalidad de pagos recurrentes se encuentra insertada en el plugin 'culqi-woocommerce', se deberá reemplazar todo el contenido de la carpeta en producción o testing y usar las llaves de integración que culqi tiene disponible. Estas llaves estan en la base de datos adjunta y en la cuenta de culqi.

### Botone preventDefault();
> Se recomienda eliminar los preventDefault de todos los botones de '/subscripcion' y de '/pago'. Esta funcionalidad se encuentra en el main.js que es parte del desarrollador anterior. El mismo archivo se encuentra minificado y es importante que se edite desde la estructura inicial de desarrollo. 

### Membresias 
> Las membresias se controlan con el plugin 'woocommerce-memberships' el cual ha sido adaptado para las finalidades de barker.pe. Las memresias ya estan armadas y configuradas en la base de datos adjunta. Deberan importarse al entorno de producción.

### Iconos en Mis Pedidos
> Considerar en el deploy, cambiar al url del recursos SVG que esta en el archivo "../barker-theme/asstes/js/barker-js.js/" lineas 276, 277 y 278, reemplazar la url de demostración por la URL que se usará en el proyecto.